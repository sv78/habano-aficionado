-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Хост: 10.0.0.134:3306
-- Время создания: Дек 03 2017 г., 13:05
-- Версия сервера: 10.1.29-MariaDB
-- Версия PHP: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `fatbody_habanoaficionado`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `intro` text,
  `img` varchar(255) DEFAULT NULL,
  `body` text,
  `after_body` text,
  `before_body` text,
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_robots` varchar(18) DEFAULT 'index, follow',
  `meta_author` varchar(96) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `lifted_up_at` int(10) unsigned DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `used_by_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `fk_articles_sections_idx` (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `section_id`, `slug`, `title`, `intro`, `img`, `body`, `after_body`, `before_body`, `published`, `featured`, `meta_title`, `meta_description`, `meta_keywords`, `meta_robots`, `meta_author`, `created_at`, `lifted_up_at`, `modified_at`, `used_by_user_id`) VALUES
(1, 1, 'contacts', 'Контакты', '', NULL, '<h1>Контакты</h1>\r\n\r\n<p style=\\"text-align:center\\">г. Москва, ул. Краснопролетарская, д. 8, стр. 1</p>\r\n\r\n<p style=\\"text-align:center\\">+7&nbsp;(499)&nbsp;978&nbsp;22&nbsp;87</p>\r\n\r\n<p style=\\"text-align:center\\"><a href=\\"https://yandex.ru/maps/213/moscow/?ll=37.608050%2C55.775565&amp;z=18&amp;mode=search&amp;where=37.608050%2C55.775565&amp;ol=biz&amp;oid=1055493370\\" target=\\"_blank\\" title=\\"Посмотреть на карте\\">Посмотреть на&nbsp;карте</a></p>', NULL, NULL, 1, 0, 'Контакты', '', '', 'index, follow', '', '2017-02-20 12:47:45', 1487598465, '2017-10-23 21:10:16', 1),
(6, 1, 'politics', 'Условия использования', 'Terms and conditions of use', NULL, '<h1>Условия использования</h1>\r\n\r\n<p>Autem est, expedita assumenda. esse, quam quibusdam sit adipisci commodi maxime quisquam maxime unde. a laudantium, esse, ab cumque autem quam officiis voluptatem ex ipsa, ea possimus, nisi incidunt, dolor ea doloremque explicabo saepe et aut odio assumenda enim. ad autem sunt. sunt. quisquam ut error omnis consequuntur. quas ipsa. quia iste. sapiente provident. blanditiis consectetur voluptas suscipit explicabo. aut.</p>\r\n\r\n<h3 style=\\"text-align:center\\">Personal and non-commercial use</h3>\r\n\r\n<p>Autem est, expedita assumenda. esse, quam quibusdam sit adipisci commodi maxime quisquam maxime unde. a laudantium, esse, ab cumque autem quam officiis voluptatem ex ipsa, ea possimus, nisi incidunt, dolor ea doloremque explicabo saepe et aut odio assumenda enim. ad autem sunt. sunt. quisquam ut error omnis consequuntur. quas ipsa. quia iste. sapiente provident. blanditiis consectetur voluptas suscipit explicabo. aut.</p>\r\n\r\n<h3 style=\\"text-align:center\\">Cookies policy</h3>\r\n\r\n<p>Autem est, expedita assumenda. esse, quam quibusdam sit adipisci commodi maxime quisquam maxime unde. a laudantium, esse, ab cumque autem quam officiis voluptatem ex ipsa, ea possimus, nisi incidunt, dolor ea doloremque explicabo saepe et aut odio assumenda enim. ad autem sunt. sunt. quisquam ut error omnis consequuntur. quas ipsa. quia iste. sapiente provident. blanditiis consectetur voluptas suscipit explicabo. aut.</p>\r\n\r\n<h3 style=\\"text-align:center\\">Etc</h3>', NULL, NULL, 1, 0, 'Условия использования', '', '', 'index, follow', '', '2017-02-20 13:41:54', 1487601714, '2017-03-23 10:27:39', NULL),
(7, 2, 'the-19-habano-festival-begins-with-the-montecrsito-brand-as-the-main-protagonist', 'The XIX Habano Festival begins with the Montecrsito brand as the main protagonist', 'Since today, La Habana hosts the XIX Habano Festival, the biggest international event for the lovers of the best tobacco in the world. This event will be held from February 27th to March 3rd, and will participate international attendees who love these products.', 'QdO_cr.jpg', '<h1>The XIX Habano Festival begins with the Montecrsito brand as the main protagonist</h1>\r\n\r\n<p>In the press conference hosted by Leopoldo Cintra Gonz&aacute;lez, Commercial Vice-president from Habanos, S.A. and Javier Terr&eacute;s de Ercilla, Development Vice-president from Habanos, S.A., it has been introduced the main launches for this year, in particular the new L&iacute;nea 1935 from Montecristo. This new line takes its name from the year of this legendary brand foundation being the premium line of the Montecristo brand. The new L&iacute;nea 1935 has three vitolas, Malt&eacute;s (53&times;153 mm), Dumas (49 x 130 mm) and Leyenda (55 x 165 mm). The first two vitolas are brand new vitolas in the Habanos portfolio, and the third one takes the form of the special limited edition Montecristo 80 Aniversario, launched in 2015 to commemorate the brand&rsquo;s 80th Anniversary. The Habanos of Montecristo L&iacute;nea 1935 have a new and differentiated presentation and a full strength flavour blend for the first time in this brand.</p>\r\n\r\n<p><img alt=\\"fest\\" class=\\"img-blog\\" src=\\"/assets/uploads/images/blog/articles/mc.jpg\\" title=\\"fest\\" /></p>\r\n\r\n<p>One of this year main releases is the launch of H. Upmann Sir Winston Gran Reserva 2011 (47 x 178 mm), in which the filler, binder and wrapper leaves used have undergone a long and careful five-year aging process. This is the first Gran Reserva by H. Upmann, and is presented in a unique and exclusive selection of only 5,000 numbered cases. The Welcome Evening of the Festival will be dedicated to H. Upmann Sir Winston Gran Reserva 2011, which will take place this afternoon at the Club Habana, an historic venue in Havana.</p>\r\n\r\n<h3 style=\\"text-align:center\\">Any other content goes&hellip;</h3>', '', '', 0, 1, 'The XIX Habano Festival begins with the Montecrsito brand as the main protagonist', 'The XIX Habano Festival begins with the Montecrsito brand as the main protagonist', 'Since today, La Habana hosts the XIX Habano Festival, the biggest international event for the lovers of the best tobacco in the world. This event will be held from February 27th to March 3rd, and will participate international attendees who love these pro', 'noindex, nofollow', 'habanos.com proto', '2017-03-22 14:40:34', 1490207398, '2017-10-23 20:54:45', NULL),
(8, 2, 'the-habanos-festival-is-almost-here', 'The habanos festival is almost here', 'Havana is preparing to host the XIX Habanos Festival, to be held from 27th February to 3rd March, which will unveil the biggest new developments for 2017.', 'fest_img_34.jpg', '<h1>The habanos festival is almost here</h1>\r\n\r\n<p>Havana is preparing to host the XIX Habanos Festival, to be held from 27th February to 3rd March, which will unveil the biggest new developments for 2017. New launches will be presented this year both regarding products for the regular portfolio as well as specialties, which attendees will be able to find out all about and taste, exclusively, throughout the event.</p>\r\n\r\n<p><img alt=\\"fest is here\\" class=\\"img-blog\\" src=\\"/assets/uploads/images/blog/articles/fest_img_34.jpg\\" title=\\"fest is here\\" /></p>\r\n\r\n<p style=\\"text-align:center\\">and content goes on&hellip;</p>', '', '', 0, 1, 'The habanos festival is almost here', 'The habanos festival is almost her', 'The habanos festival is almost her', 'noindex, nofollow', '', '2017-03-22 15:02:00', 1490194920, '2017-10-23 20:54:50', NULL),
(9, 2, 'cohiba-atmosphere-in-shanghai-reopens', 'Cohiba Atmosphere in Shanghai reopens', 'Cohiba Atmosphere in Shanghai has reopened at new site on December 08th, 2016. This club is for members only with total 400 square meters, which has VIP room, lounge with wine cabinet, bar and 40 private cigar lockers with constant temperature and humidity control.', 'sdfA_cr.jpg', '<h1>Cohiba Atmosphere in Shanghai reopens</h1>\r\n\r\n<p>Cohiba Atmosphere in Shanghai has reopened at new site on December 08th, 2016. This club is for members only with total 400 square meters, which has VIP room, lounge with wine cabinet, bar and 40 private cigar lockers with constant temperature and humidity control. The club is located at the northern bund alongside the Huangpu River. This is a sister club with Cohiba Atmosphere Beijing, both Shanghai and Beijing&rsquo;s members could visit the other club as members.</p>\r\n\r\n<h4 style=\\"text-align:center\\">and content goes on&hellip;</h4>', '', '', 0, 1, 'Cohiba Atmosphere in Shanghai reopens', 'Cohiba Atmosphere in Shanghai reopens', 'Cohiba Atmosphere in Shanghai reopens', 'noindex, nofollow', '', '2017-03-22 18:20:41', 1490206841, '2017-10-23 20:54:48', NULL),
(10, 3, 'la-disfida-habanos-challenge', 'La Disfida Habanos Challenge', 'November 11th and 12th was the chosen data to celebrate the final of &ldquo;La Disfida&rdquo;, teams challenge, organized by Cigar Club Alto Salento and Diadema S. p. A. (Official Distributor of Habanos for Italy) exclusively open for enthusiasts previously graduated in Habanos Academy.', 'they_cr.jpg', '<h1>La Disfida Habanos Challenge</h1>\r\n\r\n<p>November 11th and 12th was the chosen data to celebrate the final of &ldquo;La Disfida&rdquo;, teams challenge, organized by Cigar Club Alto Salento and Diadema S. p. A. (Official Distributor of Habanos for Italy) exclusively open for enthusiasts previously graduated in Habanos Academy.</p>\r\n\r\n<p>The challenge included five play offs previous to the grand final celebrated in the city of Lecce. These play offs were celebrated in these cities: Iseo, Matelica, Monselice, Napoli and Torino.</p>\r\n\r\n<p><img alt=\\"alt text of image\\" class=\\"img-blog\\" src=\\"/assets/uploads/images/blog/articles/they.jpg\\" title=\\"title of image\\" /></p>\r\n\r\n<p>The winning team would enjoy a prestigious price: participating together with Diadema in the 19th Edition of the Festival del Habano. The final was played between the teams from: Torino, Napoli, Monselice and Iseo. Unfortunately Matelica&rsquo;s team could not participate due to the earthquakes that have been hitting the center of the Italic Peninsula.</p>\r\n\r\n<p style=\\"text-align:center\\">and go on&hellip;</p>', '', '', 0, 1, 'La Disfida Habanos Challenge', 'La Disfida Habanos Challenge', 'La Disfida Habanos Challenge', 'noindex, nofollow', '', '2017-03-22 18:29:22', 1490207407, '2017-10-23 20:54:42', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` text,
  `newtab` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `alt_text` varchar(255) DEFAULT NULL,
  `img_src` text,
  `published` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `lifted_up_at` int(10) unsigned DEFAULT NULL,
  `used_by_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `banners`
--

INSERT INTO `banners` (`id`, `name`, `url`, `newtab`, `alt_text`, `img_src`, `published`, `lifted_up_at`, `used_by_user_id`) VALUES
(1, 'Банер про виски', 'http://www.jackdaniels.com/en-us/', 1, 'Jack Daniels', 'uploads/images/banners/whiskey.jpg', 1, NULL, NULL),
(2, 'Банер про сигары Oliva', 'http://www.olivacigar.com/', 1, 'Oliva Cigars', 'uploads/images/banners/olivia.jpg', 1, NULL, NULL),
(3, 'Банер про кубинские сигары', 'http://www.lacasadelhabano.lu/', 1, 'Cigars of Cuba', 'uploads/images/banners/custom_pleasure.jpg', 1, NULL, NULL),
(4, 'Размеры банера', 'http://google.com', 1, 'Banner sizes', 'uploads/images/banners/banner_1140x440px.gif', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` smallint(3) unsigned NOT NULL DEFAULT '55',
  `slug` varchar(128) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `intro` text,
  `html` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_robots` varchar(18) DEFAULT 'index, follow',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lifted_up_at` int(10) unsigned DEFAULT NULL,
  `is_cigar_brand` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `brand_of_day` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `used_by_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `brands`
--

INSERT INTO `brands` (`id`, `country`, `slug`, `name`, `img`, `intro`, `html`, `meta_title`, `meta_description`, `meta_keywords`, `meta_robots`, `published`, `lifted_up_at`, `is_cigar_brand`, `brand_of_day`, `used_by_user_id`) VALUES
(1, 55, 'cohiba', 'Cohiba', 'Cohiba.jpg', 'Марка была создана в 1966 году по личной инициативе Фиделя Кастро. В свое время в интервью он вспоминал об этой истории так: &laquo;У меня был телохранитель, который тоже все время курил сигары и они имели настолько превосходный аромат, что однажды я не выдержал и спросил, что за марку сигар он курит? Он угостил меня одной сигарой, и это было действительно восхитительно. Мой охранник сказал, что эти сигары крутит его друг, мы нашли его, организовали производство на фабрике El Laguito, а теперь кубинские сигары Cohiba знает весь мир. Сигары Cohiba, наверное, сейчас самые кубинские, если можно так сказать, несмотря на то, мы дали им &laquo;американское&raquo; имя. Так индейцы называли сигары&raquo;.', '<p style=\\"text-align:center\\">Марка была создана в 1966 году по личной инициативе Фиделя Кастро. В свое время в интервью он вспоминал об этой истории так: &laquo;У меня был телохранитель, который тоже все время курил сигары и они имели настолько превосходный аромат, что однажды я не выдержал и спросил, что за марку сигар он курит? Он угостил меня одной сигарой, и это было действительно восхитительно. Мой охранник сказал, что эти сигары крутит его друг, мы нашли его, организовали производство на фабрике El Laguito, а теперь кубинские сигары Cohiba знает весь мир. Сигары Cohiba, наверное, сейчас самые кубинские, если можно так сказать, несмотря на то, мы дали им &laquo;американское&raquo; имя. Так индейцы называли сигары&raquo;.</p>\r\n\r\n<p style=\\"text-align:center\\">Некогда продукция El Laguito доставалась в качестве подарков главам государства и дипломатам, посещавшим Кубу, но с 1982 года стало возможно <strong>купить сигары</strong> Cohiba в лучших сигарных магазинах Испании, а затем и в других странах. Эти сигары обрели столь высокую популярность у ценителей, что их стали подделывать. Чтобы <strong>приобрести эти превосходные сигары в Москве</strong>, необходимо знать, что оригинальные сигары Cohiba продаются только в сертифицированных магазинах Habanos Specialist, La Casa del Habano и Habanos Point. Cohiba &ndash; это действительно выдающийся бренд: для него отбираются лучшие из лучших листья с пяти лучших полей Vegas Finas Primera в San Juan y Martinez и San Luis в регионе Vuelta Abajo. Уникальная начинка из смеси листьев seco и ligero проходит три ферментации в бочках, что добавляет ей гладкости и полноты вкуса.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: Linea Clasica &ndash; средняя и выше средней, Linea 1492 &ndash; средняя, Linea Maduro 5 &ndash; выше средней, Linea Behike &ndash; выше средней.</p>', 'Cohiba', 'Сигары Cohiba', NULL, 'index, follow', 1, 1490034075, 1, 1, NULL),
(2, 55, 'montecristo', 'Montecristo', 'Montecristo.jpg', 'Один из самых известных в мире брендов сигар получил свое имя в честь героя романа Дюма &laquo;Граф Монте-Кристо&raquo;. Эту книгу обожал чтец, развлекавший крутильщиков-торседорес чтением во время работы. Первоначально, в 1935 году, ассортимент сигар Montecristo состоял всего из пяти номерных витол, но на нынешний день список выпускаемых фабрикой форматов огромен и способен удовлетворить любого.', '<p style=\\"text-align:center\\">Один из самых известных в мире брендов сигар получил свое имя в честь героя романа Дюма &laquo;Граф Монте-Кристо&raquo;. Эту книгу обожал чтец, развлекавший крутильщиков-торседорес чтением во время работы. Первоначально, в 1935 году, ассортимент сигар Montecristo состоял всего из пяти номерных витол, но на нынешний день список выпускаемых фабрикой форматов огромен и способен удовлетворить любого. Все они, от крошечных Joyita и Petit Edmundo до монументальной Montecristo А, создаются вручную из длиннолистовых табаков, произрастающих в регионе Vuelta Abajo. <strong>Заказать и купить в Москве сигары</strong> Montecristo несложно, труднее выбрать свой идеал крепости (от средней до сильной) и любимую витолу: хороши все без исключения!</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: выше средней.</p>', 'Montecristo', 'Сигары Montecristo', NULL, 'index, follow', 1, 1490034071, 1, 0, NULL),
(3, 55, 'partagas', 'Partagas', 'Partagas.jpg', 'C 1845 года любой житель Гаваны знал &ndash; купить сигары с тяжеловатым, но очень приятным узнаваемым землистым привкусом и характерным насыщенным ароматом с тонами кожи проще всего на сигарной фабрике дона Хайме Партагаса, по адресу Industria Street, 520. Секрет успеха марки  Partagas кроется в грамотном выборе длиннолистового наполнителя (tripa larga) для сигар: регион Vuelta Abajo, откуда получает табак эта фабрика, обладает идеальными почвами и климатом для выращивания табачного листа и издавна является основным поставщиком сырья для сигар премиум и супер премиум классов.', '<p style=\\"text-align:center\\">C 1845 года любой житель Гаваны знал &ndash; <strong>купить сигары</strong> с тяжеловатым, но очень приятным узнаваемым землистым привкусом и характерным насыщенным ароматом с тонами кожи проще всего на сигарной фабрике дона Хайме Партагаса, по адресу Industria Street, 520. Секрет успеха марки &nbsp;Partagas кроется в грамотном выборе длиннолистового наполнителя (tripa larga) для сигар: регион Vuelta Abajo, откуда получает табак эта фабрика, обладает идеальными почвами и климатом для выращивания табачного листа и издавна является основным поставщиком сырья для сигар премиум и супер премиум классов.</p>\r\n\r\n<p style=\\"text-align:center\\">Также обращает на себя внимание неизменно качественная ручная (totalmente a mano) скрутка сигар.</p>\r\n\r\n<p style=\\"text-align:center\\">Сегодня <strong>заказать и купить сигары Partagas можно и в Москве</strong>, что не может не радовать истинных афисионадо.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: высокая, полная.</p>', 'Partagas', 'Сигары Partagas', NULL, 'index, follow', 1, 1490034060, 1, 0, NULL),
(4, 55, 'romeo-y-julieta', 'Romeo Y Julieta', 'Romeo.jpg', 'Бренд, появившийся в 1875 году, получил свое название в честь одноименной трагедии Шекспира. Более ничего трагического в сигарах Romeo y Julieta нет, наоборот: они исполнены позитива. Средняя крепость (что немаловажно для начинающих афисионадо), прекрасные мешки из длинных листьев (tripa larga) табаков региона Vuelta Abajo, полностью ручная скрутка (totalmente a mano).', '<p style=\\"text-align:center\\">Бренд, появившийся в 1875 году, получил свое название в честь одноименной трагедии Шекспира. Более ничего трагического в сигарах Romeo y Julieta нет, наоборот: они исполнены позитива. Средняя крепость (что немаловажно для начинающих афисионадо), прекрасные мешки из длинных листьев (tripa larga) табаков региона Vuelta Abajo, полностью ручная скрутка (totalmente a mano).</p>\r\n\r\n<p style=\\"text-align:center\\">Забавный нюанс: в начале ХХ века компанией руководил талантливый промоутер дон Хосе &laquo;Пепин&raquo; Родригес Фернандес, придававший особое значение оформлению сигар (до него этим вопросом не особо озадачивались). В результате продажи выросли &ndash; каждому хотелось <strong>купить сигары</strong> с необычным украшением. Коллекция сигарных бантов Romeo y Julieta насчитывает на сегодняшний день более 20000 экземпляров.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: выше средней.</p>', 'Romeo Y Julieta', 'Сигары Romeo Y Julieta', NULL, 'index, follow', 1, 1490034065, 1, 0, NULL),
(5, 55, 'hoyo-de-monterrey', 'Hoyo De Monterrey', 'Hoyo.jpg', 'Сигары Hoyo de Monterrey родом из города Сан-Луис Мартинес, находящегося в центре знаменитого табачного региона Vuelta Abajo. Плантации Hoyo de Monterrey начинаются буквально за украшенными надписью &laquo;Hoyo de Monterrey. Jose Gener. 1860&raquo; воротами старого города. Хосе Женер &ndash; имя предприимчивого каталонца, в 1865 году придумавшего выпускать и продавать сигары высочайшего качество под брендом Hoyo de Monterrey.', '<p style=\\"text-align:center\\">Сигары Hoyo de Monterrey родом из города Сан-Луис Мартинес, находящегося в центре знаменитого табачного региона Vuelta Abajo. Плантации Hoyo de Monterrey начинаются буквально за украшенными надписью &laquo;Hoyo de Monterrey. Jose Gener. 1860&raquo; воротами старого города. Хосе Женер &ndash; имя предприимчивого каталонца, в 1865 году придумавшего выпускать и продавать сигары высочайшего качество под брендом Hoyo de Monterrey.</p>\r\n\r\n<p style=\\"text-align:center\\">Романтично звучащее слово hoyo в буквальном переводе означает &laquo;дыры&raquo;. Нелестно? Зато довольно точно указывает на местоположение плантаций &ndash; они находятся в низине, на берегах реки Сан-Хуан Мартинес. Все форматы Hoyo de Monterrey скручены вручную, наполнитель &ndash; из длинных листьев табака.</p>\r\n\r\n<p style=\\"text-align:center\\">Аромат сигар Hoyo de Monterrey делает эти сигары привлекательным выбором для тех, кто стремится к утонченным ароматам Habano, что легки на вкус, но обладают элегантностью и сложностью.</p>\r\n\r\n<p style=\\"text-align:center\\">Наиболее известны форматы Double Corona и Epicure № 1 и № 2, но в рамках Hoyo de Monterrey есть также целый ряд стандартных витол, известеных как Le Hoyo Series.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: от мягкой до средней.</p>', 'Hoyo De Monterey', 'Сигары Hoyo De Monterey', NULL, 'index, follow', 1, 1490034055, 1, 0, NULL),
(6, 55, 'h-upmann', 'H. Upmann', 'Hupmann.jpg', 'Герман Упманн &ndash; немецкий банкир, сумевший остаться в памяти потомков не столько как финансист, но и как незаурядный афисионадо, создатель популярного бренда. В 1844 году он покинул Германию и обосновался в Гаване, чтобы иметь возможность посвятить себя созданию новых сигар. Банк Упманна прекратил существование еще в начале 1920-х годов, зато сигары пережили своего создателя и по сей день радуют тех, кто хочет купить сигары ручной скрутки с длиннолистовым наполнителем, и при этом не слишком крепкие, но вкусные и ароматные.', '<p style=\\"text-align:center\\">Герман Упманн &ndash; немецкий банкир, сумевший остаться в памяти потомков не столько как финансист, но и как незаурядный афисионадо, создатель популярного бренда. В 1844 году он покинул Германию и обосновался в Гаване, чтобы иметь возможность посвятить себя созданию новых сигар. Банк Упманна прекратил существование еще в начале 1920-х годов, зато сигары пережили своего создателя и по сей день радуют тех, кто хочет <strong>купить сигары ручной скрутки с длиннолистовым наполнителем</strong>, и при этом не слишком крепкие, но вкусные и ароматные.</p>\r\n\r\n<p style=\\"text-align:center\\">По традиции, сложившейся с момента рождения бренда, коробки этих сигар украшены факсимильной подписью Германа Упманна. За время своего существования <strong>марка, которую можно купить и в Москве</strong>, завоевала золотые медали одиннадцати международных выставок.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: средняя и выше средней.</p>', 'H. Upmann', 'Сигары H. Upmann', NULL, 'index, follow', 1, 1490034049, 1, 0, NULL),
(7, 55, 'jose-l-piedra', 'Jose L. Piedra', 'Jlp.jpg', 'Семья Пьедра пришла на Кубу в 1880-х из испанской провинции Астурия. Они поселились в центре региона Vuelta Arriba, где табак выращивается с XVI века. Хосе Ламадрид Пьедра, родившийся уже на Кубе, неподалеку от живописного городка Санта Клара, решил, что мало выращивать хороший табак &ndash; нужно самому делать из него сигары. Так появился бренд Jose L. Piedra.', '<p style=\\"text-align:center\\">Семья Пьедра пришла на Кубу в 1880-х из испанской провинции Астурия. Они поселились в центре региона Vuelta Arriba, где табак выращивается с XVI века. Хосе Ламадрид Пьедра, родившийся уже на Кубе, неподалеку от живописного городка Санта Клара, решил, что мало выращивать хороший табак &ndash; нужно самому делать из него сигары. Так появился бренд Jose L. Piedra.<br />\r\nСигары производятся &laquo;totalmente a mano, tripa corta&raquo; &ndash; т. е. скручиваются вручную, но наполнитель для них делается не из цельного листа, а из нарезанного табака. Зато весь табак &ndash; только из родного региона Vuelta Arriba, у сторонних фермеров лист не закупают.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: средняя, выше средней.</p>', 'Jose L. Piedra', 'Сигары Jose L. Piedra', NULL, 'index, follow', 1, 1490034042, 1, 0, NULL),
(8, 55, 'cuaba', 'Cuaba', 'Cuaba.jpg', 'Эти сигары производятся на фабрике Romeo y Julieta и славятся у любителей &laquo;классических&raquo; сигар тем, что все сигары Cuaba выпускаются в уникальном старинном формате, известном как &laquo;двойной figurado&raquo;. В конце XIX века он был неимоверно популярен, но в наши дни о нем вспомнили только производители Cuaba &ndash; да и то лишь в 1996 году.\r\nИзготовление &laquo;double figurados&raquo; считается вершиной искусства кубинских торседоров, поэтому всякий энтузиаст-афисионадо, где бы он ни жил, в Москве или в Риме, непременно должен включить в свою коллекцию хотя бы пару подобных сигар.', '<p style=\\"text-align:center\\">Эти сигары производятся на фабрике Romeo y Julieta и славятся у любителей &laquo;классических&raquo; сигар тем, что все сигары Cuaba выпускаются в уникальном старинном формате, известном как &laquo;двойной figurado&raquo;. В конце XIX века он был неимоверно популярен, но в наши дни о нем вспомнили только производители Cuaba &ndash; да и то лишь в 1996 году.<br />\r\nИзготовление &laquo;double figurados&raquo; считается вершиной искусства кубинских торседоров, поэтому всякий энтузиаст-афисионадо, где бы он ни жил, в Москве или в Риме, непременно должен включить в свою коллекцию хотя бы пару подобных сигар.</p>\r\n\r\n<p style=\\"text-align:center\\"><strong>Покупая сигары Cuaba</strong>, также можно почувствовать себя настоящим кубинским аборигеном: слово, ставшее именем этих сигар, у индейцев таино служило названием куста, веточками которого они разжигали свои сигары. &nbsp;Помните, как назвались сами сигары? Cohiba, конечно же!</p>\r\n\r\n<p style=\\"text-align:center\\">Так же, как Cohiba, сигары Cuaba производятся вручную из длинных табачных листьев, а табак для них поступает только и исключительно из региона Vuelta Abajo.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: от средней до полной.</p>', 'Cuaba', 'Сигары Cuaba', NULL, 'index, follow', 1, 1490034028, 1, 0, NULL),
(9, 55, 'san-cristobal-de-la-habana', 'San Cristobal De La Habana', 'Sancristobal.jpg', 'Несмотря на то, что они получили свое название в честь Гаваны, сигары San Cristobal de La Habana возможно купить почти во всем мире. Их популярность не случайна &ndash; достаточно молодая марка не только использует лучшие длиннолистовые табаки из региона Vuelta Abajo и поручает работу высокопрофессиональным торседорам, но и уделяет огромное внимание оригинальности своей продукции.', '<p style=\\"text-align:center\\">Несмотря на то, что они получили свое название в честь Гаваны, <strong>сигары San Cristobal de La Habana возможно купить</strong> почти во всем мире. Их популярность не случайна &ndash; достаточно молодая марка не только использует лучшие длиннолистовые табаки из региона Vuelta Abajo и поручает работу высокопрофессиональным торседорам, но и уделяет огромное внимание оригинальности своей продукции.</p>\r\n\r\n<p style=\\"text-align:center\\">Три витолы (Mercaderes, Oficios и Muralla) являются эксклюзивом, который можно приобрести только в La Casa Del Habano, и более нигде. Два формата, El Morro и La Fuerza, уникальны на свой лад &ndash; аналогов нет ни в одном другом бренде. Это не только одни из самых оригинальных сигар, которые можно на сегодняшний день купить в Москве, но и одни из самых вкусных для начинающих афисионадо. Крепость San Cristobal de La Habana варьируется от легкой до средней. Несмотря на то, что форматы названы в честь оборонительных крепостей, некогда защищавших столицу Кубы, вкус и аромат этих сигар нисколько не агрессивны.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: от мягкой до средней.</p>', 'San Cristobal De La Habana', 'Сигары San Cristobal De La Habana', NULL, 'index, follow', 1, 1490034021, 1, 0, NULL),
(10, 55, 'trinidad', 'Trinidad', 'Trinidad.jpg', 'Сигары Trinidad названы в честь La Santisima Trinidad &ndash; города на южном побережье Кубы, основанного в XVI веке и названного в честь Святой Троицы. Он внесен в список &nbsp;исторических памятников, составленный ЮНЕСКО &ndash; ну а его тезки-сигары, вне всякого сомнения, внесены в список наиболее ценных и интересных любому просвещенному афисионадо сигар.', '<p style=\\"text-align:center\\">Сигары Trinidad названы в честь La Santisima Trinidad &ndash; города на южном побережье Кубы, основанного в XVI веке и названного в честь Святой Троицы. Он внесен в список &nbsp;исторических памятников, составленный ЮНЕСКО &ndash; ну а его тезки-сигары, вне всякого сомнения, внесены в список наиболее ценных и интересных любому просвещенному афисионадо сигар.</p>\r\n\r\n<p style=\\"text-align:center\\">Бренд появился в 1969 году на фабрике El Laguito (прославленный дом Cohiba), но долгое время, почти 30 лет, в свободную продажу эти сигары не поступали. Сегодня <strong>Trinidad можно купить в Москве</strong>, но далеко не в любом магазине. Эти редкие сигары изготавливаются полностью вручную из смеси отборных длиннолистовых табаков из региона Vuelta Abajo, выдержанных по специальной технологии.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: средняя и выше средней.</p>', 'Trinidad', 'Сигары Trinidad', NULL, 'index, follow', 1, 1490034014, 1, 0, NULL),
(11, 55, 'bolivar', 'Bolivar', 'Bolivar.jpg', 'Сигары Bolivar появились в 1902 году и получили свое имя в честь Симона Боливара, героя борьбы стран Южной Америки против испанского владычества. Неудивительно, что сигары этого бренда обладают запоминающимся, крепким и оригинальным вкусом: как говорится, титул обязывает. Богатство вкуса смеси табаков из региона Vuelta Abajo делает Bolivar одной из самых востребованных марок среди опытных курильщиков.', '<p style=\\"text-align:center\\">Сигары Bolivar появились в 1902 году и получили свое имя в честь Симона Боливара, героя борьбы стран Южной Америки против испанского владычества. Неудивительно, что сигары этого бренда обладают запоминающимся, крепким и оригинальным вкусом: как говорится, титул обязывает. Богатство вкуса смеси табаков из региона Vuelta Abajo делает Bolivar одной из самых востребованных марок среди опытных курильщиков. Производятся они на знаменитой фабрике Partagas в центре Гаваны, ну а <strong>купить эти сигары можно и в Москве</strong>.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: выше средней, до высокой.</p>', 'Bolivar', 'Сигары Bolivar', NULL, 'index, follow', 1, 1490034007, 1, 0, NULL),
(12, 55, 'fonseca', 'Fonseca', 'Fonseca.jpg', 'Дон Франсиско Фонсека, основавший собственный сигарный бренд в 1890 году, был щеголем и эстетом. Это не могло не сказаться на характере сигар Fonseca: они нежные и ароматные, легкие. Вне зависимости, изготовлены они из длинных листьев табака или наполнены &laquo;tripa corta&raquo;, резаным листом, эти сигары скручиваются вручную и снабжаются нарядной одеждой &ndash; каждая оборачивается тонким шелковым лоскутком. Красиво, необычно, притягательно &ndash; и очень приятно. Доставая такую сигару, ее приходится разворачивать, словно новогодний подарок в детстве. Любой афисионадо не устоит перед тем, чтобы сделать себе такой презент, если сможет купить эти сигары в Москве.', '<p style=\\"text-align:center\\">Дон Франсиско Фонсека, основавший собственный сигарный бренд в 1890 году, был щеголем и эстетом. Это не могло не сказаться на характере сигар Fonseca: они нежные и ароматные, легкие. Вне зависимости, изготовлены они из длинных листьев табака или наполнены &laquo;tripa corta&raquo;, резаным листом, эти сигары скручиваются вручную и снабжаются нарядной одеждой &ndash; каждая оборачивается тонким шелковым лоскутком. Красиво, необычно, притягательно &ndash; и очень приятно. Доставая такую сигару, ее приходится разворачивать, словно новогодний подарок в детстве. Любой афисионадо не устоит перед тем, чтобы сделать себе такой презент, если сможет <strong>купить эти сигары в Москве</strong>.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: мягкая.</p>', 'Fonseca', 'Сигары Fonseca', NULL, 'index, follow', 1, 1490034000, 1, 0, NULL),
(13, 55, 'punch', 'Punch', 'Punch.jpg', 'Эти сигары названы в честь старого английского юмористического  журнала, а на этикетке коробки неизменно изображается клоун, шут. Нетипично для Гаваны? Зато подойдет для Лондона! &ndash; так или примерно так рассуждали в середине XIX века основатели бренда, рассчитывая покорить британский рынок.', '<p style=\\"text-align:center\\">Эти сигары названы в честь старого английского юмористического &nbsp;журнала, а на этикетке коробки неизменно изображается клоун, шут. Нетипично для Гаваны? Зато подойдет для Лондона! &ndash; так или примерно так рассуждали в середине XIX века основатели бренда, рассчитывая покорить британский рынок.</p>\r\n\r\n<p style=\\"text-align:center\\">С 1925 года сигары Punch производятся на фабрике Hoyo de Monterrey. Неизвестно, насколько они популярны в Лондоне, но зато в Москве сигары Punch неизменно пользуются популярностью. Удобные и разнообразные форматы, средняя крепость, смесь табаков из региона Vuelta Abajo и полностью ручная скрутка &ndash; их есть за что любить!</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: средняя.</p>', 'Punch', 'Сигары Punch', NULL, 'index, follow', 1, 1490033994, 1, 0, NULL),
(14, 55, 'quintero', 'Quintero', 'Quintero.jpg', 'Изначально Агустина Кинтеро, совладелица небольшой лавки в городе Сьенфуэгос, известного под романтичным прозвищем &laquo;Жемчужина Юга&raquo;, всего-навсего хотела порадовать своих четверых братьев скрученными специально для них сигарами. Сочетание ненавязчивого аромата, средней крепости, мягкого вкуса и доступной цены оказалось по душе не только братьям Кинтеро, но и их друзьям, а позже &ndash; и большинству любителей сигар в городке. Так началась история бренда  Quintero.', '<p style=\\"text-align:center\\">Изначально Агустина Кинтеро, совладелица небольшой лавки в городе Сьенфуэгос, известного под романтичным прозвищем &laquo;Жемчужина Юга&raquo;, всего-навсего хотела порадовать своих четверых братьев скрученными специально для них сигарами. Сочетание ненавязчивого аромата, средней крепости, мягкого вкуса и доступной цены оказалось по душе не только братьям Кинтеро, но и их друзьям, а позже &ndash; и большинству любителей сигар в городке. Так началась история бренда &nbsp;Quintero.</p>\r\n\r\n<p style=\\"text-align:center\\">По сей день сигары, некогда покорившие Сьенфуэгос, производятся вручную из резаных табаков Vuelta Abajo. Изменилось только одно: теперь эти сигары можно купить не только в маленькой лавке Кинтеро, но и по всему миру, в том числе и в Москве.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: средняя.</p>', 'Quintero', 'Сигары Quintero', NULL, 'index, follow', 1, 1490033989, 1, 0, NULL),
(15, 55, 'vegas-robaina', 'Vegas Robaina', 'Robaina.jpg', 'Семья Робайна с 1845 года выращивает табак на драгоценных табачных полях (vegas) Cuchillas de Barbacoa, находящихся в городе Сан-Луис. Тем не менее, сигары с этим именем появились всего 20 лет назад &ndash; так было решено увековечить имя дона Алехандро Робайна, патриарха клана и мастера выращивания табака.  Покупая сигары Vegas Robaina, вы покупаете в некотором роде идеальные сигары &ndash; они сделаны вручную, наполнитель и покровный лист выращены в фамильных угодьях семейства Робайна, в регионе Вуэлта Абахо.', '<p style=\\"text-align:center\\">Семья Робайна с 1845 года выращивает табак на драгоценных табачных полях (vegas) Cuchillas de Barbacoa, находящихся в городе Сан-Луис. Тем не менее, сигары с этим именем появились всего 20 лет назад &ndash; так было решено увековечить имя дона Алехандро Робайна, патриарха клана и мастера выращивания табака. <strong>Покупая сигары Vegas Robaina</strong>, вы покупаете в некотором роде идеальные сигары &ndash; они сделаны вручную, наполнитель и покровный лист выращены в фамильных угодьях семейства Робайна, в регионе Вуэлта Абахо.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: от средней до полной, чаще средняя.</p>', 'Vegas Robaina', 'Сигары Vegas Robaina', NULL, 'index, follow', 1, 1490033981, 1, 0, NULL),
(16, 55, 'diplomaticos', 'Diplomaticos', 'Diplomaticos.jpg', 'Diplomaticos &ndash; сигары ручной скрутки из длиннолистовых табаков, так же, как и многие другие сигарные бренды Кубы, появившиеся в 60-е годы ХХ века. Подобно Montecristo, фабрика нумерует выпускаемые форматы и использует для покрова и наполнителя только длиннолистовые табаки. Некоторые знатоки считают, что и вкус Diplomaticos очень напоминает вкус знаменитых  Montecristo. Можно самолично проверить, так ли это, купив сигары Diplomaticos в Москве.', '<p style=\\"text-align:center\\">Diplomaticos &ndash; сигары ручной скрутки из длиннолистовых табаков, так же, как и многие другие сигарные бренды Кубы, появившиеся в 60-е годы ХХ века. Подобно Montecristo, фабрика нумерует выпускаемые форматы и использует для покрова и наполнителя только длиннолистовые табаки. Некоторые знатоки считают, что и вкус Diplomaticos очень напоминает вкус знаменитых &nbsp;Montecristo. Можно самолично проверить, так ли это, <strong>купив сигары Diplomaticos в Москве</strong>.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: от средней до полной.</p>', 'Diplomaticos', 'Сигары Diplomaticos', NULL, 'index, follow', 1, 1490033973, 1, 0, NULL),
(17, 55, 'el-rey-del-mundo', 'El Rey Del Mundo', 'Elrey.jpg', 'В 1882 году фабрика Antonio Allones подарила афисионадо всего мира новую марку сигар премиум класса. Скромное название El Rey del Mundo (т. е. &laquo;Король мира&raquo;) наглядно демонстрирует, какие надежды возлагал производитель на новый бренд. Лучшие длиннолистовые табаки региона Vuelta Abajo, ручная скрутка безупречного качества, сложный, но нежный вкус первосортных гаванских сигар &ndash; вот что такое El Rey del Mundo. Возможно, лучшими сигарами мира они и не являются, но это &ndash; один из лучших сигарных брендов, который можно купить в Москве.', '<p style=\\"text-align:center\\">В 1882 году фабрика Antonio Allones подарила афисионадо всего мира новую марку сигар премиум класса. Скромное название El Rey del Mundo (т. е. &laquo;Король мира&raquo;) наглядно демонстрирует, какие надежды возлагал производитель на новый бренд. Лучшие длиннолистовые табаки региона Vuelta Abajo, ручная скрутка безупречного качества, сложный, но нежный вкус первосортных гаванских сигар &ndash; вот что такое El Rey del Mundo. Возможно, лучшими сигарами мира они и не являются, но это &ndash; <strong>один из лучших сигарных брендов, который можно купить в Москве</strong>.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: от мягкой до средней.</p>', 'El Rey Del Mundo', 'Сигары El Rey Del Mundo', NULL, 'index, follow', 1, 1490033962, 1, 0, NULL),
(18, 55, 'juan-lopez', 'Juan Lopez', 'Juanlopez.jpg', 'Сегодня сигары Juan Lopez мало кому известны и их не так-то легко купить в Москве, придется потратить немало времени на поиски. Однако в 1870-х годах предприятие испанца Хуана Лопеса Диаса было известно практически любому поклоннику гаванских сигар. Если вы хотите приобрести сигары, изготовленные вручную по старинным рецептурам, максимально сохраняющим вкус и аромат длиннолистовых табаков из региона Vuelta Abajo, ваш выбор обязательно падет на сигары Juan Lopez.', '<p style=\\"text-align:center\\"><strong>Сегодня сигары Juan Lopez мало кому известны и их не так-то легко купить в Москве</strong>, придется потратить немало времени на поиски. Однако в 1870-х годах предприятие испанца Хуана Лопеса Диаса было известно практически любому поклоннику гаванских сигар. Если вы хотите <strong>приобрести сигары</strong>, изготовленные вручную по старинным рецептурам, максимально сохраняющим вкус и аромат длиннолистовых табаков из региона Vuelta Abajo, ваш выбор обязательно падет на сигары Juan Lopez.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: от средней до высокой.</p>', 'Juan Lopez', 'Сигары Juan Lopez', NULL, 'index, follow', 1, 1490033953, 1, 0, NULL),
(19, 55, 'flor-de-cano', 'La Flor De Cano', 'Jcano.jpg', 'Под романтичным названием &laquo;Цветок тростника&raquo; скрываются приятные и недорогие сигары. Ничего цветочного, кроме названия, в них нет:  только первоклассные резаные табаки из регионов Vuelta Abajo и Semi Vuelta. Средняя крепость, приятный аромат и нерезкий вкус (а также возможность купить эти сигары в Москве по невысокой для настоящих кубинских сигар цене) обеспечили им место в сердцах и хьюмидорах столичных афисионадо.', '<p style=\\"text-align:center\\">Под романтичным названием &laquo;Цветок тростника&raquo; скрываются приятные и недорогие сигары. Ничего цветочного, кроме названия, в них нет: &nbsp;только первоклассные резаные табаки из регионов Vuelta Abajo и Semi Vuelta. Средняя крепость, приятный аромат и нерезкий вкус (а также <strong>возможность купить эти сигары в Москве по невысокой для настоящих кубинских сигар цене</strong>) обеспечили им место в сердцах и хьюмидорах столичных афисионадо.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: средняя.</p>', 'La Flor De Cano', 'Сигары La Flor De Cano', NULL, 'index, follow', 1, 1490033945, 1, 0, NULL),
(20, 55, 'la-gloria-cubana', 'La Gloria Cubana', 'Gloria.jpg', 'Сигары La Gloria Cubana традиционно для высококачественных кубинских сигар изготовлены вручную из длиннолистового табака региона Vuelta Abajo, но опытные курильщики неизменно отдают им предпочтение перед прочими сортами. Это &ndash; истинная жемчужина, способная покорить даже на редкость взыскательного афисионадо! С момента своего появления в 1885 году La Gloria Cubana потрясает своей элегантностью, изысканностью и редкостью. Не так-то просто купить эти сигары, о них мало кто знает &ndash; хотя &laquo;Слава Кубы&raquo; (именно так переводится название бренда) &ndash; заслуживает мировой известности.', '<p style=\\"text-align:center\\">Сигары La Gloria Cubana традиционно для высококачественных кубинских сигар изготовлены вручную из длиннолистового табака региона Vuelta Abajo, но опытные курильщики неизменно отдают им предпочтение перед прочими сортами. Это &ndash; истинная жемчужина, способная покорить даже на редкость взыскательного афисионадо! С момента своего появления в 1885 году La Gloria Cubana потрясает своей элегантностью, изысканностью и редкостью. Не так-то просто <strong>купить эти сигары</strong>, о них мало кто знает &ndash; хотя &laquo;Слава Кубы&raquo; (именно так переводится название бренда) &ndash; заслуживает мировой известности.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: средняя.</p>', 'La Gloria Cubana', 'Сигары La Gloria Cubana', NULL, 'index, follow', 1, 1490033938, 1, 0, NULL);
INSERT INTO `brands` (`id`, `country`, `slug`, `name`, `img`, `intro`, `html`, `meta_title`, `meta_description`, `meta_keywords`, `meta_robots`, `published`, `lifted_up_at`, `is_cigar_brand`, `brand_of_day`, `used_by_user_id`) VALUES
(21, 55, 'por-larranaga', 'Por Larranaga', 'Larra.jpg', 'Сигары Por Larranaga можно купить как в более &laquo;пижонском&raquo; исполнении &ndash; из длиннолистового табака, так и в вариации попроще, с наполнителем из резаного листа. Вне зависимости от того, что вы выберете, Por Larranaga будет средней или невысокой крепости и порадует приятным ароматом и насыщенным, но мягким вкусом. Более чем полторы сотни лет (марка основана в 1834 году) эти сигары готовы купить как опытные афисионадо, так и новички.', '<p style=\\"text-align:center\\">Сигары Por Larranaga можно купить как в более &laquo;пижонском&raquo; исполнении &ndash; из длиннолистового табака, так и в вариации попроще, с наполнителем из резаного листа. Вне зависимости от того, что вы выберете, Por Larranaga будет средней или невысокой крепости и порадует приятным ароматом и насыщенным, но мягким вкусом. Более чем полторы сотни лет (марка основана в 1834 году) эти сигары готовы купить как опытные афисионадо, так и новички.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: от мягкой до средней.</p>', 'Por Larranaga', 'Сигары Por Larranaga', NULL, 'index, follow', 1, 1490033929, 1, 0, NULL),
(22, 55, 'quai-d-orsay', 'Quai d\\''Orsay', 'Quai.jpg', NULL, NULL, 'Quai d\\''Orsay', NULL, NULL, 'index, follow', 1, 1490033922, 1, 0, NULL),
(23, 55, 'rafael-gonzalez', 'Rafael Gonzalez', 'Rafael.jpg', 'Rafael Gonzalez &ndash; бренд, пленивший в 1930-х годах знаменитого афисионадо лорда Лонсдейла. Знаменитый курильщик был настолько покорен новыми (на тот момент) сигарами, что даже заказал на фабрике Гонзалеса уникальный формат лично для себя. Витола 165мм в длину и 42RG выпускается и по сей день. Она названа в честь лорда, и сегодня такой формат сигар можно найти и купить не только в ассортименте Rafael Gonzalez.', '<p style=\\"text-align:center\\">Rafael Gonzalez &ndash; бренд, пленивший в 1930-х годах знаменитого афисионадо лорда Лонсдейла. Знаменитый курильщик был настолько покорен новыми (на тот момент) сигарами, что даже заказал на фабрике Гонзалеса уникальный формат лично для себя. Витола 165мм в длину и 42RG выпускается и по сей день. Она названа в честь лорда, и сегодня <strong>такой формат сигар можно найти и купить</strong> не только в ассортименте Rafael Gonzalez.</p>\r\n\r\n<p style=\\"text-align:center\\">Эти сигары отличаются легкостью при сохранении всей полноты аромата и вкуса табаков Vuelta Abajo. Благодаря отсутствию излишней крепости эти сигары &ndash; практически универсальный выбор, их можно курить в любое время и с любым сопровождением &ndash; или вовсе без оного.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: мягкие.</p>', 'Rafael Gonzalez', 'Сигары Rafael Gonzalez', NULL, 'index, follow', 1, 1490033914, 1, 0, NULL),
(24, 55, 'ramon-allones', 'Ramon Allones', 'Ramon.jpg', 'Одной из старейших марок кубинских сигар является Ramon Allones. Фабрика впервые заявила о себе в 1837 году и немедленно заинтересовала курильщиков. Не только ассортиментом форматов и интересным ароматом смесей табаков из Vuelta Abajo, но и &ndash; не в последнюю очередь &ndash; разнообразием и оригинальностью упаковок. Галисиец Рамон Аллонес первым стал упаковывать сигары в яркие коробки, украшенные эмблемой фабрики. Удобная и привлекательная &laquo;расфасовка&raquo; привлекала любопытных: купить сигары в красивой коробке были готовы даже те, кто первый раз слышал об этом бренде. Попробовав сигары, случайные покупатели быстро становились завсегдатаями и поклонниками &ndash; ведь Ramon Allones уделял огромное внимание не только оформлению, но и содержанию. Если представится случай купить в Москве сигары Ramon Allones Gigantes &ndash; обязательно сделайте это! Уникальные смеси для этой витолы составляются из длиннолистовых табаков, прошедших дополнительный строгий отбор.', '<p style=\\"text-align:center\\">Одной из старейших марок кубинских сигар является Ramon Allones. Фабрика впервые заявила о себе в 1837 году и немедленно заинтересовала курильщиков. Не только ассортиментом форматов и интересным ароматом смесей табаков из Vuelta Abajo, но и &ndash; не в последнюю очередь &ndash; разнообразием и оригинальностью упаковок. Галисиец Рамон Аллонес первым стал упаковывать сигары в яркие коробки, украшенные эмблемой фабрики. Удобная и привлекательная &laquo;расфасовка&raquo; привлекала любопытных: <strong>купить сигары в красивой коробке</strong> были готовы даже те, кто первый раз слышал об этом бренде. Попробовав сигары, случайные покупатели быстро становились завсегдатаями и поклонниками &ndash; ведь Ramon Allones уделял огромное внимание не только оформлению, но и содержанию. Если представится случай <strong>купить в Москве сигары</strong> Ramon Allones Gigantes &ndash; обязательно сделайте это! Уникальные смеси для этой витолы составляются из длиннолистовых табаков, прошедших дополнительный строгий отбор.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: полная.</p>', 'Ramon Allones', 'Сигары Ramon Allones', NULL, 'index, follow', 1, 1490033904, 1, 0, NULL),
(25, 55, 'saint-luis-rey', 'Saint Luis Rey', 'Saintluis.jpg', 'Эти загадочные сигары появились незадолго до Второй Мировой войны, но знатоки до сих пор спорят о том, как и почему возникло красивое название. Прагматики утверждают, что, поскольку сигары изготавливаются из смеси табаков двух регионов, Vuelta Abajo и Semi Vuelta, название для них было выбрано &laquo;по карте&raquo; &ndash; в честь городка Сан-Луис, рядом с которым выращивают лучший покровный лист во всем Vuelta Abajo. Романтики же склонны полагать, что своим именем сигары Saint Luis Rey обязаны популярному перед войной роману Торнтона Уайлдера &laquo;Мосты в Сан-Луис-Рей&raquo;.', '<p style=\\"text-align:center\\">Эти загадочные сигары появились незадолго до Второй Мировой войны, но знатоки до сих пор спорят о том, как и почему возникло красивое название. Прагматики утверждают, что, поскольку сигары изготавливаются из смеси табаков двух регионов, Vuelta Abajo и Semi Vuelta, название для них было выбрано &laquo;по карте&raquo; &ndash; в честь городка Сан-Луис, рядом с которым выращивают лучший покровный лист во всем Vuelta Abajo. Романтики же склонны полагать, что своим именем сигары Saint Luis Rey обязаны популярному перед войной роману Торнтона Уайлдера &laquo;Мосты в Сан-Луис-Рей&raquo;.</p>\r\n\r\n<p style=\\"text-align:center\\">Неизвестно, кто из них прав, но любители крепких сигар не <strong>откажутся купить в Москве сигары Saint Luis Rey</strong>, даже если они сроду не слышали ни об Уайлдере, ни о городке Сан-Луис: сильная крепость сочетается в произведениях этой марки с насыщенным и богатым ароматом и стабильным, приятным вкусом.</p>\r\n\r\n<p style=\\"text-align:center\\">Крепость: от средней до полной.</p>', 'Saint Luis Rey', 'Сигары Saint Luis Rey', NULL, 'index, follow', 1, 1490033897, 1, 0, NULL),
(26, 55, 'sancho-panza', 'Sancho Panza', 'Sancho.jpg', 'Еще один бренд сигар, словно специально созданный для библиофилов: если кто-то начнет  собирать хьюмидор-&laquo;библиотеку&raquo;, наполненный сигарами, названными в честь литературных героев, мимо сигар Sancho Panza он никак не сможет пройти! Ну как не купить сигары, названные в честь верного оруженосца Дон Кихота?', '<p style=\\"text-align:center\\">Еще один бренд сигар, словно специально созданный для библиофилов: если кто-то начнет &nbsp;собирать хьюмидор-&laquo;библиотеку&raquo;, наполненный сигарами, названными в честь литературных героев, мимо сигар Sancho Panza он никак не сможет пройти! Ну как не <strong>купить сигары</strong>, названные в честь верного оруженосца Дон Кихота?</p>\r\n\r\n<p style=\\"text-align:center\\">Как известно, описанный Сервантесом Санчо, в отличие от своего исхудавшего господина, отличался плотной комплекцией. Солидными габаритами, присущими их литературному тезке, отличаются и наиболее известные сигары марки, Sancho Gran Corona. Их длина составляет внушительные 235 мм. Благодаря тщательному подбору табаков для смеси и грамотной ручной скрутке даже крупные форматы сигар Sancho Panza радуют не слишком крепким, но запоминающимся и полным вкусом.</p>', 'Sancho Panza', 'Сигары Sancho Panza', NULL, 'index, follow', 1, 1490033890, 1, 0, NULL),
(27, 55, 'vegueros', 'Vegueros', 'Vegueros.jpg', 'В самом известном табачном регионе Кубы, Vuelta Abajo, издавна считалось, что плох тот фермер, который только выращивает табак, но не умеет составить смесь и изготовить собственные сигары. Практичные крестьяне рассуждали логично: купить сигары, конечно, можно, но откуда заранее знать, хороши ли они? Зато если крутишь сигару сам, прямо на земле &ndash; знаешь, каким окажется вкус, еще не поднеся уголек к кончику &laquo;продукта&raquo;.', '<p style=\\"text-align:center\\">В самом известном табачном регионе Кубы, Vuelta Abajo, издавна считалось, что плох тот фермер, который только выращивает табак, но не умеет составить смесь и изготовить собственные сигары. Практичные крестьяне рассуждали логично: купить сигары, конечно, можно, но откуда заранее знать, хороши ли они? Зато если крутишь сигару сам, прямо на земле &ndash; знаешь, каким окажется вкус, еще не поднеся уголек к кончику &laquo;продукта&raquo;. В честь фермерских сигар-самокруток в 1996 году на фабрике Francisco Donatien, что в Пинар-дель-Рио, был создан бренд &nbsp;Vegueros (в переводе это слово означает &laquo;фермер&raquo;). &laquo;Фермерские&raquo; сигары отличаются интенсивным вкусом и ароматом, они создавались в соответствии с традициями жителей региона. Можно купить сигары Vegueros в Москве и, не выезжая на табачные поля, выяснить, годитесь ли вы в фермеры Пинар-дель-Рио.<br />\r\nКрепость: выше средней.</p>', 'Vegueros', 'Сигары Vegueros', NULL, 'index, follow', 1, 1490033884, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
  `id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `time` int(10) unsigned NOT NULL,
  `ip` varchar(45) NOT NULL,
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`id`,`word`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=283 ;

--
-- Дамп данных таблицы `captcha`
--

INSERT INTO `captcha` (`id`, `time`, `ip`, `word`) VALUES
(282, 1500480929, '88.210.26.7', '65693114');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(128) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `intro` text,
  `html` text,
  `img` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_robots` varchar(18) DEFAULT 'index, follow',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_cigar_category` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lifted_up_at` int(10) unsigned DEFAULT NULL,
  `used_by_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `slug`, `name`, `intro`, `html`, `img`, `meta_title`, `meta_description`, `meta_keywords`, `meta_robots`, `published`, `is_cigar_category`, `featured`, `lifted_up_at`, `used_by_user_id`) VALUES
(1, 'cigars', 'Сигары', 'Лучшие кубинские сигары в городе. Официальные поставки Habanos.', '<h1>Сигары</h1>\r\n\r\n<p>Consequuntur culpa. velit consectetur, harum quae. fugiat. doloremque qui adipisci obcaecati. ab similique sunt, nostrum omnis. dolorum sunt. porro. exercitationem impedit, est, veritatis. molestias animi, nihil eaque possimus, commodi ut et molestiae esse, unde. impedit. eos doloremque deserunt optio, beatae ut velit nihil a quisquam. cumque aut in non laboriosam, corrupti, est ea rerum quo quod quidem enim rem quod.</p>', 'cat_cigars.png', 'Сигары', NULL, NULL, 'index, follow', 1, 1, 0, 1487601382, NULL),
(2, 'cigarillos', 'Сигариллы', 'Сигариллы. Изящество тонких линий для гурманов.', NULL, 'cat_cigarillos.png', 'Сигариллы', NULL, NULL, 'index, follow', 1, 0, 0, 1487601379, NULL),
(3, 'tabacco', 'Табак', 'Табак только высшего класса. Для ценителей натуральности.', NULL, 'cat_tabacco.png', 'Табак', NULL, NULL, 'index, follow', 1, 0, 0, 1487601377, NULL),
(4, 'humidors', 'Хьюмидоры', 'Хьюмидоры. Деревянные, металлические, пластиковые. Электронные.', '<h1>Хьюмидоры</h1>\r\n\r\n<p>Ad nisi. nihil. rerum et tenetur aut et qui. velit, voluptatibus. voluptatem id, dolore. quaerat sapiente. suscipit tempora officia aliquid officiis velit, veritatis tempora provident, aut nobis. et in. reiciendis. amet, in aperiam ab qui earum consequatur nobis architecto aut minima et nostrum nihil quod autem ut amet, delectus, aut velit, et vero. molestiae magni impedit, magnam. vitae iste totam.</p>', 'cat_humidors.png', 'Хьюмидоры', NULL, NULL, 'index, follow', 1, 0, 0, 1487601373, NULL),
(5, 'cutters', 'Гильотины', 'Гильотины на любой калибр. Стильные, модные, классические.', NULL, 'cat_accessories.png', 'Гильотины', NULL, NULL, 'index, follow', 1, 0, 0, 1487601371, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `ips`
--

CREATE TABLE IF NOT EXISTS `ips` (
  `ip` varchar(45) NOT NULL,
  `access_time` int(10) unsigned NOT NULL,
  `block_time` int(10) unsigned DEFAULT NULL,
  `access_attempt_counter` smallint(2) unsigned DEFAULT '1',
  PRIMARY KEY (`ip`),
  UNIQUE KEY `ip_UNIQUE` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `address` text,
  `phone` varchar(24) DEFAULT NULL,
  `products` text COMMENT 'serialized array of ids and quantities of its products',
  `packs` text COMMENT 'serialized array of ids and quantities of its packs',
  `time` int(10) DEFAULT NULL,
  `is_new` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_done` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_orders_users_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `packs`
--

CREATE TABLE IF NOT EXISTS `packs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `html` text,
  `num_of_items` smallint(5) unsigned NOT NULL DEFAULT '10',
  `price` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quantity` smallint(4) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_boxes_products_idx` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `brand_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) NOT NULL,
  `is_cigar_product` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `price` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `vitola` smallint(3) unsigned NOT NULL DEFAULT '0',
  `line` smallint(3) unsigned NOT NULL DEFAULT '0',
  `length` smallint(3) unsigned NOT NULL DEFAULT '0',
  `cepo` smallint(2) unsigned NOT NULL DEFAULT '26',
  `intensity` smallint(2) unsigned NOT NULL DEFAULT '0',
  `flavour` smallint(2) unsigned NOT NULL DEFAULT '0',
  `wrapper` smallint(2) unsigned NOT NULL DEFAULT '0',
  `country_made` smallint(3) unsigned NOT NULL DEFAULT '55' COMMENT 'the id of production country. it can be other than brand country',
  `name` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `images` text COMMENT 'serialized array of file names of images',
  `intro` text,
  `html` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_robots` varchar(18) DEFAULT 'index, follow',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lifted_up_at` int(10) unsigned DEFAULT NULL,
  `quantity` smallint(4) unsigned NOT NULL DEFAULT '99',
  `used_by_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `fk_products_brands_idx` (`brand_id`),
  KEY `fk_products_categories_idx` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=221 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `category_id`, `brand_id`, `slug`, `is_cigar_product`, `price`, `vitola`, `line`, `length`, `cepo`, `intensity`, `flavour`, `wrapper`, `country_made`, `name`, `img`, `images`, `intro`, `html`, `meta_title`, `meta_description`, `meta_keywords`, `meta_robots`, `published`, `featured`, `lifted_up_at`, `quantity`, `used_by_user_id`) VALUES
(6, 4, 5, 'force-humido-chains', 0, 3000, 0, 0, 0, 26, 0, 0, 0, 55, 'Force Humido Chains', 'Small-Davidoff-humidor-with-cigar-scissors2016-808x900.png', NULL, NULL, NULL, 'Humidor One', NULL, NULL, 'index, follow', 1, 0, 1487609965, 99, NULL),
(7, 4, 6, 'just-a-box', 0, 12000, 0, 0, 0, 26, 0, 0, 0, 55, 'Just A Box', 'The-Engraved-Traveler-12-Cigar-Humidor-HUM-TR12M-ENG-31.png', NULL, NULL, NULL, 'Humidor Two', NULL, NULL, 'index, follow', 1, 0, 1487610005, 99, NULL),
(8, 4, 1, 'atmo-double-impact', 0, 36000, 0, 0, 0, 26, 0, 0, 0, 55, 'Atmo Double Impact', '82370-dakota-cigar-humidors-big.png', NULL, NULL, NULL, 'Humidor Three', NULL, NULL, 'index, follow', 1, 0, 1487610021, 99, NULL),
(9, 4, 6, 'humid-killer', 0, 20000, 0, 0, 0, 26, 0, 0, 0, 55, 'Humid Killer', 'Ted_s_Cigar_Humidor_2_edit.png', NULL, NULL, NULL, 'Humidor Four', NULL, NULL, 'index, follow', 1, 0, 1487610037, 99, NULL),
(10, 1, 1, 'cohiba-bhk-52', 1, 2500, 18, 0, 8, 52, 0, 0, 0, 55, 'Cohiba BHK 52', 'cohiba_bhk_52.png', NULL, NULL, NULL, 'Cohiba BHK 52', NULL, NULL, 'index, follow', 1, 0, 1490034768, 99, NULL),
(11, 1, 1, 'cohiba-bhk-54', 1, 2550, 7, 0, 25, 54, 0, 0, 0, 55, 'Cohiba BHK 54', 'cohiba_bhk_54.png', NULL, NULL, NULL, 'Cohiba BHK 54', NULL, NULL, 'index, follow', 1, 0, 1490041259, 99, NULL),
(12, 1, 1, 'cohiba-bhk-56', 1, 2600, 1, 0, 36, 56, 0, 0, 0, 55, 'Cohiba BHK 56', 'cohiba_bhk_56.png', NULL, NULL, NULL, 'Cohiba BHK 56', NULL, NULL, 'index, follow', 1, 0, 1490041403, 99, NULL),
(13, 1, 1, 'cohiba-coronas-especiales', 1, 1800, 76, 0, 28, 38, 1, 0, 0, 55, 'Cohiba Coronas Especiales', 'cohiba_coronas_especiales.png', NULL, NULL, NULL, 'Cohiba Coronas Especiales', NULL, NULL, 'index, follow', 1, 0, 1490041611, 99, NULL),
(14, 1, 1, 'cohiba-esplendidos', 1, 2000, 33, 0, 38, 47, 1, 0, 0, 55, 'Cohiba Esplendidos', 'cohiba_esplendidos.png', NULL, NULL, NULL, 'Cohiba Esplendidos', NULL, NULL, 'index, follow', 1, 0, 1490041916, 99, NULL),
(15, 1, 1, 'cohiba-exquisitos', 1, 2300, 86, 0, 13, 33, 1, 0, 0, 55, 'Cohiba Exquisitos', 'cohiba_exquisitos.png', NULL, NULL, NULL, 'Cohiba Exquisitos', NULL, NULL, 'index, follow', 1, 0, 1490042088, 99, NULL),
(16, 1, 1, 'cohiba-genios', 1, 2350, 14, 0, 21, 52, 1, 0, 0, 55, 'Cohiba Genios', 'cohiba_genios.png', NULL, NULL, NULL, 'Cohiba Genios', NULL, NULL, 'index, follow', 1, 0, 1490042408, 99, NULL),
(17, 1, 1, 'cohiba-lanceros', 1, 2050, 75, 0, 40, 38, 1, 0, 0, 55, 'Cohiba Lanceros', 'cohiba_lanceros.png', NULL, NULL, NULL, 'Cohiba Lanceros', NULL, NULL, 'index, follow', 1, 0, 1490042551, 99, NULL),
(18, 1, 1, 'cohiba-magicos', 1, 2600, 19, 0, 6, 52, 1, 0, 0, 55, 'Cohiba Magicos', 'cohiba_magicos.png', NULL, NULL, NULL, 'Cohiba Magicos', NULL, NULL, 'index, follow', 1, 0, 1490042744, 99, NULL),
(19, 1, 1, 'cohiba-panetelas', 1, 1850, 90, 0, 6, 26, 1, 0, 0, 55, 'Cohiba Panetelas', 'cohiba_panetelas.png', NULL, NULL, NULL, 'Cohiba Panetelas', NULL, NULL, 'index, follow', 1, 0, 1490042909, 99, NULL),
(20, 1, 1, 'cohiba-piramide-extra', 1, 2450, 5, 0, 33, 54, 2, 0, 0, 55, 'Cohiba Piramide Extra', 'cohiba_piramide_extra.png', NULL, NULL, NULL, 'Cohiba Piramide Extra', NULL, NULL, 'index, follow', 1, 0, 1490043286, 99, NULL),
(21, 1, 1, 'cohiba-robustos', 1, 2250, 25, 0, 11, 50, 1, 0, 0, 55, 'Cohiba Robustos', 'cohiba_robustos.png', NULL, NULL, NULL, 'Cohiba Robustos', NULL, NULL, 'index, follow', 1, 0, 1490043457, 99, NULL),
(22, 1, 1, 'cohiba-secretos', 1, 1950, 71, 0, 5, 40, 1, 0, 0, 55, 'Cohiba Secretos', 'cohiba_secretos.png', NULL, NULL, NULL, 'Cohiba Secretos', NULL, NULL, 'index, follow', 1, 0, 1490043626, 99, NULL),
(23, 1, 1, 'cohiba-siglo-1', 1, 1750, 72, 0, 3, 40, 2, 0, 0, 55, 'Cohiba Siglo I', 'cohiba_siglo_1.png', NULL, NULL, NULL, 'Cohiba Siglo 1', NULL, NULL, 'index, follow', 1, 0, 1490043851, 99, NULL),
(24, 1, 1, 'cohiba-siglo-1-tubo', 1, 2050, 72, 0, 3, 40, 2, 0, 0, 55, 'Cohiba Siglo I Tubo', 'cohiba_siglo_1_tubo.png', NULL, NULL, NULL, 'Cohiba Siglo 1 Tubo', NULL, NULL, 'index, follow', 1, 0, 1490043986, 99, NULL),
(25, 1, 1, 'cohiba-siglo-2', 1, 2150, 56, 0, 15, 42, 2, 0, 0, 55, 'Cohiba Siglo II', 'cohiba_siglo_2.png', NULL, NULL, NULL, 'Cohiba Siglo II', NULL, NULL, 'index, follow', 1, 0, 1490044276, 99, NULL),
(26, 1, 1, 'cohiba-siglo-2-tubo', 1, 2250, 56, 0, 15, 42, 2, 0, 0, 55, 'Cohiba Siglo II Tubo', 'cohiba_siglo_2_tubo.png', NULL, NULL, NULL, 'Cohiba Siglo II Tubo', NULL, NULL, 'index, follow', 1, 0, 1490044406, 99, NULL),
(27, 1, 1, 'cohiba-siglo-3', 1, 2000, 51, 0, 29, 42, 2, 0, 0, 55, 'Cohiba Siglo III', 'cohiba_siglo_3.png', NULL, NULL, NULL, 'Cohiba Siglo III', NULL, NULL, 'index, follow', 1, 0, 1490045757, 99, NULL),
(28, 1, 1, 'cohiba-siglo-3-tubo', 1, 2400, 51, 0, 29, 42, 2, 0, 0, 55, 'Cohiba Siglo III Tubo', 'cohiba_siglo_3_tubo.png', NULL, NULL, NULL, 'Cohiba Siglo III Tubo', NULL, NULL, 'index, follow', 1, 0, 1490045976, 99, NULL),
(29, 1, 1, 'cohiba-siglo-4', 1, 2400, 37, 0, 24, 46, 2, 0, 0, 55, 'Cohiba Siglo IV', 'cohiba_siglo_4.png', NULL, NULL, NULL, 'Cohiba Siglo IV', NULL, NULL, 'index, follow', 1, 0, 1490046145, 99, NULL),
(30, 1, 1, 'cohiba-siglo-4-tubo', 1, 2450, 37, 0, 24, 46, 2, 0, 0, 55, 'Cohiba Siglo IV Tubo', 'cohiba_siglo_4_tubo.png', NULL, NULL, NULL, 'Cohiba Siglo IV Tubo', NULL, NULL, 'index, follow', 1, 0, 1490046290, 99, NULL),
(31, 1, 1, 'cohiba-siglo-5', 1, 2400, 44, 0, 37, 43, 2, 0, 0, 55, 'Cohiba Siglo V', 'cohiba_siglo_5.png', NULL, NULL, NULL, 'Cohiba Siglo V', NULL, NULL, 'index, follow', 1, 0, 1490046461, 99, NULL),
(32, 1, 1, 'cohiba-siglo-5-tubo', 1, 2500, 44, 0, 37, 43, 2, 0, 0, 55, 'Cohiba Siglo V Tubo', 'cohiba_siglo_5_tubo.png', NULL, NULL, NULL, 'Cohiba Siglo V Tubo', NULL, NULL, 'index, follow', 1, 0, 1490046580, 99, NULL),
(33, 1, 1, 'cohiba-siglo-6', 1, 2500, 12, 0, 27, 52, 2, 0, 0, 55, 'Cohiba Siglo VI', 'cohiba_siglo_6.png', NULL, NULL, NULL, 'Cohiba Siglo VI', NULL, NULL, 'index, follow', 1, 0, 1490046797, 99, NULL),
(34, 1, 1, 'cohiba-siglo-6-tubo', 1, 2600, 12, 0, 27, 52, 2, 0, 0, 55, 'Cohiba Siglo VI Tubo', 'cohiba_siglo_6_tubo.png', NULL, NULL, NULL, 'Cohiba Siglo VI Tubo', NULL, NULL, 'index, follow', 1, 0, 1490046901, 99, NULL),
(35, 1, 1, 'cohiba-piramides', 1, 3250, 11, 3, 30, 52, 1, 0, 0, 55, 'Cohiba Piramides', 'cohiba_piramides.png', NULL, NULL, NULL, 'Cohiba Piramides', NULL, NULL, 'index, follow', 1, 0, 1490047056, 99, NULL),
(36, 1, 2, 'montecristo-80-aniversario-2015', 1, 2350, 2, 0, 35, 55, 1, 0, 0, 55, 'Montecristo 80 Aniversario - 2015', 'montecristo_80_aniversario_2015.png', NULL, NULL, NULL, 'Montecristo 80 Aniversario - 2015', NULL, NULL, 'index, follow', 1, 0, 1490087432, 99, NULL),
(37, 1, 2, 'montecristo-double-edmundo', 1, 2600, 22, 0, 29, 50, 1, 0, 0, 55, 'Montecristo Double Edmundo', 'montecristo_double_edmundo.png', NULL, NULL, NULL, 'Montecristo Double Edmundo', NULL, NULL, 'index, follow', 1, 0, 1490087629, 99, NULL),
(38, 1, 2, 'montecristo-eagle', 1, 2200, 6, 0, 27, 54, 2, 0, 0, 55, 'Montecristo Eagle', 'montecristo_eagle.png', NULL, NULL, NULL, 'Montecristo Eagle', NULL, NULL, 'index, follow', 1, 0, 1490087783, 99, NULL),
(39, 1, 2, 'montecristo-eagle-tubo', 1, 2450, 6, 0, 27, 54, 2, 0, 0, 55, 'Montecristo Eagle Tubo', 'montecristo_eagle_tubo.png', NULL, NULL, NULL, 'Montecristo Eagle Tubo', NULL, NULL, 'index, follow', 1, 0, 1490087916, 99, NULL),
(40, 1, 2, 'montecristo-edmundo', 1, 1200, 15, 0, 19, 52, 1, 0, 0, 55, 'Montecristo Edmundo', 'montecristo_edmundo.png', NULL, NULL, NULL, 'Montecristo Edmundo', NULL, NULL, 'index, follow', 1, 0, 1490088062, 99, NULL),
(41, 1, 2, 'montecristo-edmundo-tubos', 1, 1800, 15, 0, 19, 52, 1, 0, 0, 55, 'Montecristo Edmundo Tubos', 'montecristo_edmundo_tubos.png', NULL, NULL, NULL, 'Montecristo Edmundo Tubos', NULL, NULL, 'index, follow', 1, 0, 1490088190, 99, NULL),
(42, 1, 2, 'montecristo-joyitas', 1, 1400, 90, 0, 6, 26, 1, 0, 0, 55, 'Montecristo Joyitas', 'montecristo_joyitas.png', NULL, NULL, NULL, 'Montecristo Joyitas', NULL, NULL, 'index, follow', 1, 0, 1490088328, 99, NULL),
(43, 1, 2, 'montecristo-junior', 1, 1600, 78, 0, 5, 38, 2, 0, 0, 55, 'Montecristo Junior', 'montecristo_junior.png', NULL, NULL, NULL, 'Montecristo Junior', NULL, NULL, 'index, follow', 1, 0, 1490088455, 99, NULL),
(44, 1, 2, 'montecristo-junior-tubo', 1, 1900, 78, 0, 5, 38, 2, 0, 0, 55, 'Montecristo Junior Tubo', 'montecristo_junior_tubo.png', NULL, NULL, NULL, 'Montecristo Junior Tubo', NULL, NULL, 'index, follow', 1, 0, 1490088590, 99, NULL),
(45, 1, 2, 'montecristo-master', 1, 2000, 25, 0, 11, 50, 2, 0, 0, 55, 'Montecristo Master', 'montecristo_master.png', NULL, NULL, NULL, 'Montecristo Master', NULL, NULL, 'index, follow', 1, 0, 1490089642, 99, NULL),
(46, 1, 2, 'montecristo-master-tubo', 1, 2050, 25, 0, 11, 50, 2, 0, 0, 55, 'Montecristo Master Tubo', 'montecristo_master_tubo.png', NULL, NULL, NULL, 'Montecristo Master Tubo', NULL, NULL, 'index, follow', 1, 0, 1490089778, 99, NULL),
(47, 1, 2, 'montecristo-montecristo-a', 1, 2150, 32, 0, 42, 47, 1, 0, 0, 55, 'Montecristo Montecristo A', 'montecristo_montecristo_a.png', NULL, NULL, NULL, 'Montecristo Montecristo A', NULL, NULL, 'index, follow', 1, 0, 1490089896, 99, NULL),
(48, 1, 2, 'montecristo-montecristo-especial', 1, 2050, 75, 0, 40, 38, 1, 0, 0, 55, 'Montecristo Montecristo Especial', 'montecristo_montecristo_especial.png', NULL, NULL, NULL, 'Montecristo Montecristo Especial', NULL, NULL, 'index, follow', 1, 0, 1490090039, 99, NULL),
(49, 1, 2, 'montecristo-montecristo-especial-2', 1, 2250, 76, 0, 28, 38, 1, 0, 0, 55, 'Montecristo Montecristo Especial No.2', 'montecristo_montecristo_especial_2.png', NULL, NULL, NULL, 'Montecristo Montecristo Especial No.2', NULL, NULL, 'index, follow', 1, 0, 1490090199, 99, NULL),
(50, 1, 2, 'montecristo-montecristo-1', 1, 1900, 50, 0, 35, 42, 1, 0, 0, 55, 'Montecristo Montecristo No.1', 'montecristo_montecristo_1.png', NULL, NULL, NULL, 'Montecristo Montecristo No.1', NULL, NULL, 'index, follow', 1, 0, 1490090442, 99, NULL),
(51, 1, 2, 'montecristo-montecristo-2', 1, 1950, 11, 0, 30, 52, 1, 0, 0, 55, 'Montecristo Montecristo No.2', 'montecristo_montecristo_2.png', NULL, NULL, NULL, 'Montecristo Montecristo No.2', NULL, NULL, 'index, follow', 1, 0, 1490090585, 99, NULL),
(52, 1, 2, 'montecristo-montecristo-3', 1, 1950, 52, 0, 23, 42, 1, 0, 0, 55, 'Montecristo Montecristo No.3', 'montecristo_montecristo_3.png', NULL, NULL, NULL, 'Montecristo Montecristo No.3', NULL, NULL, 'index, follow', 1, 0, 1490090723, 99, NULL),
(53, 1, 2, 'montecristo-montecristo-4', 1, 2000, 56, 0, 15, 42, 1, 0, 0, 55, 'Montecristo Montecristo No.4', 'montecristo_montecristo_4.png', NULL, NULL, NULL, 'Montecristo Montecristo No.4', NULL, NULL, 'index, follow', 1, 0, 1490090848, 99, NULL),
(54, 1, 2, 'montecristo-montecristo-5', 1, 2050, 72, 0, 3, 40, 1, 0, 0, 55, 'Montecristo Montecristo No.5', 'montecristo_montecristo_5.png', NULL, NULL, NULL, 'Montecristo Montecristo No.5', NULL, NULL, 'index, follow', 1, 0, 1490090948, 99, NULL),
(55, 1, 2, 'montecristo-montecristo-petit-2', 1, 2000, 17, 0, 9, 52, 1, 0, 0, 55, 'Montecristo Montecristo Petit No.2', 'montecristo_montecristo_petit_2.png', NULL, NULL, NULL, 'Montecristo Montecristo Petit No.2', NULL, NULL, 'index, follow', 1, 0, 1490091061, 99, NULL),
(56, 1, 2, 'montecristo-montecristo-tubos', 1, 2100, 51, 0, 29, 42, 1, 0, 0, 55, 'Montecristo Montecristo Tubos', 'montecristo_montecristo_tubos.png', NULL, NULL, NULL, 'Montecristo Montecristo Tubos', NULL, NULL, 'index, follow', 1, 0, 1490091170, 99, NULL),
(57, 1, 2, 'montecristo-petit-edmundo', 1, 1900, 20, 0, 5, 52, 1, 0, 0, 55, 'Montecristo Petit Edmundo', 'montecristo_petit_edmundo.png', NULL, NULL, NULL, 'Montecristo Petit Edmundo', NULL, NULL, 'index, follow', 1, 0, 1490091265, 99, NULL),
(58, 1, 2, 'montecristo-petit-edmundo-tubo', 1, 2200, 20, 0, 5, 52, 1, 0, 0, 55, 'Montecristo Petit Edmundo Tubo', 'montecristo_petit_edmundo_tubo.png', NULL, NULL, NULL, 'Montecristo Petit Edmundo Tubo', NULL, NULL, 'index, follow', 1, 0, 1490091364, 99, NULL),
(59, 1, 2, 'montecristo-petit-tubos', 1, 2000, 56, 0, 15, 42, 1, 0, 0, 55, 'Montecristo Petit Tubos', 'montecristo_petit_tubos.png', NULL, NULL, NULL, 'Montecristo Petit Tubos', NULL, NULL, 'index, follow', 1, 0, 1490091492, 99, NULL),
(60, 1, 2, 'montecristo-regata', 1, 2100, 38, 0, 19, 46, 2, 0, 0, 55, 'Montecristo Regata', 'montecristo_regata.png', NULL, NULL, NULL, 'Montecristo Regata', NULL, NULL, 'index, follow', 1, 0, 1490091593, 99, NULL),
(61, 1, 2, 'montecristo-regata-tubo', 1, 1400, 38, 0, 19, 46, 2, 0, 0, 55, 'Montecristo Regata Tubo', 'montecristo_regata_tubo.png', NULL, NULL, NULL, 'Montecristo Regata Tubo', NULL, NULL, 'index, follow', 1, 0, 1490091697, 99, NULL),
(62, 1, 2, 'montecristo-robustos', 1, 2600, 25, 3, 11, 50, 1, 0, 0, 55, 'Montecristo Robustos', 'montecristo_robustos.png', NULL, NULL, NULL, 'Montecristo Robustos', NULL, NULL, 'index, follow', 1, 0, 1490091789, 99, NULL),
(63, 1, 2, 'montecristo-churchills-anejados', 1, 2800, 33, 6, 38, 47, 1, 0, 0, 55, 'Montecristo Churchills Anejados', 'montecristo_churchills_anejados.png', NULL, NULL, NULL, 'Montecristo Churchills Anejados', NULL, NULL, 'index, follow', 1, 0, 1490091956, 99, NULL),
(64, 1, 8, 'cuaba-distinguidos', 1, 1600, 10, 0, 34, 52, 1, 0, 0, 55, 'Cuaba Distinguidos', 'cuaba_distinguidos.png', NULL, NULL, NULL, 'Cuaba Distinguidos', NULL, NULL, 'index, follow', 1, 0, 1490092222, 99, NULL),
(65, 1, 8, 'cuaba-divinos', 1, 1350, 49, 0, 2, 43, 1, 0, 0, 55, 'Cuaba Divinos', 'cuaba_divinos.png', NULL, NULL, NULL, 'Cuaba Divinos', NULL, NULL, 'index, follow', 1, 0, 1490092685, 99, NULL),
(66, 1, 8, 'cuaba-exclusivos', 1, 1850, 36, 0, 26, 46, 1, 0, 0, 55, 'Cuaba Exclusivos', 'cuaba_exclusivos.png', NULL, NULL, NULL, 'Cuaba Exclusivos', NULL, NULL, 'index, follow', 1, 0, 1490092844, 99, NULL),
(67, 1, 8, 'cuaba-solomon', 1, 1600, 0, 0, 39, 57, 1, 0, 0, 55, 'Cuaba Solomon', 'cuaba_solomon.png', NULL, NULL, NULL, 'Cuaba Solomon', NULL, NULL, 'index, follow', 1, 0, 1490092939, 99, NULL),
(68, 1, 8, 'cuaba-tradicionales', 1, 1950, 58, 0, 9, 42, 1, 0, 0, 55, 'Cuaba Tradicionales', 'cuaba_tradicionales.png', NULL, NULL, NULL, 'Cuaba Tradicionales', NULL, NULL, 'index, follow', 1, 0, 1490093048, 99, NULL),
(69, 1, 4, 'romeo-y-julieta-belicosos', 1, 650, 13, 0, 21, 52, 2, 0, 0, 55, 'Romeo Y Julieta Belicosos', 'romeo_y_julieta_belicosos.png', NULL, NULL, NULL, 'Romeo Y Julieta Belicosos', NULL, NULL, 'index, follow', 1, 0, 1490093438, 99, NULL),
(70, 1, 4, 'romeo-y-julieta-belvederes', 1, 1750, 74, 0, 12, 39, 2, 0, 0, 55, 'Romeo Y Julieta Belvederes', 'romeo_y_julieta_belvederes.png', NULL, NULL, NULL, 'Romeo Y Julieta Belvederes', NULL, NULL, 'index, follow', 1, 0, 1490093760, 99, NULL),
(71, 1, 4, 'romeo-y-julieta-cazadores', 1, 1650, 45, 0, 34, 43, 2, 0, 0, 55, 'Romeo Y Julieta Cazadores', 'romeo_y_julieta_cazadores.png', NULL, NULL, NULL, 'Romeo Y Julieta Cazadores', NULL, NULL, 'index, follow', 1, 0, 1490093851, 99, NULL),
(72, 1, 4, 'romeo-y-julieta-cedros-de-luxe-1', 1, 3000, 50, 0, 35, 42, 2, 0, 0, 55, 'Romeo Y Julieta Cedros De Luxe No.1', 'romeo_y_julieta_cedros_de_luxe_1.png', NULL, NULL, NULL, 'Romeo Y Julieta Cedros De Luxe No.1', NULL, NULL, 'index, follow', 1, 0, 1490094198, 99, NULL),
(73, 1, 4, 'romeo-y-julieta-cedros-de-luxe-2', 1, 2650, 52, 0, 23, 42, 2, 0, 0, 55, 'Romeo Y Julieta Cedros De Luxe No.2', 'romeo_y_julieta_cedros_de_luxe_2.png', NULL, NULL, NULL, 'Romeo Y Julieta Cedros De Luxe No.2', NULL, NULL, 'index, follow', 1, 0, 1490094364, 99, NULL),
(74, 1, 4, 'romeo-y-julieta-cedros-de-luxe-3', 1, 1850, 56, 0, 15, 42, 2, 0, 0, 55, 'Romeo Y Julieta Cedros De Luxe No.3', 'romeo_y_julieta_cedros_de_luxe_3.png', NULL, NULL, NULL, 'Romeo Y Julieta Cedros De Luxe No.3', NULL, NULL, 'index, follow', 1, 0, 1490094513, 99, NULL),
(75, 1, 4, 'romeo-y-julieta-churchills', 1, 2000, 33, 0, 38, 47, 2, 0, 0, 55, 'Romeo Y Julieta Churchills', 'romeo_y_julieta_churchills.png', NULL, NULL, NULL, 'Romeo Y Julieta Churchills', NULL, NULL, 'index, follow', 1, 0, 1490094660, 99, NULL),
(76, 1, 4, 'romeo-y-julieta-churchills-tubo', 1, 3100, 33, 0, 38, 47, 2, 0, 0, 55, 'Romeo Y Julieta Churchills Tubo', 'romeo_y_julieta_churchills_tubo.png', NULL, NULL, NULL, 'Romeo Y Julieta Churchills Tubo', NULL, NULL, 'index, follow', 1, 0, 1490094842, 99, NULL),
(77, 1, 4, 'romeo-y-julieta-coronitas-en-cedro', 1, 1500, 66, 0, 15, 40, 2, 0, 0, 55, 'Romeo Y Julieta Coronitas En Cedro', 'romeo_y_julieta_coronitas_en_cedro.png', NULL, NULL, NULL, 'Romeo Y Julieta Coronitas En Cedro', NULL, NULL, 'index, follow', 1, 0, 1490095164, 99, NULL),
(78, 1, 4, 'romeo-y-julieta-exhibicion-4', 1, 1400, 31, 0, 14, 48, 2, 0, 0, 55, 'Romeo Y Julieta Exhibicion No.4', 'romeo_y_julieta_exhibicion_4.png', NULL, NULL, NULL, 'Romeo Y Julieta Exhibicion No.4', NULL, NULL, 'index, follow', 1, 0, 1490095349, 99, NULL),
(79, 1, 4, 'romeo-y-julieta-julieta', 1, 1200, 87, 0, 9, 33, 2, 0, 0, 55, 'Romeo Y Julieta Julieta', 'romeo_y_julieta_julieta.png', NULL, NULL, NULL, 'Romeo Y Julieta Julieta', NULL, NULL, 'index, follow', 1, 0, 1490095692, 99, NULL),
(80, 1, 4, 'romeo-y-julieta-mille-fleurs', 1, 990, 57, 0, 15, 42, 2, 0, 0, 55, 'Romeo Y Julieta Mille Fleurs', 'romeo_y_julieta_mille_fleurs.png', NULL, NULL, NULL, 'Romeo Y Julieta Mille Fleurs', NULL, NULL, 'index, follow', 1, 0, 1490095913, 99, NULL),
(81, 1, 4, 'romeo-y-julieta-petit-churchills', 1, 1600, 27, 0, 3, 50, 2, 0, 0, 55, 'Romeo Y Julieta Petit Churchills', 'romeo_y_julieta_petit_churchills.png', NULL, NULL, NULL, 'Romeo Y Julieta Petit Churchills', NULL, NULL, 'index, follow', 1, 0, 1490096068, 99, NULL),
(82, 1, 4, 'romeo-y-julieta-petit-coronas', 1, 750, 56, 0, 15, 42, 2, 0, 0, 55, 'Romeo Y Julieta Petit Coronas', 'romeo_y_julieta_petit_coronas.png', NULL, NULL, NULL, 'Romeo Y Julieta Petit Coronas', NULL, NULL, 'index, follow', 1, 0, 1490096188, 99, NULL),
(83, 1, 4, 'romeo-y-julieta-petit-julietas', 1, 850, 89, 0, 1, 30, 2, 0, 0, 55, 'Romeo Y Julieta Petit Julietas', 'romeo_y_julieta_petit_julietas.png', NULL, NULL, NULL, 'Romeo Y Julieta Petit Julietas', NULL, NULL, 'index, follow', 1, 0, 1490096305, 99, NULL),
(84, 1, 4, 'romeo-y-julieta-regalias-de-londres', 1, 900, 69, 0, 7, 40, 2, 0, 0, 55, 'Romeo Y Julieta Regalias De Londres', 'romeo_y_julieta_regalias_de_londres.png', NULL, NULL, NULL, 'Romeo Y Julieta Regalias De Londres', NULL, NULL, 'index, follow', 1, 0, 1490096410, 99, NULL),
(85, 1, 4, 'romeo-y-julieta-romeo-1', 1, 1650, 62, 0, 21, 40, 2, 0, 0, 55, 'Romeo Y Julieta Romeo No.1', 'romeo_y_julieta_romeo_1.png', NULL, NULL, NULL, 'Romeo Y Julieta Romeo No.1', NULL, NULL, 'index, follow', 1, 0, 1490096515, 99, NULL),
(86, 1, 4, 'romeo-y-julieta-romeo-2', 1, 1700, 57, 0, 15, 42, 2, 0, 0, 55, 'Romeo Y Julieta Romeo No.2', 'romeo_y_julieta_romeo_2.png', NULL, NULL, NULL, 'Romeo Y Julieta Romeo No.2', NULL, NULL, 'index, follow', 1, 0, 1490097088, 99, NULL),
(87, 1, 4, 'romeo-y-julieta-romeo-3', 1, 1750, 69, 0, 7, 40, 2, 0, 0, 55, 'Romeo Y Julieta Romeo No.3', 'romeo_y_julieta_romeo_3.png', NULL, NULL, NULL, 'Romeo Y Julieta Romeo No.3', NULL, NULL, 'index, follow', 1, 0, 1490097780, 99, NULL),
(88, 1, 4, 'romeo-y-julieta-short-churchills', 1, 2200, 25, 0, 11, 50, 2, 0, 0, 55, 'Romeo Y Julieta Short Churchills', 'romeo_y_julieta_short_churchills.png', NULL, NULL, NULL, 'Romeo Y Julieta Short Churchills', NULL, NULL, 'index, follow', 1, 0, 1490097949, 99, NULL),
(89, 1, 4, 'romeo-y-julieta-short-churchills-tubo', 1, 2500, 25, 0, 11, 50, 2, 0, 0, 55, 'Romeo Y Julieta Short Churchills Tubo', 'romeo_y_julieta_short_churchills_tubo.png', NULL, NULL, NULL, 'Romeo Y Julieta Short Churchills Tubo', NULL, NULL, 'index, follow', 1, 0, 1490098051, 99, NULL),
(90, 1, 4, 'romeo-y-julieta-sports-largos', 1, 1350, 82, 0, 7, 35, 2, 0, 0, 55, 'Romeo Y Julieta Sports Largos', 'romeo_y_julieta_sports_largos.png', NULL, NULL, NULL, 'Romeo Y Julieta Sports Largos', NULL, NULL, 'index, follow', 1, 0, 1490098168, 99, NULL),
(91, 1, 4, 'romeo-y-julieta-wide-churchills', 1, 2100, 3, 0, 16, 55, 2, 0, 0, 55, 'Romeo Y Julieta Wide Churchills', 'romeo_y_julieta_wide_churchills.png', NULL, NULL, NULL, 'Romeo Y Julieta Wide Churchills', NULL, NULL, 'index, follow', 1, 0, 1490098271, 99, NULL),
(92, 1, 4, 'romeo-y-julieta-wide-churchills-gran-reserva-cosecha-2009', 1, 3400, 3, 1, 16, 55, 2, 0, 0, 55, 'Romeo Y Julieta Wide Churchills Gran Reserva Cosecha 2009', 'romeo_y_julieta_wide_churchills_gran_reserva_cosecha_2009.png', NULL, NULL, NULL, 'Romeo Y Julieta Wide Churchills Gran Reserva Cosecha 2009', NULL, NULL, 'index, follow', 1, 0, 1490098444, 99, NULL),
(93, 1, 4, 'romeo-y-julieta-piramides-anejados', 1, 1550, 11, 0, 30, 52, 2, 0, 0, 55, 'Romeo Y Julieta Piramides Anejados', 'romeo_y_julieta_piramides_anejados.png', NULL, NULL, NULL, 'Romeo Y Julieta Piramides Anejados', NULL, NULL, 'index, follow', 1, 0, 1490098588, 99, NULL),
(94, 1, 3, 'partagas-8-9-8', 1, 600, 44, 0, 37, 43, 0, 0, 0, 55, 'Partagas 8-9-8', 'partagas_8_9_8.png', NULL, NULL, NULL, 'Partagas 8-9-8', NULL, NULL, 'index, follow', 1, 0, 1490098879, 99, NULL),
(95, 1, 3, 'partagas-aristocrats', 1, 1000, 66, 0, 15, 40, 0, 0, 0, 55, 'Partagas Aristocrats', 'partagas_aristocrats.png', NULL, NULL, NULL, 'Partagas Aristocrats', NULL, NULL, 'index, follow', 1, 0, 1490099012, 99, NULL),
(96, 1, 3, 'partagas-coronas-junior', 1, 1400, 69, 0, 7, 40, 0, 0, 0, 55, 'Partagas Coronas Junior', 'partagas_coronas_junior.png', NULL, NULL, NULL, 'Partagas Coronas Junior', NULL, NULL, 'index, follow', 1, 0, 1490099161, 99, NULL),
(97, 1, 3, 'partagas-coronas-senior', 1, 1800, 55, 0, 17, 42, 0, 0, 0, 55, 'Partagas Coronas Senior', 'partagas_coronas_senior.png', NULL, NULL, NULL, 'Partagas Coronas Senior', NULL, NULL, 'index, follow', 1, 0, 1490099273, 99, NULL),
(98, 1, 3, 'partagas-habaneros', 1, 1850, 74, 0, 12, 39, 0, 0, 0, 55, 'Partagas Habaneros', 'partagas_hananeros.png', NULL, NULL, NULL, 'Partagas Habaneros', NULL, NULL, 'index, follow', 1, 0, 1490099373, 99, NULL),
(99, 1, 3, 'partagas-lusitanias', 1, 2000, 29, 0, 41, 49, 0, 0, 0, 55, 'Partagas Lusitanias', 'partagas_lusitanias.png', NULL, NULL, NULL, 'Partagas Lusitanias', NULL, NULL, 'index, follow', 1, 0, 1490099464, 99, NULL),
(100, 1, 3, 'partagas-mille-fleurs', 1, 1650, 57, 0, 15, 42, 0, 0, 0, 55, 'Partagas Mille Fleurs', 'partagas_mille_fleurs.png', NULL, NULL, NULL, 'Partagas Mille Fleurs', NULL, NULL, 'index, follow', 1, 0, 1490099551, 99, NULL),
(101, 1, 3, 'partagas-de-luxe', 1, 2800, 62, 0, 21, 40, 0, 0, 0, 55, 'Partagas De Luxe', 'partagas_de_luxe.png', NULL, NULL, NULL, 'Partagas De Luxe', NULL, NULL, 'index, follow', 1, 0, 1490099655, 99, NULL),
(102, 1, 3, 'partagas-petit-coronas-especiales', 1, 1780, 55, 0, 17, 42, 0, 0, 0, 55, 'Partagas Petit Coronas Especiales', 'partagas_petit_coronas_especiales.png', NULL, NULL, NULL, 'Partagas Petit Coronas Especiales', NULL, NULL, 'index, follow', 1, 0, 1490099753, 99, NULL),
(103, 1, 3, 'partagas-presidentes', 1, 1300, 34, 0, 31, 47, 0, 0, 0, 55, 'Partagas Presidentes', 'partagas_presidentes.png', NULL, NULL, NULL, 'Partagas Presidentes', NULL, NULL, 'index, follow', 1, 0, 1490099864, 99, NULL),
(104, 1, 3, 'partagas-serie-d-4', 1, 1900, 25, 0, 11, 50, 0, 0, 0, 55, 'Partagas Serie D No.4', 'partagas_serie_d_4.png', NULL, NULL, NULL, 'Partagas Serie D No.4', NULL, NULL, 'index, follow', 1, 0, 1490099987, 99, NULL),
(105, 1, 3, 'partagas-serie-d-4-tubo', 1, 2400, 25, 0, 11, 50, 0, 0, 0, 55, 'Partagas Serie D No.4 Tubo', 'partagas_serie_d_4_tubo.png', NULL, NULL, NULL, 'Partagas Serie D No.4 Tubo', NULL, NULL, 'index, follow', 1, 0, 1490100103, 99, NULL),
(106, 1, 3, 'partagas-serie-d-5', 1, 1900, 26, 0, 5, 50, 0, 0, 0, 55, 'Partagas Serie D No.5', 'partagas_serie_d_5.png', NULL, NULL, NULL, 'Partagas Serie D No.5', NULL, NULL, 'index, follow', 1, 0, 1490100189, 99, NULL),
(107, 1, 3, 'partagas-serie-d-6', 1, 1950, 28, 0, 0, 50, 0, 0, 0, 55, 'Partagas Serie D No.6', 'partagas_serie_d_6.png', NULL, NULL, NULL, 'Partagas Serie D No.6', NULL, NULL, 'index, follow', 1, 0, 1490100395, 99, NULL),
(108, 1, 3, 'partagas-serie-e-2', 1, 1400, 8, 0, 21, 54, 0, 0, 0, 55, 'Partagas Serie E No.2', 'partagas_serie_e_2.png', NULL, NULL, NULL, 'Partagas Serie E No.2', NULL, NULL, 'index, follow', 1, 0, 1490100510, 99, NULL),
(109, 1, 3, 'partagas-serie-p-2', 1, 2050, 11, 0, 30, 52, 0, 0, 0, 55, 'Partagas Serie P No.2', 'partagas_serie_p_2.png', NULL, NULL, NULL, 'Partagas Serie P No.2', NULL, NULL, 'index, follow', 1, 0, 1490100590, 99, NULL),
(110, 1, 3, 'partagas-serie-p-2-tubo', 1, 1850, 11, 0, 30, 52, 0, 0, 0, 55, 'Partagas Serie P No.2 Tubo', 'partagas_serie_p_2_tubo.png', NULL, NULL, NULL, 'Partagas Serie P No.2 Tubo', NULL, NULL, 'index, follow', 1, 0, 1490100715, 99, NULL),
(111, 1, 3, 'partagas-shorts', 1, 800, 59, 0, 5, 42, 0, 0, 0, 55, 'Partagas Shorts', 'partagas_shorts.png', NULL, NULL, NULL, 'Partagas Shorts', NULL, NULL, 'index, follow', 1, 0, 1490100798, 99, NULL),
(112, 1, 3, 'partagas-super-partagas', 1, 1000, 62, 0, 21, 40, 0, 0, 0, 55, 'Partagas Super Partagas', 'partagas_super_partagas.png', NULL, NULL, NULL, 'Partagas Super Partagas', NULL, NULL, 'index, follow', 1, 0, 1490100883, 99, NULL),
(113, 1, 3, 'partagas-seleccion-privada-2014', 1, 2600, 21, 3, 33, 50, 0, 0, 0, 55, 'Partagas Seleccion Privada - 2014', 'partagas_seleccion_privada_2014.png', NULL, NULL, NULL, 'Partagas Seleccion Privada - 2014', NULL, NULL, 'index, follow', 1, 0, 1490101002, 99, NULL),
(114, 1, 3, 'partagas-serie-d-3', 1, 1200, 37, 0, 24, 46, 0, 0, 0, 55, 'Partagas Serie D No.3', 'partagas_serie_d_3.png', NULL, NULL, NULL, 'Partagas Serie D No.3', NULL, NULL, 'index, follow', 1, 0, 1490101105, 99, NULL),
(115, 1, 3, 'partagas-culebras-cdh', 1, 2000, 73, 4, 43, 39, 0, 0, 0, 55, 'Partagas Culebras (CDH)', 'partagas_culebras_cdh.png', NULL, NULL, NULL, 'Partagas Culebras (CDH)', NULL, NULL, 'index, follow', 1, 0, 1490101217, 99, NULL),
(116, 1, 3, 'partagas-solomones-cdh', 1, 2100, 0, 4, 39, 57, 0, 0, 0, 55, 'Partagas Solomones (CDH)', 'partagas_solomones_cdh.png', NULL, NULL, NULL, 'Partagas Solomones (CDH)', NULL, NULL, 'index, follow', 1, 0, 1490101505, 99, NULL),
(117, 1, 16, 'diplomaticos-diplomaticos-2', 1, 1600, 11, 0, 30, 52, 1, 0, 0, 55, 'Diplomaticos Diplomaticos No.2', 'diplomaticos_diplomaticos_2.png', NULL, NULL, NULL, 'Diplomaticos Diplomaticos No.2', NULL, NULL, 'index, follow', 1, 0, 1490103019, 99, NULL),
(118, 1, 17, 'el-rey-del-mundo-choix-supreme', 1, 1600, 31, 0, 14, 48, 3, 0, 0, 55, 'El Rey Del Mundo Choix Supreme', 'el_rey_del_mundo_choix_supreme.png', NULL, NULL, NULL, 'El Rey Del Mundo Choix Supreme', NULL, NULL, 'index, follow', 1, 0, 1490103313, 99, NULL),
(119, 1, 17, 'el-rey-del-mundo-demi-tasse', 1, 1000, 89, 0, 1, 30, 3, 0, 0, 55, 'El Rey Del Mundo Demi Tasse', 'el_rey_del_mundo_demmi_tasse.png', NULL, NULL, NULL, 'El Rey Del Mundo Demi Tasse', NULL, NULL, 'index, follow', 1, 0, 1490103455, 99, NULL),
(120, 1, 19, 'flor-de-cano-petit-coronas', 1, 1200, 68, 0, 10, 40, 2, 0, 0, 55, 'Flor De Cano Petit Coronas', 'flor_de_cano_petit_coronas.png', NULL, NULL, NULL, 'Flor De Cano Petit Coronas', NULL, NULL, 'index, follow', 1, 0, 1490106908, 99, NULL),
(121, 1, 19, 'flor-de-cano-selectos', 1, 1100, 60, 0, 27, 41, 2, 0, 0, 55, 'Flor De Cano Selectos', 'flor_de_cano_selectos.png', NULL, NULL, NULL, 'Flor De Cano Selectos', NULL, NULL, 'index, follow', 1, 0, 1490107032, 99, NULL),
(122, 1, 19, 'flor-de-cano-siboney-2014', 1, 1700, 59, 5, 5, 42, 2, 0, 0, 55, 'Flor De Cano Siboney - 2014', 'flor_de_cano_siboney_2014.png', NULL, NULL, NULL, 'Flor De Cano Siboney - 2014', NULL, NULL, 'index, follow', 1, 0, 1490107282, 99, NULL),
(123, 1, 12, 'fonseca-cosacos', 1, 1650, 53, 0, 19, 42, 4, 0, 0, 55, 'Fonseca Cosacos', 'fonseca_cosacos.png', NULL, NULL, NULL, 'Fonseca Cosacos', NULL, NULL, 'index, follow', 1, 0, 1490107455, 99, NULL),
(124, 1, 12, 'fonseca-delicias', 1, 1650, 68, 0, 10, 40, 4, 0, 0, 55, 'Fonseca Delicias', 'fonseca_delicias.png', NULL, NULL, NULL, 'Fonseca Delicias', NULL, NULL, 'index, follow', 1, 0, 1490107560, 99, NULL),
(125, 1, 12, 'fonseca-fonseca-1', 1, 1600, 45, 0, 34, 43, 4, 0, 0, 55, 'Fonseca Fonseca No.1', 'fonseca_fonseca_1.png', NULL, NULL, NULL, 'Fonseca Fonseca No.1', NULL, NULL, 'index, follow', 1, 0, 1490107683, 99, NULL),
(126, 1, 12, 'fonseca-kdt-cadetes', 1, 1500, 81, 0, 6, 36, 4, 0, 0, 55, 'Fonseca KDT Cadetes', 'fonseca_kdt_cadetes.png', NULL, NULL, NULL, 'Fonseca KDT Cadetes', NULL, NULL, 'index, follow', 1, 0, 1490107811, 99, NULL),
(127, 1, 18, 'juan-lopez-seleccion-1', 1, 1750, 37, 0, 24, 46, 1, 0, 0, 55, 'Juan Lopez Seleccion No.1', 'juan_lopez_seleccion_1.png', NULL, NULL, NULL, 'Juan Lopez Seleccion No.1', NULL, NULL, 'index, follow', 1, 0, 1490108073, 99, NULL),
(128, 1, 18, 'juan-lopez-seleccion-2', 1, 1650, 25, 0, 11, 50, 1, 0, 0, 55, 'Juan Lopez Seleccion No.2', 'juan_lopez_seleccion_2.png', NULL, NULL, NULL, 'Juan Lopez Seleccion No.2', NULL, NULL, 'index, follow', 1, 0, 1490108226, 99, NULL),
(129, 1, 20, 'la-gloria-cubana-medaille-dor-4', 1, 1400, 88, 0, 28, 32, 2, 0, 0, 55, 'La Gloria Cubana Medaille d\\''Or No.4', 'la_gloria_cubana_medaille_dor_4.png', NULL, NULL, NULL, 'La Gloria Cubana Medaille d\\''Or No.4', NULL, NULL, 'index, follow', 1, 0, 1490108701, 99, NULL),
(130, 1, 21, 'por-larranaga-montecarlos', 1, 1000, 85, 0, 32, 33, 3, 0, 0, 55, 'Por Larranaga Montecarlos', 'por_larranaga_montecarlos.png', NULL, NULL, NULL, 'Por Larranaga Montecarlos', NULL, NULL, 'index, follow', 1, 0, 1490111106, 99, NULL),
(131, 1, 21, 'por-larranaga-panetelas', 1, 1000, 79, 0, 14, 37, 3, 0, 0, 55, 'Por Larranaga Panetelas', 'por_larranaga_panetelas.png', NULL, NULL, NULL, 'Por Larranaga Panetelas', NULL, NULL, 'index, follow', 1, 0, 1490111233, 99, NULL),
(132, 1, 21, 'por-larranaga-petit-coronas', 1, 1100, 56, 0, 15, 42, 3, 0, 0, 55, 'Por Larranaga Petit Coronas', 'por_larranaga_petit_coronas.png', NULL, NULL, NULL, 'Por Larranaga Petit Coronas', NULL, NULL, 'index, follow', 1, 0, 1490111419, 99, NULL),
(133, 1, 22, 'quai-d-orsay-coronas-claro', 1, 1000, 52, 0, 23, 42, 4, 0, 0, 55, 'Quai d\\''Orsay Coronas Claro', 'quai_dorsay_coronas_claro.png', NULL, NULL, NULL, 'Quai d\\''Orsay Coronas Claro', NULL, NULL, 'index, follow', 1, 0, 1490111714, 99, NULL),
(134, 1, 22, 'quai-d-orsay-belicoso-royal-2013', 1, 1350, 16, 5, 12, 52, 4, 0, 0, 55, 'Quai d\\''Orsay Belicoso Royal - 2013', 'quai_dorsay_belicoso_royal_2013.png', NULL, NULL, NULL, 'Quai d\\''Orsay Belicoso Royal - 2013', NULL, NULL, 'index, follow', 1, 0, 1490111901, 99, NULL),
(135, 1, 11, 'bolivar-belicosos-finos', 1, 1200, 13, 0, 21, 52, 0, 0, 0, 55, 'Bolivar Belicosos Finos', 'bolivar_belicosos_finos.png', NULL, NULL, NULL, 'Bolivar Belicosos Finos', NULL, NULL, 'index, follow', 1, 0, 1490113543, 99, NULL),
(136, 1, 11, 'bolivar-tubos-1', 1, 1600, 52, 0, 23, 42, 0, 0, 0, 55, 'Bolivar Tubos No.1', 'bolivar_tubos_1.png', NULL, NULL, NULL, 'Bolivar Tubos No.1', NULL, NULL, 'index, follow', 1, 0, 1490113828, 99, NULL),
(137, 1, 11, 'bolivar-tubos-2', 1, 1700, 56, 0, 15, 42, 0, 0, 0, 55, 'Bolivar Tubos No.2', 'bolivar_tubos_2.png', NULL, NULL, NULL, 'Bolivar Tubos No.2', NULL, NULL, 'index, follow', 1, 0, 1490114069, 99, NULL),
(138, 1, 11, 'bolivar-tubos-3', 1, 1600, 84, 0, 12, 34, 0, 0, 0, 55, 'Bolivar Tubos No.3', 'bolivar_tubos_3.png', NULL, NULL, NULL, 'Bolivar Tubos No.3', NULL, NULL, 'index, follow', 1, 0, 1490114254, 99, NULL),
(139, 1, 11, 'bolivar-coronas-gigantes', 1, 1800, 33, 0, 38, 47, 0, 0, 0, 55, 'Bolivar Coronas Gigantes', 'bolivar_coronas_gigantes.png', NULL, NULL, NULL, 'Bolivar Coronas Gigantes', NULL, NULL, 'index, follow', 1, 0, 1490114344, 99, NULL),
(140, 1, 11, 'bolivar-coronas-junior', 1, 1900, 59, 0, 5, 42, 0, 0, 0, 55, 'Bolivar Coronas Junior', 'bolivar_coronas_junior.png', NULL, NULL, NULL, 'Bolivar Coronas Junior', NULL, NULL, 'index, follow', 1, 0, 1490114450, 99, NULL),
(141, 1, 11, 'bolivar-petit-coronas', 1, 2000, 56, 0, 15, 42, 0, 0, 0, 55, 'Bolivar Petit Coronas', 'bolivar_petit_coronas.png', NULL, NULL, NULL, 'Bolivar Petit Coronas', NULL, NULL, 'index, follow', 1, 0, 1490114530, 99, NULL),
(142, 1, 11, 'bolivar-royal-coronas', 1, 2300, 25, 0, 11, 50, 0, 0, 0, 55, 'Bolivar Royal Coronas', 'bolivar_royal_coronas.png', NULL, NULL, NULL, 'Bolivar Royal Coronas', NULL, NULL, 'index, follow', 1, 0, 1490114613, 99, NULL),
(143, 1, 11, 'bolivar-royal-coronas-tubo', 1, 2600, 25, 0, 11, 50, 0, 0, 0, 55, 'Bolivar Royal Coronas Tubo', 'bolivar_royal_coronas_tubo.png', NULL, NULL, NULL, 'Bolivar Royal Coronas Tubo', NULL, NULL, 'index, follow', 1, 0, 1490114718, 99, NULL),
(144, 1, 11, 'bolivar-super-coronas', 1, 2500, 30, 0, 21, 48, 0, 0, 0, 55, 'Bolivar Super Coronas', 'bolivar_super_coronas.png', NULL, NULL, NULL, 'Bolivar Super Coronas', NULL, NULL, 'index, follow', 1, 0, 1490114803, 99, NULL),
(145, 1, 11, 'bolivar-libertador', 1, 3200, 4, 4, 44, 54, 0, 0, 0, 55, 'Bolivar Libertador', 'bolivar_libertador.png', NULL, NULL, NULL, 'Bolivar Libertador', NULL, NULL, 'index, follow', 1, 0, 1490114931, 99, NULL),
(146, 1, 5, 'hoyo-de-monterrey-coronations', 1, 1000, 57, 0, 15, 42, 4, 0, 0, 55, 'Hoyo De Monterrey Coronations', 'hoyo_de_monterrey_coronations.png', NULL, NULL, NULL, 'Hoyo De Monterrey Coronations', NULL, NULL, 'index, follow', 1, 0, 1490115576, 99, NULL),
(147, 1, 5, 'hoyo-de-monterrey-double-coronas', 1, 2800, 29, 0, 41, 49, 4, 0, 0, 55, 'Hoyo De Monterrey Double Coronas', 'hoyo_de_monterrey_double_coronas.png', NULL, NULL, NULL, 'Hoyo De Monterrey Double Coronas', NULL, NULL, 'index, follow', 1, 0, 1490115753, 99, NULL),
(148, 1, 5, 'hoyo-de-monterrey-epicure-especial', 1, 2300, 23, 0, 22, 50, 4, 0, 0, 55, 'Hoyo De Monterrey Epicure Especial', 'hoyo_de_monterrey_epicure_especial.png', NULL, NULL, NULL, 'Hoyo De Monterrey Epicure Especial', NULL, NULL, 'index, follow', 1, 0, 1490115915, 99, NULL),
(149, 1, 5, 'hoyo-de-monterrey-epicure-especial-tubo', 1, 2600, 23, 0, 22, 50, 4, 0, 0, 55, 'Hoyo De Monterrey Epicure Especial Tubo', 'hoyo_de_monterrey_epicure_especial_tubo.png', NULL, NULL, NULL, 'Hoyo De Monterrey Epicure Especial Tubo', NULL, NULL, 'index, follow', 1, 0, 1490116006, 99, NULL),
(150, 1, 5, 'hoyo-de-monterrey-epicure-1', 1, 2700, 37, 0, 24, 46, 4, 0, 0, 55, 'Hoyo De Monterrey Epicure No.1', 'hoyo_de_monterrey_epicure_1.png', NULL, NULL, NULL, 'Hoyo De Monterrey Epicure No.1', NULL, NULL, 'index, follow', 1, 0, 1490116108, 99, NULL),
(151, 1, 5, 'hoyo-de-monterrey-epicure-1-tubo', 1, 400, 37, 0, 24, 46, 4, 0, 0, 55, 'Hoyo De Monterrey Epicure No.1 Tubo', 'hoyo_de_monterrey_epicure_1_tubo.png', NULL, NULL, NULL, 'Hoyo De Monterrey Epicure No.1 Tubo', NULL, NULL, 'index, follow', 1, 0, 1490116220, 99, NULL),
(152, 1, 5, 'hoyo-de-monterrey-epicure-2', 1, 350, 25, 0, 11, 50, 4, 0, 0, 55, 'Hoyo De Monterrey Epicure No.2', 'hoyo_de_monterrey_epicure_2.png', NULL, NULL, NULL, 'Hoyo De Monterrey Epicure No.2', NULL, NULL, 'index, follow', 1, 0, 1490116311, 99, NULL),
(153, 1, 5, 'hoyo-de-monterrey-le-hoyo-de-san-juan', 1, 2000, 6, 0, 27, 54, 4, 0, 0, 55, 'Hoyo De Monterrey Le Hoyo De San Juan', 'hoyo_de_monterrey_le_hoyo_de_san_juan.png', NULL, NULL, NULL, 'Hoyo De Monterrey Le Hoyo De San Juan', NULL, NULL, 'index, follow', 1, 0, 1490116426, 99, NULL),
(154, 1, 5, 'hoyo-de-monterrey-le-hoyo-du-depute', 1, 1000, 78, 0, 5, 38, 4, 0, 0, 55, 'Hoyo De Monterrey Le Hoyo Du Depute', 'hoyo_de_monterrey_le_hoyo_du_depute.png', NULL, NULL, NULL, 'Hoyo De Monterrey Le Hoyo Du Depute', NULL, NULL, 'index, follow', 1, 0, 1490116570, 99, NULL),
(155, 1, 5, 'hoyo-de-monterrey-le-hoyo-du-maire', 1, 500, 89, 0, 5, 30, 4, 0, 0, 55, 'Hoyo De Monterrey Le Hoyo Du Maire', 'hoyo_de_monterrey_le_hoyo_du_maire.png', NULL, NULL, NULL, 'Hoyo De Monterrey Le Hoyo Du Maire', NULL, NULL, 'index, follow', 1, 0, 1490116703, 99, NULL),
(156, 1, 5, 'hoyo-de-monterrey-le-hoyo-du-prince', 1, 150, 65, 0, 16, 40, 4, 0, 0, 55, 'Hoyo De Monterrey Le Hoyo Du Prince', 'hoyo_de_monterrey_le_hoyo_du_prince.png', NULL, NULL, NULL, 'Hoyo De Monterrey Le Hoyo Du Prince', NULL, NULL, 'index, follow', 1, 0, 1490116796, 99, NULL),
(157, 1, 5, 'hoyo-de-monterrey-palmas-extra', 1, 200, 62, 0, 21, 40, 4, 0, 0, 55, 'Hoyo De Monterrey Palmas Extra', 'hoyo_de_monterrey_palmas_extra.png', NULL, NULL, NULL, 'Hoyo De Monterrey Palmas Extra', NULL, NULL, 'index, follow', 1, 0, 1490116899, 99, NULL),
(158, 1, 5, 'hoyo-de-monterrey-petit-robustos', 1, 250, 27, 0, 3, 50, 4, 0, 0, 55, 'Hoyo De Monterrey Petit Robustos', 'hoyo_de_monterrey_petit_robustos.png', NULL, NULL, NULL, 'Hoyo De Monterrey Petit Robustos', NULL, NULL, 'index, follow', 1, 0, 1490117020, 99, NULL),
(159, 1, 5, 'hoyo-de-monterrey-epicure-especial-le', 1, 420, 23, 3, 22, 50, 4, 0, 0, 55, 'Hoyo De Monterrey Epicure Especial', 'hoyo_de_monterrey_epicure_especial_le.png', NULL, NULL, NULL, 'Hoyo De Monterrey Epicure Especial', NULL, NULL, 'index, follow', 1, 0, 1490117161, 99, NULL),
(160, 1, 5, 'hoyo-de-monterrey-particulares', 1, 3300, 32, 3, 42, 47, 4, 0, 0, 55, 'Hoyo De Monterrey Particulares', 'hoyo_de_monterrey_particulares.png', NULL, NULL, NULL, 'Hoyo De Monterrey Particulares', NULL, NULL, 'index, follow', 1, 0, 1490117318, 99, NULL),
(161, 1, 6, 'h-upmann-connossieur-1', 1, 320, 31, 0, 14, 48, 3, 0, 0, 55, 'H. Upmann Connossieur No.1', 'hupmann_connossieur_1.png', NULL, NULL, NULL, 'H. Upmann Connossieur No.1', NULL, NULL, 'index, follow', 1, 0, 1490117594, 99, NULL),
(162, 1, 6, 'h-upmann-coronas-junior', 1, 160, 81, 0, 6, 36, 3, 0, 0, 55, 'H. Upmann Coronas Junior', 'hupmann_coronas_junior.png', NULL, NULL, NULL, 'H. Upmann Coronas Junior', NULL, NULL, 'index, follow', 1, 0, 1490117723, 99, NULL),
(163, 1, 6, 'h-upmann-coronas-major', 1, 450, 55, 0, 17, 42, 3, 0, 0, 55, 'H. Upmann Coronas Major', 'hupmann_coronas_major.png', NULL, NULL, NULL, 'H. Upmann Coronas Major', NULL, NULL, 'index, follow', 1, 0, 1490117882, 99, NULL),
(164, 1, 6, 'h-upmann-coronas-minor', 1, 620, 69, 0, 7, 40, 3, 0, 0, 55, 'H. Upmann Coronas Minor', 'hupmann_coronas_minor.png', NULL, NULL, NULL, 'H. Upmann Coronas Minor', NULL, NULL, 'index, follow', 1, 0, 1490118001, 99, NULL),
(165, 1, 6, 'h-upmann-epicures', 1, 200, 83, 0, 5, 35, 3, 0, 0, 55, 'H. Upmann Epicures', 'hupmann_epicures.png', NULL, NULL, NULL, 'H. Upmann Epicures', NULL, NULL, 'index, follow', 1, 0, 1490118065, 99, NULL),
(166, 1, 6, 'h-upmann-half-corona', 1, 560, 43, 0, 0, 44, 3, 0, 0, 55, 'H. Upmann Half Corona', 'hupmann_half_corona.png', NULL, NULL, NULL, 'H. Upmann Half Corona', NULL, NULL, 'index, follow', 1, 0, 1490118192, 99, NULL),
(167, 1, 6, 'h-upmann-magnum-46', 1, 800, 37, 0, 24, 46, 3, 0, 0, 55, 'H. Upmann Magnum 46', 'hupmann_magnum_46.png', NULL, NULL, NULL, 'H. Upmann Magnum 46', NULL, NULL, 'index, follow', 1, 0, 1490118305, 99, NULL),
(168, 1, 6, 'h-upmann-magnum-46-tubo', 1, 840, 37, 0, 24, 46, 3, 0, 0, 55, 'H. Upmann Magnum 46 Tubo', 'hupmann_magnum_46_tubo.png', NULL, NULL, NULL, 'H. Upmann Magnum 46 Tubo', NULL, NULL, 'index, follow', 1, 0, 1490118489, 99, NULL),
(169, 1, 6, 'h-upmann-magnum-50', 1, 760, 21, 0, 33, 50, 3, 0, 0, 55, 'H. Upmann Magnum 50', 'hupmann_magnum_50.png', NULL, NULL, NULL, 'H. Upmann Magnum 50', NULL, NULL, 'index, follow', 1, 0, 1490118576, 99, NULL),
(170, 1, 6, 'h-upmann-magnum-50-tubo', 1, 960, 21, 0, 33, 50, 3, 0, 0, 55, 'H. Upmann Magnum 50 Tubo', 'hupmann_magnum_50_tubo.png', NULL, NULL, NULL, 'H. Upmann Magnum 50 Tubo', NULL, NULL, 'index, follow', 1, 0, 1490118668, 99, NULL),
(171, 1, 6, 'h-upmann-majestic', 1, 500, 62, 0, 21, 40, 3, 0, 0, 55, 'H. Upmann Majestic', 'hupmann_majestic.png', NULL, NULL, NULL, 'H. Upmann Majestic', NULL, NULL, 'index, follow', 1, 0, 1490118763, 99, NULL),
(172, 1, 6, 'h-upmann-petit-coronas', 1, 300, 56, 0, 15, 42, 3, 0, 0, 55, 'H. Upmann Petit Coronas', 'hupmann_petit_coronas.png', NULL, NULL, NULL, 'H. Upmann Petit Coronas', NULL, NULL, 'index, follow', 1, 0, 1490118840, 99, NULL),
(173, 1, 6, 'h-upmann-regalias', 1, 180, 57, 0, 15, 42, 3, 0, 0, 55, 'H. Upmann Regalias', 'hupmann_regalias.png', NULL, NULL, NULL, 'H. Upmann Regalias', NULL, NULL, 'index, follow', 1, 0, 1490118920, 99, NULL),
(174, 1, 6, 'h-upmann-sir-winston', 1, 660, 33, 0, 38, 47, 3, 0, 0, 55, 'H. Upmann Sir Winston', 'hupmann_sir_winston.png', NULL, NULL, NULL, 'H. Upmann Sir Winston', NULL, NULL, 'index, follow', 1, 0, 1490119010, 99, NULL),
(175, 1, 6, 'h-upmann-upmann-2', 1, 360, 11, 0, 30, 52, 3, 0, 0, 55, 'H. Upmann Upmann No.2', 'hupmann_upmann_2.png', NULL, NULL, NULL, 'H. Upmann Upmann No.2', NULL, NULL, 'index, follow', 1, 0, 1490119136, 99, NULL),
(176, 1, 6, 'h-upmann-upmann-2-reserva-cosecha-2010', 1, 1400, 11, 2, 30, 52, 3, 0, 0, 55, 'H. Upmann Upmann No.2 Reserva Cosecha (2010)', 'hupmann_upmann_2_reserva_cosecha_2010.png', NULL, NULL, NULL, 'H. Upmann Upmann No.2 Reserva Cosecha (2010)', NULL, NULL, 'index, follow', 1, 0, 1490119274, 99, NULL),
(177, 1, 6, 'h-upmann-magnum-50-le', 1, 750, 21, 0, 33, 50, 3, 0, 0, 55, 'H. Upmann Magnum 50', 'hupmann_magnum_50_le.png', NULL, NULL, NULL, 'H. Upmann Magnum 50', NULL, NULL, 'index, follow', 1, 0, 1490119390, 99, NULL),
(178, 1, 7, 'jose-l-piedra-brevas', 1, 230, 54, 0, 18, 42, 1, 0, 0, 55, 'Jose L. Piedra Brevas', 'jose_l_piedra_brevas.png', NULL, NULL, NULL, 'Jose L. Piedra Brevas', NULL, NULL, 'index, follow', 1, 0, 1490119967, 99, NULL),
(179, 1, 7, 'jose-l-piedra-cazadores', 1, 260, 46, 0, 28, 43, 1, 0, 0, 55, 'Jose L. Piedra Cazadores', 'jose_l_piedra_cazadores.png', NULL, NULL, NULL, 'Jose L. Piedra Cazadores', NULL, NULL, 'index, follow', 1, 0, 1490120090, 99, NULL),
(180, 1, 7, 'jose-l-piedra-conservas', 1, 320, 41, 0, 21, 44, 1, 0, 0, 55, 'Jose L. Piedra Conservas', 'jose_l_piedra_conservas.png', NULL, NULL, NULL, 'Jose L. Piedra Conservas', NULL, NULL, 'index, follow', 1, 0, 1490120173, 99, NULL),
(181, 1, 7, 'jose-l-piedra-cremas', 1, 440, 64, 0, 20, 40, 1, 0, 0, 55, 'Jose L. Piedra Cremas', 'jose_l_piedra_cremas.png', NULL, NULL, NULL, 'Jose L. Piedra Cremas', NULL, NULL, 'index, follow', 1, 0, 1490120251, 99, NULL),
(182, 1, 7, 'jose-l-piedra-petit-cazadores', 1, 260, 48, 0, 4, 43, 1, 0, 0, 55, 'Jose L. Piedra Petit Cazadores', 'jose_l_piedra_petit_cazadores.png', NULL, NULL, NULL, 'Jose L. Piedra Petit Cazadores', NULL, NULL, 'index, follow', 1, 0, 1490120341, 99, NULL),
(183, 1, 7, 'jose-l-piedra-petit-cetros', 1, 520, 77, 0, 14, 38, 1, 0, 0, 55, 'Jose L. Piedra Petit Cetros', 'jose_l_piedra_petit_cetros.png', NULL, NULL, NULL, 'Jose L. Piedra Petit Cetros', NULL, NULL, 'index, follow', 1, 0, 1490120443, 99, NULL),
(184, 1, 13, 'punch-coronations', 1, 3600, 57, 0, 15, 42, 2, 0, 0, 55, 'Punch Coronations', 'punch_coronations.png', NULL, NULL, NULL, 'Punch Coronations', NULL, NULL, 'index, follow', 1, 0, 1490135376, 99, NULL),
(185, 1, 13, 'punch-double-coronas', 1, 3000, 29, 0, 41, 49, 2, 0, 0, 55, 'Punch Double Coronas', 'punch_double_coronas.png', NULL, NULL, NULL, 'Punch Double Coronas', NULL, NULL, 'index, follow', 1, 0, 1490135595, 99, NULL),
(186, 1, 13, 'punch-petit-coronations', 1, 2600, 69, 0, 7, 40, 2, 0, 0, 55, 'Punch Petit Coronations', 'punch_petit_coronations.png', NULL, NULL, NULL, 'Punch Petit Coronations', NULL, NULL, 'index, follow', 1, 0, 1490135708, 99, NULL),
(187, 1, 13, 'punch-punch', 1, 1500, 37, 0, 24, 46, 2, 0, 0, 55, 'Punch Punch', 'punch_punch.png', NULL, NULL, NULL, 'Punch Punch', NULL, NULL, 'index, follow', 1, 0, 1490135801, 99, NULL),
(188, 1, 13, 'punch-punch-tubo', 1, 3500, 37, 0, 24, 46, 2, 0, 0, 55, 'Punch Punch Tubo', 'punch_punch_tubo.png', NULL, NULL, NULL, 'Punch Punch Tubo', NULL, NULL, 'index, follow', 1, 0, 1490135897, 99, NULL),
(189, 1, 13, 'punch-royal-coronations', 1, 2000, 47, 0, 26, 43, 2, 0, 0, 55, 'Punch Royal Coronations', 'punch_royal_coronations.png', NULL, NULL, NULL, 'Punch Royal Coronations', NULL, NULL, 'index, follow', 1, 0, 1490136023, 99, NULL),
(190, 1, 13, 'punch-robustos', 1, 2200, 25, 5, 11, 50, 2, 0, 0, 55, 'Punch Robustos', 'punch_robustos.png', NULL, NULL, NULL, 'Punch Robustos', NULL, NULL, 'index, follow', 1, 0, 1490136108, 99, NULL),
(191, 1, 14, 'quintero-brevas', 1, 700, 63, 0, 21, 40, 2, 0, 0, 55, 'Quintero Brevas', 'quintero_brevas.png', NULL, NULL, NULL, 'Quintero Brevas', NULL, NULL, 'index, follow', 1, 0, 1490136350, 99, NULL),
(192, 1, 14, 'quintero-londres-extra', 1, 1500, 67, 0, 10, 40, 2, 0, 0, 55, 'Quintero Londres Extra', 'quintero_londres_extra.png', NULL, NULL, NULL, 'Quintero Londres Extra', NULL, NULL, 'index, follow', 1, 0, 1490136451, 99, NULL),
(193, 1, 14, 'quintero-nacionales', 1, 1600, 63, 0, 21, 40, 2, 0, 0, 55, 'Quintero Nacionales', 'quintero_nacionales.png', NULL, NULL, NULL, 'Quintero Nacionales', NULL, NULL, 'index, follow', 1, 0, 1490136541, 99, NULL),
(194, 1, 14, 'quintero-panetelas', 1, 1200, 79, 0, 14, 37, 2, 0, 0, 55, 'Quintero Panetelas', 'quintero_panetelas.png', NULL, NULL, NULL, 'Quintero Panetelas', NULL, NULL, 'index, follow', 1, 0, 1490136624, 99, NULL),
(195, 1, 14, 'quintero-petit-quintero', 1, 1100, 48, 0, 4, 43, 2, 0, 0, 55, 'Quintero Petit Quintero', 'quintero_petit_quintero.png', NULL, NULL, NULL, 'Quintero Petit Quintero', NULL, NULL, 'index, follow', 1, 0, 1490136740, 99, NULL),
(196, 1, 14, 'quintero-tubulares', 1, 1300, 54, 0, 18, 42, 2, 0, 0, 55, 'Quintero Tubulares', 'quintero_tubulares.png', NULL, NULL, NULL, 'Quintero Tubulares', NULL, NULL, 'index, follow', 1, 0, 1490136847, 99, NULL),
(197, 1, 23, 'rafael-gonzalez-panetelas-extra', 1, 1300, 80, 0, 14, 37, 4, 0, 0, 55, 'Rafael Gonzalez Panetelas Extra', 'rafael_gonzalez_panetelas_extra.png', NULL, NULL, NULL, 'Rafael Gonzalez', NULL, NULL, 'index, follow', 1, 0, 1490137083, 99, NULL),
(198, 1, 23, 'rafael-gonzalez-perlas', 1, 1000, 0, 0, 3, 40, 4, 0, 0, 55, 'Rafael Gonzalez Perlas', 'rafael_gonzalez_perlas.png', NULL, NULL, NULL, 'Rafael Gonzalez Perlas', NULL, NULL, 'index, follow', 1, 0, 1490137248, 99, NULL),
(199, 1, 23, 'rafael-gonzalez-petit-coronas', 1, 1400, 56, 0, 15, 42, 4, 0, 0, 55, 'Rafael Gonzalez Petit Coronas', 'rafael_gonzalez_petit_coronas.png', NULL, NULL, NULL, 'Rafael Gonzalez Petit Coronas', NULL, NULL, 'index, follow', 1, 0, 1490137369, 99, NULL),
(200, 1, 24, 'ramon-allones-allones-specially-selected', 1, 1200, 25, 0, 11, 50, 0, 0, 0, 55, 'Ramon Allones Allones Specially Selected', 'ramon_allones_allones_specially_selected.png', NULL, NULL, NULL, 'Ramon Allones Allones Specially Selected', NULL, NULL, 'index, follow', 1, 0, 1490137657, 99, NULL),
(201, 1, 24, 'ramon-allones-gigantes', 1, 1800, 29, 0, 41, 49, 0, 0, 0, 55, 'Ramon Allones Gigantes', 'ramon_allones_gigantes.png', NULL, NULL, NULL, 'Ramon Allones Gigantes', NULL, NULL, 'index, follow', 1, 0, 1490137860, 99, NULL),
(202, 1, 24, 'ramon-allones-small-club-coronas', 1, 1700, 59, 0, 5, 42, 0, 0, 0, 55, 'Ramon Allones Small Club Coronas', 'ramon_allones_small_club_coronas.png', NULL, NULL, NULL, 'Ramon Allones Small Club Coronas', NULL, NULL, 'index, follow', 1, 0, 1490137939, 99, NULL),
(203, 1, 24, 'ramon-allones-club-allones-2015', 1, 2500, 35, 3, 19, 47, 0, 0, 0, 55, 'Ramon Allones Club Allones - 2015', 'ramon_allones_club_allones_2015.png', NULL, NULL, NULL, 'Ramon Allones Club Allones - 2015', NULL, NULL, 'index, follow', 1, 0, 1490138087, 99, NULL),
(204, 1, 24, 'ramon-allones-short-perfectos-2014', 1, 1750, 24, 5, 14, 50, 0, 0, 0, 55, 'Ramon Allones Short Perfectos - 2014', 'ramon_allones_short_perfectos_2014.png', NULL, NULL, NULL, 'Ramon Allones Short Perfectos - 2014', NULL, NULL, 'index, follow', 1, 0, 1490138220, 99, NULL),
(205, 1, 25, 'saint-luis-rey-regios', 1, 900, 31, 0, 14, 48, 0, 0, 0, 55, 'Saint Luis Rey Regios', 'saint_lois_rey_Regios.png', NULL, NULL, NULL, 'Saint Luis Rey Regios', NULL, NULL, 'index, follow', 1, 0, 1490169725, 99, NULL),
(206, 1, 9, 'san-cristobal-de-la-habana-el-principe', 1, 1000, 59, 0, 5, 42, 3, 0, 0, 55, 'San Cristobal De La Habana El Principe', 'san_cristobal_de_la_habana_el_principe.png', NULL, NULL, NULL, 'San Cristobal De La Habana El Principe', NULL, NULL, 'index, follow', 1, 0, 1490169906, 99, NULL),
(207, 1, 9, 'san-cristobal-de-la-habana-la-fuerza', 1, 1200, 23, 0, 22, 50, 3, 0, 0, 55, 'San Cristobal De La Habana La Fuerza', 'san_cristobal_de_la_habana_la_fuerza.png', NULL, NULL, NULL, 'San Cristobal De La Habana La Fuerza', NULL, NULL, 'index, follow', 1, 0, 1490170019, 99, NULL),
(208, 1, 9, 'san-cristobal-de-la-habana-la-punta', 1, 1600, 13, 0, 21, 52, 3, 0, 0, 55, 'San Cristobal De La Habana La Punta', 'san_cristobal_de_la_habana_la_punta.png', NULL, NULL, NULL, 'San Cristobal De La Habana La Punta', NULL, NULL, 'index, follow', 1, 0, 1490170110, 99, NULL),
(209, 1, 26, 'sancho-panza-belicosos', 1, 750, 13, 0, 21, 52, 2, 0, 0, 55, 'Sancho Panza Belicosos', 'sancho_panza_belicosos.png', NULL, NULL, NULL, 'Sancho Panza Belicosos', NULL, NULL, 'index, follow', 1, 0, 1490170234, 99, NULL),
(210, 1, 26, 'sancho-panza-non-plus', 1, 800, 56, 0, 15, 42, 2, 0, 0, 55, 'Sancho Panza Non Plus', 'sancho_panza_non_plus.png', NULL, NULL, NULL, 'Sancho Panza Non Plus', NULL, NULL, 'index, follow', 1, 0, 1490170356, 99, NULL),
(211, 1, 10, 'trinidad-coloniales', 1, 1400, 42, 0, 17, 44, 2, 0, 0, 55, 'Trinidad Coloniales', 'trinidad_coloniales.png', NULL, NULL, NULL, 'Trinidad Coloniales', NULL, NULL, 'index, follow', 1, 0, 1490170509, 99, NULL),
(212, 1, 10, 'trinidad-fundadores', 1, 2100, 61, 0, 40, 40, 2, 0, 0, 55, 'Trinidad Fundadores', 'trinidad_fundadores.png', NULL, NULL, NULL, 'Trinidad Fundadores', NULL, NULL, 'index, follow', 1, 0, 1490170587, 99, NULL),
(213, 1, 10, 'trinidad-reyes', 1, 950, 70, 0, 5, 40, 2, 0, 0, 55, 'Trinidad Reyes', 'trinidad_reyes.png', NULL, NULL, NULL, 'Trinidad Reyes', NULL, NULL, 'index, follow', 1, 0, 1490170653, 99, NULL),
(214, 1, 10, 'trinidad-vigia', 1, 850, 9, 0, 5, 54, 2, 0, 0, 55, 'Trinidad Vigia', 'trinidad_vigia.png', NULL, NULL, NULL, 'Trinidad Vigia', NULL, NULL, 'index, follow', 1, 0, 1490170719, 99, NULL);
INSERT INTO `products` (`id`, `category_id`, `brand_id`, `slug`, `is_cigar_product`, `price`, `vitola`, `line`, `length`, `cepo`, `intensity`, `flavour`, `wrapper`, `country_made`, `name`, `img`, `images`, `intro`, `html`, `meta_title`, `meta_description`, `meta_keywords`, `meta_robots`, `published`, `featured`, `lifted_up_at`, `quantity`, `used_by_user_id`) VALUES
(215, 1, 15, 'vegas-robaina-don-alejandro', 1, 2800, 29, 0, 41, 49, 1, 0, 0, 55, 'Vegas Robaina Don Alejandro', 'vegas_robaina_don_alejandro.png', NULL, NULL, NULL, 'Vegas Robaina Don Alejandro', NULL, NULL, 'index, follow', 1, 0, 1490170851, 99, NULL),
(216, 1, 15, 'vegas-robaina-famosos', 1, 1400, 31, 0, 14, 48, 1, 0, 0, 55, 'Vegas Robaina Famosos', 'vegas_robaina_famosos.png', NULL, NULL, NULL, 'Vegas Robaina Famosos', NULL, NULL, 'index, follow', 1, 0, 1490170929, 99, NULL),
(217, 1, 15, 'vegas-robaina-unicos', 1, 1600, 11, 0, 30, 52, 1, 0, 0, 55, 'Vegas Robaina Unicos', 'vegas_robaina_unicos.png', NULL, NULL, NULL, 'Vegas Robaina Unicos', NULL, NULL, 'index, follow', 1, 0, 1490170994, 99, NULL),
(218, 1, 27, 'vegueros-entretiempos', 1, 1600, 20, 0, 5, 52, 0, 0, 0, 55, 'Vegueros Entretiempos', 'vegueros_entretiempos.png', NULL, NULL, NULL, 'Vegueros Entretiempos', NULL, NULL, 'index, follow', 1, 0, 1490171113, 99, NULL),
(219, 1, 27, 'vegueros-mananitas', 1, 1200, 40, 0, 1, 46, 0, 0, 0, 55, 'Vegueros Mananitas', 'vegueros_mananitas.png', NULL, NULL, NULL, 'Vegueros Mananitas', NULL, NULL, 'index, follow', 1, 0, 1490171186, 99, NULL),
(220, 1, 27, 'vegueros-tapados', 1, 1000, 39, 0, 9, 46, 0, 0, 0, 55, 'Vegueros Tapados', 'vegueros_tapados.png', NULL, NULL, NULL, 'Vegueros Tapados', NULL, NULL, 'index, follow', 1, 0, 1490171254, 99, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(64) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `intro` text,
  `img` varchar(255) DEFAULT NULL,
  `body` text,
  `after_body` text,
  `before_body` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_robots` varchar(18) DEFAULT 'index, follow',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lifted_up_at` int(10) unsigned DEFAULT NULL,
  `used_by_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `sections`
--

INSERT INTO `sections` (`id`, `slug`, `title`, `intro`, `img`, `body`, `after_body`, `before_body`, `meta_title`, `meta_description`, `meta_keywords`, `meta_robots`, `published`, `lifted_up_at`, `used_by_user_id`) VALUES
(1, 'uncategorized', 'Separate Site Pages', '', NULL, '', '', '', '', '', '', 'index, follow', 1, 1487596579, NULL),
(2, 'news', 'Новости', 'Ad nisi. nihil. rerum et tenetur aut et qui. velit, voluptatibus. voluptatem id, dolore. quaerat sapiente. suscipit tempora officia aliquid officiis velit, veritatis.', 'Gala_destacada_cr.jpg', '<h1>Новости сайта</h1>\r\n\r\n<p>Ad nisi. nihil. rerum et tenetur aut et qui. velit, voluptatibus. voluptatem id, dolore. quaerat sapiente. suscipit tempora officia aliquid officiis velit, veritatis tempora provident, aut nobis. et in. reiciendis. amet, in aperiam ab qui earum consequatur nobis architecto aut minima et nostrum nihil quod autem ut amet, delectus, aut velit, et vero. molestiae magni impedit, magnam. vitae iste totam.</p>', '', '', 'Новости', '', '', 'index, follow', 1, 1487600270, NULL),
(3, 'events', 'События', 'Incidunt, nesciunt, dolorum velit earum qui cupiditate esse, illo omnis. rerum illum, est unde. sapiente cumque nisi ex quae neque perspiciatis.', 'events_cr.jpg', '<h1>События</h1>\r\n\r\n<p>Incidunt, nesciunt, dolorum velit earum qui cupiditate esse, illo omnis. rerum illum, est unde. sapiente cumque nisi ex quae neque perspiciatis et voluptate aliquid suscipit qui impedit, impedit, rerum eligendi suscipit cumque asperiores vel omnis.</p>', '', '', 'События', '', '', 'index, follow', 1, 1487600238, NULL),
(4, 'history', 'История', 'Sapiente cumque nisi ex quae neque perspiciatis et voluptate aliquid suscipit qui impedit, impedit, rerum eligendi suscipit cumque asperiores vel omnis.', 'b462f7fce0ce2c8d4e4fea7b64015451.jpg', '<h1>История в людях</h1>\r\n\r\n<p>Incidunt, nesciunt, dolorum velit earum qui cupiditate esse, illo omnis. rerum illum, est unde. sapiente cumque nisi ex quae neque perspiciatis et voluptate aliquid suscipit qui impedit, impedit, rerum eligendi suscipit cumque asperiores vel omnis.</p>', '', '', 'История', '', '', 'index, follow', 1, 1487600254, NULL),
(5, 'culture', 'Культура', 'Autem est, expedita assumenda. esse, quam quibusdam sit adipisci commodi maxime quisquam maxime unde.', '431045f9e2d1adc409b4294d47dcacb7.jpg', '<h1>Культура</h1>\r\n\r\n<p>Autem est, expedita assumenda. esse, quam quibusdam sit adipisci commodi maxime quisquam maxime unde. a laudantium, esse, ab cumque autem quam officiis voluptatem ex ipsa, ea possimus, nisi incidunt, dolor ea doloremque explicabo saepe et aut odio assumenda enim. ad autem sunt. sunt. quisquam ut error omnis consequuntur. quas ipsa. quia iste. sapiente provident. blanditiis consectetur voluptas suscipit explicabo. aut.</p>', '', '', 'Культура', '', '', 'index, follow', 0, 1487597083, NULL),
(6, 'tasting', 'Дегустации', 'Consequuntur culpa. velit consectetur, harum quae. fugiat. doloremque qui adipisci obcaecati. ab similique sunt, nostrum omnis. dolorum sunt. porro. exercitationem impedit.', 'wine_cr.jpg', '<h1>Дегустации</h1>\r\n\r\n<p>Consequuntur culpa. velit consectetur, harum quae. fugiat. doloremque qui adipisci obcaecati. ab similique sunt, nostrum omnis. dolorum sunt. porro. exercitationem impedit, est, veritatis. molestias animi, nihil eaque possimus, commodi ut et molestiae esse, unde. impedit. eos doloremque deserunt optio, beatae ut velit nihil a quisquam. cumque aut in non laboriosam, corrupti, est ea rerum quo quod quidem enim rem quod.</p>\r\n\r\n<p>Ad nisi. nihil. rerum et tenetur aut et qui. velit, voluptatibus. voluptatem id, dolore. quaerat sapiente. suscipit tempora officia aliquid officiis velit, veritatis tempora provident, aut nobis. et in. reiciendis. amet, in aperiam ab qui earum consequatur nobis architecto aut minima et nostrum nihil quod autem ut amet, delectus, aut velit, et vero. molestiae magni impedit, magnam. vitae iste totam.</p>\r\n\r\n<p>Autem est, expedita assumenda. esse, quam quibusdam sit adipisci commodi maxime quisquam maxime unde. a laudantium, esse, ab cumque autem quam officiis voluptatem ex ipsa, ea possimus, nisi incidunt, dolor ea doloremque explicabo saepe et aut odio assumenda enim. ad autem sunt. sunt. quisquam ut error omnis consequuntur. quas ipsa. quia iste. sapiente provident. blanditiis consectetur voluptas suscipit explicabo. aut.</p>', NULL, NULL, 'Дегустации', '', '', 'index, follow', 1, 1487600257, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `temp_password` varchar(255) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `phone` varchar(24) DEFAULT NULL,
  `address` text,
  `is_admin` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `reg_confirmed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `reg_confirm_code` varchar(64) DEFAULT NULL,
  `reg_time` int(10) unsigned NOT NULL,
  `rst_pwd_time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `temp_password`, `name`, `phone`, `address`, `is_admin`, `reg_confirmed`, `reg_confirm_code`, `reg_time`, `rst_pwd_time`) VALUES
(1, 'superadmin@site.com', '$2y$10$Wdg.trXv0SIHDuKsIPET6uBtVYSgF75Paafsw0XVNXb.JnteWFHfm', NULL, 'James Wood', '1004657926', 'NY, 7th Avenue, ap. 634', 1, 1, NULL, 1487595703, NULL),
(2, 'admin@site.com', '$2y$10$B3dd4Jb9jDCAs4PHQ4GT5elNypRP7zylPj3es64di9IG6ZsfKXRPi', NULL, NULL, NULL, NULL, 1, 1, NULL, 1487595728, NULL),
(3, 'customer@site.com', '$2y$10$nLHVUxjB86WxCO5Z8AVR7erRJmAgRs/IUWYk5A5SYhXsvHpsxPBXK', NULL, NULL, NULL, NULL, 0, 1, NULL, 1487595758, NULL),
(4, 'user@site.com', '$2y$10$eu.pL8zaZWPNURlQc8BVv.SgdDuNcwXz.GDo56nXBigqAxlTY9Zvq', NULL, NULL, NULL, NULL, 0, 1, NULL, 1487596191, NULL),
(5, 'admin3@site.com', '$2y$10$sHHtEkxYdHpEvr1TCMB7gOOfUxVwWGWZfPtgzFr/G4nAhdU3UGl8i', NULL, NULL, NULL, NULL, 0, 1, NULL, 1488558529, 1488561208),
(6, 'gaga@site.com', '$2y$10$BaX3zQqafyD4Dj77Q6avUutl/KBhGLr/IqbBNj1XwIAKVIsDCwn.K', NULL, NULL, NULL, NULL, 0, 1, NULL, 1489842126, 1489842586),
(7, 'sale4bucks@gmail.com', '$2y$10$mPOLPlc4KlgKqqrxR4KjPOf/jyiZHCxTTBODJnf6bnapw8iYxBcpO', NULL, NULL, NULL, NULL, 0, 1, NULL, 1498601343, NULL),
(8, 'le_marmot@mail.ru', '$2y$10$BFNYhCmRoppAhpjV9Jjd.eo5QujcYJjs8/hwaoSc0QrL6D.tFdLHi', NULL, NULL, NULL, NULL, 0, 0, '546fa4a7538b150f3a286de0d5116bc334171c1db40932dc49c58a2b9395f8f9', 1498726290, NULL);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `fk_articles_sections` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_orders_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `packs`
--
ALTER TABLE `packs`
  ADD CONSTRAINT `fk_boxes_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_brands` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_products_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
