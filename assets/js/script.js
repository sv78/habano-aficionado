'use strict';
//JQUERY IS REQUIRED

var abstractCssClassToAlignVertical = 'sheduler-aligned';
var t, popwindow_is_open, sheduler, menucaller, mw_cigar, currentScrollTop, elementsToAlignVertical, rows, action_hashes = 0;
var menu_is_open = false;
var filter_is_open = false;
var window_resized_in_period = false;
var sitewrapper;
var body_default_bg_color = '#000000';
var menu_window_bg_color = '#e9e8df';
var ss_picker_binded_element_id = undefined;
var spinner_default_value = 1;
var spinner_default_max_value = 100;

var filter_block_count_result_numer = 0;
var params_to_continue_showing_ajax_results;
var current_filter_request_number = 1;
var filter_gets_new_request = true;
var av_cookie_name = 'ha_av';

window.onload = init;

function init() {

  sitewrapper = document.getElementById('sitewrapper');

  if (document.getElementById('ha_banner')) {
    $("#ha_banner").owlCarousel({
      items: 1,
      margin: 20,
      //mouseDrag: true,
      //touchDrag: true,
      autoplaySpeed: 2000,
      dragEndSpeed: 500,
      navSpeed: 2000,
      //autoHeight: true,
      //autoWidth: false,
      loop: true,
      autoplay: true,
      autoplayTimeout: 15000,
      autoplayHoverPause: false,
      center: true,
      //navText: ['Вперед', 'Назад'],
      //dots: true,
      //dotsEach: true,
      //nav: true
    });
    //document.getElementById('ha_banner_container').style.zIndex = -1; /drag not working
    document.getElementById('ha_banner_container').style.zIndex = 0; // drag working
  }

  if (document.getElementById('ha_cats')) {
    $("#ha_cats").owlCarousel({
      responsiveBaseElement: $("#ha_cats_container"),
      responsive: {
	/*
	 960: {
	 items: 5
	 },*/
	740: {
	  items: 4
	},
	640: {
	  items: 3
	},
	540: {
	  items: 2
	},
	200: {
	  items: 1
	}
      },
      margin: 40,
      mouseDrag: true,
      touchDrag: true,
      autoplaySpeed: 2000,
      dragEndSpeed: 500,
      navSpeed: 500,
      //autoHeight: true,
      autoWidth: false,
      loop: true,
      autoplay: true,
      autoplayTimeout: 12000,
      autoplayHoverPause: false,
      center: false,
      //lazyLoad: false,
      //navElement: ''
      //dots: true,
      //dotsEach: true,
      navContainer: '#cats_nav',
      navText: ['<div class="cat_nav_next"></div>', '<div class="cat_nav_prev"></div>'],
      nav: true
    });
    document.getElementById('ha_cats_container').style.zIndex = 0; // drag working
  }




  //document.getElementById("menuwindowcloser").href = action_hashes.menuclosed;

  window.addEventListener('click', windowClickListener, false);
  //window.addEventListener('keyup', windowKeyUpListener, false);
  window.addEventListener("scroll", scrollListener);
  window.addEventListener("resize", windowResizeListener);
  //window.addEventListener("hashchange", hashChanged);

  //hashChanged();

  elementsToAlignVertical = document.body.getElementsByClassName(abstractCssClassToAlignVertical);

  alignVerticalBySheduler();

  sheduler = setInterval(shedulerHandler, 500);

  toggleScrollDependantButtons();

  setTimeout(removePreloader, 1000);

  //spinners_init('spinner', 1, 1, 100, 1); // class name or id with # , default value, min value, max value, iteration value

  spinners_init('abstract-product-spinner', false, 1, 100, 1); // class name or id with # , default value, min value, max value, iteration value
  spinners_init('abstract-pack-spinner', false, 1, 100, 1); // class name or id with # , default value, min value, max value, iteration value
  spinners_init('abstract-cart-item-spinner', false, 1, 100, 1);

  if (document.getElementById('ha_filter_super_search_system')) {
    initSS(document.getElementById('ha_filter_super_search_system'));
  }
  
  verify_age();

}



function set_body_bg_color(color_value) {
  document.body.style.backgroundColor = color_value;
}

function removePreloader() {
  if (document.getElementById("preloader")) {
    document.body.removeChild(document.getElementById("preloader"));
    document.body.removeChild(document.getElementById("preloader-script-post"));
    document.body.removeChild(document.getElementById("preloader-script-pre"));
    document.head.removeChild(document.getElementById("preloader-style"));
    flashScreen();
  }
}

function windowResizeListener(e) {
  window_resized_in_period = true;
}

function scrollListener(e) {
  toggleScrollDependantButtons();
}

function isScrolled() {
  return  ($(window).scrollTop() > 60) ? true : false;
}

function toggleScrollDependantButtons() {
  isScrolled() ? showScrollDependantButtons() : hideScrollDependantButtons();
}

function showScrollDependantButtons() {
  $("#menucaller").removeClass("menucaller-hidden").addClass("menucaller-visible");
  $("#menucaller_sm").removeClass("menucaller-sm-hidden").addClass("menucaller-sm-visible");
  $("#backtotop").removeClass("backtotop-hidden").addClass("backtotop-visible");
  $("#backtotop_sm").removeClass("backtotop-sm-hidden").addClass("backtotop-sm-visible");
}

function hideScrollDependantButtons() {
  $("#menucaller").removeClass("menucaller-visible").addClass("menucaller-hidden");
  $("#menucaller_sm").removeClass("menucaller-sm_visible").addClass("menucaller-sm-hidden");
  $("#backtotop").removeClass("backtotop-visible").addClass("backtotop-hidden");
  $("#backtotop_sm").removeClass("backtotop-sm-visible").addClass("backtotop-sm-hidden");
}


function alignVerticalBySheduler() {
  var ln = elementsToAlignVertical.length;
  for (var i = 0; i < ln; i++) {
    alignCenterVertical(elementsToAlignVertical[i], elementsToAlignVertical[i].parentNode);
  }
}






function hashChanged(e) {
  if (location.hash == action_hashes.menuopen) {
    currentScrollTop = $(window).scrollTop();
    if (!menu_is_open) {
      openSiteMenu();
    }
  }
  if (location.hash == action_hashes.menuclosed) {
    if (menu_is_open) {
      closeSiteMenu();
    }
  }
}

function shedulerHandler() {
  if (menu_is_open && window_resized_in_period) {
    alignCenterVertical(document.getElementById('mw_content'), document.getElementById('mw_menuwindow'));
    window_resized_in_period = false;
  }
  if (popwindow_is_open) {
    alignPopWindow();
  }
  alignVerticalBySheduler();
}



function windowClickListener(e) {
  if (e.target.id == 'backtotop' || e.target.id == 'backtotop_sm') {
    $(window).scrollTop(0);
  }

  if (e.target.hasAttribute('data-action-type')) {
    if (e.target.getAttribute('data-action-type') === 'flashed-link') {
      flashScreen();
    }
    if (e.target.getAttribute('data-action-type') === 'pop-window-link') {
      if (e.target.getAttribute('data-pop-link-id') !== undefined) {
	openSitePopWindow(e.target.getAttribute('data-pop-link-id'));
      }
    }
    if (e.target.getAttribute('data-action-type') === 'pw-closer') {
      closeSitePopWindow();
    }
    if (e.target.getAttribute('data-action-type') === 'av-confirm') {
      confirmAgeVerification(e.target.getAttribute('data-av-confirm-value'));
    }
    if (e.target.getAttribute('data-action-type') === 'open-site-menu') {
      openSiteMenu();
    }
    if (e.target.getAttribute('data-action-type') === 'close-site-menu') {
      closeSiteMenu();
    }
    if (e.target.getAttribute('data-action-type') === 'submit-filter-block-form') {
      if (document.getElementById('filter_results_holder').hasChildNodes()) {
	$("#filter_results_holder").slideUp(500, function () {
	  $("#filter_results_holder").empty().show();
	  filter_gets_new_request = true;
	  current_filter_request_number = 1;
	  performRequestViaAjax();
	});
      } else {
	filter_gets_new_request = true;
	current_filter_request_number = 1;
	performRequestViaAjax();
      }
    }
    if (e.target.getAttribute('data-action-type') === 'filter-show-further-results') {
      filterShowFurtherResults();
    }
  }
}

function flashScreen() {
  $('#screenflasher').removeClass('hidden').addClass('block');
  setTimeout(function () {
    document.getElementById('screenflasher').style.opacity = 0;
  }, 50);
  setTimeout(function () {
    $('#screenflasher').removeClass('block').addClass('hidden');
    document.getElementById('screenflasher').style.opacity = 1;
  }, 480);
}



/*
 function windowKeyUpListener(e) {
 if (e.keyCode === 27 && menu_is_open) {
 closeSiteMenu();
 }
 if (e.keyCode === 27 && popwindow_is_open) {
 closeSitePopWindow();
 }
 }
 */

function menuKeyEventCloseListener(e) {
  if (e.keyCode === 27 && menu_is_open) {
    closeSiteMenu();
  }
}




/* === Menu Togglers === */

function openSiteMenu() {
  menu_is_open = true;
  currentScrollTop = $(window).scrollTop();
  $('#sitewrapper').removeClass('block').addClass('hidden');
  document.getElementById('mw_content').style.top = $("#mw_content").height() / 3 + "px";
  document.getElementById('mw_content').style.opacity = 0;
  $('#mw_menuwindow').removeClass('hidden').addClass('block');
  setTimeout(function () {
    document.getElementById('mw_cigar').style.opacity = 1;
    document.getElementById('mw_content').style.opacity = 1;
    alignCenterVertical(document.getElementById('mw_content'), document.getElementById('mw_menuwindow'));
    menu_is_open = true;
  }, 50);
  window.addEventListener('keyup', menuKeyEventCloseListener, false);
}
function closeSiteMenu() {
  menu_is_open = false;
  //location.hash = action_hashes.menuclosed;
  document.getElementById('mw_content').style.top = -$("#mw_content").height() / 3 + "px";
  document.getElementById('mw_content').style.opacity = 0;
  document.getElementById('mw_cigar').style.opacity = 0;
  $('#sitewrapper').removeClass('hidden').addClass('block');
  setTimeout(function () {
    $('#mw_menuwindow').removeClass('block').addClass('hidden');
    $(window).scrollTop(currentScrollTop);
    flashScreen();
  }, 400);
  window.removeEventListener('keyup', menuKeyEventCloseListener, false);
}
/* === / Menu Togglers === */


/* === PopWindow Togglers === */
function toggleSitePopWindow(id) {
  popwindow_is_open ? closeSiteMenu(id) : openSiteMenu(id);
}

function getPWFileById(id) {
  var len = SR_POP_WINDOWS.length;
  for (var i = 0; i < len; i++) {
    if (SR_POP_WINDOWS[i].id === id) {
      return SR_POP_WINDOWS[i].file;
    }
  }
  return false;
}

function openSitePopWindow(id) {
  flashScreen();
  var id = parseInt(id);
  var file = getPWFileById(id);
  document.getElementById('pw_content').innerHTML = "";
  $.ajax({
    method: "POST",
    url: "/handlers/popupwindows/" + file,
    data: {uid: decodeURI(Date.now())},
    statusCode: {
      404: function () {
	//console.log('not found…');
	return;
      }
    }
  })
	  .done(function (msg) {
	    document.getElementById('pw_content').innerHTML = msg;
	    $('#pw-popwindow').removeClass('hidden').addClass('block');
	    alignPopWindow();
	    popwindow_is_open = true;
	    if (id === 17) {
	      createMap();
	    }
	  });
}

function closeSitePopWindow() {
  popwindow_is_open = false;
  document.getElementById('pw_inner_wraper').scrollTop = 0;
  document.getElementById('pw_content').innerHTML = "";
  $('#pw-popwindow').removeClass('block').addClass('hidden');
  flashScreen();
}

/* /=== PopWindow Togglers === */

function alignPopWindow() {
  alignCenterVertical(document.getElementById('pw_outer_wraper'), document.getElementById('pw-popwindow'));
}



