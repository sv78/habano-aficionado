'use strict';

// creates intensities from array
function create_filter_window_intensities_list(items_array) {
  items_array.unshift([-1, '--- неважно ---']);
  for (var i = 0; i < items_array.length; i++) {
    var intensity_li = document.createElement('LI');
    intensity_li.innerHTML = items_array[i][1];
    intensity_li.className = 'flt-listed-value';
    intensity_li.setAttribute('data-ha-filter-type', 'intensities');
    intensity_li.setAttribute('data-ha-filter-value', i - 1);
    intensity_li.setAttribute('data-ha-filter-string', items_array[i][1]);
    if (items_array[i][0] == -1) {
      intensity_li.setAttribute('data-ha-filter-string', 'не важно');
    }
    document.getElementById('flt_items_vals_view').appendChild(intensity_li);
  }
}

// creates wrappers from array
function create_filter_window_wrappers_list(items_array) {
  items_array.unshift([-1, '--- неважно ---']);
  for (var i = 0; i < items_array.length; i++) {
    var item_li = document.createElement('LI');
    item_li.innerHTML = items_array[i][1];
    item_li.className = 'flt-listed-value';
    item_li.setAttribute('data-ha-filter-type', 'wrappers');
    item_li.setAttribute('data-ha-filter-value', i - 1);
    item_li.setAttribute('data-ha-filter-string', items_array[i][1]);
    if (items_array[i][0] == -1) {
      item_li.setAttribute('data-ha-filter-string', 'не важно');
    }
    document.getElementById('flt_items_vals_view').appendChild(item_li);
  }
}

// creates flavours from array
function create_filter_window_flavours_list(items_array) {
  items_array.unshift([-1, '--- неважно ---']);
  for (var i = 0; i < items_array.length; i++) {
    var item_li = document.createElement('LI');
    item_li.innerHTML = items_array[i][1];
    item_li.className = 'flt-listed-value';
    item_li.setAttribute('data-ha-filter-type', 'flavours');
    item_li.setAttribute('data-ha-filter-value', i - 1);
    item_li.setAttribute('data-ha-filter-string', items_array[i][1]);
    if (items_array[i][0] == -1) {
      item_li.setAttribute('data-ha-filter-string', 'не важно');
    }
    document.getElementById('flt_items_vals_view').appendChild(item_li);
  }
}


// creates lines from array
function create_filter_window_lines_list(items_array) {
  items_array.unshift([-1, '--- неважно ---']);
  for (var i = 0; i < items_array.length; i++) {
    var item_li = document.createElement('LI');
    item_li.innerHTML = items_array[i][1];
    item_li.className = 'flt-listed-value';
    item_li.setAttribute('data-ha-filter-type', 'lines');
    item_li.setAttribute('data-ha-filter-value', i - 1);
    item_li.setAttribute('data-ha-filter-string', items_array[i][1]);
    if (items_array[i][0] == -1) {
      item_li.setAttribute('data-ha-filter-string', 'не важно');
    }
    document.getElementById('flt_items_vals_view').appendChild(item_li);
  }
}

// creates lengths from array
function create_filter_window_lengths_list(items_array) {
  items_array.unshift([-1, '--- неважно ---']);
  for (var i = 0; i < items_array.length; i++) {
    var item_li = document.createElement('LI');
    item_li.innerHTML = items_array[i][1];
    item_li.className = 'flt-listed-value';
    item_li.setAttribute('data-ha-filter-type', 'lengths');
    item_li.setAttribute('data-ha-filter-value', i - 1);
    item_li.setAttribute('data-ha-filter-string', items_array[i][1]);
    if (items_array[i][0] == -1) {
      item_li.setAttribute('data-ha-filter-string', 'не важно');
    }
    document.getElementById('flt_items_vals_view').appendChild(item_li);
  }
}

// creates countries from array
function create_filter_window_countries_list(items_array) {
  items_array.unshift([-1, '--- неважно ---', '', '']);
  for (var i = 0; i < items_array.length; i++) {
    var item_li = document.createElement('LI');
    item_li.innerHTML = items_array[i][1];
    item_li.className = 'flt-listed-value';
    item_li.setAttribute('data-ha-filter-type', 'countries');
    item_li.setAttribute('data-ha-filter-value', i - 1);
    item_li.setAttribute('data-ha-filter-string', items_array[i][1]);
    if (items_array[i][0] == -1) {
      item_li.setAttribute('data-ha-filter-string', 'не важно');
    }
    document.getElementById('flt_items_vals_view').appendChild(item_li);
  }
}

// creates cepos from array
function create_filter_window_cepos_list(items_array) {
  items_array = items_array.reverse();
  items_array.unshift(-1);

  var cepo_size_q = 1.4;
  var cepo_font_size = 16;
  var cepo_form_and_cell_size_differ = 4;

  var cepo_max_form_size = cepo_size_q * get_array_max_value(items_array);
  var cepo_min_form_size = cepo_size_q * get_array_min_value(items_array);

  var cepo_cell_size = parseInt(cepo_max_form_size + cepo_form_and_cell_size_differ);
  //var cepo_cell_text_frame_height = 60;

  for (var i = 0; i < items_array.length; i++) {
    var cepo_cell = document.createElement('LI');
    cepo_cell.style.width = cepo_cell_size + "px";
    cepo_cell.style.height = cepo_cell_size + "px";
    cepo_cell.className = 'flt-cepo-cell';

    var cepo_form = document.createElement('DIV');
    var cepo_text = document.createElement('DIV');
    cepo_text.innerHTML = Math.round(25.4 / 64 * items_array[i] * 100) / 100 + ' mm';
    cepo_form.className = 'flt-cepo-form';
    cepo_text.className = 'flt-cepo-form-desc';
    if (items_array[i] === -1) {
      cepo_text.innerHTML = '';
      var font_size_differ = 5;
      cepo_form.style.fontSize = cepo_font_size - font_size_differ + "px";
      cepo_form.style.width = cepo_cell_size + "px";
      cepo_form.style.height = cepo_cell_size + "px";
      cepo_form.style.paddingTop = parseInt((cepo_cell_size - cepo_font_size + font_size_differ / 2) / 2) + 'px';
      cepo_form.innerHTML = 'Не важно';
    } else {
      cepo_form.style.fontSize = cepo_font_size + "px";
      cepo_form.style.width = parseInt(cepo_size_q * items_array[i]) + "px";
      cepo_form.style.height = parseInt(cepo_size_q * items_array[i]) + "px";
      cepo_form.style.paddingTop = parseInt((parseInt(cepo_size_q * items_array[i]) - cepo_font_size) / 2) - 1 + 'px';
      cepo_form.innerHTML = items_array[i];
    }

    cepo_form.setAttribute('data-ha-filter-type', 'cepos');
    cepo_form.setAttribute('data-ha-filter-value', items_array[i]);
    cepo_form.setAttribute('data-ha-filter-string', items_array[i]);
    if (items_array[i] == -1) {
      cepo_form.setAttribute('data-ha-filter-string', 'не важно');
    }

    if (items_array[i] > 49) {
      cepo_form.style.backgroundColor = '#a89873';
    } else if (items_array[i] > 39) {
      cepo_form.style.backgroundColor = '#525252';
    } else {
      cepo_form.style.backgroundColor = '#d71e2e';
    }
    if (items_array[i] === -1) {
      cepo_form.style.backgroundColor = '#414a53';
    }

    cepo_cell.appendChild(cepo_form);
    cepo_cell.appendChild(cepo_text);
    document.getElementById('flt_items_vals_view').appendChild(cepo_cell);
  }
}


// creates vitolas from array
function create_filter_window_vitolas_list(items_array) {
  var vitolas_images_path = '/assets/images/vitolas/';
  items_array.unshift([-1, '--- неважно ---', '']);
  for (var i = 0; i < items_array.length; i++) {
    var vitola_li = document.createElement('LI');
    var vitola_img = document.createElement('IMG');
    var vitola_span = document.createElement('SPAN');
    var vtl_desc = items_array[i][3] + ' X ' + items_array[i][4] + ' mm';
    vitola_img.className = 'flt-vitola-img';
    vitola_li.innerHTML = items_array[i][1];
    vitola_li.className = 'flt-listed-value-vitola';
    if (items_array[i][0] === -1) {
      vitola_li.innerHTML = '--- не важно ---';
      vtl_desc = '';
      vitola_img.src = vitolas_images_path + 'no_matter_vitola.png';
      vitola_span.innerHTML = items_array[i][3];
    } else {
      vitola_img.src = vitolas_images_path + items_array[i][2];
    }

    vitola_span.innerHTML = vtl_desc;
    vitola_li.setAttribute('data-ha-filter-type', 'vitolas');
    vitola_li.setAttribute('data-ha-filter-value', i - 1);
    vitola_li.setAttribute('data-ha-filter-string', items_array[i][1]);

    vitola_img.setAttribute('data-ha-filter-type', 'vitolas');
    vitola_img.setAttribute('data-ha-filter-value', i - 1);
    vitola_img.setAttribute('data-ha-filter-string', items_array[i][1]);
    
    vitola_span.setAttribute('data-ha-filter-type', 'vitolas');
    vitola_span.setAttribute('data-ha-filter-value', i - 1);
    vitola_span.setAttribute('data-ha-filter-string', items_array[i][1]);

    if (items_array[i][0] === -1) {
      vitola_li.setAttribute('data-ha-filter-string', 'не важно');
      vitola_img.setAttribute('data-ha-filter-string', 'не важно');
    }

    vitola_li.appendChild(vitola_img);
    vitola_li.appendChild(vitola_span);
    document.getElementById('flt_items_vals_view').appendChild(vitola_li);
  }
}

// creates brands from array
function create_filter_window_brands_list(items_array) {
  var brands_images_path = '/assets/images/brands/';
  items_array.unshift([-1, 'Не важно', 'dummy_brand.jpg']);
  for (var i = 0; i < items_array.length; i++) {
    var brand_li = document.createElement('LI');
    var brand_img = document.createElement('IMG');
    var brand_span = document.createElement('SPAN');
    brand_li.className = 'flt-brand-cell-li';
    brand_img.className = 'flt-brand-img';
    brand_span.className = 'flt-brand-name';
    brand_img.src = brands_images_path + items_array[i][2];
    brand_span.innerHTML = items_array[i][1];
    brand_img.setAttribute('data-ha-filter-type', 'brands');
    brand_img.setAttribute('data-ha-filter-value', items_array[i][0]);
    brand_img.setAttribute('data-ha-filter-string', items_array[i][1]);
    brand_span.setAttribute('data-ha-filter-type', 'brands');
    brand_span.setAttribute('data-ha-filter-value', items_array[i][0]);
    brand_span.setAttribute('data-ha-filter-string', items_array[i][1]);

    if (items_array[i][0] == -1) {
      brand_img.setAttribute('data-ha-filter-string', 'не важно');
      brand_span.setAttribute('data-ha-filter-string', 'не важно');
    }

    brand_li.appendChild(brand_img);
    brand_li.appendChild(brand_span);
    document.getElementById('flt_items_vals_view').appendChild(brand_li);
  }
}

// creates prices from array
function create_filter_window_prices_list(prices_range_array) {
  var price_descreet_step = 500;
  var items_array = create_price_array(prices_range_array, price_descreet_step);
  items_array.unshift(-1);
  for (var i = 0; i < items_array.length; i++) {
    var price_li = document.createElement('LI');
    price_li.innerHTML = "до " + items_array[i] + " руб./шт.";
    price_li.className = 'flt-listed-value';
    if (items_array[i] === -1) {
      price_li.innerHTML = '--- не важно ---';
    }
    price_li.setAttribute('data-ha-filter-type', 'prices');
    price_li.setAttribute('data-ha-filter-value', items_array[i]);
    price_li.setAttribute('data-ha-filter-string', "до " + items_array[i] + " руб./шт.");

    if (items_array[i] == -1) {
      price_li.setAttribute('data-ha-filter-string', 'не важно');
    }

    document.getElementById('flt_items_vals_view').appendChild(price_li);
  }
}



function create_price_array(range, step) {
  var min_val = range[0];
  var max_val = range[1];
  var price_array = [];
  if (min_val <= step) {
    price_array.push(step);
  } else {
    var st = step;
    do {
      st += step;
    } while (st < min_val)
    price_array.push(st);
  }

  do {
    var new_val = price_array[price_array.length - 1] + step;
    if (new_val % step != 0) {
      var new_val = parseInt(new_val / step) * step;
    }
    price_array.push(new_val);
  } while (price_array[price_array.length - 1] <= max_val)

  return price_array;
}