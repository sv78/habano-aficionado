'use strict';

function process_spinner_value(val, default_value) {
  var current_default_value = spinner_default_value;
  if (default_value !== undefined && typeof (default_value) === 'number' && default_value > 0) {
    current_default_value = default_value;
  }
  val = val.replace(/\D/g, "");
  val = parseInt(val);
  if (isNaN(val) || val < 1 || val === false || val === undefined || val === null) {
    val = current_default_value;
  }
  if (val > spinner_default_max_value) {
    val = spinner_default_max_value;
  }
  return val;
}


function spinners_init(class_or_id_str, default_value, min_value, max_value, iteration_value) {
  // Don't use css spinner class name because of css match! Use anything other

  if (class_or_id_str == '' || class_or_id_str == undefined) {
    throw 'Spinner class name or id is empty or undefined!';
    return;
  }
  if (class_or_id_str[0] === '#') {
    var spinners = document.getElementById(class_or_id_str.substr(1, class_or_id_str.length - 1));
  } else {
    var spinners = document.getElementsByClassName(class_or_id_str);
  }

  if (spinners.length === 0) {
    return;
  }

  for (var i = 0, len = spinners.length; i < len; i++) {
    var spinner = spinners[i];
    spinner.setAttribute('data-spinner-number', i);
    if (default_value === false || typeof (default_value) != 'number') {
      if (isNaN(parseInt(spinner.value))) {
	throw "Spinner : Wrong default value parameter";
	return;
      }
      var vl = parseInt(spinner.value);
      spinner.value = vl > max_value ? max_value : vl;
      spinner.value = vl < min_value ? min_value : vl;
    } else {
      spinner.value = default_value;
    }



    spinner.addEventListener('blur', spinnerChangeListener, false)
    spinner.addEventListener('change', spinnerChangeListener, false)
    spinner.addEventListener('keydown', spinnerKeyDownListener, false)

    var spinner_plus = document.createElement('DIV');
    var spinner_minus = document.createElement('DIV');

    spinner_plus.className = 'product-amount-multiplier-btn pam-plus';
    spinner_minus.className = 'product-amount-multiplier-btn pam-minus';

    spinner_minus.setAttribute('data-spinner-minus', i);
    spinner_plus.setAttribute('data-spinner-plus', i);

    spinner.parentNode.insertBefore(spinner_minus, spinner);
    spinner.parentNode.insertBefore(spinner_plus, spinner.nextSibling);

    spinner_minus.addEventListener('click', spinnerClickListener, false)
    spinner_plus.addEventListener('click', spinnerClickListener, false)
  }

  function spinnerKeyDownListener(e) {
    if (e.keyCode === 13 || e.keyCode === 27) {
      e.target.blur();
    }
    /*
     if (e.keyCode != 8 && e.keyCode != 37 && e.keyCode != 39 && /\D/g.test(e.key)) {
     e.preventDefault();
     }*/
  }

  function spinnerChangeListener(e) {
    spinnerChangeListenerAction(e.target);
  }


  function spinnerChangeListenerAction(elm) {
    var val = elm.value.trim();
    /*
     if (/\D/g.test(val)) {
     val = val.replace(/\D/g, "");
     val = val === '' ? default_value : parseInt(val);
     } else {
     console.log(123);
     val = parseInt(val);
     }
     */
    val = process_spinner_value(val, default_value);

    val = val > max_value ? max_value : val;
    val = val < min_value ? min_value : val;
    elm.value = val;
    //console.log(elm.value);
  }

  function spinnerClickListener(e) {

    if (window.getSelection()) {
      window.getSelection().removeAllRanges();
    }

    var spinner_to_act, action_type;
    if (e.target.hasAttribute('data-spinner-minus')) {
      spinner_to_act = getSpinnerByNumber(e.target.getAttribute('data-spinner-minus'));
      action_type = 'minus';
    } else if (e.target.hasAttribute('data-spinner-plus')) {
      spinner_to_act = getSpinnerByNumber(e.target.getAttribute('data-spinner-plus'));
      action_type = 'plus';
    } else {
      return;
    }
    if (isNaN(parseInt(spinner_to_act.value))) {
      var current_spinner_value = default_value;
    } else {
      var current_spinner_value = parseInt(spinner_to_act.value);
    }
    //var new_spinner_value = (action_type == 'plus') ? current_spinner_value + spinner_iteration_value : current_spinner_value - spinner_iteration_value;
    var new_spinner_value;
    if (action_type === 'plus') {
      if (current_spinner_value + iteration_value > max_value) {
	new_spinner_value = current_spinner_value;
      } else {
	new_spinner_value = current_spinner_value + iteration_value;
      }
    } else {
      if (current_spinner_value - iteration_value < min_value) {
	new_spinner_value = current_spinner_value;
      } else {
	new_spinner_value = current_spinner_value - iteration_value;
      }
    }
    spinner_to_act.value = new_spinner_value;
    spinnerChangeListenerAction(spinner_to_act);

    if ("createEvent" in document) {
      var evt = document.createEvent("HTMLEvents");
      evt.initEvent("change", false, true);
      spinner_to_act.dispatchEvent(evt);
    } else
      spinner_to_act.fireEvent("onchange");

  }

  function getSpinnerByNumber(number_as_string) {
    // next time better to check class names as well !!!
    var spinner_number = number_as_string;
    for (var i = 0; i < spinners.length; i++) {
      if (spinners[i].hasAttribute('data-spinner-number') && spinners[i].getAttribute('data-spinner-number') == spinner_number) {
	return spinners[i];
      }
    }
    return false;
  }

}