'use strict';
/*==========================================================================*/
/* ============================== FUNCTIONS =============================== */
/*==========================================================================*/



function showLineMessage(html_str, bgcolor) {
  var elm_id = 'hdjsk4hjkdsh45hj43khdfsj4';
  if (document.getElementById(elm_id)) {
    return;
  }
  var elm = document.createElement('DIV');
  elm.id = elm_id;
  elm.className = 'cart-update-popup';
  elm.innerHTML = html_str;
  if (bgcolor != undefined) {
    elm.style.backgroundColor = bgcolor;
  }
  document.body.appendChild(elm);

  setTimeout(function () {
    document.body.removeChild(elm);
  }, 1500)

}



function showInputIsNotValid(obj) {
  $(obj).addClass('input-error');
}

function showInputValid(obj) {
  $(obj).removeClass('input-error');
}


function array_remove_duplicates(arr) {
  if (arr.length === 0)
    return false;
  if (arr.length === 1)
    return arr;
  var new_arr = [];
  new_arr[0] = arr[0];
  for (var i = 0, arr_len = arr.length; i < arr_len; i++) {
    if (is_in_new_array(arr[i])) {
      continue;
    }
    new_arr.push(arr[i]);
  }

  return new_arr;

  function is_in_new_array(val) {
    for (var k = 0, na_len = new_arr.length; k < na_len; k++) {
      if (val === new_arr[k]) {
	return true;
      }
    }
    return false;
  }
}


function array_remove_empty_values(arr) {
  var new_arr = Array();
  for (var i = 0, len = arr.length; i < len; i++) {
    if (arr[i] === '') {
      continue;
    }
    new_arr.push(arr[i]);
  }
  return new_arr;
}


function trim_input_field(e) {
  var str = e.target.value.trim();
  e.target.value = str;
}

function remove_all_input_spaces(e) {
  var str = e.target.value.trim();
  str = str.replace(/\ /g, "");
  e.target.value = str;
}

function to_slug(e) {
  var str = e.target.value.trim();
  str = str.replace(/\ /g, "-");
  str = str.toLowerCase();
  e.target.value = str;
}

function reverse_back_slashes(e) {
  var str = e.target.value.trim();
  str = str.replace(/\\/g, "/");
  e.target.value = str;
}

function remove_double_slashes(e) {
  var str = e.target.value.trim();
  str = str.replace(/(\/)+/g, "/");
  e.target.value = str;
}

function remove_double_dots(e) {
  var str = e.target.value.trim();
  str = str.replace(/(\.)+/g, ".");
  e.target.value = str;
}

function remove_double_hyphens(e) {
  var str = e.target.value.trim();
  str = str.replace(/(-)+/g, "-");
  e.target.value = str;
}

function remove_first_slash(e) {
  var str = e.target.value.trim();
  var first_symbol = str.substr(0, 1);
  var other_str = str.substr(1);
  if (first_symbol === '/') {
    first_symbol = '';
  }
  str = first_symbol + other_str;
  e.target.value = str;
}

function prepare_relative_uri(e) {
  remove_all_input_spaces(e);
  reverse_back_slashes(e);
  remove_double_slashes(e);
  remove_first_slash(e);
}

function prepare_custom_url(e) {
  remove_all_input_spaces(e);
  reverse_back_slashes(e);
}

function prepare_phone_number_from_input(str) {
  var str = str.trim();
  str = str.replace(/\D/g, '');
  return str;
}

function trimDotsAndHyphens(e) {
  var str = e.target.value.trim();
  while (str[str.length - 1] === '.' || str[str.length - 1] === '-') {
    str = str.slice(0, str.length - 1);
  }
  while (str[0] === '.' || str[0] === '-') {
    str = str.slice(1, str.length);
  }
  e.target.value = str;
}


function is_valid_phone_number(str) {
  if (str.length != 11) {
    return false;
  }
  if (/[^0-9]/g.test(str) === true) {
    return false;
  }
  return true;
}

// checks email string and return true if ok and false if wrong
function email_is_valid(str) {
  var patt = /^.+@[^@]{2,}\.[^@]{2,}$/i;
  if (patt.test(str)) {
    return true;
  }
  return false;
}

// checks password string and return true if ok and false if wrong
function password_is_valid(str) {
  var patt = /[^0-9A-Za-z\-\_\!\@\#\$\%\^\&\*\(\)\~\+\?\.\[\]\{\}]/gi;
  if (str.length > 1 && str !== '' && !patt.test(str)) {
    return true;
  }
  return false;
}

function captcha_is_valid(str) {
  if (str.length != 8 || /\D/g.test(str)) {
    return false;
  }
  return true;
}



function remove_double_spaces(e) {
  var str = e.target.value.trim();
  str = str.replace(/( )+/g, " ");
  e.target.value = str;
}

function prepare_name_input(e) {
  remove_double_spaces(e);
  remove_double_dots(e);
  remove_double_hyphens(e);
  trimDotsAndHyphens(e);
  var str = e.target.value.trim();
  str = str.replace(/[~`!@"'#№$;%^:&?*()_+={}\[\]\|\/\\\<\>]/g, "");
  e.target.value = str;
}

function prepare_address_input(e) {
  var str = e.target.value.trim();
  remove_double_spaces(e);
  str = str.replace(/[\^={}\[\]\|\/\\\<\>]/g, "");
  e.target.value = str;
}




/* ======= COMPUTING DIMENSIONS, POSITIONING ======= */

function getElementHeight(obj) {
  var h;
  if (obj == window) {
    h = (obj.innerHeight) ? obj.innerHeight : obj.clientHeight;
  } else {
    h = obj.offsetHeight;
  }
  return h;
}

function getElementWidth(obj) {
  var w;
  if (obj == window) {
    w = (obj.innerWidth) ? obj.innerWidth : obj.clientWidth;
  } else {
    w = obj.offsetWidth;
  }
  return w;
}

function alignCenterVertical(objToAlign, relativeObj, offsetToTop) {
  if (offsetToTop == undefined) {
    offsetToTop = 0;
  }
  var ota_h = getElementHeight(objToAlign);
  var ro_h = getElementHeight(relativeObj);
  objToAlign.style.top = (ro_h - ota_h) / 2 - offsetToTop + "px";
}



/* ======= MAX AND MIN VALUES FROM ARRAY ======= */

function get_array_max_value(arr) {
  var max_val = arr[0];
  var len = arr.length;
  for (var i = 1; i < len; i++) {
    max_val = arr[i] > max_val ? arr[i] : max_val;
  }
  return max_val;
}
function get_array_min_value(arr) {
  var min_val = arr[0];
  var len = arr.length;
  for (var i = 1; i < len; i++) {
    min_val = arr[i] < min_val ? arr[i] : min_val;
  }
  return min_val;
}