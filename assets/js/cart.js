'use strict';
// requires cookies library
/*==========================================================================*/
/* === CART =============================================================== */
/*==========================================================================*/


function updateCartFromInput(obj, cart_item_type, id) {
  //console.log(cart_item_type + ' : ' + id + ' : ' + obj.value);
  var val = process_spinner_value(obj.value);
  var uni_value_to_cookie = [parseInt(id), val];
  if (cart_item_type === 'product') {
    var cart_item_cookie_name = 'cart_products';
  } else if (cart_item_type === 'pack') {
    var cart_item_cookie_name = 'cart_packs';
  } else {
    throw 'Cart: error!';
    return;
  }

  var cart_items = Cookies.get(cart_item_cookie_name);


  if (cart_items === undefined) {
    cart_items = [];
  } else {
    cart_items = eval(cart_items);
  }
  if (typeof (cart_items) != 'object') {
    throw 'Error occured in shopping cart script!';
  }
  replaceCartAmounts(cart_item_cookie_name, uni_value_to_cookie);


  // here is browser view only

  if (!obj.hasAttribute('data-binded-cart-item-price-element') || obj.getAttribute('data-binded-cart-item-price-element') == '') {
    throw 'Cart: no price binded element!';
    return;
  }

  var item_binded_obj = document.getElementById(obj.getAttribute('data-binded-cart-item-price-element'));
  if (!item_binded_obj) {
    throw 'Cart: no price binded element!';
    return;
  }

  var new_item_sum_value = parseInt(val * parseInt(obj.getAttribute('data-cart-item-price')));
  item_binded_obj.innerHTML = new_item_sum_value;

  updateTotalPrice();

}

function updateTotalPrice() {
  var objs = document.getElementsByClassName('abstract-binded-to-total-price');
  var totalsum = 0;
  for (var i = 0; i < objs.length; i++) {
    var val = parseInt(objs[i].innerHTML);
    totalsum += val;
  }
  document.getElementById('cart_total_price_value').innerHTML = totalsum;
}


function setToCart(cookie_name, cart_items_array) {
  Cookies.set(cookie_name, cart_items_array);
}

function clearCart() {
  Cookies.remove('cart_products');
  Cookies.remove('cart_packs');
  location.replace(location.pathname);
}

function removeItemFromCart(product_type, id) {
  var cart_item_cookie_name = product_type === 'product' ? 'cart_products' : 'cart_packs';
  var cart_items = eval(Cookies.get(cart_item_cookie_name));
  var new_cart_items = [];
  for (var i = 0; i < cart_items.length; i++) {
    if (cart_items[i][0] != id) {
      new_cart_items.push(cart_items[i]);
    }
  }
  setToCart(cart_item_cookie_name, new_cart_items);

  if (document.getElementById(product_type + '_' + id)) {
    document.getElementById(product_type + '_' + id).parentNode.removeChild(document.getElementById(product_type + '_' + id));
  }
  updateTotalPrice();
  checkCartIsEmpty();
  showLineMessage('Удалено из резерва!', 'forestgreen');
}

function checkCartIsEmpty() {
  var cart_products, cart_packs;
  if (Cookies.get('cart_products') == undefined || Cookies.get('cart_products') == '') {
    Cookies.set('cart_products', '[]');
  }
  if (Cookies.get('cart_packs') == undefined || Cookies.get('cart_packs') == '') {
    Cookies.set('cart_packs', '[]');
  }
  cart_products = eval(Cookies.get('cart_products'));
  cart_packs = eval(Cookies.get('cart_packs'));
  if (cart_products.length === 0) {
    Cookies.remove('cart_products');
  }
  if (cart_packs.length === 0) {
    Cookies.remove('cart_packs');
  }
  if (cart_products.length === 0 && cart_packs.length === 0) {
    Cookies.remove('cart_products');
    Cookies.remove('cart_packs');
    location.replace(location.pathname);
  }
}

function addToCartFromInput(table, id, input_id) {
  var input_value = process_spinner_value(document.getElementById(input_id).value);
  addToCart(table, id, input_value);
}


function addToCart(table, id, amount) {
  var uni_value_to_cookie = [parseInt(id), parseInt(amount)];
  //console.log(uni_value_to_cookie);

  var cart_items, cart_item_cookie_name;
  if (table === 'products') {
    cart_item_cookie_name = 'cart_products';
    cart_items = Cookies.get(cart_item_cookie_name);
  } else if (table === 'packs') {
    cart_item_cookie_name = 'cart_packs';
    cart_items = Cookies.get(cart_item_cookie_name);
  } else {
    throw "Something goes wrong here…";
    return;
  }

  if (cart_items === undefined) {
    cart_items = [];
  } else {
    cart_items = eval(cart_items);
  }
  if (typeof (cart_items) != 'object') {
    throw 'Error occured in shopping cart script!';
  }

  cart_items.push(uni_value_to_cookie);
  cart_items = joinCartAmounts(cart_items);

  setToCart(cart_item_cookie_name, cart_items);

  if (document.getElementById('cart_top_nav_enter_link')) {
    document.getElementById('cart_top_nav_enter_link').innerHTML = 'Ваш резерв';
  }
  showLineMessage('Добавлено в резерв!', 'forestgreen');
}



function replaceCartAmounts(cookie_name, arr) {
  var dest_id = arr[0];
  var dest_id_value = arr[1];
  var cart_items = eval(Cookies.get(cookie_name));
  for (var i = 0; i < cart_items.length; i++) {
    if (cart_items[i][0] === dest_id) {
      cart_items[i][1] = dest_id_value;
    }
  }
  setToCart(cookie_name, cart_items);
}



function joinCartAmounts(arr) {
  var ids = [];
  for (var i = 0; i < arr.length; i++) {
    ids.push(arr[i][0]);
  }
  ids = array_remove_duplicates(ids);
  var new_arr = [];
  for (var i = 0; i < ids.length; i++) {
    var amount = 0;
    for (var k = 0; k < arr.length; k++) {
      if (arr[k][0] == ids[i]) {
	amount += arr[k][1];
      }
    }
    new_arr.push([ids[i], amount]);
  }
  return new_arr;
}