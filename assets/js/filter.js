'use strict';

function filterShowFurtherResults() {
  if (document.getElementById('show_further_base_element')) {
    $('#show_further_base_element').slideUp(200, function () {
      document.getElementById('show_further_base_element').parentNode.removeChild(document.getElementById('show_further_base_element'));
      current_filter_request_number++;
      filter_gets_new_request = false;
      performRequestViaAjax();
    });
  }
}

function performRequestViaAjax() {
  if (filter_block_count_result_numer === 0) {
    //alert('no results found!');
    return;
  }
  if (filter_gets_new_request) {
    var country = JSON.parse(Cookies.get('flt_countries')).val;
    var brand = JSON.parse(Cookies.get('flt_brands')).val;
    var vitola = JSON.parse(Cookies.get('flt_vitolas')).val;
    var line = JSON.parse(Cookies.get('flt_lines')).val;
    var length = JSON.parse(Cookies.get('flt_lengths')).val;
    var cepo = JSON.parse(Cookies.get('flt_cepos')).val;
    var intensity = JSON.parse(Cookies.get('flt_intensities')).val;
    var wrapper = JSON.parse(Cookies.get('flt_wrappers')).val;
    var flavour = JSON.parse(Cookies.get('flt_flavours')).val;
    var price_max = JSON.parse(Cookies.get('flt_prices')).val;

    params_to_continue_showing_ajax_results = {
      uid: Date.now(),
      current_request_number: current_filter_request_number,
      country: country,
      brand: brand,
      vitola: vitola,
      line: line,
      length: length,
      cepo: cepo,
      intensity: intensity,
      wrapper: wrapper,
      flavour: flavour,
      price_max: price_max
    };
  }

  filter_gets_new_request = false;
  params_to_continue_showing_ajax_results.current_request_number = current_filter_request_number;

  //console.log(params_to_continue_showing_ajax_results);

  $.ajax({
    url: "ajax/get_cigars_by_params",
    data: params_to_continue_showing_ajax_results,
    type: 'POST',
    success: function (result) {
      //console.log(result);
      var holder_id = 'results_part_' + Date.now();
      var holder = document.createElement('DIV');
      holder.id = holder_id;
      $(holder).hide();
      holder.innerHTML = result;
      document.getElementById('filter_results_holder').appendChild(holder);

      $(holder).slideDown(500);

      var to_top_element = $("#filter_results_holder");
      if (current_filter_request_number != 1) {
	to_top_element = holder;
      }
      $('html, body').animate({
	scrollTop: $(to_top_element).offset().top
      }, 500);
    },
    failure: function (result) {
      //console.log(result);
    }
  });
}

function initSS(filter_events_wrapper_element) {
  filter_events_wrapper_element.addEventListener('click', ssClickListener, false);
  setFilterParametersFromCookies();
}

function sendRequestToCountResults() {
  var country = JSON.parse(Cookies.get('flt_countries')).val;
  var brand = JSON.parse(Cookies.get('flt_brands')).val;
  var vitola = JSON.parse(Cookies.get('flt_vitolas')).val;
  var line = JSON.parse(Cookies.get('flt_lines')).val;
  var length = JSON.parse(Cookies.get('flt_lengths')).val;
  var cepo = JSON.parse(Cookies.get('flt_cepos')).val;
  var intensity = JSON.parse(Cookies.get('flt_intensities')).val;
  var wrapper = JSON.parse(Cookies.get('flt_wrappers')).val;
  var flavour = JSON.parse(Cookies.get('flt_flavours')).val;
  var price_max = JSON.parse(Cookies.get('flt_prices')).val;

  $.ajax({
    url: "ajax/count_search_results",
    data: {
      uid: Date.now(),
      country: country,
      brand: brand,
      vitola: vitola,
      line: line,
      length: length,
      cepo: cepo,
      intensity: intensity,
      wrapper: wrapper,
      flavour: flavour,
      price_max: price_max
    },
    type: 'POST',
    success: function (result) {
      //console.log(result);
      var res = parseInt(result);
      if (isNaN(res)) {
	res = 0;
      }
      filter_block_count_result_numer = res;
      if (res === 0) {
	$('#count_search_result_holder').removeClass('color-ha-light');
      } else {
	$('#count_search_result_holder').addClass('color-ha-light');
      }
      document.getElementById('count_search_result_holder').innerHTML = res;
    },
    failure: function (result) {
      filter_block_count_result_numer = 0;
      document.getElementById('count_search_result_holder').innerHTML = 0;
      //console.log(result);
    }
  });
}

function setFilterParametersFromCookies() {
  var flt_default_str_value = 'не важно';
  var flt_default_value = -1;
  var needs_to_update = false;

  // INTENSITIES
  try {
    var flt_intensities = JSON.parse(Cookies.get('flt_intensities'));
    document.getElementById('ss_filter_intensities_value_element').innerHTML = flt_intensities.str;
  } catch (err) {
    Cookies.remove('flt_intensities');
    Cookies.set('flt_intensities', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    needs_to_update = true;
  }
  // WRAPPERS
  try {
    var flt_wrappers = JSON.parse(Cookies.get('flt_wrappers'));
    document.getElementById('ss_filter_wrappers_value_element').innerHTML = flt_wrappers.str;
  } catch (err) {
    Cookies.remove('flt_wrappers');
    Cookies.set('flt_wrappers', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    needs_to_update = true;
  }
  // FLAVOURS
  try {
    var flt_flavours = JSON.parse(Cookies.get('flt_flavours'));
    document.getElementById('ss_filter_flavours_value_element').innerHTML = flt_flavours.str;
  } catch (err) {
    Cookies.remove('flt_flavours');
    Cookies.set('flt_flavours', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    needs_to_update = true;
  }
  // LINES
  try {
    var flt_lines = JSON.parse(Cookies.get('flt_lines'));
    document.getElementById('ss_filter_lines_value_element').innerHTML = flt_lines.str;
  } catch (err) {
    Cookies.remove('flt_lines');
    Cookies.set('flt_lines', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    needs_to_update = true;
  }
  // LENGTHS
  try {
    var flt_lengths = JSON.parse(Cookies.get('flt_lengths'));
    document.getElementById('ss_filter_lengths_value_element').innerHTML = flt_lengths.str;
  } catch (err) {
    Cookies.remove('flt_lengths');
    Cookies.set('flt_lengths', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    needs_to_update = true;
  }
  // VITOLAS
  try {
    var flt_vitolas = JSON.parse(Cookies.get('flt_vitolas'));
    document.getElementById('ss_filter_vitolas_value_element').innerHTML = flt_vitolas.str;
  } catch (err) {
    Cookies.remove('flt_vitolas');
    Cookies.set('flt_vitolas', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    needs_to_update = true;
  }
  // PRICES
  try {
    var flt_prices = JSON.parse(Cookies.get('flt_prices'));
    document.getElementById('ss_filter_prices_value_element').innerHTML = flt_prices.str;
  } catch (err) {
    Cookies.remove('flt_prices');
    Cookies.set('flt_prices', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    needs_to_update = true;
  }
  // BRANDS
  try {
    var flt_brands = JSON.parse(Cookies.get('flt_brands'));
    document.getElementById('ss_filter_brands_value_element').innerHTML = flt_brands.str;
  } catch (err) {
    Cookies.remove('flt_brands');
    Cookies.set('flt_brands', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    needs_to_update = true;
  }
  // COUNTRIES
  try {
    var flt_countries = JSON.parse(Cookies.get('flt_countries'));
    if (parseInt(flt_countries.val) != 55) { // Cuba code in our array
      Cookies.set('flt_countries', JSON.stringify({'val': 55, 'str': 'Cuba'}));
    }
    document.getElementById('ss_filter_countries_value_element').innerHTML = flt_countries.str;
  } catch (err) {
    Cookies.remove('flt_countries');
    Cookies.set('flt_countries', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    Cookies.set('flt_countries', JSON.stringify({'val': 55, 'str': 'Cuba'}));
    needs_to_update = true;
  }
  // CEPOS
  try {
    var flt_cepos = JSON.parse(Cookies.get('flt_cepos'));
    document.getElementById('ss_filter_cepos_value_element').innerHTML = flt_cepos.str;
  } catch (err) {
    Cookies.remove('flt_cepos');
    Cookies.set('flt_cepos', JSON.stringify({'val': flt_default_value, 'str': flt_default_str_value}));
    needs_to_update = true;
  }

  if (needs_to_update) {
    setFilterParametersFromCookies();
  } else {
    sendRequestToCountResults();
  }

}

function ssClickListener(e) {
  if (e.target.hasAttribute('data-ss-filter-type-picker')) {
    var ss_picker_value = e.target.getAttribute('data-ss-filter-type-picker');
    open_ss_filter_window(ss_picker_value);
  }
  if (e.target.hasAttribute('data-ss-filter-reset')) {
    ssFilterReset();
  }
}

function ssFilterReset() {
  Cookies.remove('flt_countries');
  Cookies.remove('flt_brands');
  Cookies.remove('flt_vitolas');
  Cookies.remove('flt_lines');
  Cookies.remove('flt_lengths');
  Cookies.remove('flt_cepos');
  Cookies.remove('flt_intensities');
  Cookies.remove('flt_wrappers');
  Cookies.remove('flt_flavours');
  Cookies.remove('flt_prices');
  setFilterParametersFromCookies();
  //$("#filter_results_holder").empty();
  $("#filter_results_holder").slideUp(500, function () {
    $("#filter_results_holder").empty().show();
  })
}

function open_ss_filter_window(picker_value) {
  $.ajax({
    url: "ajax/get_picked_filter_array",
    data: {
      uid: Date.now(),
      array_name: picker_value
    },
    type: 'POST',
    success: function (result) {
      create_filter_window(picker_value, result);
    },
    failure: function (result) {
      //console.log(result);
    }
  });
}


function create_filter_window(picker_value, filter_array_string) {
  //console.log(filter_array_string);
  $.ajax({
    url: "ajax/get_picked_filter_window",
    data: {
      window_name: picker_value
    },
    type: 'POST',
    success: function (result) {
      //console.log(result);
      var flt_window_inner_container = document.createElement('DIV');
      flt_window_inner_container.id = 'flt_window_inner_container';
      flt_window_inner_container.innerHTML = result;
      document.getElementById('flt_window').appendChild(flt_window_inner_container);

      var js_ftl_arr = eval(filter_array_string);
      //eval("create_filter_window_" + picker_value + "_list(js_ftl_arr)");
      window["create_filter_window_" + picker_value + "_list"](js_ftl_arr); // calling function this way instead of eval

      filter_is_open = true;
      currentScrollTop = $(window).scrollTop();
      set_body_bg_color(menu_window_bg_color);
      $(sitewrapper).removeClass('block').addClass('hidden');
      $('#flt_window').removeClass('hidden').addClass('block');
      flashScreen();
      window.addEventListener('keyup', filterWindowKeyEventCloseListener, false);
      window.addEventListener('click', filterWindowClickListener, false);
      $(window).scrollTop(0);

    },
    failure: function (result) {
      //console.log(result);
    }
  });
}


function filterWindowClickListener(e) {
  if (e.target.getAttribute('data-action-type') === 'close-filter-window') {
    closeFilterWindow();
  }
  if (!e.target.hasAttribute('data-ha-filter-type')) {
    return;
  }
  Cookies.set('flt_' + e.target.getAttribute('data-ha-filter-type'), JSON.stringify({'val': parseInt(e.target.getAttribute('data-ha-filter-value')), 'str': e.target.getAttribute('data-ha-filter-string')}));
  closeFilterWindow();
  setFilterParametersFromCookies();
}



function closeFilterWindow() {
  document.getElementById('flt_window').removeChild(document.getElementById('flt_window_inner_container'));
  set_body_bg_color(body_default_bg_color);
  $(sitewrapper).removeClass('hidden').addClass('block');
  $('#flt_window').removeClass('block').addClass('hidden');
  $(window).scrollTop(currentScrollTop);
  flashScreen();
  filter_is_open = false;
}



function filterWindowKeyEventCloseListener(e) {
  if (e.keyCode === 27 && filter_is_open) {
    closeFilterWindow();
  }
}