'use strict';
// requires cookies library
/*==========================================================================*/
/* === AUTH =============================================================== */
/*==========================================================================*/


function validateRegisterForm(e) {
  var email_input = document.getElementById('email_input');
  var password_input = document.getElementById('password_input');
  var captcha_input = document.getElementById('captcha_input');

  var form_is_valid = true;
  var err_msg = 'Пожалуйста еще раз проверьте все данные!';

  if (!email_is_valid(email_input.value)) {
    form_is_valid = false;
    showInputIsNotValid(email_input);
  }

  if (!password_is_valid(password_input.value)) {
    form_is_valid = false;
    showInputIsNotValid(password_input);
  }

  if (!captcha_is_valid(captcha_input.value)) {
    form_is_valid = false;
    showInputIsNotValid(captcha_input);
  }

  if (!form_is_valid) {
    e.preventDefault();
    showLineMessage(err_msg, 'darkred');
  }
}

function validateLoginForm(e) {
  var email_input = document.getElementById('email_input');
  var password_input = document.getElementById('password_input');

  var form_is_valid = true;
  var err_msg = 'Пожалуйста еще раз проверьте все данные!';

  if (!email_is_valid(email_input.value)) {
    form_is_valid = false;
    showInputIsNotValid(email_input);
  }

  if (!password_is_valid(password_input.value)) {
    form_is_valid = false;
    showInputIsNotValid(password_input);
  }

  if (!form_is_valid) {
    e.preventDefault();
    showLineMessage(err_msg, 'darkred');
  }
}


function validateResetPasswordForm(e) {
  validateRegisterForm(e);
  // they are the same
}