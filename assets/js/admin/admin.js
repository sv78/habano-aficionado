'use strict';
var strange_id = "hzjdfk4hj43k4573Js48K9548";

/*==========================================================================*/
/* ============================== FUNCTIONS ============================== */
/*==========================================================================*/



function tableMouseOverListener(e) {
  if (e.target.tagName === "IMG") {
    if (document.getElementById(strange_id)) {
      return;
    }
    var preview_img = document.createElement('IMG');
    preview_img.id = strange_id;
    preview_img.src = e.target.src;
    preview_img.className = 'admin-item-preview-img';
    e.target.parentNode.appendChild(preview_img);
  } else {
    if (document.getElementById(strange_id)) {
      document.getElementById(strange_id).parentNode.removeChild(document.getElementById(strange_id));
    }
  }
}



function quantity_change_by_id(dom_element) {
  console.log('This function is not emplemented yet.');
  if (document.getElementById(strange_id)) {
    return;
  }
  var q_curr = dom_element.innerHTML;
  var q_inp = document.createElement("INPUT");
  q_inp.id = strange_id;
  q_inp.setAttribute('type', 'text');
  q_inp.className = 'admin-quantity-quick-input';
  q_inp.value = q_curr;
  dom_element.appendChild(q_inp);
  q_inp.focus();
  q_inp.select();
  q_inp.addEventListener('blur', removeInput);
  q_inp.addEventListener('keyup', q_inp_key_listener);
}


function removeInput(e) {
  if (document.getElementById(strange_id)) {
    document.getElementById(strange_id).parentNode.removeChild(document.getElementById(strange_id));
  }
}
function q_inp_key_listener(e) {
  var new_value = e.target.value;
  if (e.keyCode === 27) {
    e.target.blur();
  } else if (e.keyCode === 13) {
    if (!/[^0-9]/ig.test(new_value) && new_value != '' && new_value.length < 11) {
      console.log("GOOD");
      e.target.parentNode.innerHTML = new_value;
      e.target.blur();
    } else {
      return;
    }
  }
}




function get_particular_parent(elm, searching_tagname) {
  var searching_tagname = searching_tagname.toUpperCase();
  var elm = elm;
  while (elm.parentNode.tagName.toUpperCase() != searching_tagname) {
    if (elm.parentNode.tagName.toUpperCase() == 'BODY' || elm.parentNode.tagName.toUpperCase() == 'HTML') {
      return false;
    }
    elm = elm.parentNode;
  }
  return elm.parentNode;
}

function decrease_total_record_counter() {
  if (document.getElementById('total_records_counter')) {
    var elm = document.getElementById('total_records_counter');
    var old_value = parseInt(elm.innerHTML);
    if (old_value <= 0)
      return;
    elm.innerHTML = old_value - 1;
  }
}


function get_id_attribute_name(dom_element) {
  var possible_values = [
    'section-id',
    'article-id',
    'user-id',
    'brand-id',
    'category-id',
    'product-id',
    'pack-id',
    'order-id',
    'banner-id'
  ];
  for (var i = 0, len = possible_values.length; i < len; i++) {
    if (dom_element.hasAttribute(possible_values[i])) {
      return possible_values[i];
    }
  }
  return false;
}


function get_table_name_by_id_attribute_name(id_attribute_name) {
  var woo = {
    'article-id': 'articles',
    'section-id': 'sections',
    'user-id': 'users',
    'brand-id': 'brands',
    'category-id': 'categories',
    'product-id': 'products',
    'pack-id': 'packs',
    'order-id': 'orders',
    'banner-id': 'banners'
  };
  for (var val in woo) {
    if (val === id_attribute_name) {
      return woo[val];
    }
  }
  return false;
}

function get_table_and_item_id(dom_element) {
  var id_attribute_name = get_id_attribute_name(dom_element);
  if (!id_attribute_name) {
    throw ': Required DOM element attribute name not found';
    return;
  }
  var table_name = get_table_name_by_id_attribute_name(id_attribute_name);
  if (!table_name) {
    throw ': Matched table not found';
    return;
  }
  var item_id = dom_element.getAttribute(id_attribute_name);
  if (/[^\d]+/g.test(item_id)) {
    throw ': ID is not an integer';
    return false;
  }
  return {'table': table_name, 'id': item_id};
}

function toggle_featured(dom_element) {
  if (!confirm("Do you want to make this item featured? This will change the list order in public site."))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/toggle-item-featured-by-id",
    method: "POST",
    data: {table: obj.table, id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg !== '') {
      //alert('Failure!');
    } else {
      update_icon_featured(dom_element);
      /*
       var conf = confirm('Done! Do you wand to update page to see results?');
       if (conf) {
       location.reload();
       }
       */
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}

function lift_up(dom_element) {
  if (!confirm("Do you want to lift this item up? This will change the list order in public site."))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/lift-up-item-by-id",
    method: "POST",
    data: {table: obj.table, id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg === '') {
      var conf = confirm('Done! Do you wand to update page to see results?');
      if (conf) {
	location.reload();
      }
      return;
    } else {
      alert('Failure!');
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}

function delete_article_by_id(dom_element) {
  if (!confirm("Do you want to delete article?"))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/delete-article-by-id",
    method: "POST",
    data: {id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg != '') {
      alert("Can not delete item because something went wrong.");
    } else {
      var tr = get_particular_parent(dom_element, "TR");
      tr.parentNode.removeChild(tr);
      decrease_total_record_counter();
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}

function delete_brand_by_id(dom_element) {
  if (!confirm("Do you want to delete brand?"))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/delete-brand-by-id",
    method: "POST",
    data: {id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg != '') {
      alert("Can not delete item because something went wrong.");
    } else {
      var tr = get_particular_parent(dom_element, "TR");
      tr.parentNode.removeChild(tr);
      decrease_total_record_counter();
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}


function delete_order_by_id(dom_element) {
  if (!confirm("Do you want to delete order?"))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/delete-order-by-id",
    method: "POST",
    data: {id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg != '') {
      alert("Can not delete item because something went wrong.");
    } else {
      var tr = get_particular_parent(dom_element, "TR");
      tr.parentNode.removeChild(tr);
      decrease_total_record_counter();
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}


function delete_category_by_id(dom_element) {
  if (!confirm("Do you want to delete category?"))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/delete-category-by-id",
    method: "POST",
    data: {id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg != '') {
      alert("Can not delete item because something went wrong.");
    } else {
      var tr = get_particular_parent(dom_element, "TR");
      tr.parentNode.removeChild(tr);
      decrease_total_record_counter();
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}


function delete_banner_by_id(dom_element) {
  if (!confirm("Do you want to delete banner?"))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/delete-banner-by-id",
    method: "POST",
    data: {id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg != '') {
      alert("Can not delete item because something went wrong.");
    } else {
      var tr = get_particular_parent(dom_element, "TR");
      tr.parentNode.removeChild(tr);
      decrease_total_record_counter();
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}

function delete_product_by_id(dom_element) {
  if (!confirm("Do you want to delete product?"))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/delete-product-by-id",
    method: "POST",
    data: {id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg != '') {
      alert("Can not delete item because something went wrong.");
    } else {
      var tr = get_particular_parent(dom_element, "TR");
      tr.parentNode.removeChild(tr);
      decrease_total_record_counter();
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}


function delete_pack_by_id(dom_element) {
  if (!confirm("Do you want to delete pack?"))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/delete-pack-by-id",
    method: "POST",
    data: {id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg != '') {
      alert("Can not delete item because something went wrong.");
    } else {
      var tr = get_particular_parent(dom_element, "TR");
      tr.parentNode.removeChild(tr);
      decrease_total_record_counter();
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}


function delete_section_by_id(dom_element) {
  if (!confirm("Do you want to delete section?"))
    return;
  var obj = get_table_and_item_id(dom_element);
  var request = $.ajax({
    url: "/ajax/delete-section-by-id",
    method: "POST",
    data: {id: obj.id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg != '') {
      alert("Can not delete section because of related item(s) exist(s).");
      return;
    } else {
      var tr = get_particular_parent(dom_element, "TR");
      tr.parentNode.removeChild(tr);
      decrease_total_record_counter();
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}

function toggle_published_item_by_id(element) {
  if (!confirm("Do you want to change published status? This will affect public site drastically!"))
    return;
  var obj = get_table_and_item_id(element);
  var request = $.ajax({
    url: "/ajax/toggle-published-item-by-id",
    method: "POST",
    data: {table: obj.table, id: obj.id}
  });
  var res = false;
  request.done(function (msg) {
    //$("#log").html(msg);
    if (msg == '') {
      return;
    }
    update_icon_published(element, msg);
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Request failed: " + textStatus);
  });
}

function delete_user_by_id(dom_element) {
  if (!dom_element || !dom_element.hasAttribute('action-type') || !dom_element.getAttribute('action-type').toLowerCase() === 'delete') {
    return;
  }
  var user_id = dom_element.getAttribute('user-id');
  if (!confirm("Are you sure to delete user?"))
    return;
  var request = $.ajax({
    url: "/ajax/delete-user-by-id",
    method: "POST",
    data: {id: user_id}
  });
  request.done(function (msg) {
    //console.log(msg);
    //document.getElementById('dbg').innerHTML = msg;
    if (msg === '1') {
      var tr = get_particular_parent(dom_element, "TR");
      tr.parentNode.removeChild(tr);
      decrease_total_record_counter();
      return;
    } else {
      alert("Can not delete user because of server error.");
    }
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Error!!!");
  });
}

function toggle_user_status_by_id(element) {
  if (!element || !element.hasAttribute('action-type') || !element.getAttribute('action-type').toLowerCase() === 'status') {
    return;
  }
  var user_id = element.getAttribute('user-id');
  if (!confirm("Are you sure to change user status?"))
    return;
  var request = $.ajax({
    url: "/ajax/toggle-user-status-by-id",
    method: "POST",
    data: {id: user_id}
  });
  var res = false;
  request.done(function (msg) {
    //$("#log").html(msg);
    if (msg == '') {
      return;
    }
    //console.log(msg);
    var is_admin = parseInt(msg);
    update_user_element_and_tr(element, is_admin);
  });
  request.fail(function (jqXHR, textStatus) {
    alert("Request failed: " + textStatus);
  });
}

function update_user_element_and_tr(span, is_admin) {
  span.innerHTML = is_admin === 1 ? "Ad" : "Cu";
  var tr = get_particular_parent(span, "TR");
  var removing_class = is_admin == 0 ? "success" : "";
  var adding_class = is_admin == 0 ? "" : "success";
  $(tr).removeClass(removing_class).addClass(adding_class);
}


function update_icon_published(element, published_new_value) {
  var removing_class = published_new_value == '0' ? "glyphicon-ok" : "glyphicon-lock";
  var adding_class = published_new_value == '0' ? "glyphicon-lock" : "glyphicon-ok";
  $(element).removeClass(removing_class).addClass(adding_class);
}

function update_icon_featured(elm) {
  if (!elm.hasAttribute('class')) {
    return;
  }
  if (elm.parentNode.getElementsByClassName('glyphicon-star').length > 0) {
    var removing_class = 'glyphicon-star';
    var adding_class = 'glyphicon-star-empty';
  }
  if (elm.parentNode.getElementsByClassName('glyphicon-star-empty').length > 0) {
    var removing_class = 'glyphicon-star-empty';
    var adding_class = 'glyphicon-star';
  }
  $(elm).removeClass(removing_class).addClass(adding_class);
}



/**
 * Создает значение slug из значения поля ввода title и вставляет в поле ввода slug
 */
function create_slug_from_title(title_input, slug_input, str_length = 32) {
  if (!title_input || !slug_input) {
    return false;
  }
  var title = title_input.value;
  var slug = generate_slug(title);
  slug_input.value = slug.substr(0, str_length);
}

function create_metatitle_from_title(title_input, metatitle_input) {
  if (!title_input || !metatitle_input) {
    return false;
  }
  metatitle_input.value = title_input.value;
}




function generate_slug(str = null) {
  if (str === undefined || str === null)
    return '';
  str = str.trim();
  if (str === '')
    return '';
  str = transliterate(str).toLowerCase();
  str = str.replace(/(\ )+/gm, '-');
  str = str.replace(/[^\w-]/gm, '');
  str = str.replace(/_/gm, '-');
  return str;
}





function transliterate(word) {
  var symbols = {"Ё": "YO", "Й": "I", "Ц": "TS", "У": "U", "К": "K", "Е": "E", "Н": "N", "Г": "G", "Ш": "SH", "Щ": "SCH", "З": "Z", "Х": "H", "Ъ": "'", "ё": "yo", "й": "i", "ц": "ts", "у": "u", "к": "k", "е": "e", "н": "n", "г": "g", "ш": "sh", "щ": "sch", "з": "z", "х": "h", "ъ": "'", "Ф": "F", "Ы": "I", "В": "V", "А": "a", "П": "P", "Р": "R", "О": "O", "Л": "L", "Д": "D", "Ж": "ZH", "Э": "E", "ф": "f", "ы": "i", "в": "v", "а": "a", "п": "p", "р": "r", "о": "o", "л": "l", "д": "d", "ж": "zh", "э": "e", "Я": "Ya", "Ч": "CH", "С": "S", "М": "M", "И": "I", "Т": "T", "Ь": "'", "Б": "B", "Ю": "YU", "я": "ya", "ч": "ch", "с": "s", "м": "m", "и": "i", "т": "t", "ь": "'", "б": "b", "ю": "yu"};
  return word.split('').map(function (char) {
    return symbols[char] || char;
  }).join("");
}