var gulp = require('gulp');

// importing plugin
var uglify = require('gulp-uglify');
var uglifyOptions = {
	//hoist_vars:true,
	//sequences:false,
	comments:true
}




var concat = require('gulp-concat');

/*
 gulp.task('default', function() {
 // place code for your current task here
 
 return gulp.src('./pre-js/*.js')
 .pipe(uglify())
 .pipe(gulp.dest('./js'));
 });
 
 */

gulp.task('concatjs', function () {
	return gulp.src('./.js/*.js')
					.pipe(concat('concatenated.js'))
					.pipe(gulp.dest('./js'));
});


gulp.task('jscmp', function () {
	return gulp.src('./.js/*.js')
					.pipe(uglify())
					.pipe(gulp.dest('./js'));
});

gulp.task('mixed', function () {
	//return gulp.src('./.js/*.js')
	return gulp.src(['./.js/popup_windows_data.js','./.js/functions.js', './.js/mainscript.js'])
					.pipe(concat('pentatonica.js'))
					.pipe(uglify({comments:false}))
					.pipe(gulp.dest('./js'));
});