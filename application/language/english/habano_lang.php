<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* ===================== */
/* ==== ADMIN SIDE ==== */
/* ===================== */

// === ADMIN PAGE ===
// 

$lang['habano_admin_create_new_blog_section_btn'] = 'Create new blog section';
$lang['habano_admin_create_new_blog_article_btn'] = 'Create new blog article';

$lang['habano_admin_sections_page_title'] = 'Blog sections';
// 
// ADMIN / COMMON PHRASES

$lang['habano_admin_create_new'] = 'Create new';

$lang['habano_input_title'] = 'Title *';
$lang['habano_input_slug'] = 'Slug (alias) *';
$lang['habano_input_generate_slug_from_title_btn'] = 'Generate slug from title';
$lang['habano_input_published'] = 'Published';
$lang['habano_input_featured'] = 'Featured';
$lang['habano_input_intro'] = 'Intro text';
$lang['habano_input_body'] = 'Main content';
$lang['habano_input_metatitle'] = 'Title meta tag';
$lang['habano_input_generate_metatitle_from_title_btn'] = 'Create title meta tag from title';
$lang['habano_input_metadescription'] = 'Description meta tag';
$lang['habano_input_metakeywords'] = 'Keywords meta tag';
$lang['habano_input_metaauthor'] = 'Author meta tag';
$lang['habano_input_metarobots'] = 'Robots meta tag';
$lang['habano_input_file_image'] = 'Upload image';
$lang['habano_input_submit'] = 'Submit data';
$lang['habano_input_reset'] = 'Reset all filled in data';
$lang['habano_input_reload_page_to_reset_form_after_validation'] = 'Reload and reset data';

$lang['habano_input_email'] = 'Email';
$lang['habano_input_password'] = 'Password';
$lang['habano_input_password_confirm'] = 'Confirm password';
$lang['habano_input_phone'] = 'Phone';
$lang['habano_input_name'] = 'Name';
$lang['habano_input_age'] = 'Age';
$lang['habano_input_delivery_address'] = 'Delivery address';
$lang['habano_input_captcha'] = 'Please type what you see on the image';

//   
// ADMIN / CREATE BLOG SECTION PAGE - admin/create-new-blog-section
//
$lang['habano_blog_section_create_page_title'] = 'Creating a new blog section';
$lang['habano_input_file_blog_section_restriction_text'] = 'Please note that uploading files must be only images etc.';

//
// ADMIN / CREATE BLOG ARTICLE PAGE - admin/create-new-blog-article
//
$lang['habano_blog_article_create_page_title'] = 'Creating a new blog article';
$lang['habano_input_blog_article_section_select'] = 'Choose related section';
$lang['habano_input_file_blog_article_restriction_text'] = 'Please note that uploading files must be only images etc.';


// ADMIN / CREATE BLOG ARTICLE PAGE - admin/create-new-blog-article

$lang['habano_admin_users_page_title'] = 'Users';
$lang['habano_admin_user_page_title'] = 'User data';


// ERRORS - BLOG

$lang['habano_input_blog_section_upload_error_wrong_file_name'] = 'Wrong file name used! Use only latin caracters and digits, dots, dashes and underscores.';
$lang['habano_input_blog_article_upload_error_wrong_file_name'] = 'Wrong file name used! Use only latin caracters and digits, dots, dashes and underscores.';



$lang['habano_blog_section_read_more'] = 'Show articles';
$lang['habano_blog_article_read_more'] = 'Read article';


/* ===================== */
/* ==== PUBLIC SIDE ==== */
/* ===================== */


// === HOME PAGE ===
$lang['habano_home_page_title'] = 'Habano Aficionado Home';
$lang['habano_home_page_meta_title'] = 'Habano Aficionado Home';


/* ===================== */
/* ==== AUTHENTIFICATION ==== */
/* ===================== */


// === LOGIN PAGE ===
$lang['habano_login_page_meta_title'] = 'Login to enter';
$lang['habano_login_page_title'] = 'Login to enter';
$lang['habano_logged_in_title'] = 'Logged in';
$lang['habano_logged_out_title'] = 'Logged out';

// === RESET PASSWORD PAGE ===
$lang['habano_reset_pwd_page_title'] = 'Create new password';



// === PROFILE PAGE ===
$lang['habano_profile_page_title'] = 'Profile';

// === LOGOUT PAGE ===
$lang['habano_logout_page_title'] = 'Logout';
$lang['habano_logout_page_meta_title'] = 'Logout';

// === REGISTER PAGE ===
$lang['habano_register_page_meta_title'] = 'Register';
$lang['habano_register_page_title'] = 'Register';



//$lang['habano_home_page'] = 'Home';

// === BLOG PAGE ===
// 
$lang['habano_blog_page_title'] = 'Blog';


// === BLOG ARTICLES PAGE ===
//
$lang['habano_blog_sections_page_no_items'] = 'There are no sections any more';

// === BLOG ARTICLES PAGE ===
//
$lang['habano_blog_articles_page_no_items'] = 'There are no articles any more in the current section';
