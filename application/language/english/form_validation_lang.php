<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_alpha_numeric_dash']		= 'The {field} field accepts only latin characters, digits and dashes.';
$lang['form_validation_robots_metatag']		= 'The {field} field error. Use dropdown list values only.';
$lang['form_validation_checkbox']		= 'The {field} checkbox error.';
//$lang['form_validation_captcha']		= 'The {field} word you typed seems to be incorrect.';
$lang['form_validation_captcha']		= 'Число на картинке не совпадает с указанным Вами числом.';
$lang['form_validation_pwd_regexp']		= 'The {field} field uses restricted symbols. No spaces allowed. Non-latin letters are prohibited.';
$lang['form_validation_reserved_slugs']		= 'The {field} can not be this value. Please use another one.';
$lang['form_validation_cart_phone']		= 'The {field} has wrong value. Please enter correct data.';