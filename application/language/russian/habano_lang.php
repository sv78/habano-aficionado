<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['habano_home_page'] = 'На главную страницу сайта';


$lang['habano_create_new_blog_section'] = 'Создать секцию блога';
$lang['habano_create_new_blog_article'] = 'Создать статью блога';

$lang['habano_blog_section_read_more'] = 'Читать далее этот раздел';


$lang['habano_input_slug_label'] = 'Введите url-сегмент';
$lang['habano_input_slug_placeholder'] = 'url-сегмент';


$lang['habano_input_title_label'] = 'Введите основной заголовок';
$lang['habano_input_title_placeholder'] = 'заголовок';

$lang['habano_input_intro_label'] = 'Введите вводный текст';
$lang['habano_input_intro_placeholder'] = 'вводный текст';

$lang['habano_input_body_label'] = 'Создайте содержание';
$lang['habano_input_body_placeholder'] = 'содержание';