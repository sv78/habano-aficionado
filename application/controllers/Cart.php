<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

  public function index() {

    $data['metatags']['title'] = config_item('_metatags')['cart']['title'];
    $data['metatags']['description'] = config_item('_metatags')['cart']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['cart']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['cart']['robots'];

    //$this->load->helper('cookie');
    $cart_products = Baza::decode_plain_string_from_db(get_cookie('cart_products'));
    $cart_packs = Baza::decode_plain_string_from_db(get_cookie('cart_packs'));

    if ($cart_products == '' && $cart_packs == '') {
      $this->load->view('templates/public/header', $data);
      $this->load->view('templates/public/cart_empty_view', $data);
      $this->load->view('templates/public/recently_seen_products_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    /* -- next goes if cart is not empty -- */
    // my_cart library is autoloaded

    if ($cart_products != '') {
      $cart_products = $this->shopcart->parse_cart_items($cart_products);
    }

    if ($cart_packs != '') {
      $cart_packs = $this->shopcart->parse_cart_items($cart_packs);
    }

    $data['products'] = array();
    $data['packs'] = array();
    $data['cart_items'] = array();

    if ($cart_products != '' && count($cart_products) > 0) {
      foreach ($cart_products as $cart_product) {
	//$this->db->where('id', $cart_product[0]);
	//$query = $this->db->get('products');

	$sql = 'SELECT DISTINCT products.*, '
		. 'brands.id AS brand_id, '
		. 'brands.slug AS brand_slug, '
		. 'brands.name AS brand_name, '
		. 'brands.country AS brand_country, '
		. 'categories.id AS category_id, '
		. 'categories.slug AS category_slug, '
		. 'categories.name AS category_name '
		. 'FROM products '
		. 'LEFT JOIN categories '
		. 'ON products.category_id = categories.id '
		. 'LEFT JOIN brands '
		. 'ON products.brand_id = brands.id '
		. 'WHERE products.id = "' . $cart_product[0] . '" '
		. 'AND products.published = 1 '
		. 'AND categories.published = 1 '
		. 'AND brands.published = 1';
	$query = $this->db->query($sql);
	//return $query->row_array();

	$product = $query->row_array();
	if (!$product) {
	  continue;
	}
	$product['order_amount'] = $cart_product[1];
	$product['cart_item_type'] = config_item('_cart_item_types')['product'];
	$data['products'][] = $product;
      }
    }

    if ($cart_packs != '' && count($cart_packs) > 0) {
      foreach ($cart_packs as $cart_pack) {
	//$this->db->where('id', $cart_pack[0]);
	//$query = $this->db->get('packs');

	$sql = 'SELECT DISTINCT packs.*, '
		. 'products.id AS product_id, '
		. 'products.slug AS slug, '
		. 'products.name AS product_name, '
		. 'categories.slug AS category_slug, '
		. 'brands.name AS brand_name '
		. 'FROM packs '
		. 'LEFT JOIN products '
		. 'ON packs.product_id = products.id '
		. 'LEFT JOIN categories '
		. 'ON products.category_id = categories.id '
		. 'LEFT JOIN brands '
		. 'ON products.brand_id = brands.id '
		. 'WHERE packs.id = "' . $cart_pack[0] . '" '
		. 'AND packs.published = 1 '
		. 'AND products.published = 1 '
		. 'AND brands.published = 1 '
		. 'AND categories.published = 1 ';
	$query = $this->db->query($sql);

	$pack = $query->row_array();
	if (!$pack) {
	  continue;
	}
	$pack['order_amount'] = $cart_pack[1];
	$pack['cart_item_type'] = config_item('_cart_item_types')['pack'];
	$data['packs'][] = $pack;
      }
    }

    $data['cart_items'] = array_merge($data['products'], $data['packs']);

    $data['cart_total_price'] = 0;
    foreach ($data['cart_items'] as $cart_item):
      $data['cart_total_price'] += $cart_item['price'] * $cart_item['order_amount'];
    endforeach;


    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'trim|max_length[96]');
    //$this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[11]|max_length[11]');
    $this->form_validation->set_rules('phone', 'Phone', 'trim|required|cart_phone');
    $this->form_validation->set_rules('address', 'Address', 'trim|max_length[1500]');

    if ($this->form_validation->run() === FALSE) {

      $this->session->set_userdata('cart_request_id', time());

      $this->load->view('templates/public/header', $data);
      $this->load->view('templates/public/cart_view', $data);
      $this->load->view('templates/public/recently_seen_products_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    // we don't want repeat of request by reloading or updating page
    if ($this->input->post('cart_request_id') != $this->session->userdata('cart_request_id')) { // here checking strings, not numbers )))
      redirect('/catalog');
    }

    $name = Baza::encode_plain_string_to_db($this->input->post('name'));
    $phone = Baza::encode_plain_string_to_db($this->input->post('phone'));
    $address = Baza::encode_plain_string_to_db($this->input->post('address'));

    // we will leave only digits in phone and delete country code
    // for db phone mask will be - 9999999999 (10 digits)
    $patt = '/[\ \(\)\+]/';
    $phone = preg_replace($patt, '', $phone);
    $phone = mb_substr($phone, 1);


    $db_data['phone'] = $phone;

    if ($name != '') {
      $db_data['name'] = $name;
    }
    if ($address != '') {
      $db_data['address'] = $address;
    }

    $cart_products_cookie_value = Baza::encode_plain_string_to_db(get_cookie('cart_products', true));
    $cart_packs_cookie_value = Baza::encode_plain_string_to_db(get_cookie('cart_packs', true));

    $this->load->model('cart_model');
    $this->cart_model->update_user_phone_address($db_data);

    if (strlen($cart_products_cookie_value) > 0) {
      $db_data['products'] = $cart_products_cookie_value;
    }
    if (strlen($cart_packs_cookie_value) > 0) {
      $db_data['packs'] = $cart_packs_cookie_value;
    }
    $db_data['user_id'] = $this->session->userdata('user')['id'];
    $db_data['time'] = time();

    if (!$this->cart_model->create_order($db_data)) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('templates/public/cart_send_success_failure', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    $_SESSION['user']['name'] = $name;
    $_SESSION['user']['phone'] = $phone;
    $_SESSION['user']['address'] = $address;

    delete_cookie('cart_products');
    delete_cookie('cart_packs');
    // send emails to particular persons
    $this->send_email_about_order_request();

    $this->session->set_userdata('cart_request_id', time());


    $this->load->view('templates/public/header', $data);
    $this->load->view('templates/public/cart_send_success_view', $data);
    $this->load->view('templates/public/footer', $data);
  }

  private function send_email_about_order_request() {
    $this->load->library('email');
    $this->email->from(config_item('_email_source_site_email'), config_item('_email_from'));
    $this->email->to(config_item('_email_order_request_recipients'));
    //$this->email->cc('another@another-example.com');
    //$this->email->bcc('them@their-example.com');
    $this->email->subject('Habano Aficionado Order Request');
    $msg = 'С сайта ' . site_url() . ' пришла заявка о резерве! Проверьте данные по адресу ' . site_url('admin/orders');
    $this->email->message($msg);
    if (!$this->email->send()) {
      return false;
    }
    return true;
  }

}
