<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

  public function index() {
    
    $data['metatags']['title'] = config_item('_metatags')['index']['title'];
    $data['metatags']['description'] = config_item('_metatags')['index']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['index']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['index']['robots'];

    $this->load->library('banner');
    $data['banner'] = $this->banner->get();

    $this->load->model('blog_model');
    $data['four_items'] = $this->blog_model->get4items();

    $this->load->model('brands_model');
    $brand_of_day_id = $this->brands_model->get_brand_of_day_id();

    if ($brand_of_day_id) {
      $data['brand'] = $this->brands_model->get_brand_by_id($brand_of_day_id);
      $data['products'] = $this->brands_model->get_cigars_by_brand_id($brand_of_day_id, config_item('_cigars_items_per_page'), 0);
    }


    $this->load->view('templates/public/header', $data);
    $this->load->view('templates/public/banner');
    
    $this->load->library('cigar_filter_ajax');
    
    $this->load->view('templates/public/featured_4_items', $data);
    if ($brand_of_day_id) {
      $this->load->view('brands/brand_of_day_before_cigars', $data);
      $this->load->view('templates/public/cigars_array', $data);
      $this->load->view('brands/brand_of_day_after_cigars', $data);
    }
    $this->load->view('templates/public/footer', $data);
  }

  public function error404() {
    $this->load->view('errors/my_error_404_view');
  }

}
