<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// class
class Auth extends CI_Controller {

  public function user_profile() {
    if (!$this->session->has_userdata('user')) {
      show_404();
    }
    $data['metatags']['title'] = config_item('_metatags')['userprofile']['title'];
    $data['metatags']['description'] = config_item('_metatags')['userprofile']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['userprofile']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['userprofile']['robots'];

    $this->load->view('templates/public/header', $data);
    $this->load->view('auth/user_profile_view', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function confirm_reset_password() {
    if ($this->session->has_userdata('user')) {
      redirect('/login');
    }

    $data['metatags']['title'] = config_item('_metatags')['rstpwdconf']['title'];
    $data['metatags']['description'] = config_item('_metatags')['rstpwdconf']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['rstpwdconf']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['rstpwdconf']['robots'];


    $this->load->model('auth_model');

    // update old and not confirmed resets from db back to normal state
    $this->auth_model->update_old_temp_pwds_to_null();

    $accepted_code = Baza::encode_plain_string_to_db($this->uri->segment(2));
    //echo $accepted_code;

    $users = $this->auth_model->get_all_pwd_resets();

    $reseted_password_user_id = NULL;

    foreach ($users as $user) :
      if ($user['reg_confirm_code'] === $accepted_code) {
        $reseted_password_user_id = $user['id'];
      }
    endforeach;

    //print_r($users);

    if ($reseted_password_user_id === NULL) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/rst_pwd_conf_failure_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    if ($this->auth_model->set_new_password($reseted_password_user_id)) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/rst_pwd_conf_success_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    $this->load->view('templates/public/header', $data);
    $this->load->view('auth/rst_pwd_conf_failure_view', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function confirm_registration() {
    if ($this->session->has_userdata('user')) {
      redirect('/login');
    }

    $data['metatags']['title'] = config_item('_metatags')['regconf']['title'];
    $data['metatags']['description'] = config_item('_metatags')['regconf']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['regconf']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['regconf']['robots'];

    // delete old and not confirmed registrations from db
    $expired = time() - config_item('_reg_conf_time_limit');
    $this->db->where('reg_time < ', $expired);
    $this->db->where('reg_confirmed', 0);
    $this->db->delete('users');


    $accepted_code = Baza::encode_plain_string_to_db($this->uri->segment(2));

    $this->load->model('auth_model');
    $users = $this->auth_model->get_all_unconfirmed_users();
    $confirmed_user_id = NULL;
    foreach ($users as $user) :
      if ($user['reg_confirm_code'] === $accepted_code) {
        $confirmed_user_id = $user['id'];
      }
    endforeach;

    if ($confirmed_user_id === NULL) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/reg_conf_failure_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    if ($this->auth_model->set_reg_confirmation($confirmed_user_id)) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/reg_conf_success_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    $this->load->view('templates/public/header', $data);
    $this->load->view('auth/reg_conf_failure_view', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function reset_password() {
    $data['metatags']['title'] = config_item('_metatags')['rstpwd']['title'];
    $data['metatags']['description'] = config_item('_metatags')['rstpwd']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['rstpwd']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['rstpwd']['robots'];

    $this->load->model('auth_model');

    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[6]|max_length[128]|valid_email');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|max_length[32]|pwd_regexp');
    $this->form_validation->set_rules('captcha', 'Captcha', array('trim', 'required', 'min_length[2]', 'max_length[8]', array('captcha', array($this->baza, 'check_captcha_exists'))));

    // we don't delete old captcha here because of we delete it when register
    // 
    // creating captcha
    $this->load->helper('captcha');
    $cap_conf = config_item('_captcha_config');
    $cap_conf['img_url'] = '/assets/captcha/';
    $cap = create_captcha($cap_conf);
    $data['captcha_image'] = $cap['image'];
    $data['captcha_word'] = $cap['word'];
    $data['captcha_time'] = $cap['time'];

    $cap_db_data = array(
        'time' => $cap['time'],
        'ip' => $this->input->ip_address(),
        'word' => $cap['word']
    );

    $query = $this->db->insert_string('captcha', $cap_db_data);
    $this->db->query($query);

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/reset_pwd_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    $input_email = Baza::encode_plain_string_to_db($this->input->post('email'));
    $new_temp_pwd = $this->input->post('password');

    // check if it is not superadmin, if user exists, and if he is fully registered
    $userdata = $this->auth_model->get_user_by_email($input_email);
    if (Baza::is_email_of_super_admin($input_email) OR ! $userdata OR $userdata['reg_confirmed'] == 0) {
      $data['reset_result'] = false;
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/reset_pwd_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    // user exists
    $new_pwd_hashed = Baza::encode_plain_string_to_db(password_hash($new_temp_pwd, PASSWORD_DEFAULT));
    $this->load->helper('string');
    $reg_confirm_code = md5($input_email) . random_string('md5'); // length = 64

    $db_data['user_id'] = $userdata['id'];
    $db_data['temp_password'] = $new_pwd_hashed;
    $db_data['reg_confirm_code'] = $reg_confirm_code;

    // preparing to send email to confirm password change
    $reset_pwd_confirmation_url = site_url() . 'password-reset-confirmation/' . $reg_confirm_code;
    $email_data['url'] = $reset_pwd_confirmation_url;
    $email_data['email'] = $input_email;

    // settin to db
    if ($this->auth_model->set_temp_password($db_data) && $this->send_email_reset_password_confirmation($email_data)) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/reset_pwd_success_view', $data);
      $this->load->view('templates/public/footer', $data);
    } else {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/reset_pwd_failure_view', $data);
      $this->load->view('templates/public/footer', $data);
    }
  }

  public function register() {
    if ($this->session->has_userdata('user')) {
      redirect('/login'); // this will show that user is logged in already
    }

    $data['metatags']['title'] = config_item('_metatags')['register']['title'];
    $data['metatags']['description'] = config_item('_metatags')['register']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['register']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['register']['robots'];

    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[6]|max_length[128]|valid_email|is_unique[users.email]', array('is_unique' => 'Данный email уже зарегистрировался.'));
    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|max_length[32]|pwd_regexp');
    $this->form_validation->set_rules('captcha', 'Captcha', array('trim', 'required', 'min_length[2]', 'max_length[8]', array('captcha', array($this->baza, 'check_captcha_exists'))));


    // delete old captcha from db
    $expired = time() - config_item('_captcha_config')['expiration'];
    $this->db->where('time < ', $expired);
    $this->db->delete('captcha');


    // creating captcha
    $this->load->helper('captcha');
    $cap_conf = config_item('_captcha_config');
    $cap_conf['img_url'] = '/assets/captcha/';
    $cap = create_captcha($cap_conf);
    $data['captcha_image'] = $cap['image'];
    $data['captcha_word'] = $cap['word'];
    $data['captcha_time'] = $cap['time'];

    $cap_db_data = array(
        'time' => $cap['time'],
        'ip' => $this->input->ip_address(),
        'word' => $cap['word']
    );

    $query = $this->db->insert_string('captcha', $cap_db_data);
    $this->db->query($query);



    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/register_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    /*
     * preparing data for database to $dbdata array
     */

    $this->dbdata['email'] = Baza::encode_plain_string_to_db($this->input->post('email'));
    $pwd = $this->input->post('password');
    $pwd_hashed = Baza::encode_plain_string_to_db(password_hash($pwd, PASSWORD_DEFAULT));
    $this->dbdata['password'] = $pwd_hashed;
    $this->load->helper('string');
    $reg_confirm_code = md5($this->dbdata['email']) . random_string('md5'); // length = 64
    $this->dbdata['reg_confirm_code'] = $reg_confirm_code;

    /*
     * now let's set to database with transaction
     */

    // preparing to send email to confirm registration
    $registration_confirmation_url = site_url() . 'registration-confirmation/' . $reg_confirm_code;
    $email_data['url'] = $registration_confirmation_url;
    $email_data['email'] = $this->dbdata['email'];

    $this->load->model("auth_model");
    if ($this->auth_model->set_user_registration($this->dbdata) && $this->send_email_register_confirmation($email_data)) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/register_success_view', $data);
      $this->load->view('templates/public/footer', $data);
    } else {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/register_failure_view', $data);
      $this->load->view('templates/public/footer', $data);
    }
  }

  private function send_email_register_confirmation($data) {
    $this->load->library('email');
    $this->email->from(config_item('_email_source_site_email'), config_item('_email_from'));
    $this->email->to($data['email']);
    //$this->email->cc('another@another-example.com');
    //$this->email->bcc('them@their-example.com');
    $this->email->subject('Подтверждение регистрации');
    $msg = 'Вы получили данное письмо, так как Ваш адрес электронной почты был указан при регистрации на сайте Habano-Aficionado.ru. Если Вы не регистрировались на данном сайте – просто удалите это письмо. Если же Вы регистрировались на данном сайте и желаете успешно завершить процедуру регистрации, пожалуйста, проследуйте по данной ссылке ' . $data['url'];
    $this->email->message($msg);
    if (!$this->email->send()) {
      return false;
    }
    return true;
  }

  private function send_email_reset_password_confirmation($data) {
    $this->load->library('email');
    $this->email->from(config_item('_email_source_site_email'), config_item('_email_from'));
    $this->email->to($data['email']);
    //$this->email->cc('another@another-example.com');
    //$this->email->bcc('them@their-example.com');
    $this->email->subject('Подтверждение смены пароля');
    $msg = 'Вы получили данное письмо, так как Ваш адрес электронной почты был указан при процедуре создания нового пароля на сайте Habano-Aficionado.ru. Если Вы не выполняли никаких действий по созданию нового пароля на данном сайте – просто удалите это письмо. Если же Вы выполняли действия по созданию нового пароля и желаете завершить процедуру – пожалуйста проследуйте по следующей ссылке ' . $data['url'];
    $this->email->message($msg);
    if (!$this->email->send()) {
      return false;
    }
    return true;
  }

  public function login() {
    $ip = $this->input->ip_address();
    if (!$this->input->valid_ip($ip)) {
      show_404();
      return;
    }

    if ($this->session->has_userdata('user')) {
      $data['metatags']['title'] = 'Logged in!';
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/login_success_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    $data['metatags']['title'] = config_item('_metatags')['login']['title'];
    $data['metatags']['description'] = config_item('_metatags')['login']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['login']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['login']['robots'];

    $this->load->model('auth_model');

    if (config_item('_ip_login_blocking_enabled')) {
      // сотрем из таблицы все устаревшие блокированные ip
      $expired = time() - config_item('_ip_login_block_time_limit');
      $this->db->where('block_time < ', $expired);
      $this->db->delete('ips');

      // сотрем из таблицы все ip, которые устарели по периоду времени на количество разов доступа, но кроме блокированных
      $expired = time() - config_item('_ip_login_access_attempts_period');
      $this->db->where('access_time < ', $expired);
      $this->db->where('block_time', NULL);
      $this->db->delete('ips');

      // пропишем сразу данный ip-вход в базу
      $this->auth_model->set_or_update_ip_to_db($ip);

      // сверим была ли попытка входа сюда с таким ip, если да, проверим некоторые данные
      $ip_data = $this->auth_model->get_ip_data($ip);
      if ($ip_data) {
        // проверим не забанен ли ??? потом это вообще будем думать и скорей всего для отдельной таблицы
        /*
          if ($ip_data['is_banned']) {
          $this->load->view('templates/public/header', $data);
          $this->load->view('auth/ip_banned_view', $data);
          $this->load->view('templates/public/footer', $data);
          return;
          } */

        $data['access_attempts_num_limit'] = config_item('_ip_login_access_attempt_max_number');
        $data['ip_block_time_limit'] = config_item('_ip_login_block_time_limit');

        // не заблокирован ли по времени
        if ($ip_data['block_time'] != NULL AND $ip_data['block_time'] > time() - config_item('_ip_login_block_time_limit')) {
          $this->load->view('templates/public/header', $data);
          $this->load->view('auth/access_limit_excess_view', $data);
          $this->load->view('templates/public/footer', $data);
          return;
        }
      }
      // итак... данный ip не забанен ???
      // входит 1-й раз или же не превысил максимального значения входов
    }


    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[6]|max_length[128]|valid_email');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|max_length[32]|pwd_regexp');
    
    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/login_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }

    $data['login_success'] = false; // this is for view

    $input_email = Baza::encode_plain_string_to_db($this->input->post('email'));
    $pwd = $this->input->post('password');

    $user_data = $this->auth_model->get_user_by_email($input_email);
    if (!$user_data || $user_data['reg_confirmed'] == 0) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/login_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }
    if (!password_verify($pwd, $user_data['password'])) {
      $this->load->view('templates/public/header', $data);
      $this->load->view('auth/login_view', $data);
      $this->load->view('templates/public/footer', $data);
      return;
    }
    //storing data about user to session
    $this->session->set_userdata('user', $user_data); // set_userdata is a CI session lib method
    // checks if super admin
    if ($this->baza->is_super_admin($input_email, $pwd)) {
      $_SESSION['user']['is_super_admin'] = TRUE;
    }

    // not to be seen in session array
    $_SESSION['user']['password'] = '';

    $this->load->view('templates/public/header', $data);
    $this->load->view('auth/login_success_view', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function logout() {
    if (!$this->session->has_userdata('user')) {
      //show_404();
      redirect('/login');
    }
    $data['metatags']['title'] = 'Выход';
    $data['metatags']['description'] = 'Выход';
    $data['metatags']['keywords'] = 'Выход';
    $data['metatags']['robots'] = 'noindex, nofollow';

    // deleting data about user $_SESSION['user'] in CI maner
    $this->session->unset_userdata('user');
    $this->load->view('templates/public/header', $data);
    $this->load->view('auth/logout_view', $data);
    $this->load->view('templates/public/footer', $data);
  }

}

// end of class