<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends CI_Controller {

  public function index() {
    //$this->output->enable_profiler(TRUE);
    
    $data['metatags']['title'] = config_item('_metatags')['catalog']['title'];
    $data['metatags']['description'] = config_item('_metatags')['catalog']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['catalog']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['catalog']['robots'];


    $this->load->library('banner');
    $data['banner'] = $this->banner->get();

    $this->load->model('categories_model');
    $data['categories'] = $this->categories_model->get_categories_for_slider();


    $this->load->view('templates/public/header', $data);
    $this->load->view('templates/public/all_cats', $data);
    
    $this->load->library('cigar_filter_ajax');

    $this->load->view('templates/public/banner', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function product_category($product_group_slug = NULL) {

    $this->load->model('categories_model');
    $data['category'] = $this->categories_model->get_category_by_slug($product_group_slug);

    if (!$data['category']) {
      show_404();
    }

    $data['metatags']['title'] = Baza::decode_plain_string_from_db($data['category']['meta_title']);
    $data['metatags']['description'] = Baza::decode_plain_string_from_db($data['category']['meta_description']);
    $data['metatags']['keywords'] = Baza::decode_plain_string_from_db($data['category']['meta_keywords']);
    $data['metatags']['robots'] = Baza::decode_plain_string_from_db($data['category']['meta_robots']);


    // надо определить сигарная это категория или нет по slug значению
    // если сигарная, то выборка и вывод один
    // если не сигарная то другой

    $is_cigar_cat = $data['category']['is_cigar_category'];

    if ($is_cigar_cat) {
      $num_of_products = $this->categories_model->count_cigars_by_category_id($data['category']['id']);
    } else {
      $num_of_products = $this->categories_model->count_products_by_category_id($data['category']['id']);
    }



    $this->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_products; // usually number of rows in a table out of database
    $pgn_config['per_page'] = $is_cigar_cat == 1 ? config_item("_cigars_items_per_page") : config_item("_products_items_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];

    if ($is_cigar_cat) {
      $data['products'] = $this->categories_model->get_cigars_by_category_id($data['category']['id'], $limit, $offset);
    } else {
      $data['products'] = $this->categories_model->get_products_by_category_id($data['category']['id'], $limit, $offset);
    }

    if (count($data['products']) < 1 && !empty($this->input->get($pgn_config['query_string_segment']))): // page
      redirect(current_url());
    endif;
    //$data['row_counter'] = $offset + 1;
    //$data['num_of_cigars'] = $num_of_products;


    $this->load->view('templates/public/header', $data);
    $this->load->view('categories/category_view', $data);
    /*
      if ($product_group_slug == 'cigars') {
      $this->load->view('templates/public/filter_block', $data);
      }
     * 
     */
    if ($is_cigar_cat) {
      $this->load->view('templates/public/cigars_array', $data);
    } else {
      $this->load->view('templates/public/products_array', $data);
    }

    $this->load->view('templates/public/pagination', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function product($product_group_slug = NULL, $product_slug = NULL) {
    $this->load->model('products_model');
    $product = $this->products_model->get_product_by_category_slug_and_product_slug($product_group_slug, $product_slug);
    if (!$product) {
      show_404();
    }

    $packs = $this->products_model->get_packs_by_product_id($product['id']);

    $data['product'] = $product;
    $data['packs'] = $packs;

    $data['metatags']['title'] = Baza::decode_plain_string_from_db($data['product']['meta_title']);
    $data['metatags']['description'] = Baza::decode_plain_string_from_db($data['product']['meta_description']);
    $data['metatags']['keywords'] = Baza::decode_plain_string_from_db($data['product']['meta_keywords']);
    $data['metatags']['robots'] = Baza::decode_plain_string_from_db($data['product']['meta_robots']);
    
    $this->load->library('recently_seen_products');
    $this->recently_seen_products->set($product);

    $this->load->view('templates/public/header', $data);
    if ($data['product']['is_cigar_product'] == 1) {
      $this->load->view('templates/public/product_cigar_view', $data);
    }
    else {
      $this->load->view('templates/public/product_common_view', $data);
    }
    if (!empty($data['packs'])) {
      $this->load->view('templates/public/product_packs_view', $data);
    }
    $this->load->view('templates/public/footer', $data);
  }

}
