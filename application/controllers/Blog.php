<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

  // ========================================= //

  public function index() {
    
    $data['title'] = $this->lang->line('habano_blog_page_title');
    
    $data['metatags']['title'] = config_item('_metatags')['blog']['title'];
    $data['metatags']['description'] = config_item('_metatags')['blog']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['blog']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['blog']['robots'];
    
    $this->load->model('blog_model');

    $this->db->where('published', '1');
    $this->db->where('slug !=', config_item('_reserved_section_slug'));
    $this->db->from('sections');
    $num_of_sections = $this->db->count_all_results();

    $this->load->library('pagination');
    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_sections; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_blog_sections_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['sections'] = $this->blog_model->get_sections($limit, $offset);

    //$config['total_rows'] = count($data['sections']); // usually number of rows out of database
    //$idiom = "english";
    //$this->lang->load('habano', $idiom);
    //$this->lang->load('habano'); // autoloaded now
    //$data['habano_create_new_blog_section'] = $this->lang->line('habano_create_new_blog_section');
    //$data['habano_blog_section_read_more'] = $this->lang->line('habano_blog_section_read_more');

    $this->load->view('templates/public/header', $data);
    $this->load->view('blog/view_sections', $data);
    $this->load->view('templates/public/footer', $data);
  }

  // ========================================= //

  public function section($slug = NULL) {
    if ($slug == config_item('_reserved_section_slug')) {
      show_404();
    }
    $this->load->model('blog_model');
    // проверим есть ли в sections такой slug и получим данные
    $section = $this->blog_model->section_by_slug($slug);
    if (!$section) {
      show_404();
    }

    // вытащим id (секции)
    $section_id = $section->id;

    $data['slug'] = $slug;
    $data['id'] = Baza::decode_plain_string_from_db($section_id);
    $data['title'] = Baza::decode_plain_string_from_db($section->title);
    $data['intro'] = Baza::decode_plain_string_from_db($section->intro);
    $data['img'] = Baza::decode_plain_string_from_db($section->img);
    
    $data['before_body'] = Baza::decode_html_string_from_db($section->before_body); // html
    $data['body'] = Baza::decode_html_string_from_db($section->body); // html
    $data['after_body'] = Baza::decode_html_string_from_db($section->after_body); // html
    
    $data['published'] = Baza::decode_plain_string_from_db($section->published);
    
    $data['metatags']['title'] = Baza::decode_plain_string_from_db($section->meta_title);
    $data['metatags']['description'] = Baza::decode_plain_string_from_db($section->meta_description);
    $data['metatags']['keywords'] = Baza::decode_plain_string_from_db($section->meta_keywords);
    $data['metatags']['robots'] = Baza::decode_plain_string_from_db($section->meta_robots);


    // проверим сколько articles в данном section, т.е. имеет ли смысл выполнять поиск
    // и за одно вычислим параметр для pagination

    $res = $this->db->query("SELECT COUNT(*) AS num FROM articles WHERE section_id = $section_id");
    $num_of_articles_in_section = $res->row()->num;
    if ($num_of_articles_in_section === 0) {
      echo "no articles found in this blog section";
    } else {
      //echo $num_of_articles_in_section . " articles found in this section";
    }

    $this->load->library('pagination');
    $config['base_url'] = current_url();
    $config['page_query_string'] = TRUE;
    //$config['num_links'] = 3;
    $config['use_page_numbers'] = TRUE;
    $config['query_string_segment'] = 'page';
    $config['total_rows'] = $num_of_articles_in_section;
    $config['per_page'] = config_item('_blog_articles_per_page'); // items on page
    $this->pagination->initialize($config);

    $limit = $config['per_page'];
    $page_index = $this->input->get($config['query_string_segment']);
    $page_index = intval($page_index);
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $config['per_page'] - $config['per_page'];
    $data['articles'] = $this->blog_model->get_articles($section_id, $limit, $offset);

    //$config['total_rows'] = count($data['sections']); // usually number of rows out of database
    //$idiom = "english";
    //$this->lang->load('habano', $idiom);
    //$this->lang->load('habano'); // autoloaded now
    //$data['habano_create_new_blog_section'] = $this->lang->line('habano_create_new_blog_section');

    $this->load->view('templates/public/header', $data);
    $this->load->view('blog/view_articles', $data);
    $this->load->view('templates/public/footer', $data);

    //echo "section : " . $section_slug;
  }

  // ========================================= //

  public function article($section_slug = NULL, $article_slug = NULL) {
    if ($section_slug == config_item('_reserved_section_slug')) {
      show_404();
    }
    $this->load->model('blog_model');
    // • проверим есть ли в sections такой slug и получим id
    $section_id = $this->blog_model->section_id_by_slug($section_slug);
    if (!$section_id) {
      show_404();
    }

    // • проверим есть ли в articles такой slug и получим id
    $article_id = $this->blog_model->article_id_by_slug($article_slug);
    if (!$article_id) {
      show_404();
    }

    // • найдем название title секции по id секции
    $section_title = $this->baza->get_item_by_related_item('title', 'id', $section_id, 'sections');

    // • попробуем извлечь нужный article по ее slug если он соответствует section_id
    $article = $this->blog_model->get_article_by_id_and_section_id($article_id, $section_id);
    if ($article === FALSE) {
      show_404();
    }

    if (count($article) === 0) {
      show_404();
    }

    // • подготовим данные к выводу

    $data['section_id'] = $section_id;
    $data['section_slug'] = $section_slug;
    $data['section_title'] = $section_title;

    $data['back_link'] = '/blog/' . $section_slug;

    $data['id'] = $article_id;
    $data['slug'] = $article_slug;
    $data['title'] = Baza::decode_plain_string_from_db($article->title);
    $data['intro'] = Baza::decode_plain_string_from_db($article->intro);
    
    $data['before_body'] = Baza::decode_html_string_from_db($article->before_body);
    $data['body'] = Baza::decode_html_string_from_db($article->body);
    $data['after_body'] = Baza::decode_html_string_from_db($article->after_body);
    
    $data['img'] = Baza::decode_plain_string_from_db($article->img);
    $data['published'] = $article->published;
    $data['featured'] = $article->featured;

    $data['metatags']['title'] = Baza::decode_plain_string_from_db($article->meta_title);
    $data['metatags']['description'] = Baza::decode_plain_string_from_db($article->meta_description);
    $data['metatags']['keywords'] = Baza::decode_plain_string_from_db($article->meta_keywords);
    $data['metatags']['author'] = Baza::decode_plain_string_from_db($article->meta_author);
    $data['metatags']['robots'] = $article->meta_robots;

    $data['created_at'] = $article->created_at;
    $data['lifted_up_at'] = $article->lifted_up_at;
    $data['modified_at'] = $article->modified_at;

    $this->load->view('templates/public/header', $data);
    $this->load->view('blog/article_view', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function uncategorized_article() {
    $this->load->model('blog_model');
    $uncat_section_id = $this->blog_model->section_id_by_slug(config_item('_reserved_section_slug'));
    if (!$uncat_section_id) {
      show_404();
    }

    $articles = $this->blog_model->get_articles($uncat_section_id, 0, 0); // section_id, offset,limit // in other words all
    $matched = FALSE;
    foreach ($articles as $article) :
      if ($article['slug'] == $this->uri->segment(1)) {
	$matched = TRUE;
	$page_article = $article;
	break;
      }
    endforeach;
    if (!$matched) {
      show_404();
    }

    // • подготовим данные к выводу
    $data['id'] = $page_article['id'];
    $data['slug'] = $page_article['slug'];
    $data['title'] = Baza::decode_plain_string_from_db($page_article['title']);
    $data['intro'] = Baza::decode_plain_string_from_db($page_article['intro']);
    
    $data['before_body'] = Baza::decode_html_string_from_db($page_article['before_body']);
    $data['body'] = Baza::decode_html_string_from_db($page_article['body']);
    $data['after_body'] = Baza::decode_html_string_from_db($page_article['after_body']);
    
    $data['img'] = Baza::decode_plain_string_from_db($page_article['img']);
    $data['published'] = $page_article['published'];
    $data['featured'] = $page_article['featured'];

    $data['metatags']['title'] = Baza::decode_plain_string_from_db($page_article['meta_title']);
    $data['metatags']['description'] = Baza::decode_plain_string_from_db($page_article['meta_description']);
    $data['metatags']['keywords'] = Baza::decode_plain_string_from_db($page_article['meta_keywords']);
    $data['metatags']['author'] = Baza::decode_plain_string_from_db($page_article['meta_author']);
    $data['metatags']['robots'] = $page_article['meta_robots'];

    $data['created_at'] = $page_article['created_at'];
    $data['lifted_up_at'] = $page_article['lifted_up_at'];
    $data['modified_at'] = $page_article['modified_at'];

    $this->load->view('templates/public/header', $data);
    $this->load->view('blog/article_page_view', $data);
    $this->load->view('templates/public/footer', $data);
  }

}
