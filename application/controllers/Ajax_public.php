<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_public extends CI_Controller {
    /* ==================================================================== */
    /* ==================================================================== */

    public function __construct() {
        parent::__construct();

        if (!$this->input->is_ajax_request()) {
            show_404();
            exit;
        }
    }

    public function get_cigars_by_params() {

        $flt_country = Baza::encode_plain_string_to_db($this->input->post('country'));
        $flt_brand_id = Baza::encode_plain_string_to_db($this->input->post('brand'));
        $flt_vitola = Baza::encode_plain_string_to_db($this->input->post('vitola'));
        $flt_line = Baza::encode_plain_string_to_db($this->input->post('line'));
        $flt_length = Baza::encode_plain_string_to_db($this->input->post('length'));
        $flt_cepo = Baza::encode_plain_string_to_db($this->input->post('cepo'));
        $flt_intensity = Baza::encode_plain_string_to_db($this->input->post('intensity'));
        $flt_wrapper = Baza::encode_plain_string_to_db($this->input->post('wrapper'));
        $flt_flavour = Baza::encode_plain_string_to_db($this->input->post('flavour'));
        $flt_price_max = Baza::encode_plain_string_to_db($this->input->post('price_max'));
        $flt_current_request_number = Baza::encode_plain_string_to_db($this->input->post('current_request_number'));

        $params = array(
            'flt_country' => $flt_country,
            'flt_brand_id' => $flt_brand_id,
            'flt_vitola' => $flt_vitola,
            'flt_line' => $flt_line,
            'flt_length' => $flt_length,
            'flt_cepo' => $flt_cepo,
            'flt_intensity' => $flt_intensity,
            'flt_wrapper' => $flt_wrapper,
            'flt_flavour' => $flt_flavour,
            'flt_price_max' => $flt_price_max
        );

        $this->load->model('products_model');
        $number_of_found_cigars = $this->products_model->count_search_results_by_params($params);

        $params['cigars_at_once'] = config_item('_cigars_items_per_page');
        // $params['cigars_at_once'] = 2; // for testing
        $params['current_request_number'] = $flt_current_request_number;

        $data['params'] = $params;

        $products = $this->products_model->get_cigars_by_filter_params($params);

        $data['products'] = $products;

        if ($flt_current_request_number == 1) {
            $this->load->model('brands_model');
            $brand = $this->brands_model->get_brand_by_id($flt_brand_id);
            $data['params']['flt_brand_name'] = $brand['name'];
            $this->load->view('templates/public/before_cigar_ajax_array', $data);
        }

        $this->load->view('templates/public/cigars_ajax_array', $data);

        if ($flt_current_request_number * $params['cigars_at_once'] < $number_of_found_cigars) {
            $this->load->view('templates/public/filter_show_further_button', $data);
        }
    }

    public function count_search_results() {

        $flt_country = Baza::encode_plain_string_to_db($this->input->post('country'));
        $flt_brand_id = Baza::encode_plain_string_to_db($this->input->post('brand'));
        $flt_vitola = Baza::encode_plain_string_to_db($this->input->post('vitola'));
        $flt_line = Baza::encode_plain_string_to_db($this->input->post('line'));
        $flt_length = Baza::encode_plain_string_to_db($this->input->post('length'));
        $flt_cepo = Baza::encode_plain_string_to_db($this->input->post('cepo'));
        $flt_intensity = Baza::encode_plain_string_to_db($this->input->post('intensity'));
        $flt_wrapper = Baza::encode_plain_string_to_db($this->input->post('wrapper'));
        $flt_flavour = Baza::encode_plain_string_to_db($this->input->post('flavour'));
        $flt_price_max = Baza::encode_plain_string_to_db($this->input->post('price_max'));


        $params = array(
            'flt_country' => $flt_country,
            'flt_brand_id' => $flt_brand_id,
            'flt_vitola' => $flt_vitola,
            'flt_line' => $flt_line,
            'flt_length' => $flt_length,
            'flt_cepo' => $flt_cepo,
            'flt_intensity' => $flt_intensity,
            'flt_wrapper' => $flt_wrapper,
            'flt_flavour' => $flt_flavour,
            'flt_price_max' => $flt_price_max
        );

        $this->load->model('products_model');
        $num = $this->products_model->count_search_results_by_params($params);
        echo $num;
    }

    public function get_picked_filter_window() {
        if ($this->input->post('window_name') === NULL) {
            show_404();
        }
        $window_name = Baza::decode_plain_string_from_db($this->input->post('window_name'));
        switch ($window_name) {
            case 'intensities':
                $this->load->view('templates/public/filter_ajax_window_intensities');
                break;
            case 'wrappers':
                $this->load->view('templates/public/filter_ajax_window_wrappers');
                break;
            case 'flavours':
                $this->load->view('templates/public/filter_ajax_window_flavours');
                break;
            case 'lines':
                $this->load->view('templates/public/filter_ajax_window_lines');
                break;
            case 'lengths':
                $this->load->view('templates/public/filter_ajax_window_lengths');
                break;
            case 'countries':
                $this->load->view('templates/public/filter_ajax_window_countries');
                break;
            case 'cepos':
                $this->load->view('templates/public/filter_ajax_window_cepos');
                break;
            case 'vitolas':
                $this->load->view('templates/public/filter_ajax_window_vitolas');
                break;
            case 'brands':
                $this->load->view('templates/public/filter_ajax_window_brands');
                break;
            case 'prices':
                $this->load->view('templates/public/filter_ajax_window_prices');
                break;
            default:
                show_404();
        }
    }

    public function get_picked_filter_array() {
        if ($this->input->post('array_name') === NULL) {
            show_404();
        }
        $array_name = Baza::decode_plain_string_from_db($this->input->post('array_name'));
        switch ($array_name) {
            case 'intensities':
                $this->get_intensities_array();
                break;
            case 'wrappers':
                $this->get_wrappers_array();
                break;
            case 'flavours':
                $this->get_flavours_array();
                break;
            case 'lines':
                $this->get_lines_array();
                break;
            case 'lengths':
                $this->get_lengths_array();
                break;
            case 'countries':
                $this->get_countries_array();
                break;
            case 'cepos':
                $this->get_cepos_array();
                break;
            case 'vitolas':
                $this->get_vitolas_array();
                break;
            case 'brands':
                $this->get_brands_array();
                break;
            case 'prices':
                $this->get_prices_array();
                break;
            default :
                show_404();
        }
    }

    private function get_prices_array() {
        $sql = 'SELECT MIN(price) AS min_price '
                . 'FROM products '
                . 'LEFT JOIN categories ON products.category_id = categories.id '
                . 'LEFT JOIN brands ON products.brand_id = brands.id '
                . 'WHERE products.is_cigar_product = 1 '
                . 'AND products.published = 1 '
                . 'AND products.price > 0 '
                . 'AND categories.published = 1 '
                . 'AND brands.published = 1';
        $query = $this->db->query($sql);

        $min = $query->row()->min_price;

        $sql = 'SELECT MAX(price) AS max_price '
                . 'FROM products '
                . 'LEFT JOIN categories ON products.category_id = categories.id '
                . 'LEFT JOIN brands ON products.brand_id = brands.id '
                . 'WHERE products.is_cigar_product = 1 '
                . 'AND products.published = 1 '
                . 'AND products.price > 0 '
                . 'AND categories.published = 1 '
                . 'AND brands.published = 1';
        $query = $this->db->query($sql);

        $max = $query->row()->max_price;

        echo '[' . $min . ',' . $max . ']';
    }

    private function get_brands_array() {
        $this->load->model('brands_model');
        $brands_array = $this->brands_model->get_cigar_brands();
        //print_r($brands_array);
        $brands_string_for_js = '[';
        foreach ($brands_array as $brand) {
            $str_arr = array();
            $str_arr[] = '[';
            $str_arr[] = $brand['id'];
            $str_arr[] = ',';
            $str_arr[] = '"' . Baza::decode_plain_string_from_db($brand['name']) . '"';
            $str_arr[] = ',';
            $str_arr[] = '"' . Baza::decode_plain_string_from_db($brand['img']) . '"';
            $str_arr[] = '],';
            $brands_string_for_js .= join('', $str_arr);
        }
        $brands_string_for_js = trim($brands_string_for_js, ',');
        $brands_string_for_js .= ']';
        echo $brands_string_for_js;
    }

    private function get_vitolas_array() {
        $vitolas_string_for_js = '[';
        $counter = 0;
        foreach (config_item('_flt_vitolas') as $vitola) {
            $str_arr = array();
            $str_arr[] = '[';
            $str_arr[] = $counter;
            $str_arr[] = ',';
            $str_arr[] = '"' . $vitola['name'] . '"';
            $str_arr[] = ',';
            $str_arr[] = '"' . $vitola['img'] . '"';
            $str_arr[] = ',';
            $str_arr[] = '"' . $vitola['rg_mm'] . '"';
            $str_arr[] = ',';
            $str_arr[] = '"' . $vitola['length_mm'] . '"';
            $str_arr[] = '],';
            $vitolas_string_for_js .= join('', $str_arr);
            $counter++;
        }
        $vitolas_string_for_js = trim($vitolas_string_for_js, ',');
        $vitolas_string_for_js .= ']';
        echo $vitolas_string_for_js;
    }

    private function get_cepos_array() {
        $cepos_string_for_js = '[';
        foreach (config_item('_flt_cepos') as $cepo) {
            $str_arr = array();
            $str_arr[] = $cepo;
            $str_arr[] = ',';
            $cepos_string_for_js .= join('', $str_arr);
        }
        $cepos_string_for_js = trim($cepos_string_for_js, ',');
        $cepos_string_for_js .= ']';
        echo $cepos_string_for_js;
    }

    private function get_countries_array() {
        $countries_string_for_js = '[';
        $counter = 0;
        foreach (config_item('_flt_countries') as $country) {
            $str_arr = array();
            $str_arr[] = '[';
            $str_arr[] = $counter;
            $str_arr[] = ',';
            $str_arr[] = '"' . $country['name'] . '"';
            $str_arr[] = ',';
            $str_arr[] = '"' . $country['code'] . '"';
            $str_arr[] = ',';
            $str_arr[] = '"' . $country['img'] . '"';
            $str_arr[] = '],';
            $countries_string_for_js .= join('', $str_arr);
            $counter++;
        }
        $countries_string_for_js = trim($countries_string_for_js, ',');
        $countries_string_for_js .= ']';
        echo $countries_string_for_js;
    }

    private function get_lengths_array() {
        $lengths_string_for_js = '[';
        $counter = 0;
        foreach (config_item('_flt_lengths') as $length) {
            $str_arr = array();
            $str_arr[] = '[';
            $str_arr[] = $counter;
            $str_arr[] = ',';
            $str_arr[] = '"';
            $str_arr[] = $length['mm'] . ' mm (' . $length['inches'] . ')';
            $str_arr[] = '"';
            $str_arr[] = '],';
            $lengths_string_for_js .= join('', $str_arr);
            $counter++;
        }
        $lengths_string_for_js = trim($lengths_string_for_js, ',');
        $lengths_string_for_js .= ']';
        echo $lengths_string_for_js;
    }

    private function get_lines_array() {
        $lines_string_for_js = '[';
        $counter = 0;
        foreach (config_item('_flt_lines') as $line) {
            $str_arr = array();
            $str_arr[] = '[';
            $str_arr[] = $counter;
            $str_arr[] = ',';
            $str_arr[] = '"';
            $str_arr[] = $line['name'];
            $str_arr[] = '"';
            $str_arr[] = '],';
            $lines_string_for_js .= join('', $str_arr);
            $counter++;
        }
        $lines_string_for_js = trim($lines_string_for_js, ',');
        $lines_string_for_js .= ']';
        echo $lines_string_for_js;
    }

    private function get_flavours_array() {
        $flavours_string_for_js = '[';
        $counter = 0;
        foreach (config_item('_flt_flavours') as $flavour) {
            $str_arr = array();
            $str_arr[] = '[';
            $str_arr[] = $counter;
            $str_arr[] = ',';
            $str_arr[] = '"';
            $str_arr[] = $flavour['name'];
            $str_arr[] = '"';
            $str_arr[] = '],';
            $flavours_string_for_js .= join('', $str_arr);
            $counter++;
        }
        $flavours_string_for_js = trim($flavours_string_for_js, ',');
        $flavours_string_for_js .= ']';
        echo $flavours_string_for_js;
    }

    private function get_wrappers_array() {
        $wrappers_string_for_js = '[';
        $counter = 0;
        foreach (config_item('_flt_wrappers') as $wrapper) {
            $str_arr = array();
            $str_arr[] = '[';
            $str_arr[] = $counter;
            $str_arr[] = ',';
            $str_arr[] = '"';
            $str_arr[] = $wrapper['name'];
            $str_arr[] = '"';
            $str_arr[] = '],';
            $wrappers_string_for_js .= join('', $str_arr);
            $counter++;
        }
        $wrappers_string_for_js = trim($wrappers_string_for_js, ',');
        $wrappers_string_for_js .= ']';
        echo $wrappers_string_for_js;
    }

    private function get_intensities_array() {
        $intensities_string_for_js = '[';
        $counter = 0;
        foreach (config_item('_flt_intensities') as $intensity) {
            $str_arr = array();
            $str_arr[] = '[';
            $str_arr[] = $counter;
            $str_arr[] = ',';
            $str_arr[] = '"';
            $str_arr[] = $intensity['name'];
            $str_arr[] = '"';
            $str_arr[] = '],';
            $intensities_string_for_js .= join('', $str_arr);
            $counter++;
        }
        $intensities_string_for_js = trim($intensities_string_for_js, ',');
        $intensities_string_for_js .= ']';
        echo $intensities_string_for_js;
    }

}

// end of class

