<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends CI_Controller {

  public function index() {
    $data['metatags']['title'] = config_item('_metatags')['brands']['title'];
    $data['metatags']['description'] = config_item('_metatags')['brands']['description'];
    $data['metatags']['keywords'] = config_item('_metatags')['brands']['keywords'];
    $data['metatags']['robots'] = config_item('_metatags')['brands']['robots'];

    $this->load->model('brands_model');
    $data['brands'] = $this->brands_model->get_cigar_brands();

    $this->load->library('banner');
    $data['banner'] = $this->banner->get();

    $this->load->view('templates/public/header', $data);
    $this->load->view('brands/all_brands_view', $data);
    $this->load->view('templates/public/banner', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function brand($brand_slug = FALSE) {
    $slug = $this->uri->segment(2);
    $this->load->model('brands_model');
    $data['brand'] = $this->brands_model->get_brand_by_slug($slug);
    if (!$data['brand']) {
      show_404();
    }
    
    $data['metatags']['title'] = Baza::decode_plain_string_from_db($data['brand']['meta_title']);
    $data['metatags']['description'] = Baza::decode_plain_string_from_db($data['brand']['meta_description']);
    $data['metatags']['keywords'] = Baza::decode_plain_string_from_db($data['brand']['meta_keywords']);
    $data['metatags']['robots'] = Baza::decode_plain_string_from_db($data['brand']['meta_robots']);
    
    
    $num_of_cigars = $this->brands_model->count_cigars_by_brand_id($data['brand']['id']);

    $this->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_cigars; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_cigars_items_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];

    //$data['cigars'] = $this->categories_model->get_cigars_by_category_id($data['category']['id'], $limit, $offset);
    $data['products'] = $this->brands_model->get_cigars_by_brand_id($data['brand']['id'], $limit, $offset);

    if (count($data['products']) < 1 && !empty($this->input->get($pgn_config['query_string_segment']))): // page
      redirect(current_url());
    endif;
    //$data['row_counter'] = $offset + 1;
    //$data['num_of_cigars'] = $num_of_cigars;
    
    $this->load->view('templates/public/header', $data);
    $this->load->view('brands/brand_view', $data);
    $this->load->view('templates/public/cigars_string_line', $data);
    $this->load->view('templates/public/cigars_array', $data);
    $this->load->view('templates/public/pagination', $data);
    $this->load->view('templates/public/footer', $data);
  }

}
