<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

  public function __construct() {
    parent::__construct();


    // ===============
    // === SERVICE ===
    // ===============
    //$this->output->enable_profiler(TRUE);
    // ===============
    // ===============
    // ===============
    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  public function products_by_category() {
    $category_id = $this->uri->segment(3);
    
    $this->db->where('id',$category_id);
    $this->db->select('name');
    $query = $this->db->get('categories');
    $category_name = $query->row()->name;
    
    $data['meta_title'] = "Products of " . $category_name;
    $data['title'] = $category_name;
    
    $this->db->where('category_id',$category_id);
    $this->db->select('id');
    $query = $this->db->get('products');
    $num_of_products = count($query->result_array());
    

    $this->load->model('admin/products_model');
    $this->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_products; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item('_admin_products_per_page');
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['products'] = $this->products_model->get_products_by_category_id($category_id ,$limit, $offset);

    $data['row_counter'] = $offset + 1;
    $data['num_of_products'] = $num_of_products;

    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/products/products_by_category_head', $data);
    $this->load->view('admin/products/products_table', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function all_products() {

    $data['meta_title'] = "Products";
    $num_of_products = $this->db->count_all('products');

    $this->load->model('admin/products_model');
    $this->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_products; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item('_admin_products_per_page');
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['products'] = $this->products_model->get_products($limit, $offset);

    $data['row_counter'] = $offset + 1;
    $data['num_of_products'] = $num_of_products;

    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/products/products_all_head', $data);
    $this->load->view('admin/products/products_table', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function create_product() {
    if ($this->db->count_all('categories') < 1) {
      redirect('/admin/create-category');
    }
    if ($this->db->count_all('brands') < 1) {
      redirect('/admin/create-brand');
    }
    $data['meta_title'] = "Create Product";
    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('category_id', 'Category', 'trim|required|integer');
    $this->form_validation->set_rules('brand_id', 'Brand', 'trim|required|integer');
    $this->form_validation->set_rules('name', 'Name Of Product', 'trim|required|min_length[1]|max_length[255]');
    $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs|is_unique[products.slug]', array(
	'is_unique' => 'Such a slug is already used. Slug must be unique.'
    ));
    $this->form_validation->set_rules('is_cigar_product', 'Is Cigar Product', 'checkbox');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/products/product_create', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    $this->dbdata['category_id'] = Baza::encode_plain_string_to_db($this->input->post('category_id'));
    $this->dbdata['brand_id'] = Baza::encode_plain_string_to_db($this->input->post('brand_id'));
    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['is_cigar_product'] = $this->input->post('is_cigar_product') === 'on' ? 1 : 0;
    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));

    $this->load->model("admin/products_model");
    if ($this->products_model->set_product($this->dbdata)) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/products/product_create_success', $data);
      $this->load->view('templates/admin/footer', $data);
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/products/product_create_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

  public function product_preview() {

    $data['meta_title'] = "Preview Product";
    $data['title'] = "Preview Product";

    $id = $this->uri->segment(3);
    $this->load->model('admin/products_model');
    $product = $this->products_model->get_product_by_id($id, true);
    if (!$product) {
      show_404();
    }

    $data['id'] = $id;
    $data['slug'] = $product['slug'];
    $data['category_name'] = Baza::decode_plain_string_from_db($product['category_name']);
    $data['brand_name'] = Baza::decode_plain_string_from_db($product['brand_name']);
    $data['brand_img'] = Baza::decode_plain_string_from_db($product['brand_img']);
    $data['name'] = Baza::decode_plain_string_from_db($product['name']);
    $data['img'] = Baza::decode_plain_string_from_db($product['img']);
    $data['intro'] = Baza::decode_plain_string_from_db($product['intro']);
    $data['country'] = Baza::decode_plain_string_from_db($product['country']);
    $data['country_made'] = Baza::decode_plain_string_from_db($product['country_made']);
    $data['price'] = Baza::decode_plain_string_from_db($product['price']);
    $data['quantity'] = Baza::decode_plain_string_from_db($product['quantity']);
    $data['num_of_packs'] = Baza::decode_plain_string_from_db($product['num_of_packs']);
    if (Baza::decode_plain_string_from_db($product['images']) != NULL OR Baza::decode_plain_string_from_db($product['images']) != '') {
      $data['images'] = unserialize(Baza::decode_plain_string_from_db($product['images']));
    } else {
      $data['images'] = array();
    }

    $data['vitola'] = Baza::decode_plain_string_from_db($product['vitola']);
    $data['line'] = Baza::decode_plain_string_from_db($product['line']);
    $data['length'] = Baza::decode_plain_string_from_db($product['length']);
    $data['cepo'] = Baza::decode_plain_string_from_db($product['cepo']);
    $data['intensity'] = Baza::decode_plain_string_from_db($product['intensity']);
    $data['flavour'] = Baza::decode_plain_string_from_db($product['flavour']);
    $data['wrapper'] = Baza::decode_plain_string_from_db($product['wrapper']);

    $data['html'] = Baza::decode_html_string_from_db($product['html']); // html

    $data['published'] = Baza::decode_plain_string_from_db($product['published']);
    $data['featured'] = Baza::decode_plain_string_from_db($product['featured']);
    $data['is_cigar_product'] = Baza::decode_plain_string_from_db($product['is_cigar_product']);

    $data['meta_title'] = Baza::decode_plain_string_from_db($product['meta_title']);
    $data['meta_description'] = Baza::decode_plain_string_from_db($product['meta_description']);
    $data['meta_keywords'] = Baza::decode_plain_string_from_db($product['meta_keywords']);
    $data['meta_robots'] = Baza::decode_plain_string_from_db($product['meta_robots']);

    $this->load->view('templates/public/header', $data);
    $this->load->view('admin/products/product_preview', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function product_edit() {
    $id = $this->uri->segment(3);
    $this->load->model('admin/products_model');
    $data['product'] = $this->products_model->get_product_by_id($id);
    if (!$data['product']) {
      show_404();
    }

    // если id пользователя и id использующего не равны и это не суперадмин - нахер его
    if ($data['product']['used_by_user_id'] != null && $data['product']['used_by_user_id'] != $this->session->userdata('user')['id'] && !isset($this->session->userdata('user')['is_super_admin'])) {
      show_404();
    }

    $data['item']['img'] = Baza::decode_plain_string_from_db($data['product']['img']);
    $data['title'] = Baza::decode_plain_string_from_db($data['product']['name']);
    $data['is_cigar_product'] = Baza::decode_plain_string_from_db($data['product']['is_cigar_product']);
    $data['images'] = Baza::decode_plain_string_from_db($data['product']['images']);

    $data['meta_title'] = 'Editing product: ' . $data['title'];
    // если новая загрузка страницы (т.е. без подтверждения формы)
    if (!isset($_POST['modified_fields'])) {
      $this->load->model('admin/common_model');
      $this->common_model->set_item_used_by_user('products', $id, $this->session->userdata('user')['id']);
      $this->product_edit_new_load($id, $data);
      // если подтверждение формы
    } else {
      $data['modified_fields'] = $this->input->post('modified_fields');
      $this->product_edit_update($id, $data);
    }
  }

  private function product_edit_new_load($id, $data) {
    // article data here

    $data['item']['id'] = $id;
    $data['item']['category_id'] = Baza::decode_plain_string_from_db($data['product']['category_id']);
    $data['item']['brand_id'] = Baza::decode_plain_string_from_db($data['product']['brand_id']);
    $data['item']['country_made'] = Baza::decode_plain_string_from_db($data['product']['country_made']);

    $data['item']['name'] = Baza::decode_plain_string_from_db($data['product']['name']);
    $data['item']['slug'] = $data['product']['slug'];

    $data['item']['intro'] = Baza::decode_plain_string_from_db($data['product']['intro']);
    //$data['item']['img'] = Baza::decode_plain_string_from_db($data['product']['img']);

    $data['item']['images'] = Baza::decode_plain_string_from_db($data['product']['images']);

    $data['item']['html'] = Baza::decode_html_string_from_db($data['product']['html']); // html

    $data['item']['published'] = Baza::decode_plain_string_from_db($data['product']['published']);
    $data['item']['featured'] = Baza::decode_plain_string_from_db($data['product']['featured']);
    $data['item']['is_cigar_product'] = Baza::decode_plain_string_from_db($data['product']['is_cigar_product']);

    $data['item']['vitola'] = Baza::decode_plain_string_from_db($data['product']['vitola']);
    $data['item']['line'] = Baza::decode_plain_string_from_db($data['product']['line']);
    $data['item']['length'] = Baza::decode_plain_string_from_db($data['product']['length']);
    $data['item']['cepo'] = Baza::decode_plain_string_from_db($data['product']['cepo']);
    $data['item']['intensity'] = Baza::decode_plain_string_from_db($data['product']['intensity']);
    $data['item']['flavour'] = Baza::decode_plain_string_from_db($data['product']['flavour']);
    $data['item']['wrapper'] = Baza::decode_plain_string_from_db($data['product']['wrapper']);

    $data['item']['price'] = Baza::decode_plain_string_from_db($data['product']['price']);
    $data['item']['quantity'] = Baza::decode_plain_string_from_db($data['product']['quantity']);

    $data['item']['meta_title'] = Baza::decode_plain_string_from_db($data['product']['meta_title']);
    $data['item']['meta_description'] = Baza::decode_plain_string_from_db($data['product']['meta_description']);
    $data['item']['meta_keywords'] = Baza::decode_plain_string_from_db($data['product']['meta_keywords']);
    $data['item']['meta_robots'] = Baza::decode_plain_string_from_db($data['product']['meta_robots']);


    $data['title'] = $data['item']['name'];
    $data['meta_title'] = "Category edit: " . $data['item']['name'];

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/products/product_edit', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  private function product_edit_update($id, $data) {

    $data['item']['id'] = $id;
    $data['item']['is_cigar_product'] = $data['is_cigar_product'];
    $data['item']['images'] = $data['images'];

    // now form validation goes
    $this->load->library('form_validation');


    $this->form_validation->set_rules('category_id', 'Category', 'trim|required|integer');
    $this->form_validation->set_rules('brand_id', 'Brand', 'trim|required|integer');
    $this->form_validation->set_rules('country_made', 'Country of Rea; Production', 'trim|required|integer|greater_than[-1]|less_than[' . count(config_item('_flt_countries')) . ']');
    $this->form_validation->set_rules('name', 'Name Of Product', 'trim|required|min_length[1]|max_length[255]');

    $this->form_validation->set_rules('price', 'Price', 'trim|required|integer|greater_than[-1]|less_than[16777215]');
    $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|integer|greater_than[-1]|less_than[9999]');

    // если значение slug в массиве POST равен значению в базе, то проверять на уникальнсть не будем и обновлять значение тоже
    if ($this->input->post('slug') === $data['product']['slug']) {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    } else {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs|is_unique[products.slug]', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    }

    $this->form_validation->set_rules('published', 'Published', 'checkbox');
    $this->form_validation->set_rules('featured', 'Featured', 'checkbox');
    $this->form_validation->set_rules('is_cigar_product', 'Is Cigar Product', 'checkbox');
    $this->form_validation->set_rules('meta_robots', 'Meta robots', 'required|robots_metatag');


    // only for cigar products
    if ($data['item']['is_cigar_product'] == 1) {
      $this->form_validation->set_rules('vitola', 'Vitola', 'trim|required|integer|greater_than[-1]|less_than[' . count(config_item('_flt_vitolas')) . ']');
      $this->form_validation->set_rules('line', 'Lines', 'trim|required|integer');
      $this->form_validation->set_rules('length', 'Length', 'trim|required|integer');
      $this->form_validation->set_rules('cepo', 'Cepo', 'trim|required|integer');
      $this->form_validation->set_rules('intensity', 'Intensity', 'trim|required|integer');
      $this->form_validation->set_rules('flavour', 'Flavour', 'trim|required|integer');
      $this->form_validation->set_rules('wrapper', 'Wrapper', 'trim|required|integer');
    }






    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/products/product_edit', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    /*
     * preparing data for database to $dbdata array
     */

    $this->load->model('admin/common_model');

    $fields_to_update = array();
    if (null !== ($this->input->post('modified_fields')) && $this->input->post('modified_fields') != '') {
      $fields_to_update = explode(',', $this->input->post('modified_fields'));
    }

    if (count($fields_to_update) === 0) {
      // если нажали просто сохранить, то еще раз выводим это
      if ($this->input->post('submit') === config_item('_submit_save')) {
	$this->product_edit_new_load($id, $data);
	return;
      }
      // это значит сохранить и выйти. нам надо удалить маркет о юзере из базы и сделать redirect
      else {
	$this->common_model->unset_item_used_by_user('products', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    }
    unset($_POST['modified_fields']); // clear it

    $this->dbdata['category_id'] = Baza::encode_plain_string_to_db($this->input->post('category_id'));
    $this->dbdata['brand_id'] = Baza::encode_plain_string_to_db($this->input->post('brand_id'));
    $this->dbdata['country_made'] = Baza::encode_plain_string_to_db($this->input->post('country_made'));
    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['published'] = $this->input->post('published') === 'on' ? 1 : 0;
    $this->dbdata['featured'] = $this->input->post('featured') === 'on' ? 1 : 0;
    $this->dbdata['is_cigar_product'] = $this->input->post('is_cigar_product') === 'on' ? 1 : 0;

    $this->dbdata['price'] = Baza::encode_plain_string_to_db($this->input->post('price'));
    $this->dbdata['quantity'] = Baza::encode_plain_string_to_db($this->input->post('quantity'));

    // if cigar product
    $this->dbdata['vitola'] = Baza::encode_plain_string_to_db($this->input->post('vitola'));
    $this->dbdata['line'] = Baza::encode_plain_string_to_db($this->input->post('line'));
    $this->dbdata['length'] = Baza::encode_plain_string_to_db($this->input->post('length'));
    $this->dbdata['cepo'] = Baza::encode_plain_string_to_db($this->input->post('cepo'));
    $this->dbdata['intensity'] = Baza::encode_plain_string_to_db($this->input->post('intensity'));
    $this->dbdata['flavour'] = Baza::encode_plain_string_to_db($this->input->post('flavour'));
    $this->dbdata['wrapper'] = Baza::encode_plain_string_to_db($this->input->post('wrapper'));

    $this->dbdata['intro'] = Baza::encode_plain_string_to_db($this->input->post('intro'));

    $this->dbdata['html'] = Baza::encode_html_string_to_db($this->input->post('html'));

    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));
    $this->dbdata['meta_description'] = Baza::encode_plain_string_to_db($this->input->post('meta_description'));
    $this->dbdata['meta_keywords'] = Baza::encode_plain_string_to_db($this->input->post('meta_keywords'));
    $this->dbdata['meta_robots'] = Baza::encode_plain_string_to_db($this->input->post('meta_robots'));

    /*
     * now let's set to database with transaction
     */

    $this->load->model("admin/products_model");
    if ($this->products_model->update_product($id, $this->dbdata, $fields_to_update)) {
      if ($this->input->post('submit') === config_item('_submit_save')) {
	// UPDATE SUCCESS!
	redirect(current_url());
      } else {
	// UPDATE SUCCESS!
	$this->common_model->unset_item_used_by_user('products', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/products/product_edit_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

}

// end of class

