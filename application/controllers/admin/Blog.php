<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

  public function __construct() {
    parent::__construct();

    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  /*
    public function index() {
    echo __METHOD__;
    }
   */

  public function article_preview() {
    $data['meta_title'] = "Preview Article";
    $data['title'] = "Preview Article";

    $aid = $this->uri->segment(3);
    $this->load->model('admin/blog_model');
    $article = $this->blog_model->get_article_by_id($aid);
    if (!$article) {
      show_404();
    }
    $data['id'] = $aid;
    $data['slug'] = $article['slug'];
    $data['title'] = Baza::decode_plain_string_from_db($article['title']);
    $data['intro'] = Baza::decode_plain_string_from_db($article['intro']);
    $data['img'] = Baza::decode_plain_string_from_db($article['img']);

    $data['before_body'] = Baza::decode_html_string_from_db($article['before_body']); // code
    $data['body'] = Baza::decode_html_string_from_db($article['body']); // html
    $data['after_body'] = Baza::decode_html_string_from_db($article['after_body']); // code
    //
    //$data['published'] = Baza::decode_plain_string_from_db($article['published']);
    $data['meta_title'] = Baza::decode_plain_string_from_db($article['meta_title']);
    $data['meta_description'] = Baza::decode_plain_string_from_db($article['meta_description']);
    $data['meta_keywords'] = Baza::decode_plain_string_from_db($article['meta_keywords']);
    $data['meta_author'] = Baza::decode_plain_string_from_db($article['meta_author']);
    $data['meta_robots'] = Baza::decode_plain_string_from_db($article['meta_robots']);

    $this->load->view('templates/public/header', $data);
    $this->load->view('admin/blog/article_preview', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function section_preview() {
    $data['meta_title'] = "Preview Section";
    $data['title'] = "Preview Section";

    $sid = $this->uri->segment(3);
    $this->load->model('admin/blog_model');
    $section = $this->blog_model->get_section_by_id($sid);
    if (!$section) {
      show_404();
    }
    $data['id'] = $sid;
    $data['slug'] = $section['slug'];
    $data['title'] = Baza::decode_plain_string_from_db($section['title']);
    $data['intro'] = Baza::decode_plain_string_from_db($section['intro']);
    $data['img'] = Baza::decode_plain_string_from_db($section['img']);

    $data['before_body'] = Baza::decode_html_string_from_db($section['before_body']); // html
    $data['body'] = Baza::decode_html_string_from_db($section['body']); // html
    $data['after_body'] = Baza::decode_html_string_from_db($section['after_body']); // html
    //
    //$data['published'] = Baza::decode_plain_string_from_db($section['published']);
    $data['meta_title'] = Baza::decode_plain_string_from_db($section['meta_title']);
    $data['meta_description'] = Baza::decode_plain_string_from_db($section['meta_description']);
    $data['meta_keywords'] = Baza::decode_plain_string_from_db($section['meta_keywords']);
    $data['meta_author'] = config_item('_meta_author');
    $data['meta_robots'] = Baza::decode_plain_string_from_db($section['meta_robots']);

    $this->load->view('templates/public/header', $data);
    $this->load->view('admin/blog/section_preview', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function all_articles() {
    $data['meta_title'] = "All Articles";
    $data['title'] = "All Articles";
    $this->load->model('admin/blog_model');
    $num_of_articles = $this->db->count_all('articles');
    $this->load->library('pagination');
    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_articles; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_admin_blog_articles_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['articles'] = $this->blog_model->get_articles(NULL, $limit, $offset);

    $data['row_counter'] = $offset + 1;
    $data['num_of_articles'] = $num_of_articles;
    
    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/blog/articles_all_head', $data);
    $this->load->view('admin/blog/articles_table', $data);
    $this->load->view('admin/blog/articles_under_table', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function show_section_articles() {
    $sid = $this->uri->segment(3);
    $this->load->model('admin/blog_model');
    // проверим есть ли в sections такой id и получим данные
    $section = $this->blog_model->get_section_by_id($sid);
    if (!$section) {
      show_404();
    }

    // data for views
    $data['id'] = $sid;
    $data['slug'] = Baza::decode_plain_string_from_db($section['slug']);
    $data['title'] = Baza::decode_plain_string_from_db($section['title']);
    $data['meta_title'] = 'Articles : ' . Baza::decode_plain_string_from_db($section['meta_title']);


    // проверим сколько articles в данном section, т.е. имеет ли смысл выполнять поиск
    // и за одно вычислим параметр для pagination

    $res = $this->db->query("SELECT COUNT(*) AS num FROM articles WHERE section_id = $sid");
    $num_of_articles_in_section = $res->row()->num;
    if ($num_of_articles_in_section === 0) {
      echo "no articles found in this blog section";
    } else {
      //echo $num_of_articles_in_section . " articles found in this section";
    }

    $this->load->library('pagination');
    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_articles_in_section; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_admin_blog_articles_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['articles'] = $this->blog_model->get_articles($sid, $limit, $offset);

    $data['row_counter'] = $offset + 1;
    $data['num_of_articles'] = $num_of_articles_in_section;

    //$config['total_rows'] = count($data['sections']); // usually number of rows out of database
    //$idiom = "english";
    //$this->lang->load('habano', $idiom);
    //$this->lang->load('habano'); // autoloaded now
    //$data['habano_create_new_blog_section'] = $this->lang->line('habano_create_new_blog_section');
    
    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/blog/articles_of_section_head', $data);
    $this->load->view('admin/blog/articles_table', $data);
    $this->load->view('admin/blog/articles_under_table', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  /* ==================================================================== */
  /* ==================================================================== */

  public function blog_sections() {
    $data['meta_title'] = $this->lang->line('habano_admin_sections_page_title');
    $data['title'] = $this->lang->line('habano_admin_sections_page_title');

    $this->load->model('admin/blog_model');

    $this->db->from('sections');
    $num_of_sections = $this->db->count_all_results();

    $data['num_of_sections'] = $num_of_sections;

    $this->load->library('pagination');
    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_sections; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_admin_blog_sections_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['sections'] = $this->blog_model->get_sections($limit, $offset);

    $data['row_counter'] = $offset + 1;
    
    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/blog/sections', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  
  /* ==================================================================== */
  /* ==================================================================== */

  
  public function article_edit() {
    $id = $this->uri->segment(3);

    $this->load->model('admin/blog_model');
    $data['article'] = $this->blog_model->get_article_by_id($id);
    if (!$data['article']) {
      show_404();
    }

    // если id пользователя и id использующего не равны и это не суперадмин - нахер его
    if ($data['article']['used_by_user_id'] != null && $data['article']['used_by_user_id'] != $this->session->userdata('user')['id'] && !isset($this->session->userdata('user')['is_super_admin'])) {
      show_404();
    }

    $data['item']['img'] = Baza::decode_plain_string_from_db($data['article']['img']);
    $data['title'] = Baza::decode_plain_string_from_db($data['article']['title']);
    $data['meta_title'] = 'Edititing: ' . $data['title'];

    // если новая загрузка страницы (т.е. без подтверждения формы)
    if (!isset($_POST['modified_fields'])) {
      $this->load->model('admin/common_model');
      $this->common_model->set_item_used_by_user('articles', $id, $this->session->userdata('user')['id']);
      $this->article_edit_new_load($id, $data);
      // если подтверждение формы
    } else {
      $data['modified_fields'] = $this->input->post('modified_fields');
      $this->article_edit_update($id, $data);
    }
  }

  
  /* ==================================================================== */
  /* ==================================================================== */

  
  public function section_edit() {
    $id = $this->uri->segment(3);

    $this->load->model('admin/blog_model');
    $data['section'] = $this->blog_model->get_section_by_id($id);
    if (!$data['section']) {
      show_404();
    }

    // если id пользователя и id использующего не равны и это не суперадмин - нахер его
    if ($data['section']['used_by_user_id'] != null && $data['section']['used_by_user_id'] != $this->session->userdata('user')['id'] && !isset($this->session->userdata('user')['is_super_admin'])) {
      show_404();
    }

    $data['item']['img'] = Baza::decode_plain_string_from_db($data['section']['img']);
    $data['title'] = Baza::decode_plain_string_from_db($data['section']['title']);
    $data['meta_title'] = 'Edititing: ' . $data['title'];

    // если новая загрузка страницы (т.е. без подтверждения формы)
    if (!isset($_POST['modified_fields'])) {
      $this->load->model('admin/common_model');
      $this->common_model->set_item_used_by_user('sections', $id, $this->session->userdata('user')['id']);
      $this->section_edit_new_load($id, $data);
      // если подтверждение формы
    } else {
      $data['modified_fields'] = $this->input->post('modified_fields');
      $this->section_edit_update($id, $data);
    }
  }
  
  private function article_edit_update($id, $data) {

    $data['item']['id'] = $id;

    // now form validation goes
    $this->load->library('form_validation');
    
    $this->form_validation->set_rules('section_id', 'Related section', 'required|is_natural');
    $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[1]|max_length[255]');


    // если значение slug в массиве POST равен значению в базе, то проверять на уникальнсть не будем и обновлять значение тоже
    if ($this->input->post('slug') === $data['article']['slug']) {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    } else {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs|is_unique[articles.slug]', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    }



    $this->form_validation->set_rules('published', 'Published', 'checkbox');
    $this->form_validation->set_rules('featured', 'Featured', 'checkbox');
    $this->form_validation->set_rules('meta_robots', 'Meta robots', 'required|robots_metatag');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/article_edit', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    /*
     * preparing data for database to $dbdata array
     */

    $this->load->model('admin/common_model');

    $fields_to_update = array();
    if (null !== ($this->input->post('modified_fields')) && $this->input->post('modified_fields') != '') {
      $fields_to_update = explode(',', $this->input->post('modified_fields'));
    }

    if (count($fields_to_update) === 0) {
      // если нажали просто сохранить, то еще раз выводим это
      if ($this->input->post('submit') === config_item('_submit_save')) {
	$this->article_edit_new_load($id, $data);
	return;
      }
      // это значит сохранить и выйти. нам надо удалить маркет о юзере из базы и сделать redirect
      else {
	$this->common_model->unset_item_used_by_user('articles', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    }
    unset($_POST['modified_fields']); // clear it

    $this->dbdata['section_id'] = Baza::encode_plain_string_to_db($this->input->post('section_id'));
    $this->dbdata['title'] = Baza::encode_plain_string_to_db($this->input->post('title'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['published'] = $this->input->post('published') === 'on' ? 1 : 0; // не проверяем само значение здесь, т.к. оно проверена на этапе валидации
    $this->dbdata['featured'] = $this->input->post('featured') === 'on' ? 1 : 0;
    $this->dbdata['intro'] = Baza::encode_plain_string_to_db($this->input->post('intro'));

    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $this->dbdata['before_body'] = Baza::encode_html_string_to_db($this->input->post('before_body'));
    }

    $this->dbdata['body'] = Baza::encode_html_string_to_db($this->input->post('body'));

    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $this->dbdata['after_body'] = Baza::encode_html_string_to_db($this->input->post('after_body'));
    }

    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));
    $this->dbdata['meta_description'] = Baza::encode_plain_string_to_db($this->input->post('meta_description'));
    $this->dbdata['meta_keywords'] = Baza::encode_plain_string_to_db($this->input->post('meta_keywords'));
    $this->dbdata['meta_author'] = Baza::encode_plain_string_to_db($this->input->post('meta_author'));
    $this->dbdata['meta_robots'] = Baza::encode_plain_string_to_db($this->input->post('meta_robots'));

    /*
     * now let's set to database with transaction
     */

    $this->load->model("admin/blog_model");
    if ($this->blog_model->update_article($id, $this->dbdata, $fields_to_update)) {
      if ($this->input->post('submit') === config_item('_submit_save')) {
	// UPDATE SUCCESS!
	redirect(current_url());
      } else {
	// UPDATE SUCCESS!
	$this->common_model->unset_item_used_by_user('articles', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/article_edit_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

  private function section_edit_update($id, $data) {

    $data['item']['id'] = $id;

    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[1]|max_length[255]');


    // если значение slug в массиве POST равен значению в базе, то проверять на уникальнсть не будем и обновлять значение тоже
    if ($this->input->post('slug') === $data['section']['slug']) {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    } else {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs|is_unique[sections.slug]', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    }



    $this->form_validation->set_rules('published', 'Published', 'checkbox');
    $this->form_validation->set_rules('meta_robots', 'Meta robots', 'required|robots_metatag');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/section_edit', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    /*
     * preparing data for database to $dbdata array
     */

    $this->load->model('admin/common_model');

    $fields_to_update = array();
    if (null !== ($this->input->post('modified_fields')) && $this->input->post('modified_fields') != '') {
      $fields_to_update = explode(',', $this->input->post('modified_fields'));
    }

    if (count($fields_to_update) === 0) {
      // если нажали просто сохранить, то еще раз выводим это
      if ($this->input->post('submit') === config_item('_submit_save')) {
	$this->section_edit_new_load($id, $data);
	return;
      }
      // это значит сохранить и выйти. нам надо удалить маркет о юзере из базы и сделать redirect
      else {
	$this->common_model->unset_item_used_by_user('sections', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    }
    unset($_POST['modified_fields']); // clear it

    $this->dbdata['title'] = Baza::encode_plain_string_to_db($this->input->post('title'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['published'] = $this->input->post('published') === 'on' ? 1 : 0; // не проверяем само значение здесь, т.к. оно проверена на этапе валидации
    $this->dbdata['intro'] = Baza::encode_plain_string_to_db($this->input->post('intro'));

    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $this->dbdata['before_body'] = Baza::encode_html_string_to_db($this->input->post('before_body'));
    }

    $this->dbdata['body'] = Baza::encode_html_string_to_db($this->input->post('body'));

    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $this->dbdata['after_body'] = Baza::encode_html_string_to_db($this->input->post('after_body'));
    }

    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));
    $this->dbdata['meta_description'] = Baza::encode_plain_string_to_db($this->input->post('meta_description'));
    $this->dbdata['meta_keywords'] = Baza::encode_plain_string_to_db($this->input->post('meta_keywords'));
    $this->dbdata['meta_robots'] = Baza::encode_plain_string_to_db($this->input->post('meta_robots'));

    /*
     * now let's set to database with transaction
     */

    $this->load->model("admin/blog_model");
    if ($this->blog_model->update_section($id, $this->dbdata, $fields_to_update)) {
      if ($this->input->post('submit') === config_item('_submit_save')) {
	// UPDATE SUCCESS!
	redirect(current_url());
      } else {
	// UPDATE SUCCESS!
	$this->common_model->unset_item_used_by_user('sections', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/section_edit_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

  private function section_edit_new_load($id, $data) {

    // section data here

    $data['item']['id'] = $id;
    $data['item']['slug'] = $data['section']['slug'];
    $data['item']['title'] = Baza::decode_plain_string_from_db($data['section']['title']);

    $data['title'] = $data['item']['title'];

    $data['item']['intro'] = Baza::decode_plain_string_from_db($data['section']['intro']);

    //$data['item']['img'] = Baza::decode_plain_string_from_db($data['section']['img']);

    $data['item']['before_body'] = Baza::decode_html_string_from_db($data['section']['before_body']); // html
    $data['item']['body'] = Baza::decode_html_string_from_db($data['section']['body']); // html
    $data['item']['after_body'] = Baza::decode_html_string_from_db($data['section']['after_body']); // html

    $data['item']['published'] = Baza::decode_plain_string_from_db($data['section']['published']);
    $data['item']['meta_title'] = Baza::decode_plain_string_from_db($data['section']['meta_title']);
    $data['item']['meta_description'] = Baza::decode_plain_string_from_db($data['section']['meta_description']);
    $data['item']['meta_keywords'] = Baza::decode_plain_string_from_db($data['section']['meta_keywords']);
    //$data['item']['meta_author'] = config_item('_meta_author');
    $data['item']['meta_robots'] = Baza::decode_plain_string_from_db($data['section']['meta_robots']);


    $data['meta_title'] = "Editing : " . $data['item']['title'];

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/blog/section_edit', $data);
    $this->load->view('templates/admin/footer', $data);
  }
  
  private function article_edit_new_load($id, $data) {

    // article data here

    $data['item']['id'] = $id;
    $data['item']['section_id'] = $data['article']['section_id'];
    $data['item']['slug'] = $data['article']['slug'];
    $data['item']['title'] = Baza::decode_plain_string_from_db($data['article']['title']);

    $data['title'] = $data['item']['title'];

    $data['item']['intro'] = Baza::decode_plain_string_from_db($data['article']['intro']);

    //$data['item']['img'] = Baza::decode_plain_string_from_db($data['article']['img']);

    $data['item']['before_body'] = Baza::decode_html_string_from_db($data['article']['before_body']); // html
    $data['item']['body'] = Baza::decode_html_string_from_db($data['article']['body']); // html
    $data['item']['after_body'] = Baza::decode_html_string_from_db($data['article']['after_body']); // html

    $data['item']['published'] = Baza::decode_plain_string_from_db($data['article']['published']);
    
    $data['item']['featured'] = Baza::decode_plain_string_from_db($data['article']['featured']);
    
    $data['item']['meta_title'] = Baza::decode_plain_string_from_db($data['article']['meta_title']);
    $data['item']['meta_description'] = Baza::decode_plain_string_from_db($data['article']['meta_description']);
    $data['item']['meta_keywords'] = Baza::decode_plain_string_from_db($data['article']['meta_keywords']);
    
    //$data['item']['meta_author'] = config_item('_meta_author');
    
    $data['item']['meta_author'] = Baza::decode_plain_string_from_db($data['article']['meta_author']);
    
    $data['item']['meta_robots'] = Baza::decode_plain_string_from_db($data['article']['meta_robots']);


    $data['meta_title'] = "Editing : " . $data['item']['title'];

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/blog/article_edit', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  /* ==================================================================== */
  /* ==================================================================== */

  public function create_new_blog_section() { // start of method
    $data['meta_title'] = $this->lang->line('habano_blog_section_create_page_title');
    $data['upload_error'] = '';
    $data['upload_data'] = '';
    $data['image_lib_error'] = '';

    // поскольку этот же контроллер принимает форму c файлом,
    // и файл уже может существовать в массиве $_FILES,
    // то проверим сразу имя возможного файла и остановим, если не годится
    if (isset($_FILES['img']['name']) && isset($_FILES['img']['size']) && $_FILES['img']['size'] !== 0) {
      if (!Baza::is_uploading_file_name_is_valid($_FILES['img']['name'])) {
	$data['upload_error'] = $this->lang->line('habano_input_blog_section_upload_error_wrong_file_name');
	$this->load->view('templates/admin/header', $data);
	$this->load->view('admin/blog/create_section', $data);
	$this->load->view('templates/admin/footer', $data);
	return;
      }
    }

    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[1]|max_length[255]');
    $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs|is_unique[sections.slug]', array(
	'is_unique' => 'Such a slug is already used. Slug must be unique.'
    ));
    $this->form_validation->set_rules('published', 'Published', 'checkbox');
    $this->form_validation->set_rules('meta_robots', 'Meta robots', 'required|robots_metatag');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/create_section', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    // FILE UPLOAD !!!
    // now let's upload image file... and then process
    $this->dbdata['upload_file_name'] = NULL;
    if (isset($_FILES['img']['name']) && isset($_FILES['img']['size']) && $_FILES['img']['size'] !== 0) {
      $upload_config = config_item('_blog_section_image_upload_config'); // from custom config
      $this->load->library('upload', $upload_config);

      if (!$this->upload->do_upload('img')) {
	$data['upload_error'] = $this->upload->display_errors();
	$this->load->view('templates/admin/header', $data);
	$this->load->view('admin/blog/create_section', $data);
	$this->load->view('templates/admin/footer', $data);
	return;
      }
      $uploaded_file_data = $this->upload->data();
      $this->dbdata['upload_file_name'] = $uploaded_file_data['file_name'];

// now let's resize and crop uploaded image if it is neccessary

      $resized_and_cropped = $this->baza->resize_and_crop_blog_section_image($uploaded_file_data);
      if (!$resized_and_cropped) {
	$data['image_lib_error'] = $this->image_lib->display_errors();
	$this->load->view('templates/admin/header', $data);
	$this->load->view('admin/blog/create_section', $data);
	$this->load->view('templates/admin/footer', $data);
	return;
      }
    }// end of file upload


    /*
     * preparing data for database to $dbdata array
     */

    $this->dbdata['title'] = Baza::encode_plain_string_to_db($this->input->post('title'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['published'] = $this->input->post('published') === 'on' ? 1 : 0; // не проверяем само значение здесь, т.к. оно проверена на этапе валидации
    $this->dbdata['intro'] = Baza::encode_plain_string_to_db($this->input->post('intro'));
    //$this->dbdata['upload_file_name'] = $dbdata['upload_file_name']; it's done before

    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $this->dbdata['before_body'] = Baza::encode_html_string_to_db($this->input->post('before_body'));
    }

    $this->dbdata['body'] = Baza::encode_html_string_to_db($this->input->post('body'));

    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $this->dbdata['after_body'] = Baza::encode_html_string_to_db($this->input->post('after_body'));
    }

    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));
    $this->dbdata['meta_description'] = Baza::encode_plain_string_to_db($this->input->post('meta_description'));
    $this->dbdata['meta_keywords'] = Baza::encode_plain_string_to_db($this->input->post('meta_keywords'));
    $this->dbdata['meta_robots'] = Baza::encode_plain_string_to_db($this->input->post('meta_robots'));

    /*
     * now let's set to database with transaction
     */

    $this->load->model("admin/blog_model");
    if ($this->blog_model->set_section($this->dbdata)) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/create_section_success', $data);
      $this->load->view('templates/admin/footer', $data);
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/create_section_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

// end of method


  /* ==================================================================== */
  /* ==================================================================== */


  public function create_new_blog_article() {
    $data['meta_title'] = $this->lang->line('habano_blog_article_create_page_title');

    if ($this->db->count_all('sections') === 0) {
      redirect('admin/create-new-blog-section');
    }

    $this->load->model("admin/blog_model");
    $data['sections'] = $this->blog_model->get_sections_ids_and_titles();

    $data['upload_error'] = '';
    $data['upload_data'] = '';
    $data['image_lib_error'] = '';

    // поскольку этот же контроллер принимает форму c фалом,
    // и файл уже может существовать в массиве $_FILES,
    // то проверим сразу имя возможного файла и остановим, если не годится
    if (isset($_FILES['img']['name']) && isset($_FILES['img']['size']) && $_FILES['img']['size'] !== 0) {
      if (!Baza::is_uploading_file_name_is_valid($_FILES['img']['name'])) {
	$data['upload_error'] = $this->lang->line('habano_input_blog_article_upload_error_wrong_file_name');
	$this->load->view('templates/admin/header', $data);
	$this->load->view('admin/blog/create_article', $data);
	$this->load->view('templates/admin/footer', $data);
	return;
      }
    }

    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[1]|max_length[255]');
    $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[255]|alpha_numeric_dash|reserved_slugs|is_unique[articles.slug]', array(
	'is_unique' => 'Such a slug is already used. Slug must be unique.'
    ));
    $this->form_validation->set_rules('section_id', 'Related section', 'required|is_natural');
    $this->form_validation->set_rules('published', 'Published', 'checkbox');
    $this->form_validation->set_rules('featured', 'Featured', 'checkbox');
    $this->form_validation->set_rules('meta_robots', 'Meta robots', 'required|robots_metatag');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/create_article', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    // FILE UPLOAD !!!
    // now let's upload image file... and then process
    $this->dbdata['upload_file_name'] = NULL;
    if (isset($_FILES['img']['name']) && isset($_FILES['img']['size']) && $_FILES['img']['size'] !== 0) {
      $upload_config = config_item('_blog_article_image_upload_config'); // from custom config
      $this->load->library('upload', $upload_config);

      if (!$this->upload->do_upload('img')) {
	$data['upload_error'] = $this->upload->display_errors();
	$this->load->view('templates/admin/header', $data);
	$this->load->view('admin/blog/create_article', $data);
	$this->load->view('templates/admin/footer', $data);
	return;
      }
      $uploaded_file_data = $this->upload->data();
      $this->dbdata['upload_file_name'] = $uploaded_file_data['file_name'];

// now let's resize and crop uploaded image if it is neccessary

      $resized_and_cropped = $this->baza->resize_and_crop_blog_article_image($uploaded_file_data);
      if (!$resized_and_cropped) {
	$data['image_lib_error'] = $this->image_lib->display_errors();
	$this->load->view('templates/admin/header', $data);
	$this->load->view('admin/blog/create_article', $data);
	$this->load->view('templates/admin/footer', $data);
	return;
      }
    }// end of file upload

    /*
     * preparing data for database to $dbdata array
     */

    $this->dbdata['section_id'] = Baza::encode_plain_string_to_db($this->input->post('section_id'));
    $this->dbdata['title'] = Baza::encode_plain_string_to_db($this->input->post('title'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['published'] = $this->input->post('published') === 'on' ? 1 : 0; // не проверяем само значение здесь, т.к. оно проверена на этапе валидации
    $this->dbdata['featured'] = $this->input->post('featured') === 'on' ? 1 : 0;
    $this->dbdata['intro'] = Baza::encode_plain_string_to_db($this->input->post('intro'));
    //$this->dbdata['upload_file_name'] = $dbdata['upload_file_name']; it's done before

    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $this->dbdata['before_body'] = Baza::encode_html_string_to_db($this->input->post('before_body'));
    }

    $this->dbdata['body'] = Baza::encode_html_string_to_db($this->input->post('body'));

    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $this->dbdata['after_body'] = Baza::encode_html_string_to_db($this->input->post('after_body'));
    }

    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));
    $this->dbdata['meta_description'] = Baza::encode_plain_string_to_db($this->input->post('meta_description'));
    $this->dbdata['meta_keywords'] = Baza::encode_plain_string_to_db($this->input->post('meta_keywords'));
    $this->dbdata['meta_author'] = Baza::encode_plain_string_to_db($this->input->post('meta_author'));
    $this->dbdata['meta_robots'] = Baza::encode_plain_string_to_db($this->input->post('meta_robots'));

    /*
     * now let's set to database with transaction
     */

    if ($this->blog_model->set_article($this->dbdata)) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/create_article_success', $data);
      $this->load->view('templates/admin/footer', $data);
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/blog/create_article_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

// end of method
}

// end of class

