<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
  /* ==================================================================== */
  /* ==================================================================== */

  public function __construct() {
    parent::__construct();

    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || ( $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest' ) || $_SERVER['HTTP_REFERER'] == '') {
      //die("Direct Access Denied…");
      show_404();
    }

    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  /* ==================================================================== */
  /* ==================================================================== */

  public function update_images_by_id() {
    if ($this->input->post('images') === null OR $this->input->post('id') === null OR $this->input->post('table') === null) {
      echo "Error…";
      return;
    }
    $table = $this->input->post('table');
    $id = $this->input->post('id');
    $images = ($this->input->post('images') != '') ? explode(',', $this->input->post('images')) : array();
    $this->load->model('admin/common_model');
    $item = $this->common_model->get_item_by_id($table, $id, true);
    $in_db_images = unserialize(Baza::decode_plain_string_from_db($item['images']));
    if (!is_array($in_db_images) OR $in_db_images == '') {
      $in_db_images = array(); // empty
    }
    $files_to_delete = array_diff($in_db_images, $images);

    $images_str = (count($images) === 0) ? NULL : serialize($images);
    if (!$this->common_model->update_item_images($table, $id, $images_str)) {
      echo "Error!";
      return;
    }
  }

  public function toggle_item_featured_by_id() {
    $this->load->model('admin/common_model');
    $this->common_model->toggle_item_featured_by_id($this->input->post('table'), $this->input->post('id'));
  }

  public function lift_up_item_by_id() {
    $this->load->model('admin/common_model');
    $this->common_model->lift_up_item_by_id($this->input->post('table'), $this->input->post('id'));
  }

  public function toggle_published_by_id() {
    // also we need check `used_by_user_id` with current user id
    // probably first check input post before!!!
    $this->load->model('admin/common_model');
    $new_value = $this->common_model->invert_published($this->input->post('table'), $this->input->post('id'));
    echo $new_value;
  }

  public function delete_section_by_id() {
    $id = $this->input->post('id');
    $this->load->model('admin/common_model');
    $section = $this->common_model->get_item_by_id('sections', $id);
    if (!$section OR $section->used_by_user_id != NULL) {
      echo "Error : no section or used by user";
      return FALSE;
    }
    $file = config_item('_blog_section_image_path') . $section->img;
    $this->load->model('admin/blog_model');
    if ($this->blog_model->delete_section_by_id($id)) {
      if (file_exists($file) && !is_dir($file)) {
	unlink($file);
      }
      return;
    }
    echo "Error occured…";
  }

  public function delete_brand_by_id() {
    $id = $this->input->post('id');
    $this->load->model('admin/common_model');
    $brand = $this->common_model->get_item_by_id('brands', $id);
    if (!$brand OR $brand->used_by_user_id != NULL) {
      echo "Error : no brand or used by user";
      return FALSE;
    }
    $file = config_item('_brands_image_path') . $brand->img;
    //$this->load->model('admin/blog_model');
    $this->load->model('admin/brands_model');
    if ($this->brands_model->delete_brand_by_id($id)) {
      if (file_exists($file) && !is_dir($file)) {
	unlink($file);
      }
      return;
    }
    echo "Error occured…";
  }

  public function delete_banner_by_id() {
    $id = $this->input->post('id');
    $this->load->model('admin/common_model');
    $banner = $this->common_model->get_item_by_id('banners', $id);
    if (!$banner OR $banner->used_by_user_id != NULL) {
      echo "Error : no brand or used by user";
      return FALSE;
    }
    $this->load->model('admin/banners_model');
    if ($this->banners_model->delete_banner_by_id($id)) {
      return;
    }
    echo "Error occured…";
  }
  
  public function delete_order_by_id() {
    // ONLY SUPERADMIN CAN DELETE ORDER
    if (empty($this->session->userdata('user')['is_super_admin']) || $this->session->userdata('user')['is_super_admin'] != 1) {
      echo "Error occured…";
      return;
    }
    $id = $this->input->post('id');
    $this->load->model('admin/common_model');
    $order = $this->common_model->get_item_by_id('orders', $id);
    if (!$order) {
      echo "Error : no order!";
      return FALSE;
    }
    $this->load->model('admin/orders_model');
    if ($this->orders_model->delete_order_by_id($id)) {
      return;
    }
    echo "Error occured…";
  }

  public function delete_product_by_id() {
    $id = $this->input->post('id');
    $this->load->model('admin/common_model');
    $product = $this->common_model->get_item_by_id('products', $id);
    if (!$product OR $product->used_by_user_id != NULL) {
      echo "Error : no product or used by user";
      return FALSE;
    }
    $file = config_item('_products_image_path') . $product->img;
    $this->load->model('admin/products_model');
    if ($this->products_model->delete_product_by_id($id)) {
      if (file_exists($file) && !is_dir($file)) {
	unlink($file);
      }
      return;
    }
    echo "Error occured…";
  }

  public function delete_pack_by_id() {
    $id = $this->input->post('id');
    $this->load->model('admin/common_model');
    $pack = $this->common_model->get_item_by_id('packs', $id);
    if (!$pack) {
      echo "Error : no product or used by user";
      return FALSE;
    }
    // we must check 'product.used_by_user_id' that is related to this pack!!! Do it if possible later!
    $file = config_item('_packs_image_path') . $pack->img;
    $this->load->model('admin/packs_model');
    if ($this->packs_model->delete_pack_by_id($id)) {
      if (file_exists($file) && !is_dir($file)) {
	unlink($file);
      }
      return;
    }
    echo "Error occured…";
  }

  public function delete_category_by_id() {
    $id = $this->input->post('id');
    $this->load->model('admin/common_model');
    $category = $this->common_model->get_item_by_id('categories', $id);
    if (!$category OR $category->used_by_user_id != NULL) {
      echo "Error : no category or used by user";
      return FALSE;
    }
    $file = config_item('_categories_image_path') . $category->img;
    $this->load->model('admin/categories_model');
    if ($this->categories_model->delete_category_by_id($id)) {
      if (file_exists($file) && !is_dir($file)) {
	unlink($file);
      }
      return;
    }
    echo "Error occured…";
  }

  public function delete_article_by_id() {
    $id = $this->input->post('id');
    $this->load->model('admin/common_model');
    $article = $this->common_model->get_item_by_id('articles', $id);
    if (!$article OR $article->used_by_user_id != NULL) {
      echo "Error : no article or used by user";
      return FALSE;
    }
    $file = config_item('_blog_article_image_path') . $article->img;
    $this->load->model('admin/blog_model');
    if ($this->blog_model->delete_article_by_id($id)) {
      if (file_exists($file) && !is_dir($file)) {
	unlink($file);
      }
      return;
    }
    echo "Error occured…";
  }

  public function toggle_user_status_by_id() {
    // only superadmin feature
    if (!isset($this->session->userdata('user')['is_super_admin'])) {
      return;
    }
    $user_id = $this->input->post('id');
    $this->load->model('admin/users_model');
    $new_value = $this->users_model->invert_status($user_id);
    echo $new_value;
  }

  public function delete_user_by_id() {
    // only superadmin feature
    if (!isset($this->session->userdata('user')['is_super_admin'])) {
      return;
    }
    $user_id = $this->input->post('id');
    $this->load->model('admin/users_model');
    $user_data = $this->users_model->get_user_by_id($user_id);
    // superadmins can not be deleted
    if (Baza::is_email_of_super_admin($user_data->email)) {
      echo 0;
      return;
    }
    if ($this->users_model->delete_user_by_id($user_id)) {
      echo 1;
      return;
    }
    echo 0;
  }

}

// end of class

