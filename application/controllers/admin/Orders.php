<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

  public function __construct() {
    parent::__construct();

    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  public function index() {
    $data['meta_title'] = 'Заявки c сайта';
    $data['title'] = 'Заявки c сайта';

    $this->load->model('admin/orders_model');

    $this->db->from('orders');
    $num_of_orders = $this->db->count_all_results();

    $data['num_of_orders'] = $num_of_orders;

    $this->load->library('pagination');
    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_orders; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_admin_orders_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['orders'] = $this->orders_model->get_orders($limit, $offset);

    $data['row_counter'] = $offset + 1;
    
    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/orders/orders_all_head', $data);
    $this->load->view('admin/orders/orders_table', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function order($id = NULL) {
    $data['meta_title'] = 'Заявка';
    $this->load->model('admin/common_model');
    $order = $this->common_model->get_item_by_id('orders', $id, true);
    if (!$order) {
      show_404();
    }

    if ($order['is_new'] == 1) {
      $this->load->model('admin/orders_model');
      $this->orders_model->set_order_visited($id);
    }

    $this->load->library('shopcart');
    $this->load->model('admin/products_model');
    $this->load->model('admin/packs_model');
    $this->load->model('admin/users_model');
    
    $customer = $this->users_model->get_user_by_id($order['user_id']);

    $cart_products = $this->shopcart->parse_cart_items(Baza::decode_plain_string_from_db($order['products']));

    $products_and_amounts = array();
    if (count($cart_products) > 0) {
      foreach ($cart_products as $cp) {
	if (!empty($cp[0])) {
	  $pr = $this->products_model->get_product_by_id($cp[0]);
	  $products_and_amounts[] = array($pr, $cp[1]);
	}
      }
    }


    $cart_packs = $this->shopcart->parse_cart_items(Baza::decode_plain_string_from_db($order['packs']));
    $packs_and_amounts = array();
    foreach ($cart_packs as $cp) {
      if (!empty($cp[0])) {
	$pc = $this->packs_model->get_pack_with_product_by_pack_id($cp[0]);
	$packs_and_amounts[] = array($pc, $cp[1]);
      }
    }
    
    $data['customer'] = $customer;
    $data['order'] = $order;
    $data['products_and_amounts'] = $products_and_amounts;
    $data['packs_and_amounts'] = $packs_and_amounts;
    
    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/orders/order_view', $data);
    $this->load->view('templates/admin/footer', $data);
  }

}

// end of class

