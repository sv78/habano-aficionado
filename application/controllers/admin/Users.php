<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

  public function __construct() {
    parent::__construct();

    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  public function index() {
    $data['meta_title'] = $this->lang->line('habano_admin_users_page_title');
    $data['title'] = $this->lang->line('habano_admin_users_page_title');

    $this->load->model('admin/users_model');

    $this->db->from('users');
    $num_of_users = $this->db->count_all_results();

    $data['num_of_users'] = $num_of_users;

    $this->load->library('pagination');
    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_users; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_admin_users_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['users'] = $this->users_model->get_users($limit, $offset);

    $data['row_counter'] = $offset + 1;

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/users/users', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function user() {
    $data['meta_title'] = $this->lang->line('habano_admin_user_page_title');
    $data['title'] = $this->lang->line('habano_admin_user_page_title');
    $user_id = $this->uri->segment(3);
    if (preg_match('/[^0-9]/', $user_id)) {
      show_404();
      return;
    }
    $this->load->model('admin/users_model');
    $user_data = $this->users_model->get_user_by_id($user_id);
    if (!$user_data) {
      show_404();
      return;
    }
    $data['user'] = $user_data;
    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/users/user', $data);
    $this->load->view('templates/admin/footer', $data);
  }

}

// end of class

