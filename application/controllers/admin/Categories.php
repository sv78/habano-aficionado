<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

  public function __construct() {
    parent::__construct();

    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  public function all_categories() {
    $data['meta_title'] = "Categories";
    $num_of_categories = $this->db->count_all('categories');

    $this->load->model('admin/categories_model');
    $this->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_categories; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_admin_categories_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['categories'] = $this->categories_model->get_categories($limit, $offset);

    $data['row_counter'] = $offset + 1;
    $data['num_of_categories'] = $num_of_categories;

    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    //$this->load->view('admin/blog/articles_all_head', $data);
    $this->load->view('admin/categories/categories_view', $data);
    //$this->load->view('admin/blog/articles_under_table', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function create_category() {
    $data['meta_title'] = "Create Category";
    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name Of Category', 'trim|required|min_length[1]|max_length[255]');
    $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs|is_unique[categories.slug]', array(
	'is_unique' => 'Such a slug is already used. Slug must be unique.'
    ));
    $this->form_validation->set_rules('is_cigar_category', 'Is Cigar Category', 'checkbox');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/categories/category_create', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['is_cigar_category'] = $this->input->post('is_cigar_category') === 'on' ? 1 : 0;
    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));

    $this->load->model("admin/categories_model");
    if ($this->categories_model->set_category($this->dbdata)) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/categories/category_create_success', $data);
      $this->load->view('templates/admin/footer', $data);
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/categories/category_create_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

  public function category_preview() {
    
    $data['meta_title'] = "Preview Category";
    $data['title'] = "Preview Category";

    $id = $this->uri->segment(3);
    $this->load->model('admin/common_model');
    $category = $this->common_model->get_item_by_id('categories', $id, true);
    if (!$category) {
      show_404();
    }

    $data['id'] = $id;
    $data['slug'] = $category['slug'];
    $data['name'] = Baza::decode_plain_string_from_db($category['name']);
    $data['intro'] = Baza::decode_plain_string_from_db($category['intro']);
    $data['img'] = Baza::decode_plain_string_from_db($category['img']);

    $data['html'] = Baza::decode_html_string_from_db($category['html']); // html

    $data['published'] = Baza::decode_plain_string_from_db($category['published']);
    $data['featured'] = Baza::decode_plain_string_from_db($category['featured']);
    $data['is_cigar_category'] = Baza::decode_plain_string_from_db($category['is_cigar_category']);
    
    $data['meta_title'] = Baza::decode_plain_string_from_db($category['meta_title']);
    $data['meta_description'] = Baza::decode_plain_string_from_db($category['meta_description']);
    $data['meta_keywords'] = Baza::decode_plain_string_from_db($category['meta_keywords']);
    $data['meta_robots'] = Baza::decode_plain_string_from_db($category['meta_robots']);

    $this->load->view('templates/public/header', $data);
    $this->load->view('admin/categories/category_preview', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function category_edit() {
    $id = $this->uri->segment(3);
    $this->load->model('admin/common_model');
    $data['category'] = $this->common_model->get_item_by_id('categories', $id, TRUE); // return as array if true
    if (!$data['category']) {
      show_404();
    }

    // если id пользователя и id использующего не равны и это не суперадмин - нахер его
    if ($data['category']['used_by_user_id'] != null && $data['category']['used_by_user_id'] != $this->session->userdata('user')['id'] && !isset($this->session->userdata('user')['is_super_admin'])) {
      show_404();
    }

    $data['item']['img'] = Baza::decode_plain_string_from_db($data['category']['img']);
    //$data['title'] = Baza::decode_plain_string_from_db($data['category']['name']);
    //$data['meta_title'] = 'Edititing category: ' . $data['title'];

    // если новая загрузка страницы (т.е. без подтверждения формы)
    if (!isset($_POST['modified_fields'])) {
      $this->common_model->set_item_used_by_user('categories', $id, $this->session->userdata('user')['id']);
      $this->category_edit_new_load($id, $data);
      // если подтверждение формы
    } else {
      $data['modified_fields'] = $this->input->post('modified_fields');
      $this->category_edit_update($id, $data);
    }
  }

  private function category_edit_new_load($id, $data) {
    // article data here

    $data['item']['id'] = $id;
    $data['item']['slug'] = $data['category']['slug'];
    $data['item']['name'] = Baza::decode_plain_string_from_db($data['category']['name']);

    $data['item']['intro'] = Baza::decode_plain_string_from_db($data['category']['intro']);

    //$data['item']['img'] = Baza::decode_plain_string_from_db($data['category']['img']);

    $data['item']['html'] = Baza::decode_html_string_from_db($data['category']['html']); // html

    $data['item']['published'] = Baza::decode_plain_string_from_db($data['category']['published']);

    $data['item']['featured'] = Baza::decode_plain_string_from_db($data['category']['featured']);
    
    $data['item']['is_cigar_category'] = Baza::decode_plain_string_from_db($data['category']['is_cigar_category']);

    $data['item']['meta_title'] = Baza::decode_plain_string_from_db($data['category']['meta_title']);
    $data['item']['meta_description'] = Baza::decode_plain_string_from_db($data['category']['meta_description']);
    $data['item']['meta_keywords'] = Baza::decode_plain_string_from_db($data['category']['meta_keywords']);
    $data['item']['meta_robots'] = Baza::decode_plain_string_from_db($data['category']['meta_robots']);


    $data['title'] = $data['item']['name'];
    $data['meta_title'] = "Category edit: " . $data['item']['name'];

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/categories/category_edit', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  private function category_edit_update($id, $data) {

    $data['item']['id'] = $id;

    // now form validation goes
    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'Name Of Category', 'trim|required|min_length[1]|max_length[255]');

    // если значение slug в массиве POST равен значению в базе, то проверять на уникальнсть не будем и обновлять значение тоже
    if ($this->input->post('slug') === $data['category']['slug']) {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    } else {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs|is_unique[categories.slug]', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    }

    $this->form_validation->set_rules('published', 'Published', 'checkbox');
    $this->form_validation->set_rules('featured', 'Featured', 'checkbox');
    $this->form_validation->set_rules('is_cigar_category', 'Is Cigar Category', 'checkbox');
    $this->form_validation->set_rules('meta_robots', 'Meta robots', 'required|robots_metatag');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/categories/category_edit', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    /*
     * preparing data for database to $dbdata array
     */

    $this->load->model('admin/common_model');

    $fields_to_update = array();
    if (null !== ($this->input->post('modified_fields')) && $this->input->post('modified_fields') != '') {
      $fields_to_update = explode(',', $this->input->post('modified_fields'));
    }

    if (count($fields_to_update) === 0) {
      // если нажали просто сохранить, то еще раз выводим это
      if ($this->input->post('submit') === config_item('_submit_save')) {
	$this->category_edit_new_load($id, $data);
	return;
      }
      // это значит сохранить и выйти. нам надо удалить маркет о юзере из базы и сделать redirect
      else {
	$this->common_model->unset_item_used_by_user('categories', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    }
    unset($_POST['modified_fields']); // clear it

    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['published'] = $this->input->post('published') === 'on' ? 1 : 0;
    $this->dbdata['featured'] = $this->input->post('featured') === 'on' ? 1 : 0;
    $this->dbdata['is_cigar_category'] = $this->input->post('is_cigar_category') === 'on' ? 1 : 0;

    $this->dbdata['intro'] = Baza::encode_plain_string_to_db($this->input->post('intro'));

    $this->dbdata['html'] = Baza::encode_html_string_to_db($this->input->post('html'));

    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));
    $this->dbdata['meta_description'] = Baza::encode_plain_string_to_db($this->input->post('meta_description'));
    $this->dbdata['meta_keywords'] = Baza::encode_plain_string_to_db($this->input->post('meta_keywords'));
    $this->dbdata['meta_robots'] = Baza::encode_plain_string_to_db($this->input->post('meta_robots'));

    /*
     * now let's set to database with transaction
     */

    $this->load->model("admin/categories_model");
    if ($this->categories_model->update_category($id, $this->dbdata, $fields_to_update)) {
      if ($this->input->post('submit') === config_item('_submit_save')) {
	// UPDATE SUCCESS!
	redirect(current_url());
      } else {
	// UPDATE SUCCESS!
	$this->common_model->unset_item_used_by_user('categories', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/categories/category_edit_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

}

// end of class

