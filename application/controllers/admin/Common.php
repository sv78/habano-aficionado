<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

  //public $dbdata = [];

  public function __construct() {
    parent::__construct();

    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  public function index() {
    $data['meta_title'] = "Administrator Tools";
    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/admin_view', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function file_manager() {
    $data['meta_title'] = "File manager";
    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/file_manager_view', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function empty_iframe() {
    echo 'I\'m empty iframe target for your form…';
  }

  public function iframe_upload_image() {

    // here we must get 3 parameters through the post array - table, id and img

    if ($this->input->post('id') === null OR $this->input->post('table') === null) {
      echo '<script>alert("Error!");</script>';
      return; // error
    }
    if (!isset($_FILES['img']['name']) || !isset($_FILES['img']['size']) || $_FILES['img']['size'] === 0) {
      echo '<script>alert("Error!");</script>';
      return; // error
    }

    $id = $this->input->post('id');
    $table = $this->input->post('table');

    if ($table != 'cigars' && !$this->db->field_exists('img', $table)) {
      echo '<script>alert("Error!");</script>';
      return; // error
    }

    // FILE UPLOAD !!!
    // now let's upload image file... and then process
    //$this->dbdata['upload_file_name'] = NULL;

    switch ($table) {
      case 'sections' :
        $upload_config = config_item('_blog_section_image_upload_config'); // from config
        break;
      case 'articles' :
        $upload_config = config_item('_blog_article_image_upload_config'); // from config
        break;
      case 'brands' :
        $upload_config = config_item('_brand_image_upload_config'); // from config
        break;
      case 'categories' :
        $upload_config = config_item('_category_image_upload_config'); // from config
        break;
      case 'products' :
        $upload_config = config_item('_product_image_upload_config'); // from config
        break;
      case 'packs' :
        $upload_config = config_item('_pack_image_upload_config'); // from config
        break;
      default:
        $upload_config = config_item('_product_image_upload_config'); // from config
    }


    $this->load->library('upload', $upload_config);

    if (!$this->upload->do_upload('img')) {
      print_r($this->upload->display_errors());
      echo '<script>alert("Error!");</script>';
      return;
    }
    $uploaded_file_data = $this->upload->data();
    $this->dbdata['upload_file_name'] = $uploaded_file_data['file_name'];

// now let's resize and crop uploaded image if it is neccessary

    switch ($table) {
      case 'sections':
        $resized_and_cropped = $this->baza->resize_and_crop_blog_section_image($uploaded_file_data);
        break;
      case 'articles':
        $resized_and_cropped = $this->baza->resize_and_crop_blog_article_image($uploaded_file_data);
        break;
      case 'brands':
        $resized_and_cropped = $this->baza->resize_and_crop_brand_image($uploaded_file_data);
        break;
      case 'categories':
        $resized_and_cropped = $this->baza->resize_and_crop_category_image($uploaded_file_data);
        break;
      case 'products':
        $resized_and_cropped = $this->baza->resize_and_crop_product_image($uploaded_file_data);
        break;
      case 'cigars':
        $resized_and_cropped = $this->baza->resize_and_crop_cigar_image($uploaded_file_data);
        break;
      case 'packs':
        $resized_and_cropped = $this->baza->resize_and_crop_pack_image($uploaded_file_data);
        break;
      default:
        $resized_and_cropped = $this->baza->resize_and_crop_blog_section_image($uploaded_file_data);
    }

    if (!$resized_and_cropped) {
      print_r($this->image_lib->display_errors());
      echo '<script>alert("Error!");</script>';
      return;
    }
    // end of file upload

    $this->load->model('admin/common_model');
    if ($table == 'cigars' OR $table == 'products') {
      $table = $table == 'cigars' ? 'products' : 'products'; // the same table actually :))
    }
    if (!$this->common_model->update_image($table, $id, $uploaded_file_data['file_name'])) {
      echo '<script>alert("Error!");</script>';
      return;
    }
    switch ($table) {
      case 'sections' :
        $img_path = "/" . config_item('_blog_section_image_path_url');
        break;
      case 'articles' :
        $img_path = "/" . config_item('_blog_article_image_path_url');
        break;
      case 'brands' :
        $img_path = "/" . config_item('_brands_image_path_url');
        break;
      case 'categories' :
        $img_path = "/" . config_item('_categories_image_path_url');
        break;
      case 'products' :
        $img_path = "/" . config_item('_products_image_path_url');
        break;
      case 'cigars' :
        $img_path = "/" . config_item('_products_image_path_url');
        break;
      case 'packs' :
        $img_path = "/" . config_item('_packs_image_path_url');
        break;
      default:
        $img_path = "/" . config_item('_blog_section_image_path_url');
    }
    // HTML goes
    // JS script will update src attribute of img tag
    ?>
    <script>
      window.top.window.document.getElementById('uploaded_image_tag').src = "<?php echo $img_path . $uploaded_file_data['file_name']; ?>";
      window.top.window.document.getElementById('img_filename_loading').className = window.top.window.document.getElementById('img_filename_loading').className + " hidden";
    </script>
    <?php
  }

  // end of method


  public function iframe_upload_additional_image() {

    // here we must get 3 parameters through the post array - table, id and img

    if ($this->input->post('id') === null OR $this->input->post('table') === null) {
      echo '<script>alert("Error!");</script>';
      return; // error
    }
    if (!isset($_FILES['img']['name']) || !isset($_FILES['img']['size']) || $_FILES['img']['size'] === 0) {
      echo '<script>alert("Error!");</script>';
      return; // error
    }

    $id = $this->input->post('id');
    $table = $this->input->post('table');

    if ($table != 'cigars' && !$this->db->field_exists('img', $table)) {
      echo '<script>alert("Error!");</script>';
      return; // error
    }

    // FILE UPLOAD !!!
    // now let's upload image file... and then process
    //$this->dbdata['upload_file_name'] = NULL;

    switch ($table) {
      case 'sections' :
        $upload_config = config_item('_blog_section_image_upload_config'); // from config
        break;
      case 'articles' :
        $upload_config = config_item('_blog_article_image_upload_config'); // from config
        break;
      case 'brands' :
        $upload_config = config_item('_brand_image_upload_config'); // from config
        break;
      case 'categories' :
        $upload_config = config_item('_category_image_upload_config'); // from config
        break;
      case 'products' :
        $upload_config = config_item('_product_image_upload_config'); // from config
        $upload_config['upload_path'] = './assets/images/products_additional_images/';
        break;
      case 'packs' :
        $upload_config = config_item('_pack_image_upload_config'); // from config
        break;
      default:
        $upload_config = config_item('_product_image_upload_config'); // from config
    }


    $this->load->library('upload', $upload_config);

    if (!$this->upload->do_upload('img')) {
      print_r($this->upload->display_errors());
      echo '<script>alert("Error!");</script>';
      return;
    }
    $uploaded_file_data = $this->upload->data();
    $this->dbdata['upload_file_name'] = $uploaded_file_data['file_name'];

// now let's resize and crop uploaded image if it is neccessary

    switch ($table) {
      case 'sections':
        $resized_and_cropped = $this->baza->resize_and_crop_blog_section_image($uploaded_file_data);
        break;
      case 'articles':
        $resized_and_cropped = $this->baza->resize_and_crop_blog_article_image($uploaded_file_data);
        break;
      case 'brands':
        $resized_and_cropped = $this->baza->resize_and_crop_brand_image($uploaded_file_data);
        break;
      case 'categories':
        $resized_and_cropped = $this->baza->resize_and_crop_category_image($uploaded_file_data);
        break;
      case 'products':
        $resized_and_cropped = $this->baza->resize_and_crop_product_image($uploaded_file_data);
        break;
      case 'cigars':
        $resized_and_cropped = $this->baza->resize_and_crop_cigar_image($uploaded_file_data);
        break;
      case 'packs':
        $resized_and_cropped = $this->baza->resize_and_crop_pack_image($uploaded_file_data);
        break;
      default:
        $resized_and_cropped = $this->baza->resize_and_crop_blog_section_image($uploaded_file_data);
    }

    if (!$resized_and_cropped) {
      print_r($this->image_lib->display_errors());
      echo '<script>alert("Error!");</script>';
      return;
    }
    // end of file upload

    $this->load->model('admin/common_model');
    if ($table == 'cigars' OR $table == 'products') {
      $table = $table == 'cigars' ? 'products' : 'products'; // the same table actually :))
    }
    if (!$this->common_model->update_item_images_set_last($table, $id, $uploaded_file_data['file_name'])) {
      echo '<script>alert("Error!");</script>';
      return;
    }
    switch ($table) {
      case 'sections' :
        $img_path = "/" . config_item('_blog_section_image_path_url');
        break;
      case 'articles' :
        $img_path = "/" . config_item('_blog_article_image_path_url');
        break;
      case 'brands' :
        $img_path = "/" . config_item('_brands_image_path_url');
        break;
      case 'categories' :
        $img_path = "/" . config_item('_categories_image_path_url');
        break;
      case 'products' :
        $img_path = "/" . config_item('_products_additional_image_path_url');
        break;
      case 'cigars' :
        $img_path = "/" . config_item('_products_image_path_url');
        break;
      case 'packs' :
        $img_path = "/" . config_item('_packs_image_path_url');
        break;
      default:
        $img_path = "/" . config_item('_blog_section_image_path_url');
    }
    // HTML goes
    // JS script will update src attribute of img tag
    ?>
    <script>
      window.top.window.document.getElementById('apai').innerHTML += "<img src='<?php echo $img_path . $uploaded_file_data['file_name']; ?>' alt='<?= $uploaded_file_data['file_name']; ?>'>";
      window.top.window.document.getElementById('add_img_filename_loading').className = window.top.window.document.getElementById('add_img_filename_loading').className + " hidden";
    </script>
    <?php
  }

}

// end of class




