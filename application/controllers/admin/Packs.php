<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Packs extends CI_Controller {

  public function __construct() {
    parent::__construct();


    // ===============
    // === SERVICE ===
    // ===============
    //$this->output->enable_profiler(TRUE);
    // ===============
    // ===============
    // ===============
    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  public function create_pack() {
    $product_id = $this->uri->segment(3);


    $this->db->where('id', $product_id);
    $this->db->select('name, img, used_by_user_id');
    $query = $this->db->get('products');

    $data['product_name'] = Baza::decode_plain_string_from_db($query->row()->name);
    $data['product_img'] = Baza::decode_plain_string_from_db($query->row()->img);
    $data['product_used_by_user_id'] = $query->row()->used_by_user_id;

    $data['meta_title'] = 'Create pack for product';
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name Of Pack', 'trim|required|min_length[1]|max_length[255]');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/packs/pack_create', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    $this->dbdata['product_id'] = $product_id;
    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));

    $this->load->model("admin/packs_model");
    if ($this->packs_model->set_pack($this->dbdata)) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/packs/pack_create_success', $data);
      $this->load->view('templates/admin/footer', $data);
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/packs/pack_create_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

  public function all_packs() {

    $data['meta_title'] = "Packs";
    $num_of_packs = $this->db->count_all('packs');

    $this->load->model('admin/packs_model');
    $this->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_packs; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item('_admin_packs_per_page');
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['packs'] = $this->packs_model->get_packs_by_product_id(NULL, $limit, $offset, false);

    $data['row_counter'] = $offset + 1;
    $data['num_of_packs'] = $num_of_packs;

    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/packs/packs_all_head', $data);
    $this->load->view('admin/packs/packs_table', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function packs_of_product() {

    $product_id = $this->uri->segment(3);

    $this->db->where('id', $product_id);
    $this->db->select('name, img, used_by_user_id');
    $query = $this->db->get('products');
    $product_name = Baza::decode_plain_string_from_db($query->row()->name);


    $data['meta_title'] = "Packs of " . $product_name;
    $data['title'] = $product_name;
    $data['product_id'] = $product_id;
    $data['product_img'] = Baza::decode_plain_string_from_db($query->row()->img);
    $data['product_used_by_user_id'] = $query->row()->used_by_user_id;

    $this->db->where('product_id', $product_id);
    $this->db->select('id');
    $query = $this->db->get('packs');
    $num_of_packs = count($query->result_array());


    $this->load->model('admin/packs_model');
    $this->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_packs; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item('_admin_packs_per_page');
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['packs'] = $this->packs_model->get_packs_by_product_id($product_id, $limit, $offset, false);

    $data['row_counter'] = $offset + 1;
    $data['num_of_packs'] = $num_of_packs;

    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/packs/packs_by_product_head', $data);
    $this->load->view('admin/packs/packs_table', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function pack_edit() {
    $id = $this->uri->segment(3);

    $this->load->model('admin/packs_model');
    $data['pack'] = $this->packs_model->get_pack_by_id($id, false);
    if (!$data['pack']) {
      show_404();
    }

    // если продукт используется кем угодно - нахер
    if ($data['pack']['product_used_by_user_id'] != null) {
      show_404();
    }

    $data['item']['id'] = $id;
    $data['item']['name'] = Baza::decode_plain_string_from_db($data['pack']['name']);
    $data['item']['product_name'] = Baza::decode_plain_string_from_db($data['pack']['product_name']);
    $data['item']['img'] = Baza::decode_plain_string_from_db($data['pack']['img']);
    $data['item']['html'] = Baza::decode_html_string_from_db($data['pack']['html']);
    $data['item']['num_of_items'] = Baza::decode_plain_string_from_db($data['pack']['num_of_items']);
    $data['item']['price'] = Baza::decode_plain_string_from_db($data['pack']['price']);
    $data['item']['quantity'] = Baza::decode_plain_string_from_db($data['pack']['quantity']);
    $data['item']['published'] = Baza::decode_plain_string_from_db($data['pack']['published']);

    $data['title'] = $data['item']['name'];
    $data['meta_title'] = 'Editing pack: ' . $data['title'];

    // если новая загрузка страницы (т.е. без подтверждения формы)
    if (!isset($_POST['modified_fields'])) {
      $this->load->model('admin/common_model');
      //$this->common_model->set_item_used_by_user('products', $id, $this->session->userdata('user')['id']);
      $this->pack_edit_new_load($id, $data);
      // если подтверждение формы
    } else {
      $data['modified_fields'] = $this->input->post('modified_fields');
      $this->pack_edit_update($id, $data);
    }
  }

  private function pack_edit_new_load($id, $data) {

    //$data['title'] = $data['item']['name'];
    //$data['meta_title'] = "Category edit: " . $data['item']['name'];

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/packs/pack_edit', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  private function pack_edit_update($id, $data) {

    $data['item']['id'] = $id;

    // now form validation goes
    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'Name Of Pack', 'trim|required|min_length[1]|max_length[255]');
    $this->form_validation->set_rules('price', 'Price', 'trim|required|integer|greater_than[-1]|less_than[16777215]');
    $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|integer|greater_than[-1]|less_than[9999]');
    $this->form_validation->set_rules('num_of_items', 'Number Of Items In The Pack', 'trim|required|integer|greater_than[-1]|less_than[9999]');
    $this->form_validation->set_rules('published', 'Published', 'checkbox');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/packs/pack_edit', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    /*
     * preparing data for database to $dbdata array
     */

    $this->load->model('admin/common_model');

    $fields_to_update = array();
    if (null !== ($this->input->post('modified_fields')) && $this->input->post('modified_fields') != '') {
      $fields_to_update = explode(',', $this->input->post('modified_fields'));
    }

    if (count($fields_to_update) === 0) {
      // если нажали просто сохранить, то еще раз выводим это
      if ($this->input->post('submit') === config_item('_submit_save')) {
	$this->pack_edit_new_load($id, $data);
	return;
      }
      // это значит сохранить и выйти. нам надо удалить маркет о юзере из базы и сделать redirect
      else {
	//$this->common_model->unset_item_used_by_user('products', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    }
    unset($_POST['modified_fields']); // clear it

    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));
    $this->dbdata['html'] = Baza::encode_html_string_to_db($this->input->post('html'));
    $this->dbdata['price'] = Baza::encode_plain_string_to_db($this->input->post('price'));
    $this->dbdata['num_of_items'] = Baza::encode_plain_string_to_db($this->input->post('num_of_items'));
    $this->dbdata['quantity'] = Baza::encode_plain_string_to_db($this->input->post('quantity'));
    $this->dbdata['published'] = $this->input->post('published') === 'on' ? 1 : 0;


    /*
     * now let's set to database with transaction
     */

    $this->load->model("admin/packs_model");
    if ($this->packs_model->update_pack($id, $this->dbdata, $fields_to_update)) {
      if ($this->input->post('submit') === config_item('_submit_save')) {
	// UPDATE SUCCESS!
	redirect(current_url());
      } else {
	// UPDATE SUCCESS!
	//$this->common_model->unset_item_used_by_user('products', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/packs/pack_edit_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }
  
   public function pack_preview() {
    $data['meta_title'] = "Preview Pack";
    $data['title'] = "Preview Pack";

    $id = $this->uri->segment(3);
    $this->load->model('admin/packs_model');
    $pack = $this->packs_model->get_pack_by_id($id, false);
    if (!$pack) {
      show_404();
    }

    $data['id'] = $id;
    $data['name'] = Baza::decode_plain_string_from_db($pack['name']);
    $data['product_name'] = Baza::decode_plain_string_from_db($pack['product_name']);
    $data['img'] = Baza::decode_plain_string_from_db($pack['img']);
    $data['price'] = Baza::decode_plain_string_from_db($pack['price']);
    $data['quantity'] = Baza::decode_plain_string_from_db($pack['quantity']);
    $data['num_of_items'] = Baza::decode_plain_string_from_db($pack['num_of_items']);
    $data['html'] = Baza::decode_html_string_from_db($pack['html']); // html
    $data['published'] = Baza::decode_plain_string_from_db($pack['published']);

    $this->load->view('templates/public/header', $data);
    $this->load->view('admin/packs/pack_preview', $data);
    $this->load->view('templates/public/footer', $data);
  }

}

// end of class

