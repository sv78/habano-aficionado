<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends CI_Controller {

  public function __construct() {
    parent::__construct();

    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  public function all_brands() {
    $data['meta_title'] = "Brands";
    $num_of_brands = $this->db->count_all('brands');

    $this->load->model('admin/brands_model');
    $this->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_brands; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_admin_brands_per_page");
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['brands'] = $this->brands_model->get_brands($limit, $offset);

    $data['row_counter'] = $offset + 1;
    $data['num_of_brands'] = $num_of_brands;

    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    //$this->load->view('admin/blog/articles_all_head', $data);
    $this->load->view('admin/brands/brands_view', $data);
    //$this->load->view('admin/blog/articles_under_table', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function create_brand() {
    $data['meta_title'] = "Create Brand";
    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('country', 'Country', 'trim|required|integer|greater_than[-1]|less_than[' . count(config_item('_flt_countries')) . ']');
    $this->form_validation->set_rules('name', 'Name Of Brand', 'trim|required|min_length[1]|max_length[255]');
    $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs|is_unique[brands.slug]', array(
	'is_unique' => 'Such a slug is already used. Slug must be unique.'
    ));
    $this->form_validation->set_rules('is_cigar_brand', 'Is Cigar Brand', 'checkbox');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/brands/brand_create', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    $this->dbdata['country'] = Baza::encode_plain_string_to_db($this->input->post('country'));
    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['is_cigar_brand'] = $this->input->post('is_cigar_brand') === 'on' ? 1 : 0; // не проверяем само значение здесь, т.к. оно проверена на этапе валидации
    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));

    $this->load->model("admin/brands_model");
    if ($this->brands_model->set_brand($this->dbdata)) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/brands/brand_create_success', $data);
      $this->load->view('templates/admin/footer', $data);
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/brands/brand_create_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

  public function brand_preview() {
    $data['meta_title'] = "Preview Brand";
    $data['title'] = "Preview Brand";

    $id = $this->uri->segment(3);
    $this->load->model('admin/common_model');
    $brand = $this->common_model->get_item_by_id('brands', $id, true);
    if (!$brand) {
      show_404();
    }

    $data['id'] = $id;
    $data['slug'] = $brand['slug'];
    $data['name'] = Baza::decode_plain_string_from_db($brand['name']);
    $data['country'] = config_item('_flt_countries')[Baza::decode_plain_string_from_db($brand['country'])]['name']; // careful :))) long statement
    $data['intro'] = Baza::decode_plain_string_from_db($brand['intro']);
    $data['img'] = Baza::decode_plain_string_from_db($brand['img']);

    $data['html'] = Baza::decode_html_string_from_db($brand['html']); // html

    $data['published'] = Baza::decode_plain_string_from_db($brand['published']);
    $data['is_cigar_brand'] = Baza::decode_plain_string_from_db($brand['is_cigar_brand']);
    $data['meta_title'] = Baza::decode_plain_string_from_db($brand['meta_title']);
    $data['meta_description'] = Baza::decode_plain_string_from_db($brand['meta_description']);
    $data['meta_keywords'] = Baza::decode_plain_string_from_db($brand['meta_keywords']);
    $data['meta_robots'] = Baza::decode_plain_string_from_db($brand['meta_robots']);

    $this->load->view('templates/public/header', $data);
    $this->load->view('admin/brands/brand_preview', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function brand_edit() {
    $id = $this->uri->segment(3);
    $this->load->model('admin/common_model');
    $data['brand'] = $this->common_model->get_item_by_id('brands', $id, TRUE); // return as array if true
    if (!$data['brand']) {
      show_404();
    }

    // если id пользователя и id использующего не равны и это не суперадмин - нахер его
    if ($data['brand']['used_by_user_id'] != null && $data['brand']['used_by_user_id'] != $this->session->userdata('user')['id'] && !isset($this->session->userdata('user')['is_super_admin'])) {
      show_404();
    }

    $data['item']['img'] = Baza::decode_plain_string_from_db($data['brand']['img']);
    $data['title'] = Baza::decode_plain_string_from_db($data['brand']['name']);
    $data['meta_title'] = 'Edititing: ' . $data['title'];

    // если новая загрузка страницы (т.е. без подтверждения формы)
    if (!isset($_POST['modified_fields'])) {
      $this->common_model->set_item_used_by_user('brands', $id, $this->session->userdata('user')['id']);
      $this->brand_edit_new_load($id, $data);
      // если подтверждение формы
    } else {
      $data['modified_fields'] = $this->input->post('modified_fields');
      $this->brand_edit_update($id, $data);
    }
  }

  private function brand_edit_new_load($id, $data) {
    // article data here

    $data['item']['id'] = $id;
    $data['item']['slug'] = $data['brand']['slug'];
    $data['item']['name'] = Baza::decode_plain_string_from_db($data['brand']['name']);
    $data['item']['country'] = Baza::decode_plain_string_from_db($data['brand']['country']);


    $data['item']['intro'] = Baza::decode_plain_string_from_db($data['brand']['intro']);

    //$data['item']['img'] = Baza::decode_plain_string_from_db($data['brand']['img']);

    $data['item']['html'] = Baza::decode_html_string_from_db($data['brand']['html']); // html

    $data['item']['published'] = Baza::decode_plain_string_from_db($data['brand']['published']);

    $data['item']['is_cigar_brand'] = Baza::decode_plain_string_from_db($data['brand']['is_cigar_brand']);
    $data['item']['brand_of_day'] = Baza::decode_plain_string_from_db($data['brand']['brand_of_day']);

    $data['item']['meta_title'] = Baza::decode_plain_string_from_db($data['brand']['meta_title']);
    $data['item']['meta_description'] = Baza::decode_plain_string_from_db($data['brand']['meta_description']);
    $data['item']['meta_keywords'] = Baza::decode_plain_string_from_db($data['brand']['meta_keywords']);
    $data['item']['meta_robots'] = Baza::decode_plain_string_from_db($data['brand']['meta_robots']);


    $data['title'] = $data['item']['name'];
    $data['meta_title'] = "Editing : " . $data['item']['name'];

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/brands/brand_edit', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  private function brand_edit_update($id, $data) {

    $data['item']['id'] = $id;

    // now form validation goes
    $this->load->library('form_validation');

    $this->form_validation->set_rules('country', 'Country', 'trim|required|integer|greater_than[-1]|less_than[' . count(config_item('_flt_countries')) . ']');
    $this->form_validation->set_rules('name', 'Name Of Brand', 'trim|required|min_length[1]|max_length[255]');

    // если значение slug в массиве POST равен значению в базе, то проверять на уникальнсть не будем и обновлять значение тоже
    if ($this->input->post('slug') === $data['brand']['slug']) {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    } else {
      $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[128]|alpha_numeric_dash|reserved_slugs|is_unique[brands.slug]', array(
	  'is_unique' => 'Such a slug is already used. Slug must be unique.'
      ));
    }

    $this->form_validation->set_rules('published', 'Published', 'checkbox');
    $this->form_validation->set_rules('is_cigar_brand', 'Is Cigar Brand', 'checkbox');
    $this->form_validation->set_rules('brand_of_day', 'Brand Of Day', 'checkbox');
    $this->form_validation->set_rules('meta_robots', 'Meta robots', 'required|robots_metatag');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/brands/brand_edit', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    /*
     * preparing data for database to $dbdata array
     */

    $this->load->model('admin/common_model');

    $fields_to_update = array();
    if (null !== ($this->input->post('modified_fields')) && $this->input->post('modified_fields') != '') {
      $fields_to_update = explode(',', $this->input->post('modified_fields'));
    }

    if (count($fields_to_update) === 0) {
      // если нажали просто сохранить, то еще раз выводим это
      if ($this->input->post('submit') === config_item('_submit_save')) {
	$this->brand_edit_new_load($id, $data);
	return;
      }
      // это значит сохранить и выйти. нам надо удалить маркет о юзере из базы и сделать redirect
      else {
	$this->common_model->unset_item_used_by_user('brands', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    }
    unset($_POST['modified_fields']); // clear it

    $this->dbdata['country'] = Baza::encode_plain_string_to_db($this->input->post('country'));
    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));
    $this->dbdata['slug'] = Baza::encode_plain_string_to_db($this->input->post('slug'));
    $this->dbdata['published'] = $this->input->post('published') === 'on' ? 1 : 0; // не проверяем само значение здесь, т.к. оно проверена на этапе валидации
    $this->dbdata['is_cigar_brand'] = $this->input->post('is_cigar_brand') === 'on' ? 1 : 0;
    $this->dbdata['brand_of_day'] = $this->input->post('brand_of_day') === 'on' ? 1 : 0;

    //$this->dbdata['brand_of_day'] = $this->dbdata['is_cigar_brand'] === 1 ? $this->dbdata['brand_of_day'] : NULL; // brand of day can be applied only to cigar brands

    $this->dbdata['intro'] = Baza::encode_plain_string_to_db($this->input->post('intro'));

    $this->dbdata['html'] = Baza::encode_html_string_to_db($this->input->post('html'));

    $this->dbdata['meta_title'] = Baza::encode_plain_string_to_db($this->input->post('meta_title'));
    $this->dbdata['meta_description'] = Baza::encode_plain_string_to_db($this->input->post('meta_description'));
    $this->dbdata['meta_keywords'] = Baza::encode_plain_string_to_db($this->input->post('meta_keywords'));
    $this->dbdata['meta_robots'] = Baza::encode_plain_string_to_db($this->input->post('meta_robots'));

    /*
     * now let's set to database with transaction
     */

    $this->load->model("admin/brands_model");
    if ($this->brands_model->update_brand($id, $this->dbdata, $fields_to_update)) {
      if ($this->input->post('submit') === config_item('_submit_save')) {
	// UPDATE SUCCESS!
	$this->brands_model->check_and_fix_brand_of_day($id);
	redirect(current_url());
      } else {
	// UPDATE SUCCESS!
	$this->brands_model->check_and_fix_brand_of_day($id);
	$this->common_model->unset_item_used_by_user('brands', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/brands/brand_edit_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

}

// end of class

