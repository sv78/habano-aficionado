<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends CI_Controller {

  public function __construct() {
    parent::__construct();


    // ===============
    // === SERVICE ===
    // ===============
    //$this->output->enable_profiler(TRUE);
    // ===============
    // ===============
    // ===============
    // we don't want to show admin to public - we check user status
    if (!isset($_SESSION['user'])) {
      show_404();
      exit;
    }
    if (!$_SESSION['user']['is_admin'] && (!isset($_SESSION['user']['is_super_admin']) OR ! $_SESSION['user']['is_super_admin'])) {
      show_404();
      exit;
    }
  }

  public function all_banners() {

    $data['meta_title'] = "Баннеры";
    $num_of_banners = $this->db->count_all('banners');

    $this->load->model('admin/banners_model');
    $this->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_banners; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item('_admin_banners_per_page');
    $this->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];
    $data['banners'] = $this->banners_model->get_banners($limit, $offset);

    $data['row_counter'] = $offset + 1;
    $data['num_of_banners'] = $num_of_banners;

    $this->back_url->set_parent_url();

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/banners/banners_view', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  public function create_banner() {
    $data['meta_title'] = "Создать баннер";
    // now form validation goes
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name Of Banner', 'trim|required|min_length[1]|max_length[255]');

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/banners/banner_create', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));

    $this->load->model("admin/banners_model");
    if ($this->banners_model->set_banner($this->dbdata)) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/banners/banner_create_success', $data);
      $this->load->view('templates/admin/footer', $data);
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/banners/banner_create_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

  public function banner_preview() {

    $data['meta_title'] = "Preview Баннера";
    $data['title'] = "Preview Баннера";

    $id = $this->uri->segment(3);
    $this->load->model('admin/banners_model');
    $banner = $this->banners_model->get_banner_by_id($id, FALSE);
    if (!$banner) {
      show_404();
    }

    $data['id'] = $id;
    $data['name'] = Baza::decode_plain_string_from_db($banner['name']);
    $data['url'] = Baza::decode_plain_string_from_db($banner['url']);
    $data['alt_text'] = Baza::decode_plain_string_from_db($banner['alt_text']);
    $data['img_src'] = Baza::decode_plain_string_from_db($banner['img_src']);
    $data['newtab'] = Baza::decode_plain_string_from_db($banner['newtab']);
    $data['published'] = Baza::decode_plain_string_from_db($banner['published']);

    $this->load->view('templates/public/header', $data);
    $this->load->view('admin/banners/banner_preview', $data);
    $this->load->view('templates/public/footer', $data);
  }

  public function banner_edit() {
    $id = $this->uri->segment(3);
    $this->load->model('admin/banners_model');
    $data['banner'] = $this->banners_model->get_banner_by_id($id);
    if (!$data['banner']) {
      show_404();
    }

    // если id пользователя и id использующего не равны и это не суперадмин - нахер его
    if ($data['banner']['used_by_user_id'] != null && $data['banner']['used_by_user_id'] != $this->session->userdata('user')['id'] && !isset($this->session->userdata('user')['is_super_admin'])) {
      show_404();
    }

    $data['name'] = Baza::decode_plain_string_from_db($data['banner']['name']);
    $data['alt_text'] = Baza::decode_plain_string_from_db($data['banner']['alt_text']);
    $data['img_src'] = Baza::decode_plain_string_from_db($data['banner']['img_src']);

    $data['title'] = $data['name'];
    $data['meta_title'] = 'Editing banner: ' . $data['name'];
    // если новая загрузка страницы (т.е. без подтверждения формы)
    if (!isset($_POST['modified_fields'])) {
      $this->load->model('admin/common_model');
      $this->common_model->set_item_used_by_user('banners', $id, $this->session->userdata('user')['id']);
      $this->banner_edit_new_load($id, $data);
      // если подтверждение формы
    } else {
      $data['modified_fields'] = $this->input->post('modified_fields');
      $this->banner_edit_update($id, $data);
    }
  }

  private function banner_edit_new_load($id, $data) {

    $data['item']['id'] = $id;
    $data['item']['name'] = Baza::decode_plain_string_from_db($data['banner']['name']);
    $data['item']['url'] = Baza::decode_plain_string_from_db($data['banner']['url']);
    $data['item']['alt_text'] = Baza::decode_plain_string_from_db($data['banner']['alt_text']);
    $data['item']['img_src'] = Baza::decode_plain_string_from_db($data['banner']['img_src']);

    $data['item']['newtab'] = Baza::decode_plain_string_from_db($data['banner']['newtab']);
    $data['item']['published'] = Baza::decode_plain_string_from_db($data['banner']['published']);

    $data['title'] = $data['item']['name'];
    $data['meta_title'] = "Banner edit: " . $data['item']['name'];

    $this->load->view('templates/admin/header', $data);
    $this->load->view('admin/banners/banner_edit', $data);
    $this->load->view('templates/admin/footer', $data);
  }

  private function banner_edit_update($id, $data) {
    $data['item']['id'] = $id;
    // now form validation goes
    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'Name Of Banner', 'trim|required|min_length[1]|max_length[255]');
    $this->form_validation->set_rules('url', 'Banner Destination URL', 'trim|required|min_length[11]|max_length[1024]|valid_url');
    $this->form_validation->set_rules('alt_text', 'Alt Text', 'trim|required|min_length[1]|max_length[255]');
    $this->form_validation->set_rules('img_src', 'Image Source', 'trim|required|min_length[6]|max_length[512]|valid_url');
    $this->form_validation->set_rules('newtab', 'Open In New Tab', 'checkbox');
    $this->form_validation->set_rules('published', 'Published', 'checkbox');


    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/banners/banner_edit', $data);
      $this->load->view('templates/admin/footer', $data);
      return;
    }

    /*
     * preparing data for database to $dbdata array
     */

    $this->load->model('admin/common_model');

    $fields_to_update = array();
    if (null !== ($this->input->post('modified_fields')) && $this->input->post('modified_fields') != '') {
      $fields_to_update = explode(',', $this->input->post('modified_fields'));
    }

    if (count($fields_to_update) === 0) {
      // если нажали просто сохранить, то еще раз выводим это
      if ($this->input->post('submit') === config_item('_submit_save')) {
	$this->banner_edit_new_load($id, $data);
	return;
      }
      // это значит сохранить и выйти. нам надо удалить маркет о юзере из базы и сделать redirect
      else {
	$this->common_model->unset_item_used_by_user('banners', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    }
    unset($_POST['modified_fields']); // clear it

    $this->dbdata['name'] = Baza::encode_plain_string_to_db($this->input->post('name'));
    $this->dbdata['url'] = Baza::encode_plain_string_to_db($this->input->post('url'));
    $this->dbdata['alt_text'] = Baza::encode_plain_string_to_db($this->input->post('alt_text'));
    $this->dbdata['img_src'] = Baza::encode_plain_string_to_db($this->input->post('img_src'));
    $this->dbdata['newtab'] = $this->input->post('newtab') === 'on' ? 1 : 0;
    $this->dbdata['published'] = $this->input->post('published') === 'on' ? 1 : 0;

    /*
     * now let's set to database with transaction
     */

    $this->load->model("admin/banners_model");
    if ($this->banners_model->update_banner($id, $this->dbdata, $fields_to_update)) {
      if ($this->input->post('submit') === config_item('_submit_save')) {
	// UPDATE SUCCESS!
	redirect(current_url());
      } else {
	// UPDATE SUCCESS!
	$this->common_model->unset_item_used_by_user('banners', $id, $this->session->userdata('user')['id']);
	redirect($this->back_url->get_parent_url());
      }
    } else {
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/banners/banner_edit_failure', $data);
      $this->load->view('templates/admin/footer', $data);
    }
  }

}

// end of class

