<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="pagination">
  <?= $this->pagination->create_links() ?>
</div>
