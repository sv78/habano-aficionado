<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите желаемый диаметр</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Ring Gauge</div>
    </div>

    <ul id="flt_items_vals_view" class="flt-vals-container"></ul>
    
    <p class="text-center smaller">Ring Gauge (RG) обозначает диаметр. 1 RG = 1/64 дюйма или приблизительно 0,4 миллиметра.</p>
    
    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-filter-window"></div>

  </div>