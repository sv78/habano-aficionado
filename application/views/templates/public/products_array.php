<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//print_r($products);


if (count($products) == 0) {
  return;
}
?>
<div class="container products-array">
  <div class="row">

    <?php
    foreach ($products as $product):

      $ci_category_slug = $product['category_slug'] != NULL ? Baza::decode_plain_string_from_db($product['category_slug']) : '';
      $ci_category_path = 'catalog/' . $ci_category_slug . '/';
      ?>

      <div class="product-cell column col-lg-3 col-sm-6 col-xs-12">
        <div class="product-cell-inner">


          <?php
          $intro_str = mb_substr(Baza::decode_plain_string_from_db($product['intro']), 0, 50);
          $intro_str = $intro_str == '' ? '' : $intro_str . '…';
          ?>


          <a href="/<?= $ci_category_path . Baza::decode_plain_string_from_db($product['slug']); ?>">
            <?php
            if ($product['img'] == '') {
              $img_src = "/" . config_item('_product_not_specified_image_url');
            } else if (!file_exists(config_item('_products_image_path') . $product['img'])) {
              $img_src = "/" . config_item('_product_missing_image_url');
            } else {
              $img_src = "/" . config_item('_products_image_path_url') . $product['img'];
            }
            ?>
            <img src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($product['name']) ?>" title="Нажмите, чтобы узнать подробнее">
          </a>


          <h4><a href="/<?= $ci_category_path . Baza::decode_plain_string_from_db($product['slug']); ?>"><?= Baza::decode_plain_string_from_db($product['name']) ?></a></h4>
          <p class="product-cell-description"><?= $intro_str ?></p>


          <?php
          if ($product['price'] == 0):
            ?>
            <p class="product-cell-price"><small><?=$this->config->item('_product_price_zero_substitution');?></small></p>
            <?php
          else:
            ?>
            <p class="product-cell-price"><?= Baza::decode_plain_string_from_db($product['price']) ?> руб.</p>
          <?php
          endif;
          ?>


          <p><a href="/<?= $ci_category_path . Baza::decode_plain_string_from_db($product['slug']); ?>" class="product-cell-add-to-cart-btn">Узнать подробнее</a></p>




        </div>
      </div>

      <?php
    endforeach;
    ?>
  </div>
</div>
