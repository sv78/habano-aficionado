<script id="preloader-script-pre">
  function getElementHeight(obj) {
    var h;
    if (obj == window) {
      h = (obj.innerHeight) ? obj.innerHeight : obj.clientHeight;
    } else {
      h = obj.offsetHeight;
    }
    return h;
  }
  function getElementWidth(obj) {
    var w;
    if (obj == window) {
      w = (obj.innerWidth) ? obj.innerWidth : obj.clientWidth;
    } else {
      w = obj.offsetWidth;
    }
    return w;
  }
  function alignCenterVertical(objToAlign, relativeObj, offsetToTop) {
    if (offsetToTop == undefined) {
      offsetToTop = 0;
    }
    var ota_h = getElementHeight(objToAlign);
    var ro_h = getElementHeight(relativeObj);
    objToAlign.style.top = (ro_h - ota_h) / 2 - offsetToTop + "px";
  }
</script>

<div id="preloader">
  <div id="preloader_container" class="preloader-container">
    <div class="preloader-loading">Habano-Aficionado</div>
    <div class="preloader-line"></div>
  </div>
</div>

<script id="preloader-script-post">
  if (document.getElementById("preloader_container")) {
    alignCenterVertical(document.getElementById("preloader_container"), window);
    document.getElementById("preloader_container").style.opacity = 1;
  }
</script>