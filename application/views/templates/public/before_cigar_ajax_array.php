<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//print_r($params);
?>

<div class="lined-text-small-container">
  <div class="line-text-line"></div>
  <div class="line-text-text color-ha-light">Сигары по критериям:</div>
</div>

<ul class="filter-found-by-criteria-string">
  <?php
  /*
    if ($params['flt_country'] != -1):
    ?>
    <li>
    <span>Страна:</span>
    <span><?= config_item('_flt_countries')[$params['flt_country']]['name'] ?></span>
    </li>
    <?php
    endif;
   */
  if ($params['flt_brand_id'] != -1 && $params['flt_brand_name'] != NULL):
    ?>
    <li>
      <span>Марка:</span>
      <span><?= Baza::decode_plain_string_from_db($params['flt_brand_name']) ?></span>
    </li>
    <?php
  endif;

  if ($params['flt_vitola'] != -1):
    ?>
    <li>
      <span>Витола:</span>
      <span><?= config_item('_flt_vitolas')[$params['flt_vitola']]['name'] ?></span>
    </li>
    <?php
  endif;

  if ($params['flt_line'] != -1):
    ?>
    <li>
      <span>Серия:</span>
      <span><?= config_item('_flt_lines')[$params['flt_line']]['name'] ?></span>
    </li>
    <?php
  endif;

  if ($params['flt_length'] != -1):
    ?>
    <li>
      <span>Длина:</span>
      <span><?= config_item('_flt_lengths')[$params['flt_length']]['mm'] . ' mm (' . config_item('_flt_lengths')[$params['flt_length']]['inches'] . ')' ?></span>
    </li>
    <?php
  endif;

  if ($params['flt_cepo'] != -1):
    ?>
    <li>
      <span>Ring Gauge:</span>
      <span><?= $params['flt_cepo'] ?></span>
    </li>
    <?php
  endif;

  if ($params['flt_intensity'] != -1):
    ?>
    <li>
      <span>Крепость:</span>
      <span><?= config_item('_flt_intensities')[$params['flt_intensity']]['name'] ?></span>
    </li>
    <?php
  endif;

  if ($params['flt_wrapper'] != -1):
    ?>
    <li>
      <span>Покровный лист:</span>
      <span><?= config_item('_flt_wrappers')[$params['flt_wrapper']]['name'] ?></span>
    </li>
    <?php
  endif;

  if ($params['flt_flavour'] != -1):
    ?>
    <li>
      <span>Вкус:</span>
      <span><?= config_item('_flt_flavours')[$params['flt_flavour']]['name'] ?></span>
    </li>
    <?php
  endif;

  if ($params['flt_price_max'] != -1):
    ?>
    <li>
      <span>Цена:</span>
      <span><?= 'не&nbsp;более&nbsp;' . $params['flt_price_max'] . '&nbsp;рублей за&nbsp;штуку' ?></span>
    </li>
    <?php
  endif;
  ?>
</ul>