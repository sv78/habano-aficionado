<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- TOP NAVIGATION BAR -->

<div id="topnavbar" class="container-fluid">
  <div class="container">
    <a href="/" class="tnb-logo-ha"></a>
    <div id="tnb_munubtn" data-action-type="open-site-menu" title="Меню"></div>
    <?php
    if ($this->session->userdata('user')) {
      $tbn_user_string = $this->session->userdata('user')['email'];
      $tbn_user_link = '/profile';
    } else {
      $tbn_user_string = "Войти";
      $tbn_user_link = '/login';
    }
    ?>
    <div class="tnb-phone-number-string"><?= config_item('_contacts')['topnavbar_phone_str'] ?></div>
    <a href="tel:<?= config_item('_contacts')['phone'] ?>" id="tnb_call" class="tbn-phone-icon" data-action-type="call" title="Позвонить"></a>
    <a href="<?= $tbn_user_link ?>" class="tnb-user-string"><?= $tbn_user_string ?></a>
    <a href="<?= $tbn_user_link ?>" class="tbn-user-icon"></a>
    <a href="/cart" class="tnb-order-string" id="cart_top_nav_enter_link"><?= $this->shopcart->get_top_panel_icon_value(); ?></a>
    <a href="/cart" class="tbn-order-icon"></a>
  </div>
</div>

<!-- / TOP NAVIGATION BAR -->

<div class="section-devider"></div>