<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container side-padding">
  <img class="empty-cart-pic" src="/assets/images/style/empty_cart.png" alt="Резерв пуст">
  <h1>Пока пусто…</h1>
  <h4 class="text-center">Уважаемый посетитель, Ваш резерв пока пуст!</h4>
  <p class="text-center">Добавляйте любые товары в&nbsp;резерв. Оформляйте Ваш персональный заказ на нашем сайте и&nbsp;получайте товары в&nbsp;клубных магазинах или с&nbsp;помощью нашей службы доставки! Благодарим Вас за использование нашего сервиса!</p>
</div>