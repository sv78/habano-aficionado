<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ($product['img'] == '') {
  $img_src = "/" . config_item('_cigar_not_specified_image_url');
} else if (!file_exists(config_item('_products_image_path') . $product['img'])) {
  $img_src = "/" . config_item('_cigar_missing_image_url');
} else {
  $img_src = "/" . config_item('_products_image_path_url') . $product['img'];
}
?>

<div class="container side-padding">
  <h1><?= Baza::decode_plain_string_from_db($product['name']) ?></h1>

  <div class="product-cigar-img-container">
    <img class="img-responsive" src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($product['name']) ?>">
  </div>

</div>

<?php
if ($product['images'] != NULL && $product['images'] != 'NULL'):
  $add_images = unserialize($product['images']);
  // forming additionals images gallery
  ?>

  <script src="/assets/js/magnific_popup_0.9.9/jquery.magnific-popup.min.js"></script>


  <div id="magnific-gal-container" class="container product-images-gal-thumbs">
    <ul>

      <?php
      foreach ($add_images as $image_file_name) :
        if ($image_file_name == '') {
          $img_src = "/" . config_item('_product_not_specified_image_url');
        } else if (!file_exists(config_item('_products_additional_image_path') . $image_file_name)) {
          $img_src = "/" . config_item('_product_missing_image_url');
        } else {
          $img_src = "/" . config_item('_products_additional_image_path_url') . $image_file_name;
        }
        ?>
        <li>
          <a href="<?= $img_src ?>">
            <img src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($product['name']) ?>">
          </a>
        </li>

        <?php
      endforeach;
      ?>

    </ul>
  </div>


  <script>
    $('#magnific-gal-container').magnificPopup({
      type: 'image',
      delegate: 'a', // child items selector, by clicking on it popup will open
      mainClass: 'mfp-img-mobile',
      image: {
        verticalFit: true
      },
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        //arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button
        arrowMarkup: '<span title="%title%" class="mfp-custom mfp-custom-%dir%"></span>', // markup of an arrow button

        tPrev: 'Назад (Курсор влево)', // title for left button
        tNext: 'Вперед (Курсор вправо)', // title for right button
        //tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
        tCounter: '%curr% из %total%' // markup of counter
      },

    });
  </script>


  <?php
// end of forming additionals images gallery
endif;
?>







<div class="container">
  <div class="lined-text-small-container">
    <div class="line-text-line"></div>
    <div class="line-text-text">Характеристики:</div>
  </div>
</div>



<!-- about cigar : filter values -->

<div class="container filter-search-div">

  <div class="row">

    <!-- cigar brand country -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Страна</span>
        <span class="filter-value-str"><?= config_item('_flt_countries')[$product['brand_country']]['name'] ?></span>
      </div>
    </div>

    <!-- cigar brand -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Марка</span>
        <span class="filter-value-str"><?= Baza::decode_plain_string_from_db($product['brand_name']) ?></span>
      </div>
    </div>

    <!-- cigar vitola -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Витола</span>
        <span class="filter-value-str"><?= config_item('_flt_vitolas')[$product['vitola']]['name'] ?></span>
      </div>
    </div>

    <!-- cigar line -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Серия</span>
        <span class="filter-value-str"><?= config_item('_flt_lines')[$product['line']]['name'] ?></span>
      </div>
    </div>

    <!-- cigar length -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Длина</span>
        <span class="filter-value-str"><?= config_item('_flt_lengths')[$product['length']]['mm'] . 'mm (' . config_item('_flt_lengths')[$product['length']]['inches'] . ')' ?></span>
      </div>
    </div>

    <!-- cigar cepo -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Диаметр (Ring Gauge)</span>
        <span class="filter-value-str"><?= $product['cepo'] ?></span>
      </div>
    </div>

    <!-- cigar intensity -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Кпепкость</span>
        <span class="filter-value-str"><?= config_item('_flt_intensities')[$product['intensity']]['name'] ?></span>
      </div>
    </div>

    <!-- cigar wrapper -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Лист</span>
        <span class="filter-value-str"><?= config_item('_flt_wrappers')[$product['wrapper']]['name'] ?></span>
      </div>
    </div>

    <!-- cigar flavour -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Вкус</span>
        <span class="filter-value-str"><?= config_item('_flt_flavours')[$product['flavour']]['name'] ?></span>
      </div>
    </div>

    <!-- cigar price -->
    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
        <span class="filter-name-str">Цена</span>
        <span class="filter-value-str"><?= $product['price'] ?> руб.</span>
      </div>
    </div>

  </div>

</div>

<!-- // about cigar : filter values -->


<div class="container side-padding">
  <?= Baza::decode_html_string_from_db($product['html']) ?>

  <div class="lined-text-header-container">
    <div class="line-text-line"></div>
    <?php
    if ($product['price'] == 0):
      ?>
      <div class="line-text-text"><?=$this->config->item('_product_price_zero_substitution');?></div>
      <?php
    else:
      ?>
      <div class="line-text-text"><?= Baza::decode_plain_string_from_db($product['price']) ?> руб.</div>
    <?php
    endif;
    ?>
  </div>


  <?php
  if ($product['price'] != 0):
    ?>

    <div class="text-center">
      <label class="input-text-label">Количество:</label>
      <input id="<?= 'products_' . $product['id'] . '_input' ?>" class="abstract-product-spinner spinner product-order-amount-input" type="text" name="product_amount" autocomplete="off" value="1">
    </div>

    <button onclick="addToCartFromInput('products', '<?= $product['id'] ?>', '<?= 'products_' . $product['id'] . '_input' ?>')" class="btn-sm">Добавить в резерв</button>

    <?php
  endif;
  //echo "product group: " . $product_group_slug . " <br> product : " . $product_slug;
  //echo '<p>Product array</p>';
  //echo '<pre>';
  //print_r($product);
  //echo '</pre>';
  ?>

</div>