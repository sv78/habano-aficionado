<div id="mw_menuwindow" class="hidden">

  <div id="mw_cigar" class="mw-cigar"></div>
  <div id="mw_leaf" class="mw-leaf"></div>

  <div id="mw_content" class="mw-content">

    <div class="mw-content-wraper">

      <a href="/" class="mw-logo" data-action-type="mw-link" title="На главную"></a>

      <ul id="menu_links_container">
        <li><a href="/catalog">Каталог</a></li>
        <li><a href="/brands">Марки сигар</a></li>
        <li class="menu-li-devider"></li>
        <li><a href="/cart">Ваш резерв</a></li>
        <li class="menu-li-devider"></li>
        <li><a href="/blog/events">События</a></li>
        <li><a href="/habanos-specialist">Habanos специалист</a></li>
        <li><a href="/cigar-lounges">Сигарные лаунжи</a></li>
        <li><a href="/blog/about-cigars/habanos-cigars-validation">Идентификация HABANOS</a></li>
        <li class="menu-li-devider"></li>
        <li><a href="/blog">Блог</a></li>
        <li class="menu-li-devider"></li>
        <li><a href="/contacts">Контакты</a></li>
        <li class="menu-li-devider"></li>
	<?php
	if (!empty($this->session->userdata('user')['email'])):
	  ?>
  	<li><a class="lowercase" href="/profile"><?= $this->session->userdata('user')['email'] ?></a></li>
	  <?php
	endif;
	?>
	<?php
	if (!empty($this->session->userdata('user'))) {
	  ?>
  	<li><a href="/logout">Выйти</a></li>
	  <?php
	} else {
	  ?>
  	<li><a href="/login">Вход</a></li>
	  <?php
	}
	?>
      </ul>

    </div>

  </div>

  <a href="http://www.polyartmedia.ru/?from=<?= filter_input(INPUT_SERVER, 'HTTP_HOST') ?>" class="mw-created-by" target="_blank">Сделано Polyartmedia Group</a>
  
  <div id="menuwindowcloser" class="mw-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

</div>