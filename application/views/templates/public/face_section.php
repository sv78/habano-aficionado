<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="face-section container-fluid bg-cover">
  <a href="/">
    <img class="fs-logo img-responsive" src="/<?= config_item('_style_images_path_url') . 'ha_logo_main.png'; ?>" alt="Habano-Aficionado">
  </a>
  <p class="fs-small-slogan"><strong>Официальный продавец Habanos. Habanos&nbsp;специалист.</strong></p>
  <ul>
    <li>
      <a href="/catalog">Каталог</a>
      <div class="fs-link-active-state-indicator"></div>
    </li>
    <li>
      <a href="/brands">Марки&nbsp;сигар</a>
      <div class="fs-link-active-state-indicator"></div>
    </li>
    <li>
      <a href="/blog/events">События</a>
      <div class="fs-link-active-state-indicator"></div>
    </li>
    <li>
      <a href="/cart">Разместить&nbsp;заказ</a>
      <div class="fs-link-active-state-indicator"></div>
    </li>
    <li>
      <a href="/contacts">Контакты</a>
      <div class="fs-link-active-state-indicator"></div>
    </li>
    <li>
      <a href="/blog">Блог</a>
      <div class="fs-link-active-state-indicator"></div>
    </li>
  </ul>
</div>

<div class="section-devider"></div>