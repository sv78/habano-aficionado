<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container side-padding packs-container">


  <div class="lined-text-header-container">
    <div class="line-text-line"></div>
    <div class="line-text-text uppercase">Дополнительно:</div>
  </div>

  <p class="text-center"><strong class="bold"><?= Baza::decode_plain_string_from_db($product['name']); ?> в упаковочных вариантах</strong>:</p>

  <div class="available-next-variants-arrow"></div>

  <?php
  foreach ($packs as $pack):

    if ($pack['img'] == '') {
      $img_src = "/" . config_item('_pack_not_specified_image_url');
    } else if (!file_exists(config_item('_packs_image_path') . $pack['img'])) {
      $img_src = "/" . config_item('_pack_missing_image_url');
    } else {
      $img_src = "/" . config_item('_packs_image_path_url') . $pack['img'];
    }
    ?>

    <h3 class="text-center pack-header"><small><?= Baza::decode_plain_string_from_db($product['name']) . "</small><br>" . Baza::decode_plain_string_from_db($pack['name']); ?></h3>

    <?= Baza::decode_html_string_from_db($pack['html']) ?>

    <div class="product-pack-img-container">
      <img class="img-responsive" src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($pack['name']) ?>">
    </div>


    <p class="text-center">В упаковке <?= Baza::decode_plain_string_from_db($pack['num_of_items']) ?> шт.</p>
    <?php
    if ($pack['price'] == 0):
      ?>
      <h4 class="text-center"><?=$this->config->item('_product_price_zero_substitution');?></h4>
      <?php
    else:
      ?>
      <h4 class="text-center"><?= Baza::decode_plain_string_from_db($pack['price']) ?> руб.</h4>
    <?php
    endif;
    ?>


    <?php
    if ($pack['price'] != 0):
      ?>
      <div class="text-center">
        <input id="<?= 'packs_' . $pack['id'] . '_input' ?>" class="abstract-pack-spinner spinner product-order-amount-input" type="text" name="pack_amount" autocomplete="off" value="1">
      </div>

      <button onclick="addToCartFromInput('packs', '<?= $pack['id'] ?>', '<?= 'packs_' . $pack['id'] . '_input' ?>')" class="btn-sm">Добавить в резерв</button>
      <?php
    endif;
    ?>


    <hr class="hr-pack-devider">

    <?php
  endforeach;
  ?>





  <?php
//echo "product group: " . $product_group_slug . " <br> product : " . $product_slug;
  //echo '<p>Product array</p>';
  //echo '<pre>';
  //print_r($product);
  //echo '</pre>';
  //echo '<hr><p>Packs array</p>';
  //echo count($packs);
  //echo '<pre>';
  //print_r($packs);
  //echo '</pre>';
  ?>


</div>