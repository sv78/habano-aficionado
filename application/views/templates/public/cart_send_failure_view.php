<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container side-padding">
  <h1 class="alert">Ошибка!</h1>
  <p class="text-center">Что-то пошло не так…</p>
  <a class="btn-like btn-max-width offset-top-double" href="/">На главную</a>
</div>