<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!$categories OR count($categories) == 0) {
  return;
}
?>


<div class="container-fluid super-4-cats">

  <div class="container">
    <div id="cats_nav"></div>

    <div id="ha_cats_container">

      <div id="ha_cats" class="owl-carousel">

	<?php
	foreach ($categories as $category):
	  ?>

  	<div class="super-cat">
	    <?php
	    $category_link = '/catalog/' . Baza::decode_plain_string_from_db($category['slug']);

	    if ($category['img'] == '') {
	      $img_src = "/" . config_item('_category_not_specified_image_url');
	    } else if (!file_exists(config_item('_categories_image_path') . $category['img'])) {
	      $img_src = "/" . config_item('_category_missing_image_url');
	    } else {
	      $img_src = "/" . config_item('_categories_image_path_url') . $category['img'];
	    }
	    ?>
  	  <a href="<?= $category_link ?>">

  	    <img src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($category['name']) ?>">
  	  </a>
  	  <h3><a href="<?= $category_link ?>"><?= Baza::decode_plain_string_from_db($category['name']) ?></a></h3>
  	  <p><a href="<?= $category_link ?>"><?= Baza::decode_plain_string_from_db($category['intro']) ?></a></p>
  	</div>

	  <?php
	endforeach;
	?>

      </div>

    </div>


  </div>

</div>


<div class="section-devider"></div>