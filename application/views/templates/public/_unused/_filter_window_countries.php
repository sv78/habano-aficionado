<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите страну сигарной марки</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Страна</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_countries_vals_view" class="flt-vals-container"></ul>

  </div>
</div>

<?php
// preparing countries for js array
$countries_string_for_js = '';
$countries_array = config_item('_flt_countries');

foreach ($countries_array as $cnt) {
  $str_arr = array();
  $str_arr[] = '[';
  $str_arr[] = '"';
  $str_arr[] = $cnt['name'];
  $str_arr[] = '"';
  $str_arr[] = '],';
  $countries_string_for_js .= join('', $str_arr);
}
$countries_string_for_js = trim($countries_string_for_js, ',');
?>

<script>
  var js_ftl_countries = [<?= $countries_string_for_js ?>];
  createFltLines(js_ftl_countries);
  set_body_bg_color(menu_window_bg_color);

  function createFltLines(countries_array) {
    countries_array.unshift([-1]);
    for (var i = 0; i < countries_array.length; i++) {
      var country_li = document.createElement('LI');
      country_li.innerHTML = countries_array[i][0];
      country_li.className = 'flt-listed-value';
      if (countries_array[i][0] === -1) {
	country_li.innerHTML = '--- не важно ---';
      }
      country_li.setAttribute('data-ha-filter-type', 'countries');
      country_li.setAttribute('data-ha-filter-value', i - 1);
      document.getElementById('flt_countries_vals_view').appendChild(country_li);
    }
  }
</script>