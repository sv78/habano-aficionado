<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div class="container main-banner-container">
  
  <div id="ha_banner" class="owl-carousel">

    <div>
      <a href="/">
	<img src="/assets/uploads/images/banners/olivia.jpg" alt="">
      </a>
    </div>
    <div>
      <a href="/">
	<img src="/assets/uploads/images/banners/whiskey.jpg" alt="">
      </a>
    </div>
    <div>
      <a href="/">
	<img src="/assets/uploads/images/banners/custom_pleasure.jpg" alt="">
      </a>
    </div>
    <div>
      <a href="/">
	<img src="/assets/uploads/images/banners/banner_1140x440px.gif" alt="">
      </a>
    </div>
  </div>
  
</div>