<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

      <nav>
	<a href="/">Home</a>
	<span> | </span>
	<a href="/admin">Admin</a>
	<span> | </span>
	<a href="/blog">Blog</a>
	<span> | </span>
	<a href="/registration-confirmation/xxx">Confirm Registration</a>
	<span> | </span>
	<a href="/password-reset-confirmation/xxx">Confirm Password Reset</a>
	<span> | </span>
	<a href="/reset-password">Reset Password</a>
	<span> | </span>
	<a href="/register">Register</a>
	<span> | </span>
	<a href="/login">Login</a>
	<span> | </span>
	<span onclick="logout()" style="cursor:pointer; text-decoration: underline;">Logout</span>

	<?php
	if ($this->session->has_userdata('user')) :
	  ?>
  	<span> | </span>
  	<a href="/profile" target="_self" style="color:red;"><?= $this->session->userdata('user')['email'] ?></a>
	  <?php
	endif;
	if (isset($_SESSION['user']['is_super_admin']) && $_SESSION['user']['is_super_admin'] === TRUE) {
	  echo '<span>*</span>';
	}
	?>
      </nav>

      <hr>


      <script>
	function logout() {
	  if (!confirm('Log out?'))
	    return;
	  window.location.assign(location.protocol + "//" + location.host + "/logout");
	}
      </script>


      <div id="dbg"></div>