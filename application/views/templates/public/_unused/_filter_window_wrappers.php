<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите тип покровного листа</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Лист</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_wrappers_vals_view" class="flt-vals-container"></ul>

  </div>
</div>

<?php
// preparing wrappers for js array
$wrappers_string_for_js = '';
$wrappers_array = config_item('_flt_wrappers');

foreach ($wrappers_array as $wr) {
  $str_arr = array();
  $str_arr[] = '[';
  $str_arr[] = '"';
  $str_arr[] = $wr['name'];
  $str_arr[] = '"';
  $str_arr[] = '],';
  $wrappers_string_for_js .= join('', $str_arr);
}
$wrappers_string_for_js = trim($wrappers_string_for_js, ',');
?>


<script>
  var js_ftl_wrappers = [<?= $wrappers_string_for_js ?>];
  createFltWrappers(js_ftl_wrappers);
  set_body_bg_color(menu_window_bg_color);

  function createFltWrappers(wrappers_array) {
    wrappers_array.unshift([-1]);
    for (var i = 0; i < wrappers_array.length; i++) {
      var inte_li = document.createElement('LI');
      inte_li.innerHTML = wrappers_array[i][0];
      inte_li.className = 'flt-listed-value';
      if (wrappers_array[i][0] === -1) {
	inte_li.innerHTML = '--- не важно ---';
      }
      inte_li.setAttribute('data-ha-filter-type', 'wrappers');
      inte_li.setAttribute('data-ha-filter-value', i - 1);
      document.getElementById('flt_wrappers_vals_view').appendChild(inte_li);
    }
  }
</script>