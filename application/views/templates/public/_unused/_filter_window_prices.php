<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите ценовой диапазон</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Цена</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_prices_vals_view" class="flt-vals-container"></ul>

  </div>
</div>

<?php
// preparing prices for js array
//$prices_string_for_js = '';
//$prices_range_array = config_item('_flt_prices');
/*
  foreach ($prices_range_array as $wr) {
  $str_arr = array();
  $str_arr[] = '[';
  $str_arr[] = '"';
  $str_arr[] = $wr['name'];
  $str_arr[] = '"';
  $str_arr[] = '],';
  $prices_string_for_js .= join('', $str_arr);
  }
  $prices_string_for_js = trim($prices_string_for_js, ',');
 * 
 */
?>


<script>
  var js_ftl_prices_range = [380, 5200];
  var price_descreet_step = 1000;
  var js_ftl_prices = create_price_array(js_ftl_prices_range, price_descreet_step);
  createFltPrices(js_ftl_prices); // it is automatic but we can feed manual array of descreet values

  set_body_bg_color(menu_window_bg_color);

  function create_price_array(range, step) {
    var min_val = range[0];
    var max_val = range[1];
    var price_array = [];
    if (min_val <= step) {
      price_array.push(step);
    } else {
      price_array.push(step*2);
    }

    do {
      var new_val = price_array[price_array.length - 1] + step;
      if (new_val % step != 0) {
	var new_val = parseInt(new_val / step) * step ;
      }
      price_array.push(new_val);
    } while (price_array[price_array.length - 1] <= max_val)

    return price_array;
  }

  function createFltPrices(prices_range_array) {
    prices_range_array.unshift(-1);
    for (var i = 0; i < prices_range_array.length; i++) {
      var price_li = document.createElement('LI');
      price_li.innerHTML = "до " + prices_range_array[i] + " руб./шт.";
      price_li.className = 'flt-listed-value';
      if (prices_range_array[i] === -1) {
	price_li.innerHTML = '--- не важно ---';
      }
      price_li.setAttribute('data-ha-filter-type', 'prices');
      price_li.setAttribute('data-ha-filter-value', prices_range_array[i]);
      document.getElementById('flt_prices_vals_view').appendChild(price_li);
    }
  }
</script>