<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Выберите желаемую марку</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Марка</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_brands_vals_view" class="flt-vals-container">





    </ul>

  </div>
</div>

<?php
// preparing brands for js array
$brands_string_for_js = '';
//$brands_array = config_item('_flt_vitolas');
// we should create array from db
/*
  foreach ($brands_array as $brd) {
  $str_arr = array();
  $str_arr[] = '[';
  $str_arr[] = '"';
  $str_arr[] = $brd['name'];
  $str_arr[] = '","';
  $str_arr[] = $brd['img'];
  $str_arr[] = '"],';
  $brands_string_for_js .= join('', $str_arr);
  }
  $brands_string_for_js = trim($brands_string_for_js, ',');
 * 
 */
?>

<script>
  // bu must be from database
  var js_ftl_brands = [
    [1, 'Cohiba', 'Cohiba.jpg'],
    [2, 'Montecristo', 'Montecristo.jpg'],
    [5, 'Partagas', 'Partagas.jpg'],
    [8, 'Romeo Y Julieta', 'Romeo.jpg'],
    [13, 'Hoyo De Brrr', 'Hoyo.jpg'],
    [22, 'H. Upmann', 'Hupmann.jpg'],
  ]; //[id, name, img]
  createFltBrands(js_ftl_brands);
  set_body_bg_color(menu_window_bg_color);

  function createFltBrands(brands_array) {
    brands_array.unshift([-1, 'Неважно', 'dummy_brand.jpg']);
    for (var i = 0; i < brands_array.length; i++) {
      var brand_li = document.createElement('LI');
      var brand_img = document.createElement('IMG');
      var brand_span = document.createElement('SPAN');
      brand_img.className = 'flt-brand-img';
      brand_span.innerHTML = brands_array[i][1];
      brand_span.className = 'flt-brand-name';
      brand_li.className = 'flt-brand-cell-li';
      brand_img.src = "<?= "/" . config_item('_brands_image_path_url'); ?>" + brands_array[i][2];
      brand_li.setAttribute('data-ha-filter-type', 'brands');
      brand_li.setAttribute('data-ha-filter-value', i - 1);
      brand_img.setAttribute('data-ha-filter-type', 'brands');
      brand_img.setAttribute('data-ha-filter-value', i - 1);
      brand_li.appendChild(brand_img);
      brand_li.appendChild(brand_span);
      document.getElementById('flt_brands_vals_view').appendChild(brand_li);
    }
  }
</script>