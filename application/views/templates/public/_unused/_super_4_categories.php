<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container-fluid super-4-cats">
  <div class="container">

    <div class="row">

      <div class="super-cat column col-lg-3 col-sm-6 col-xs-12">
	<a href="/catalog/cigars">
	  <img src="/assets/images/style/cat_cigars.png" alt="Сигары">
	</a>
	<h3><a href="/catalog/cigars">Сигары</a></h3>
	<p><a href="/catalog/cigars">Лучшие кубинские сигары в&nbsp;городе. Официальные поставки Habanos.</a></p>
      </div>

      <div class="super-cat column col-lg-3 col-sm-6 col-xs-12">
	<a href="/catalog/cigarillos">
	  <img src="/assets/images/style/cat_cigarillos.png" alt="Сигариллы">
	</a>
	<h3><a href="/catalog/cigarillos">Сигариллы</a></h3>
	<p><a href="/catalog/cigarillos">Кубинские сигариллы и&nbsp;сигареты. Изящество тонких линий.</a></p>
      </div>

      <div class="super-cat column col-lg-3 col-sm-6 col-xs-12">
	<a href="/catalog/tabacco">
	  <img src="/assets/images/style/cat_tabacco.png" alt="Табак">
	</a>
	<h3><a href="/catalog/tabacco">Табак</a></h3>
	<p><a href="/catalog/tabacco">Табак только высшего класса для ценителей, предпочитающих вкус табака</a></p>
      </div>

      <div class="super-cat column col-lg-3 col-sm-6 col-xs-12">
	<a href="/catalog/humidors">
	  <img src="/assets/images/style/cat_humidors.png" alt="Хьюмидоры">
	</a>
	<h3><a href="/catalog/accessories">Хьюмидоры</a></h3>
	<p><a href="/catalog/accessories">Хьюмидоры деревянные, металлические, пластиковые. Электронные.</a></p>
      </div>


    </div>

  </div>
</div>

<div class="section-devider"></div>