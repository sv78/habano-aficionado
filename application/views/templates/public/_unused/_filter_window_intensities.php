<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите интенсивность крепости</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Крепкость</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_intensities_vals_view" class="flt-vals-container"></ul>

  </div>
</div>

<?php
// preparing intensities for js array
$intensities_string_for_js = '';
$intensities_array = config_item('_flt_intensities');

foreach ($intensities_array as $inte) {
  $str_arr = array();
  $str_arr[] = '[';
  $str_arr[] = '"';
  $str_arr[] = $inte['name'];
  $str_arr[] = '"';
  $str_arr[] = '],';
  $intensities_string_for_js .= join('', $str_arr);
}
$intensities_string_for_js = trim($intensities_string_for_js, ',');
?>


<script>
  var js_ftl_intensities = [<?= $intensities_string_for_js ?>];
  createFltIntensities(js_ftl_intensities);
  set_body_bg_color(menu_window_bg_color);

  function createFltIntensities(intensities_array) {
    intensities_array.unshift([-1]);
    for (var i = 0; i < intensities_array.length; i++) {
      var inte_li = document.createElement('LI');
      inte_li.innerHTML = intensities_array[i][0];
      inte_li.className = 'flt-listed-value';
      if (intensities_array[i][0] === -1) {
	inte_li.innerHTML = '--- не важно ---';
      }
      inte_li.setAttribute('data-ha-filter-type', 'intensities');
      inte_li.setAttribute('data-ha-filter-value', i - 1);
      document.getElementById('flt_intensities_vals_view').appendChild(inte_li);
    }
  }
</script>