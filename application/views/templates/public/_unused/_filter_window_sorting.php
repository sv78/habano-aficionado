<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите желаемый тип сортировки</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Сортировать</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_wrappers_vals_view" class="flt-vals-container">
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="price_high">По качеству и статусу</li>
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="price_low">По доступной цене</li>
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="brand">По марке</li>
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="length">По длине</li>
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="vitola">По витоле</li>
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="intensity">По крепости</li>
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="flavour">По вкусу</li>
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="wrapper">По типу листа</li>
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="line">По линии</li>
      <li class="flt-listed-value" data-ha-filter-type="sort" data-ha-filter-value="cepo">По Ring Gauge</li>
    </ul>

  </div>
</div>


<script>
  set_body_bg_color(menu_window_bg_color);
</script>