<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите Желаемую Длину</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Длина</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_length_vals_view" class="flt-vals-container"></ul>

  </div>
</div>

<?php
// preparing lengths for js array
$length_string_for_js = '';
$length_array = config_item('_flt_lengths');

foreach ($length_array as $ln) {
  $str_arr = array();
  $str_arr[] = '[';
  $str_arr[] = $ln['mm'];
  $str_arr[] = ',';
  $str_arr[] = "'" . $ln['inches'] . "'";
  $str_arr[] = '],';
  $length_string_for_js .= join('', $str_arr);
}
$length_string_for_js = trim($length_string_for_js, ',');
?>







<script>
  var js_ftl_lengths = [<?= $length_string_for_js ?>];
  createFltLengths(js_ftl_lengths);
  set_body_bg_color(menu_window_bg_color);

  function createFltLengths(lengths_array) {
    lengths_array.unshift([-1, '']);
    for (var i = 0; i < lengths_array.length; i++) {
      var ln_li = document.createElement('LI');
      ln_li.innerHTML = lengths_array[i][0] + ' mm (' + lengths_array[i][1] + ')';
      ln_li.className = 'flt-listed-value';
      if (lengths_array[i][0] === -1) {
	ln_li.innerHTML = '--- не важно ---';
      }
      ln_li.setAttribute('data-ha-filter-type', 'length');
      ln_li.setAttribute('data-ha-filter-value', i - 1);
      document.getElementById('flt_length_vals_view').appendChild(ln_li);
    }
  }

</script>