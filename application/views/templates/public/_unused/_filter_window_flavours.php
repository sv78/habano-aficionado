<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите вкусо-ароматические предпочтения</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Вкус</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_flavours_vals_view" class="flt-vals-container"></ul>

  </div>
</div>

<?php
// preparing flavours for js array
$flavours_string_for_js = '';
$flavours_array = config_item('_flt_flavours');

foreach ($flavours_array as $ln) {
  $str_arr = array();
  $str_arr[] = '[';
  $str_arr[] = '"';
  $str_arr[] = $ln['name'];
  $str_arr[] = '"';
  $str_arr[] = '],';
  $flavours_string_for_js .= join('', $str_arr);
}
$flavours_string_for_js = trim($flavours_string_for_js, ',');
?>







<script>
  var js_ftl_flavours = [<?= $flavours_string_for_js ?>];
  createFltFlavours(js_ftl_flavours);
  set_body_bg_color(menu_window_bg_color);

  function createFltFlavours(flavours_array) {
    flavours_array.unshift([-1]);
    for (var i = 0; i < flavours_array.length; i++) {
      var ln_li = document.createElement('LI');
      ln_li.innerHTML = flavours_array[i][0];
      ln_li.className = 'flt-listed-value';
      if (flavours_array[i][0] === -1) {
	ln_li.innerHTML = '--- не важно ---';
      }
      ln_li.setAttribute('data-ha-filter-type', 'flavours');
      ln_li.setAttribute('data-ha-filter-value', i - 1);
      document.getElementById('flt_flavours_vals_view').appendChild(ln_li);
    }
  }
</script>