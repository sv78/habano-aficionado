<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите витолу сигары</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Vitola</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_vitolas_vals_view" class="flt-vals-container"></ul>

  </div>
</div>

<?php
// preparing vitolas for js array
$vitolas_string_for_js = '';
$vitolas_array = config_item('_flt_vitolas');

foreach ($vitolas_array as $vtl) {
  $str_arr = array();
  $str_arr[] = '[';
  $str_arr[] = '"';
  $str_arr[] = $vtl['name'];
  $str_arr[] = '","';
  $str_arr[] = $vtl['img'];
  $str_arr[] = '"],';
  $vitolas_string_for_js .= join('', $str_arr);
}
$vitolas_string_for_js = trim($vitolas_string_for_js, ',');
?>

<script>
  var js_ftl_vitolas = [<?= $vitolas_string_for_js ?>];
  createFltLines(js_ftl_vitolas);
  set_body_bg_color(menu_window_bg_color);

  function createFltLines(vitolas_array) {
    vitolas_array.unshift([-1]);
    for (var i = 0; i < vitolas_array.length; i++) {
      var vitola_li = document.createElement('LI');
      var vitola_img = document.createElement('IMG');
      vitola_img.className = 'flt-vitola-img';
      vitola_li.innerHTML = vitolas_array[i][0];
      vitola_li.className = 'flt-listed-value-vitola';
      if (vitolas_array[i][0] === -1) {
	vitola_li.innerHTML = '--- не важно ---';
	vitola_img.src = "<?= "/" . config_item('_vitolas_image_path_url'); ?>" + 'no_matter_vitola.png';
      } else {
	vitola_img.src = "<?= "/" . config_item('_vitolas_image_path_url'); ?>" + vitolas_array[i][1];
      }
      vitola_li.setAttribute('data-ha-filter-type', 'vitolas');
      vitola_li.setAttribute('data-ha-filter-value', i - 1);
      vitola_img.setAttribute('data-ha-filter-type', 'vitolas');
      vitola_img.setAttribute('data-ha-filter-value', i - 1);
      vitola_li.appendChild(vitola_img);
      document.getElementById('flt_vitolas_vals_view').appendChild(vitola_li);
    }
  }
</script>