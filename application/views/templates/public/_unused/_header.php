<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="ru-ru">
  <head>
    <meta charset="utf-8">
    <title><?= $meta_title ?></title>

    <!-- META -->
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="Polyartmedia Group">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <!-- FAVICON -->
    <link rel="icon" href="/assets/favicon/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/assets/favicon/favicon.ico">

    <!-- CSS -->
    <!--<link href="/assets/css/bootstrap-3.3.7-dist/css/bootstrap_yeti.css" rel="stylesheet" media="screen">-->
    <!--<link href="/assets/css/admin.css" rel="stylesheet" media="screen">-->

    <!-- JS -->
    <script src="/assets/js/jquery-3.1.1.min.js"></script>
    <!--<script src="/assets/css/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>-->
    <!--<script src="/assets/js/admin/admin.js"></script>-->
    <script src="/assets/js/functions.js"></script>
    <script src="/assets/js/script.js"></script>
  </head>
  <body>
    <div class="container"> <!-- wrapper -->

      <nav>
	<a href="/">Home</a>
	<span> | </span>
	<a href="/admin">Admin</a>
	<span> | </span>
	<a href="/blog">Blog</a>
	<span> | </span>
	<a href="/registration-confirmation/xxx">Confirm Registration</a>
	<span> | </span>
	<a href="/password-reset-confirmation/xxx">Confirm Password Reset</a>
	<span> | </span>
	<a href="/reset-password">Reset Password</a>
	<span> | </span>
	<a href="/register">Register</a>
	<span> | </span>
	<a href="/login">Login</a>
	<span> | </span>
	<span onclick="logout()" style="cursor:pointer; text-decoration: underline;">Logout</span>

	<?php
	if ($this->session->has_userdata('user')) :
	  ?>
  	<span> | </span>
  	<a href="/profile" target="_self" style="color:red;"><?= $this->session->userdata('user')['email'] ?></a>
	  <?php
	endif;
	if (isset($_SESSION['user']['is_super_admin']) && $_SESSION['user']['is_super_admin'] === TRUE) {
	  echo '<span>*</span>';
	}
	?>
      </nav>

      <hr>


      <script>
	function logout() {
	  if (!confirm('Log out?'))
	    return;
	  window.location.assign(location.protocol + "//" + location.host + "/logout");
	}
      </script>


      <div id="dbg"></div>