<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<!-- КАТАЛОГ HABANOS - ЗАГОЛОВОК -->


<div class="container">
  <h1 class="h1-catalog-habanos"><span class="color-ha-default">Каталог</span> <span class="color-ha-light">Habanos</span><span class="span-catalog-habanos"></span></h1>
</div>


<!-- БЛОК ПОИСКА -->

<!-- надпись параметры поиска перечеркнутый линией 1 -->
<div class="container">
  <div class="lined-text-small-container">
    <div class="line-text-line"></div>
    <div class="line-text-text">Параметры поиска сигар:</div>
  </div>
</div>


<div id="ha_filter_super_search_system" class="container filter-search-div">

  <div class="row">

    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str">Страна</span>
	<span id="ss_filter_countries_value_element" class="filter-value-str"></span>
      </div>
    </div>

    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str" data-ss-filter-type-picker="brands">Марка</span>
	<span id="ss_filter_brands_value_element" class="filter-value-str hover-underline" data-ss-filter-type-picker="brands"></span>
      </div>
    </div>

    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str" data-ss-filter-type-picker="vitolas">Витола</span>
	<span id="ss_filter_vitolas_value_element" class="filter-value-str hover-underline" data-ss-filter-type-picker="vitolas"></span>
      </div>
    </div>

    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str" data-ss-filter-type-picker="lines">Линия</span>
	<span id="ss_filter_lines_value_element" class="filter-value-str hover-underline" data-ss-filter-type-picker="lines"></span>
      </div>
    </div>


    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str" data-ss-filter-type-picker="lengths">Длина</span>
	<span id="ss_filter_lengths_value_element" class="filter-value-str hover-underline" data-ss-filter-type-picker="lengths"></span>
      </div>
    </div>

    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str" data-ss-filter-type-picker="cepos">Диаметр (Ring Gauge)</span>
	<span id="ss_filter_cepos_value_element" class="filter-value-str hover-underline" data-ss-filter-type-picker="cepos"></span>
      </div>
    </div>

    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str" data-ss-filter-type-picker="intensities">Кпепкость</span>
	<span id="ss_filter_intensities_value_element" class="filter-value-str hover-underline" data-ss-filter-type-picker="intensities"></span>
      </div>
    </div>

    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str" data-ss-filter-type-picker="wrappers">Лист</span>
	<span id="ss_filter_wrappers_value_element" class="filter-value-str hover-underline" data-ss-filter-type-picker="wrappers"></span>
      </div>
    </div>


    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str" data-ss-filter-type-picker="flavours">Вкус</span>
	<span id="ss_filter_flavours_value_element" class="filter-value-str hover-underline" data-ss-filter-type-picker="flavours"></span>
      </div>
    </div>

    <div class="column filter-string-cell col-lg-3 col-sm-6 col-xs-12">
      <div class="filter-cell">
	<span class="filter-name-str" data-ss-filter-type-picker="prices">Цена</span>
	<span id="ss_filter_prices_value_element" class="filter-value-str hover-underline" data-ss-filter-type-picker="prices"></span>
      </div>
    </div>


  </div>

  <p class="text-center uppercase">Найдено результатов: <span id="count_search_result_holder" class="search-result-number"></span></p>

  <div class="filter-submit-container side-padding">
    <button class="filter-btn-ok" data-action-type="submit-filter-block-form" title="Нажмите, чтобы посмотреть результат по указанным критериям">Показать</button>
    <button class="filter-btn-reset" data-ss-filter-reset="reset" title="Нажмите, чтобы сбросить все критерии и результат поиска">Сбросить</button>


    <?php
    $form_attributes = array('id' => 'filter_block_form', 'class' => 'filter-block-form', 'method' => 'post');
    echo form_open_multipart('catalog', $form_attributes);
    ?>
    <input id="fb_country_input" type="hidden" name="fb_country">
    <input id="fb_brand_input" type="hidden" name="fb_brand">
    <input id="fb_vitola_input" type="hidden" name="fb_vitola">
    <input id="fb_line_input" type="hidden" name="fb_line">
    <input id="fb_length_input" type="hidden" name="fb_length">
    <input id="fb_cepo_input" type="hidden" name="fb_cepo">
    <input id="fb_intensity_input" type="hidden" name="fb_intensity">
    <input id="fb_wrapper_input" type="hidden" name="fb_wrapper">
    <input id="fb_flavour_input" type="hidden" name="fb_flavour">
    <input id="fb_price_max_input" type="hidden" name="fb_price_max">
    <input id="fb_found_items_mumber_input" type="hidden" name="fb_found_items_number">
    <input id="fb_request_id_input" type="hidden" name="fb_request_id">
    <?php //echo form_error('field_name') ?>
    <?php //echo set_value('field_name'); ?>
    </form>
  </div>

  <p class="search-submit-or-reset-note side-padding">Нажмите чтобы посмотреть результат или сбросить все критерии поиска</p>

</div>

<div id="filter_results_holder" class="container"></div>
