<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите линию сигары</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Линия</div>
    </div>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

    <ul id="flt_lines_vals_view" class="flt-vals-container"></ul>

  </div>
</div>

<?php
// preparing lines for js array
$lines_string_for_js = '';
$lines_array = config_item('_flt_lines');

foreach ($lines_array as $lin) {
  $str_arr = array();
  $str_arr[] = '[';
  $str_arr[] = '"';
  $str_arr[] = $lin['name'];
  $str_arr[] = '"';
  $str_arr[] = '],';
  $lines_string_for_js .= join('', $str_arr);
}
$lines_string_for_js = trim($lines_string_for_js, ',');
?>

<script>
  var js_ftl_lines = [<?= $lines_string_for_js ?>];
  createFltLines(js_ftl_lines);
  set_body_bg_color(menu_window_bg_color);

  function createFltLines(lines_array) {
    lines_array.unshift([-1]);
    for (var i = 0; i < lines_array.length; i++) {
      var line_li = document.createElement('LI');
      line_li.innerHTML = lines_array[i][0];
      line_li.className = 'flt-listed-value';
      if (lines_array[i][0] === -1) {
	line_li.innerHTML = '--- не важно ---';
      }
      line_li.setAttribute('data-ha-filter-type', 'lines');
      line_li.setAttribute('data-ha-filter-value', i - 1);
      document.getElementById('flt_lines_vals_view').appendChild(line_li);
    }
  }
</script>