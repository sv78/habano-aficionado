<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="flt-window">
  <div class="container side-padding">

    <div class="flt-logo"></div>

    <p class="text-center">Укажите Желаемый Диаметр</p>
    <div class="lined-text-header-container">
      <div class="line-text-line-light"></div>
      <div class="line-text-text-light">Ring Gauge</div>
    </div>
    <ul id="flt_cepo_vals_view" class="flt-vals-container"></ul>

    <div id="filterwindowcloser" class="flt-closer" title="Закрыть (Esc)" data-action-type="close-site-menu"></div>

  </div>
</div>

<?php
// preparing cepo for js array
$cepos_string_for_js = '';
$cepos_array = config_item('_flt_cepos');

foreach ($cepos_array as $cepo) {
  $cepos_string_for_js .= $cepo . ',';
}
$cepos_string_for_js = trim($cepos_string_for_js, ',');
?>







<script>
  //var js_ftl_cepos = [26, 28, 30, 32, 33, 34, 35, 36, 38, 39, 40, 42, 43, 44, 46, 47, 48, 49, 50, 52, 54, 55, 56, 57];
  var js_ftl_cepos = [<?= $cepos_string_for_js ?>];
  createFltCepos(js_ftl_cepos);
  set_body_bg_color(menu_window_bg_color);

  function createFltCepos(cepos_values_array) {
    cepos_values_array = cepos_values_array.reverse();
    cepos_values_array.unshift(-1);

    var cepo_size_q = 1.4;
    var cepo_font_size = 16;
    var cepo_form_and_cell_size_differ = 4;

    var cepo_max_form_size = cepo_size_q * get_array_max_value(cepos_values_array);
    var cepo_min_form_size = cepo_size_q * get_array_min_value(cepos_values_array);

    var cepo_cell_size = parseInt(cepo_max_form_size + cepo_form_and_cell_size_differ);

    for (var i = 0; i < cepos_values_array.length; i++) {
      var cepo_cell = document.createElement('LI');
      cepo_cell.style.width = cepo_cell_size + "px";
      cepo_cell.style.height = cepo_cell_size + "px";
      cepo_cell.className = 'flt-cepo-cell';

      var cepo_form = document.createElement('DIV');
      cepo_form.className = 'flt-cepo-form';
      if (cepos_values_array[i] === -1) {
	var font_size_differ = 5;
	cepo_form.style.fontSize = cepo_font_size - font_size_differ + "px";
	cepo_form.style.width = cepo_cell_size + "px";
	cepo_form.style.height = cepo_cell_size + "px";
	cepo_form.style.paddingTop = parseInt((cepo_cell_size - cepo_font_size + font_size_differ / 2) / 2) + 'px';
	cepo_form.innerHTML = 'Не важно';
      } else {
	cepo_form.style.fontSize = cepo_font_size + "px";
	cepo_form.style.width = parseInt(cepo_size_q * cepos_values_array[i]) + "px";
	cepo_form.style.height = parseInt(cepo_size_q * cepos_values_array[i]) + "px";
	cepo_form.style.paddingTop = parseInt((parseInt(cepo_size_q * cepos_values_array[i]) - cepo_font_size) / 2) - 1 + 'px';
	cepo_form.innerHTML = cepos_values_array[i];
      }

      cepo_form.setAttribute('data-ha-filter-type', 'cepo');
      cepo_form.setAttribute('data-ha-filter-value', cepos_values_array[i]);

      if (cepos_values_array[i] > 49) {
	cepo_form.style.backgroundColor = '#a89873';
      } else if (cepos_values_array[i] > 39) {
	cepo_form.style.backgroundColor = '#525252';
      } else {
	cepo_form.style.backgroundColor = '#d71e2e';
      }
      if (cepos_values_array[i] === -1) {
	cepo_form.style.backgroundColor = '#414a53';
      }

      cepo_cell.appendChild(cepo_form);
      document.getElementById('flt_cepo_vals_view').appendChild(cepo_cell);
    }
  }

</script>