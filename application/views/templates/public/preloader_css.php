<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- PRELOADER STYLE -->
<style id="preloader-style">

  #preloader {
    display: block;
    position: fixed;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background-color: #000;
    opacity: 1;
    padding: 0;
    margin: 0;
    opacity: 1;
    z-index: 9999;
  }

  .preloader-container {
    opacity: 0;
    position: absolute;
    top: 100px;
    height: auto;
    width: 100%;
  }

  .preloader-line {
    position: relative;
    height: 1px;
    width: 100%;
    margin: 0;
    padding: 0;
    background-color: #8d8763;
    box-shadow: 0 0 4px #29271c;

    -webkit-animation-name: example; /* Safari 4.0 - 8.0 */
    -webkit-animation-duration: 6s; /* Safari 4.0 - 8.0 */
    -webkit-animation-iteration-count: infinite; /* Safari 4.0 - 8.0 */
    animation-name: lineamination;
    animation-duration: 6s;
    animation-iteration-count: infinite;
  }

  .preloader-loading{
    color: #8d8763;
    margin: 10px auto;
    font-size: 14px;
    font-family: Verdana, Arial, sans-serif;
    text-align: center;
    text-shadow: 0 0 10px #29271c;
  }
  @-webkit-keyframes lineamination {
    0%   {width:0;}
    25%  {width:50%;}
    50%  {width:100%; opacity: 1;}
    99% {opacity: 0;}
    100% {opacity: 0;}
  }
  @keyframes lineamination {
    0%   {width:0;}
    25%  {width:50%;}
    50%  {width:100%; opacity: 1;}
    99% {opacity: 0;}
    100% {opacity: 0;}
  }

</style>
<!-- // PRELOADER STYLE -->