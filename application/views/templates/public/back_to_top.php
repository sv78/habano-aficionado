<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Back to top -->
<div id="backtotop" class="backtotop-hidden" data-action-type="flashed-link" title="Наверх"></div>
<div id="backtotop_sm" class="backtotop-sm-hidden" data-action-type="flashed-link" title="Наверх"></div>
<!-- / Back to top -->