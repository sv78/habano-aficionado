<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<h1 class="side-padding">Ваш резерв</h1>


<div class="container">

  <?php
  foreach ($cart_items as $cart_item):
    
    $ci_category_slug = $cart_item['category_slug'] != NULL ? Baza::decode_plain_string_from_db($cart_item['category_slug']) : '';
    $ci_category_path = 'catalog/' . $ci_category_slug . '/';
    ?>
    <div id="<?= $cart_item['cart_item_type'] . '_' . $cart_item['id'] ?>" class="row cigar-row">

      <div class="column col-lg-3 col-sm-12">
        <h4>
  	<a href="/<?= $ci_category_path . Baza::decode_plain_string_from_db($cart_item['slug']); ?>">
	    <?php
	    if (isset($cart_item['product_name'])) {
	      echo '<small>' . Baza::decode_plain_string_from_db($cart_item['product_name']) . '</small><br>';
	    }
	    ?>


	    <?= Baza::decode_plain_string_from_db($cart_item['name']) ?>
  	</a>
        </h4>

        <p class="cigar-row-filter-description">
  	<span>Марка:</span>&nbsp;
	  <?php
	  $ci_brand_name = $cart_item['brand_name'] != NULL ? Baza::decode_plain_string_from_db($cart_item['brand_name']) : '';
	  ?>
  	<span><?= $ci_brand_name ?></span>
        </p>

        <p class="cigar-row-price"><?= Baza::decode_plain_string_from_db($cart_item['price']) ?>&nbsp;руб.</p>

        <p><a href="/<?= $ci_category_path . Baza::decode_plain_string_from_db($cart_item['slug']); ?>" class="cigar-row-add-to-cart-btn">Узнать&nbsp;подробнее</a></p>

      </div>

      <div class="column col-lg-6 col-sm-12">
	<?php
	$item_name = $cart_item['cart_item_type'];
	if ($cart_item['img'] == '') {
	  $img_src = (isset($cart_item['is_cigar_product']) && $cart_item['is_cigar_product']) ? "/" . config_item('_cigar_not_specified_image_url') : "/" . config_item('_product_not_specified_image_url');
	} else if (!file_exists(config_item('_' . $item_name . 's_image_path') . $cart_item['img'])) {
	  $img_src = (isset($cart_item['is_cigar_product']) && $cart_item['is_cigar_product']) ? "/" . config_item('_cigar_missing_image_url') : "/" . config_item('_product_missing_image_url');
	} else {
	  $img_src = "/" . config_item('_' . $item_name . 's_image_path_url') . $cart_item['img'];
	}
	$img_class_attr = (!empty($cart_item['is_cigar_product']) && $cart_item['is_cigar_product']) != 1 ? 'class="cart-common-product-img"' : '';
	?>
        <img <?= $img_class_attr ?> src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($cart_item['name']) ?>" title="Нажмите, чтобы узнать подробнее">

        <label class="input-text-label smaller">Количество:</label>
        <div class="text-center">
	  <?php
	  $binder_cart_item_price_sum_value_element_id = $cart_item['cart_item_type'] . '_' . $cart_item['id'] . '_sum';
	  ?>
  	<input id="<?= 'cart_item_' . $cart_item['id'] . '_input' ?>" class="abstract-cart-item-spinner spinner product-order-amount-input" type="text" name="cart_item_amount" autocomplete="off" value="<?= $cart_item['order_amount'] ?>" onchange="updateCartFromInput(this, '<?= $cart_item['cart_item_type'] ?>', '<?= $cart_item['id'] ?>')" data-binded-cart-item-price-element="<?= $binder_cart_item_price_sum_value_element_id ?>" data-cart-item-price="<?= $cart_item['price'] ?>">
        </div>

      </div>

      <div class="column col-lg-3 col-sm-12">
        <p><span class="smaller">Всего за эту позицию:</span></p>
        <p class="cigar-row-price color-ha-light">
  	<span id="<?= $binder_cart_item_price_sum_value_element_id ?>" class="abstract-binded-to-total-price"><?= $cart_item['price'] * $cart_item['order_amount'] ?></span>&nbsp;руб.</p>
        <p><span class="cigar-row-add-to-cart-btn" onclick="removeItemFromCart('<?= $cart_item['cart_item_type'] ?>', '<?= $cart_item['id'] ?>')">Удалить&nbsp;из&nbsp;резерва</span></p>
      </div>


    </div>
    <?php
  endforeach;
  ?>

</div>

<div class="container side-padding">
  <p class="text-center">Итого:</p>
  <div class="lined-text-header-container">
    <div class="line-text-line"></div>
    <div class="line-text-text color-ha-light"><span id="cart_total_price_value"><?= $cart_total_price ?></span>&nbsp;руб.</div>
  </div>
</div>





<?php
if (empty($this->session->userdata('user'))):
  ?>
  <div class="container side-padding">
    <h4 class="text-center">Уважаемый посетитель!</h4>
    <p class="text-center">Чтобы Вы могли отправить нам заявку Вашего резерва Вам необходимо <a href="/login" class="color-ha-light">авторизоваться</a> на&nbsp;сайте.</p>

    <p class="text-center">В случае, если Вы еще незарегистрированы, пожалуйста <a href="/register">зарегистрируйтесь</a>. Это займет не&nbsp;более минуты. После процедуры регистрации Вы&nbsp;сможете отправить Вашу заявку на&nbsp;обработку или при&nbsp;необходимости продолжить покупки.</p>

    <p class="text-center"><a class="btn-like btn-sm cart-reg-btn" href="/register">Зарегистрироваться</a></p>
  </div>
  <p class="text-center smaller"><span class="js-text-link" onclick="clearCart()">Очистить резерв</span></p>
  <?php
  return; // we return from this view
endif;
?>


<script src="/assets/js/inputmask/inputmask.min.js"></script>
<script src="/assets/js/inputmask/inputmask.phone.extensions.min.js"></script>


<div class="container side-padding login-form">

  <h4 class="text-center">Уважаемый посетитель!</h4>
  <p class="text-center">Чтобы отправить заявку на заказ Вашего резерва необходимо указать Ваши контактные данные! Пожалуйста обязательно укажите контактный телефон! Наш менеджер свяжется с&nbsp;Вами по&nbsp;указанному телефону, чтобы уточнить возможные детали Вашего заказа. Спасибо!</p>


  <?php
  $form_attributes = array('id' => 'cart_form', 'onsubmit' => 'beforeSubmit(event)');
  echo form_open_multipart('', $form_attributes);
  ?>

  <!-- name -->
  <label class="input-text-label offset-top-triple" for="name_input">Ваше имя:</label>
  <input id="name_input" type="text" name="name" placeholder="Ваше имя" autocomplete="off" value="<?php echo set_value('name'); ?>" maxlength="96" onblur="prepare_name_input(event)">
  <?php echo form_error('name') ?>

  <?php
  if ($this->session->userdata('user')['name'] != ''):
    ?>
    <p class="text-center smaller">Ранее Вы указывали свое имя как указанно в блоке ниже. Чтобы использовать это имя повторно&nbsp;- нажмите на&nbsp; этот блок.</p>
    <p class="text-center">
      <span title="Нажмите, чтобы установить это имя в поле «Ваше имя»" class="cart-user-saved-info-data" onclick="fill_input_with_saved_name('name_input')"><?= $this->session->userdata('user')['name'] ?></span>
    </p>
    <?php
  endif;
  ?>


  <!-- phone -->
  <label class="input-text-label offset-top-triple" for="phone_input">Телефон: <sup>*</sup></label>
  <input id="phone_input" type="text" name="phone" placeholder="Номер телефона" autocomplete="off" value="<?php echo set_value('phone'); ?>" onfocus="showInputValid(this)">
  <?php echo form_error('phone') ?>

  <?php
  if ($this->session->userdata('user')['phone'] != ''):
    ?>
    <p class="text-center smaller">Ранее Вы указывали номер телефона, указанный в&nbsp;блоке ниже. Чтобы использовать этот номер повторно&nbsp;- нажмите на&nbsp;этот блок.</p>
    <p class="text-center">
      <span title="Нажмите, чтобы установить этот номер в поле «Телефон»" class="cart-user-saved-info-data" onclick="fill_input_with_saved_phone('phone_input')"><?= '+7' . $this->session->userdata('user')['phone'] ?></span>
    </p>
    <?php
  endif;
  ?>


  <!-- address -->
  <label class="input-text-label offset-top-triple" for="address_input">Адрес доставки:</label>
  <p class="text-center smaller">Пожалуйста укажите адрес в&nbsp;случае, если Вы&nbsp;хотите воспользоваться нашей службой доставки.</p>
  <textarea id="address_input" name="address" placeholder="Адрес доставки" onblur="prepare_address_input(event)"><?php echo set_value('address'); ?></textarea>
  <?php echo form_error('address') ?>

  <?php
  if ($this->session->userdata('user')['address'] != ''):
    ?>
    <p class="text-center smaller">Ранее для поля «Адрес доставки» Вы использовали данные, указанные в&nbsp;блоке под этой надписью. Чтобы использовать эти данные повторно&nbsp;- нажмите на&nbsp;этот блок.</p>
    <p class="text-center">
      <span title="Нажмите, чтобы установить эти данные в поле «Адрес доставки»" id="address_input_filler" class="cart-user-saved-info-data" onclick="fill_input_with_saved_address('address_input')"><?= Baza::decode_plain_string_from_db($this->session->userdata('user')['address']) ?></span>
    </p>
    <?php
  endif;
  ?>

  <input type="hidden" name="cart_request_id" value="<?= $this->session->userdata('cart_request_id') ?>">


  <button class="offset-top-triple" title="Нажмите, чтобы отправить заявку Вашего резерва" type="submit" name="submit" value="submit">Отправить заявку</button>
  <p title="Нажмите, чтобы очистить резерв полностью" class="text-center smaller offset-top-double"><span class="js-text-link" onclick="clearCart()">Очистить резерв</span></p>

</form>

</div>



<script>

  function fill_input_with_saved_phone(target_input_id) {
    var phone_str = window.event.target.innerHTML;
    phone_str = phone_str.replace(/[^\d]/g, '')
    phone_str = phone_str.slice(1, phone_str.length);
    document.getElementById(target_input_id).value = phone_str;
    showInputValid(cart_phone_input);
  }

  function fill_input_with_saved_address(target_input_id) {
    document.getElementById(target_input_id).value = window.event.target.innerHTML;
  }

  function fill_input_with_saved_name(target_input_id) {
    document.getElementById(target_input_id).value = window.event.target.innerHTML;
  }


  if (document.getElementById("phone_input")) {
    var cart_phone_input = document.getElementById("phone_input");
    var im = new Inputmask("+7 (999) 999 99 99");
    im.mask(cart_phone_input);
  }

  function beforeSubmit(e) {
    if (cart_phone_input.value == '') {
      showLineMessage('Пожалуйста укажите номер телефона!', 'darkred');
      cart_phone_input.focus()
      showInputIsNotValid(cart_phone_input);
      e.preventDefault();
    }

    var phone_value = prepare_phone_number_from_input(cart_phone_input.value);
    if (!is_valid_phone_number(phone_value)) {
      showLineMessage('Номер телефона указан неверно!', 'darkred');
      cart_phone_input.focus()
      showInputIsNotValid(cart_phone_input);
      e.preventDefault();
    }
  }

</script>


<?php
//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";
//echo "<hr>";
//echo "<pre>";
//print_r($cart_items);
//echo "</pre>";
?>
