<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (count($four_items) != 4) {
  return;
}
?>
<!-- 4 FEATURED СТАТЕЙНЫЕ АНОНСЫ БЛОГА -->
<div class="container">
  <div class="row blog-anounce-cards">
    <?php
    foreach ($four_items as $article):
      if ($article['img'] == '') {
	$img_src = "/" . config_item('_blog_not_specified_image_url');
      } else if (!file_exists(config_item('_blog_article_image_path') . $article['img'])) {
	$img_src = "/" . config_item('_blog_missing_image_url');
      } else {
	$img_src = "/" . config_item('_blog_article_image_path_url') . $article['img'];
      }
      ?>
      <div class="blog-square-cell column col-lg-3 col-sm-6 col-xs-12">
        <a href="/blog/<?= Baza::decode_plain_string_from_db($article['section_slug']) . '/' . Baza::decode_plain_string_from_db($article['slug']); ?>">
  	<img class="img-responsive" src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($article['title']) ?>">
        </a>
        <p>
  	<a href="/blog/<?= Baza::decode_plain_string_from_db($article['section_slug']) . '/' . Baza::decode_plain_string_from_db($article['slug']); ?>"><?= Baza::decode_plain_string_from_db($article['title']) ?></a>
        </p>
      </div>
      <?php
    endforeach;
    ?>
  </div>
</div>
<!-- // 4 FEATURED СТАТЕЙНЫЕ АНОНСЫ БЛОГА -->
