<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div id="age_verification_screen" class="hidden">
  <div class="av-content-wraper">

    <div class="container side-padding">
      <div class="login-form">
	<img class="av-logo center-block" src="/<?= config_item('_style_images_path_url') . 'ha_logo_main.png'; ?>" alt="Habano Aficionado">
	<p class="text-center color-ha-light">Уважаемый посетитель, данный сайт содержит информацию о&nbsp;табаке и&nbsp;предназначен для лиц старше 18&nbsp;лет, являющихся специалистами табачной индустрии или потребителями&nbsp;табачной продукции.</p>

	<p class="text-center smaller">Пожалуйста подтвердите свой возраст!</p>

	<p class="av-buttons-container"><span class="btn-like btn-sm av-choice_btn" data-action-type="av-confirm" data-av-confirm-value="1">Более 18 лет</span><span class="btn-like btn-sm av-choice_btn" data-action-type="av-confirm" data-av-confirm-value="0">Менее 18 лет</span></p>

	<hr>

	<img class="av-eithteen-sign center-block" src="/<?= config_item('_style_images_path_url') . 'eithteen_plus.png'; ?>" alt="Для лиц старше 18 лет">
      </div>

    </div>
  </div>
</div>