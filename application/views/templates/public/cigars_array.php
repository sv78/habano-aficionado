<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//print_r($products);


if (count($products) == 0) {
  return;
}
?>
<div class="container">

  <?php
  foreach ($products as $cigar):

    $ci_category_slug = $cigar['category_slug'] != NULL ? Baza::decode_plain_string_from_db($cigar['category_slug']) : '';
    $ci_category_path = 'catalog/' . $ci_category_slug . '/';
    ?>
    <div class="row cigar-row">

      <div class="column col-lg-3 col-sm-12">
	<?php
	$title_by_intro = mb_substr(Baza::decode_plain_string_from_db($cigar['intro']) . '…', 0, 150);
	?>
        <h4 title="<?= $title_by_intro ?>"><a href="/<?= $ci_category_path . Baza::decode_plain_string_from_db($cigar['slug']); ?>"><?= Baza::decode_plain_string_from_db($cigar['name']) ?></a></h4>
        <p class="cigar-row-price"><?= $cigar['price'] != 0 ? Baza::decode_plain_string_from_db($cigar['price']) . ' руб.' : "<small>{$this->config->item('_product_price_zero_substitution')}</small>"; ?></p>
        <p><a href="/<?= $ci_category_path . Baza::decode_plain_string_from_db($cigar['slug']); ?>" class="cigar-row-add-to-cart-btn">Узнать подробнее</a></p>
      </div>

      <div class="column col-lg-7 col-sm-12">
        <a href="/<?= $ci_category_path . Baza::decode_plain_string_from_db($cigar['slug']); ?>">
	  <?php
	  if ($cigar['img'] == '') {
	    $img_src = "/" . config_item('_cigar_not_specified_image_url');
	  } else if (!file_exists(config_item('_products_image_path') . $cigar['img'])) {
	    $img_src = "/" . config_item('_cigar_missing_image_url');
	  } else {
	    $img_src = "/" . config_item('_products_image_path_url') . $cigar['img'];
	  }
	  ?>
  	<img src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($cigar['name']) ?>" title="Нажмите, чтобы узнать подробнее">
        </a>
      </div>


      <div class="column col-lg-2 col-sm-12">

        <p class="cigar-row-filter-description">
  	<span>Марка:</span>&nbsp;
	  <?php
	  $ci_brand_name = $cigar['brand_name'] != NULL ? Baza::decode_plain_string_from_db($cigar['brand_name']) : '';
	  ?>
  	<span><?= $ci_brand_name ?></span>
        </p>

        <p class="cigar-row-filter-description">
  	<span>Vitola:</span>&nbsp;
	  <?php
	  $ci_vitola = $cigar['vitola'] != NULL ? config_item('_flt_vitolas')[$cigar['vitola']]['name'] : '';
	  ?>
  	<span><?= $ci_vitola ?></span>
        </p>

        <p class="cigar-row-filter-description">
  	<span>Серия:</span>&nbsp;
	  <?php
	  $ci_line = $cigar['line'] != NULL ? config_item('_flt_lines')[$cigar['line']]['name'] : '';
	  ?>
  	<span><?= $ci_line ?></span>
        </p>

        <p class="cigar-row-filter-description">
  	<span>Длина:</span>&nbsp;
	  <?php
	  $ci_length = $cigar['length'] != NULL ? config_item('_flt_lengths')[$cigar['length']]['mm'] . 'mm (' . config_item('_flt_lengths')[$cigar['length']]['inches'] . ')' : '';
	  ?>
  	<span><?= $ci_length ?></span>
        </p>

        <p class="cigar-row-filter-description">
  	<span>Cepo/Ring Gauge:</span>&nbsp;
	  <?php
	  $ci_cepo = $cigar['cepo'] != NULL ? $cigar['cepo'] : '';
	  ?>
  	<span><?= $ci_cepo ?></span>
        </p>

        <p class="cigar-row-filter-description">
  	<span>Крепость:</span>&nbsp;
	  <?php
	  $ci_intensity = $cigar['intensity'] != NULL ? config_item('_flt_intensities')[$cigar['intensity']]['name'] : '';
	  ?>
  	<span><?= $ci_intensity ?></span>
        </p>

        <p class="cigar-row-filter-description">
  	<span>Лист:</span>&nbsp;
	  <?php
	  $ci_wrapper = $cigar['wrapper'] != NULL ? config_item('_flt_wrappers')[$cigar['wrapper']]['name'] : '';
	  ?>
  	<span><?= $ci_wrapper ?></span>
        </p>

        <p class="cigar-row-filter-description">
  	<span>Вкус:</span>&nbsp;
	  <?php
	  $ci_flavour = $cigar['flavour'] != NULL ? config_item('_flt_flavours')[$cigar['flavour']]['name'] : '';
	  ?>
  	<span><?= $ci_flavour ?></span>
        </p>

      </div>


    </div>
    <?php
  endforeach;
  ?>
</div>
