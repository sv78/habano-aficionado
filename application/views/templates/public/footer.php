<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<footer>

  <div class="footer-brands-logos-section bg-pattern">

    <div class="section-devider"></div>

    <ul>
      <li>
        <a href="/brands/cohiba">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_cohiba_logo.png'; ?>" alt="Cohiba">
        </a>
      </li>
      <li>
        <a href="/brands/montecristo">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_montecristo_logo.png'; ?>" alt="Montecristo">
        </a>
      </li>
      <li>
        <a href="/brands/partagas">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_partagas_logo.png'; ?>" alt="Partagas">
        </a>
      </li>
      <li>
        <a href="/brands/romeo-y-julieta">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_romeoyjulieta_logo.png'; ?>" alt="Romeo Y Julieta">
        </a>
      </li>
      <li>
        <a href="/brands/hoyo-de-monterrey">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_hoyodemonterrey_logo.png'; ?>" alt="Hoyo De Monterrey">
        </a>
      </li>
      <li>
        <a href="/brands/h-upmann">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_hupmann_logo.png'; ?>" alt="H. Upmann">
        </a>
      </li>
    </ul>

  </div>


  <div class="section-devider"></div>


  <div class="footer-info-section side-padding">
    <p class="footer-address"><?= config_item('_footer_address_string') ?></p>

    <ul>
      <li>
        <a href="<?= config_item('_social_links')['facebook'] ?>" target="_blank" title="Мы в FACEBOOK">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_facebook.png'; ?>" alt="Мы в FACEBOOK">
        </a>
      </li>
      <li>
        <a href="<?= config_item('_social_links')['twitter'] ?>" title="Мы в TWITTER">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_twitter.png'; ?>" alt="Мы в TWITTER">
        </a>
      </li>
      <li>
        <a href="<?= config_item('_social_links')['vk'] ?>" title="Мы в VKONTAKTE">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_vk.png'; ?>" alt="Мы в VKONTAKTE">
        </a>
      </li>
      <li>
        <a href="<?= config_item('_social_links')['instagram'] ?>" title="Мы в INSTAGRAM">
          <img src="/<?= config_item('_style_images_path_url') . 'footer_instagram.png'; ?>" alt="Мы в INSTAGRAM">
        </a>
      </li>
    </ul>



    <p class="footer-info-text"><?= config_item('_footer_info_text_string') ?></p>
    <div class="footer-hs-top-image"></div>
  </div>




  <div class="footer-bottom-section">
    <div class="container side-padding">
      <div class="footer-hs-bottom-image"></div>
      <div class="footer-bottom-copyright">© HabanoAficionado <?php echo date('Y'); ?></div>
      <div class="footer-bottom-policies">
        <a href="/politics">Условия&nbsp;использования и&nbsp;политика конфиденциальности</a>
        | <a href="http://www.polyartmedia.ru/?from=<?= filter_input(INPUT_SERVER, 'HTTP_HOST') ?>" target="_blank" title="PolyArtMedia Group">Разработка&nbsp;сайта</a>
      </div>
    </div>
  </div>

</footer>
</div>
<!-- // end of content wrapper -->

<!-- here will be special navigation buttons - menu and back to top -->

<?php $this->load->view('templates/public/back_to_top'); ?>
<?php $this->load->view('templates/public/menu_caller'); ?>


<!-- here will be navigation menu pop window -->
<?php $this->load->view('templates/public/menu_window'); ?>



<!-- here will filters pop windows -->
<div id="flt_window" class="flt-window hidden"></div>
<?php
// examples
//$this->load->view('templates/public/filter_window_lengths');
//$this->load->view('templates/public/filter_window_flavours');
//$this->load->view('templates/public/filter_window_intensities');
//$this->load->view('templates/public/filter_window_wrappers');
//$this->load->view('templates/public/filter_window_lines');
//$this->load->view('templates/public/filter_window_countries');
//$this->load->view('templates/public/filter_window_vitolas');
//$this->load->view('templates/public/filter_window_prices');
//$this->load->view('templates/public/filter_window_cepos');
//$this->load->view('templates/public/filter_window_brands');
//$this->load->view('templates/public/filter_window_sorting');
?>


<!-- here will be menu age verifier -->
<?php $this->load->view('templates/public/age_verifier'); ?>


<!-- here screen flasher -->
<div id="screenflasher" class="hidden"></div>

</body>
</html>