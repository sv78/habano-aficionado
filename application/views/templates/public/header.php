<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="ru-ru">
  <head>
    <meta charset="utf-8">

    <!-- TITLE AND META -->

    <title><?= isset($metatags['title']) ? $metatags['title'] : config_item('_metatags')['default']['title'] ?></title>
    <meta name="description" content="<?= isset($metatags['description']) ? $metatags['description'] : config_item('_metatags')['default']['description'] ?>">
    <meta name="keywords" content="<?= isset($metatags['keywords']) ? $metatags['keywords'] : config_item('_metatags')['default']['keywords'] ?>">
    <meta name="robots" content="<?= isset($metatags['robots']) ? $metatags['robots'] : config_item('_metatags')['default']['robots'] ?>">
    <meta name="author" content="<?= isset($metatags['author']) ? $metatags['author'] : config_item('_metatags')['default']['author'] ?>">

    <meta name="subject" content="<?= config_item('_metatags')['default']['subject'] ?>">
    <meta name="owner" content="<?= config_item('_metatags')['default']['owner'] ?>">
    <meta name="copyright" content="<?= config_item('_metatags')['default']['copyright'] ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    
    <!-- VERIFICATIONS -->
    
    <!-- FAVICON -->

    <link rel="icon" href="/assets/favicon/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/assets/favicon/favicon.ico">

    <!-- CSS -->

    <link href="/assets/css/normalize.css" rel="stylesheet" media="screen">
    <link href="/assets/css/css.css" rel="stylesheet" media="screen">
    <link href="/assets/js/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" media="screen">
    <link href="/assets/js/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet" media="screen">

    <!-- SCRIPTS -->

    <script src="/assets/js/jquery-3.1.1.min.js"></script>
    <script src="/assets/js/js.cookie.js"></script>
    <script src="/assets/js/owlcarousel/owl.carousel.min.js"></script>
    <script src="/assets/js/functions.js"></script>
    <script src="/assets/js/spinner.js"></script>
    <script src="/assets/js/cart.js"></script>
    <script src="/assets/js/script.js"></script>
    <script src="/assets/js/filter.js"></script>
    <script src="/assets/js/filter_list_forming_functions.js"></script>
    <script src="/assets/js/age_verification.js"></script>
    <script src="/assets/js/auth.js"></script>

    <?php
    // Yandex.Metrika
    $this->load->view('templates/public/yandex_metrika');
    
    // preloader ccs
    $this->load->view('templates/public/preloader_css');
    ?>
  </head>
  <body>
    <?php
    // Liveinternet Counter
    $this->load->view('templates/public/liveinternet_counter');
    
    // preloader html and JS code
    $this->load->view('templates/public/preloader');
    ?>
    <noscript>Your browser does not support JavaScript!</noscript>
    <div id="top"></div>

    <!-- content wrapper -->
    <div id="sitewrapper" class="wrapper">



      <?php
// top navigation bar here
      $this->load->view('templates/public/topnavbar');
      $this->load->view('templates/public/face_section');
      //$this->load->view('templates/public/temp_top_nav');
      ?>