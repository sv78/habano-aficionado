<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// print_r($this->session->userdata);

/* if (count($this->session->userdata('recently_seen_products')) != config_item('_recently_products_amount_to_remember')) {
  return;
  } */

if (!empty($this->session->userdata('recently_seen_products')) && count($this->session->userdata('recently_seen_products')) == config_item('_recently_products_amount_to_remember')) :
  ?>

  <div class="container">
    <div class="lined-text-header-container">
      <div class="line-text-line"></div>
      <div class="line-text-text">Недавно Вы смотрели</div>
    </div>
  </div>

  <div class="container products-array">
    <div class="row">
      <?php
      foreach ($this->session->userdata('recently_seen_products') as $rs_product):
        ?>
        <div class="product-cell column col-lg-3 col-sm-6 col-xs-12">
          <div class="product-cell-inner">
            <div class="rsp-img-holder">
              <a href="<?= $rs_product['url'] ?>">
                <?php
                if ($rs_product['is_cigar_product'] == 1) {
                  $img_class_attr = '';
                } else {
                  $img_class_attr = 'class="recently-seen-common-product"';
                }
                ?>
                <img src="<?= $rs_product['img_src'] ?>" alt="<?= $rs_product['name'] ?>" title="Нажмите, чтобы узнать подробнее" <?= $img_class_attr ?>>
              </a>
            </div>
            <h4><a href="<?= $rs_product['url'] ?>"><?= $rs_product['name'] ?></a></h4>
            <p class="smaller"><?= $rs_product['price'] != 0 ? $rs_product['price'] . ' руб.' : $this->config->item('_product_price_zero_substitution'); ?></p>
          </div>
        </div>
        <?php
      endforeach;
      ?>
    </div>
  </div>

  <?php
endif;