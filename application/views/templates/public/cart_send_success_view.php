<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container side-padding">
  <h1>Отправлено!</h1>
  <img class="cart-reserved-pic" src="/<?= config_item('_style_images_path_url') . 'reserved.png'; ?>" alt="Заказ принят в работу">
  <h4 class="text-center">Ваша заявка принята в&nbsp;работу!</h4>
  <p class="text-center">Сразу после обработки заказа наш специалист свяжется с&nbsp;Вами для подтверждения и&nbsp;уточнения возможных деталей Вашей заявки.</p>
  <p class="text-center color-ha-light uppercase offset-top-double">Спасибо за&nbsp;использование нашего сервиса!</p>
  <a class="btn-like btn-max-width offset-top-double" href="/catalog">Продолжить</a>
</div>