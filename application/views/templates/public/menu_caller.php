<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Menucaller -->
<div id="menucaller" class="menucaller-hidden" data-action-type="open-site-menu" title="Меню"></div>
<div id="menucaller_sm" class="menucaller-sm-hidden" data-action-type="open-site-menu" title="Меню"></div>
<!-- // Menucaller -->