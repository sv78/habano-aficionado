<footer>
  <hr>
  <?php
  //$back = $this->back_url->get();
  //echo '<p class="small"><a href="' . $back . '" target="_self">' . $back . '</a></p>';
  ?>
</footer>
</div> <!-- // wrapper -->


<!-- BACK TO TOP -->
<!-- child of the body tag -->
<span id="top-link-block" class="hidden">
  <a href="#top" class="well well-sm" onclick="$('html,body').animate({scrollTop: 0}, 400);return false;">
    <i class="glyphicon glyphicon-chevron-up"></i>
  </a>
</span>

<script>
// Only enable if the document has a long scroll bar
// Note the window height + offset
  if (($(window).height() + 100) < $(document).height()) {
    $('#top-link-block').removeClass('hidden').affix({
      // how far to scroll down before link "slides" into view
      offset: {top: 100}
    });
  }
</script>
<!-- /top-link-block -->
<!-- // BACK TO TOP -->

</body>
</html>