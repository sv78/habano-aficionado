<!DOCTYPE html>
<html lang="ru-ru">
  <head>
    <meta charset="utf-8">
    <title><?= $meta_title ?></title>

    <!-- META -->
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="Polyartmedia Group">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <!-- FAVICON -->
    <link rel="icon" href="/assets/favicon/admin/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/assets/favicon/admin/favicon.ico">

    <!-- CSS -->
    <link href="/assets/css/bootstrap-3.3.7-dist/css/bootstrap_yeti.css" rel="stylesheet" media="screen">
    <link href="/assets/css/admin.css" rel="stylesheet" media="screen">

    <!-- JS -->
    <script src="/assets/js/jquery-3.1.1.min.js"></script>
    <script src="/assets/css/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
    <script src="/assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <script src="/assets/js/functions.js"></script>
    <script src="/assets/js/admin/admin.js"></script>
  </head>
  <body>

    <!-- Admin Nav Bar -->

    <nav class="navbar navbar-default">
      <div class="container">


	<div class="navbar-header">
	  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	    <span class="sr-only">Toggle navigation</span>
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	  </button>
	  <a class="navbar-brand" href="/">
	    <img alt="Brand" src="/assets/images/style/admin/ha_logo.png" class="img-responsive" style="height: 24px;">
	  </a>
	</div>


	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	  <ul class="nav navbar-nav">

	    <li><a href="/admin">Админ</a></li>

	    <li><a href="/admin/orders">Заявки</a></li>

	    <li class="dropdown">
	      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Блог <span class="caret"></span></a>
	      <ul class="dropdown-menu">
		<li><a href="/admin/blog-sections">Разделы</a></li>
		<li><a href="/admin/create-new-blog-section">+ Раздел</a></li>
		<li role="separator" class="divider"></li>
		<li><a href="/admin/blog-articles">Статьи</a></li>
		<li><a href="/admin/create-new-blog-article">+ Статья</a></li>
	      </ul>
	    </li>



	    <li class="dropdown">
	      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Бренды <span class="caret"></span></a>
	      <ul class="dropdown-menu">
		<li><a href="/admin/brands">Бренды</a></li>
		<li><a href="/admin/create-brand">+ Бренд</a></li>
	      </ul>
	    </li>



	    <li class="dropdown">
	      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Товары <span class="caret"></span></a>
	      <ul class="dropdown-menu">
		<li><a href="/admin/products">Товары</a></li>
		<li><a href="/admin/create-product">+ Товар</a></li>
		<li role="separator" class="divider"></li>
		<li><a href="/admin/categories">Категории</a></li>
		<li><a href="/admin/create-category">+ Категория</a></li>
		<li role="separator" class="divider"></li>
		<li><a href="/admin/packs">Упаковки</a></li>
	      </ul>
	    </li>



	    <li class="dropdown">
	      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Баннеры <span class="caret"></span></a>
	      <ul class="dropdown-menu">
		<li><a href="/admin/banners">Баннеры</a></li>
		<li><a href="/admin/create-banner">+ Баннер</a></li>
	      </ul>
	    </li>


	    <li class="dropdown">
	      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Сервис <span class="caret"></span></a>
	      <ul class="dropdown-menu">
		<li><a href="/admin/file-manager">Менеджер файлов</a></li>
		<li><a href="/admin/users">Пользователи</a></li>
		<!--<li role="separator" class="divider"></li>-->
		<!--<li><a href="/admin/filters">Фильтры сигар</a></li>-->
		<!--<li role="separator" class="divider"></li>-->
		<!--<li><a href="/admin/archived-orders">Архив заказов</a></li>-->
		<!--<li><a href="/admin/statistics">Статистика</a></li>-->
		<!--<li role="separator" class="divider"></li>-->
		<!--<li><a href="#">Clear Captcha</a></li>-->
		<!--<li><a href="#">Clear Garbage</a></li>-->
		<!--<li role="separator" class="divider"></li>-->
		<!--<li><a href="#">Настройки</a></li>-->
		<!--<li><a href="#">Помощь</a></li>-->
		<!--<li><a href="#">Инфо</a></li>-->
	      </ul>
	    </li>


	  </ul>

	  <ul class="nav navbar-nav navbar-right">
	    <li class="dropdown">
	      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		<?php
		if (isset($_SESSION['user']['is_super_admin']) && $_SESSION['user']['is_super_admin'] === TRUE) {
		  echo '<small class="glyphicon glyphicon-king" aria-hidden="true" style="color:orange;" title="Super Administrator Status"></small>&nbsp;&nbsp;&nbsp;';
		}
		echo strlen($this->session->user['email']) < 32 ? $this->session->user['email'] : 'Admin';
		?>
		<span class="caret"></span></a>
	      <ul class="dropdown-menu">
		<li><a href="/admin/users/<?= $this->session->userdata('user')['id']; ?>">Вы</a></li>
		<li><a href="/reset-password">Сброс Пароля</a></li>
		<li><a href="#" onclick="logout()">Выход</a></li>
	      </ul>
	    </li>
	  </ul>

	</div>

      </div>
    </nav>

    <script>
      function logout() {
	if (!confirm('Log out?'))
	  return;
	window.location.assign(location.protocol + "//" + location.host + "/logout");
      }
    </script>

    <!-- // Admin Nav Bar -->

    <div id="dbg"></div>

    <div class="container"> <!-- wrapper -->
