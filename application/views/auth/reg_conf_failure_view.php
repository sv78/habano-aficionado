<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">
  <h1>Ошибка…</h1>
  <p class="text-center">Вероятно подтверждение просрочено или что-то пошло не так…</p>
</div>