<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container side-padding">

  <h1>Вход</h1>
  <h4 class="text-center">Пожалуйста, введите Ваш&nbsp;e&#8209;mail и&nbsp;пароль</h4>

  <?php
  if (isset($login_success)) {
    if (!$login_success) {
      $str_arr = [
	  '<p class="alert">',
	  'Не&nbsp;существует пользователя с&nbsp;такими данными. Попробуйте войти еще раз.',
	  '</p>'
      ];
      echo join($str_arr);
    }
  }

  $form_attributes = array('id' => 'login_form', 'class' => 'login-form', 'onsubmit' => 'validateLoginForm(event)');
  echo form_open_multipart('', $form_attributes);
  ?>


  <label class="input-text-label" for="email_input">E-mail&nbsp;<sup>*</sup></label>
  <input id="email_input" placeholder="Email" onblur="remove_all_input_spaces(event)" onfocus="showInputValid(this)" type="text" name="email" maxlength="128" autocomplete="off" value="">
  <?php echo form_error('email') ?>

  <label class="input-text-label" for="password_input">Пароль&nbsp;<sup>*</sup></label>
  <input id="password_input" placeholder="Пароль" onblur="remove_all_input_spaces(event)" onfocus="showInputValid(this)" type="password" name="password" maxlength="64" autocomplete="off" value="">
  <?php echo form_error('password') ?>


  <button name="submit" value="submit" class="offset-top-double">Войти</button>


</form>



<p class="text-center"><a href="/reset-password">Забыли пароль?</a> Перейдите по&nbsp;ссылке, чтобы <a href="/reset-password">создать новый.</a></p>

<p class="text-center">Если Вы еще незарегистрированы, пожалуйста <a href="/register">зарегистрируйтесь</a>.</p>

<script>
  document.getElementById('email_input').focus();
</script>

</div>