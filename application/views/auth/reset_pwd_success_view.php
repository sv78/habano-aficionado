<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">

  <h1>Готово!</h1>

  <p class="text-center">Для завершения процедуры восстановления доступа путем создания нового пароля пожалуйста войдите в почту, которую Вы указали при выполнении процедуры создания нового пароля. По указанному Вами адресу было направлено письмо от имени нашего сайта. Откройте письмо, прочитайте и пройдите по указанной в письме ссылке для завершения процедуры. Спасибо!</p>

  <p class="text-center">Процедура подтверждения по ссылке в письме будет доступна в течении <?= config_item('_rst_pwd_conf_time_limit') / 60 ?> минут.</p>

  <img class="cigars-for-welcome offset-top-fourth" src="/<?= config_item('_style_images_path_url') . 'cigars_for_welcome.jpg'; ?>" alt="Сигары">

</div>