<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">

  <h1>Регистрация</h1>
  <p class="text-center">Уважаемый посетитель, пожалуйста заполните поля формы для&nbsp;регистрации.</p>
  <h3 class="text-center">После процесса регистрации Вы&nbsp;получаете возможность:</h3>

  <div class="row">

    <div class="column col-lg-6 col-sm-12">
      <ul>
	<li>Резервировать заказы на&nbsp;сайте</li>
	<li>Получать заказы в&nbsp;клубных магазинах</li>
	<li>Получать информацию о&nbsp;новинках и&nbsp;лимитированных предложениях</li>
      </ul>
    </div>

    <div class="column col-lg-6 col-sm-12">
      <ul>
	<li>Подать заявление о&nbsp;вступлениии в&nbsp;клуб</li>
	<li>Пользоваться клубной системой доставки</li>
	<li>Посещать клубные лаунжи/салоны</li>
      </ul>
    </div>

  </div>

  <?php
//echo date("Y-m-d H:i:s", time());
//echo date("Y-m-d H:i:s", 1483960327);
//echo validation_errors();
  $form_attributes = array('id' => 'register_form', 'class' => 'login-form', 'onsubmit' => 'validateRegisterForm(event)');
  echo form_open_multipart('', $form_attributes);
  ?>


  <label class="input-text-label" for="email_input">Ваш E-mail&nbsp;<sup>*</sup></label>
  <input id="email_input" placeholder="Email" onblur="remove_all_input_spaces(event)" onfocus="showInputValid(this)" type="text" name="email" maxlength="128" autocomplete="off" value="<?php echo set_value('email'); ?>">
  <?php echo form_error('email') ?>

  <label class="input-text-label" for="password_input">Придумайте пароль&nbsp;<sup>*</sup></label>
  <input id="password_input" placeholder="Пароль" onblur="remove_all_input_spaces(event)" onfocus="showInputValid(this)" type="text" name="password" maxlength="64" autocomplete="off" value="<?php echo set_value('password'); ?>">
  <?php echo form_error('password') ?>
  <p class="text-center smaller">Для создания пароля, пожалуйста, используйте символы латинского алфавита, цифры, а также другие символы при необходимости.</p>



  <div class="captcha-container">
    <?= $captcha_image ?>
  </div>

  <label class="input-text-label" for="captcha_input">Введите число, которое указано на&nbsp;картинке&nbsp;<sup>*</sup></label>
  <input id="captcha_input" placeholder="Число" onblur="remove_all_input_spaces(event)" onfocus="showInputValid(this)" type="text" name="captcha" maxlength="8" autocomplete="off" value="">
  <?php echo form_error('captcha') ?>


  <button name="submit" value="submit" class="offset-top-double">Отправить</button>


</form>

<p class="text-center"><span class="js-text-link" onclick="location.assign(location.href);">Сбросить данные</span></p>

<p class="text-center">Если Вы уже зарегистрированы, пожалуйста <a href="/login">нажмите здесь</a>, чтобы войти.</p>



<?php
//echo "your IP : " . $this->input->ip_address();
//echo "<br>time : " . $captcha_time . "<br>";
//echo "<br>microtime : " . microtime(true) . "<br>";
// Проверку JS будем делай после серверной части, чтоб отлатить серверную сначала!!!
// Think about to check Capcha word for JS validation : php echo $captcha_word
?>

</div>
