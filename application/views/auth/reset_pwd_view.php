<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container side-padding">



  <h1>Создание нового пароля</h1>
  <!--<h4 class="text-center uppercase">Создание нового пароля</h4>-->
  <h4 class="text-center">Будте внимательны!</h4>
  <p class="text-center">На указанный Вами е-mail будет выслано письмо для&nbsp;подтверждения создания нового пароля.</p>


  <?php
  if (isset($reset_result)) {
    if (!$reset_result) {
      $str_arr = [
	  '<h4 class="alert" style="margin:40px auto;">',
	  'Данный адрес e-mail не&nbsp;числится как зарегистрированный!',
	  '</h4>'
      ];
      echo join($str_arr);
    }
  }

//echo validation_errors();
  $form_attributes = array('id' => 'reset_pwd_form', 'class' => 'login-form', 'onsubmit' => 'validateResetPasswordForm(event)');
  echo form_open_multipart('', $form_attributes);
  ?>


  <label class="input-text-label" for="email_input">Зарегистрированный e-mail&nbsp;<sup>*</sup></label>
  <input id="email_input" placeholder="Email" onblur="remove_all_input_spaces(event)" onfocus="showInputValid(this)" type="text" name="email" maxlength="128" autocomplete="off" value="<?php echo set_value('email'); ?>">
  <?php echo form_error('email') ?>

  <label class="input-text-label" for="password_input">Придумайте новый пароль&nbsp;<sup>*</sup></label>
  <input id="password_input" placeholder="Пароль" onblur="remove_all_input_spaces(event)" onfocus="showInputValid(this)" type="text" name="password" maxlength="64" autocomplete="off" value="">
  <?php echo form_error('password') ?>


  <div class="captcha-container">
    <?= $captcha_image ?>
  </div>

  <label class="input-text-label" for="captcha_input">Введите число, которое указано на&nbsp;картинке&nbsp;<sup>*</sup></label>
  <input id="captcha_input" placeholder="Число" onblur="remove_all_input_spaces(event)" onfocus="showInputValid(this)" type="text" name="captcha" maxlength="8" autocomplete="off" value="">
  <?php echo form_error('captcha') ?>

  <button name="submit" value="submit" style="margin-top: 40px;">Отправить</button>

</form>

<p class="text-center"><span class="js-text-link" onclick="location.assign(location.href);">Сбросить данные</span></p>

<p class="text-center">Если Вы еще незарегистрированы, пожалуйста <a href="/register">зарегистрируйтесь</a>.</p>

<script>
  document.getElementById('email_input').focus();
</script>

</div>