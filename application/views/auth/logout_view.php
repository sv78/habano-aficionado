<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">
  <h1>До встречи!</h1>
  <img class="cigars-for-welcome offset-top-fourth" src="/<?= config_item('_style_images_path_url') . 'cigars_for_welcome.jpg'; ?>" alt="Сигары">
  <p class="text-center">Вы вышли из системы</p>
</div>