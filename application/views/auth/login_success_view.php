<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">
  <h1>Добро пожаловать!</h1>
  <p class="text-center">Вы вошли на сайт!</p>
  <img class="cigars-for-welcome offset-top-fourth" src="/<?= config_item('_style_images_path_url') . 'cigars_for_welcome.jpg'; ?>" alt="Сигары">
  <p class="text-center smaller">
    <a href="/">На главную</a>
    | <a href="/catalog">Каталог</a>
  </p>
</div>