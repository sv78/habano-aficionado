<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container side-padding">
  <!--<h1 style="color:red;">Access attempts number excess!</h1>-->
  <h1 style="color:red;">Что-то забыли?!</h1>
  <h3 class="text-center">Превышено количество попыток входа.</h3>
  
  <p class="text-center">Вы пытались войти на сайт более <?= $access_attempts_num_limit ?> раз подряд. По соображениям безопасности Вы будете заблокированы на <?= $ip_block_time_limit / 60 ?> минут. Если Вы забыли Ваш пароль, то Вы можете восстановить его пройдя по <a href="/forgot-password">этой ссылке</a>.</p>
  
</div>