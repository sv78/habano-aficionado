<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">
  <h1>Ошибка…</h1>
  <p class="text-center">Вероятно подтверждение просрочено или что-то пошло не так…</p>
  <img class="cigars-for-welcome offset-top-fourth" src="/<?= config_item('_style_images_path_url') . 'cigars_for_welcome.jpg'; ?>" alt="Сигары">
</div>