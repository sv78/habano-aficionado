<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">
  <h1>Вы зарегистрированы!</h1>
  <p class="text-center">Поздравляем! Процесс регистрация успешно завершен!</p>
  <img class="cigars-for-welcome offset-top-fourth" src="/<?= config_item('_style_images_path_url') . 'cigars_for_welcome.jpg'; ?>" alt="Сигары">
  <p class="text-center">
    <a href="/login" class="btn-like btn-max-width">Войти</a>
  </p>
</div>