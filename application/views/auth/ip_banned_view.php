<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">
  <h1 style="color:red;">Доступ заборкирован!</h1>
  <!--<p>Unfortunately the system indicates that you did something wrong. You cannot enter or create account. To try solve this issue please contact the owner of this website.</p>-->
  <p class="text-center">К сожалению, система сообщает что Вы производите действия, которые могут оказаться небезопасными. Вы не сможете войти в свой аккаунт или создать новый. Если произошло недоразумение и чтобы попытаться разрешить этот вопрос пожалуйста свяжитесь с владельцем данного сайта.</p>
</div>