<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">
  <h1>Готово!</h1>
  <p class="text-center">Для завершения процедуры регистрации пожалуйста войдите в почту, которую Вы указали при регистрации. По указанному Вами адресу было направлено письмо от имени нашего сайта. Откройте письмо, прочитайте и подтвердите регистрацию, следуя по указанной в письме ссылке. Спасибо!</p>
  <img class="cigars-for-welcome offset-top-fourth" src="/<?= config_item('_style_images_path_url') . 'cigars_for_welcome.jpg'; ?>" alt="Сигары">
</div>
