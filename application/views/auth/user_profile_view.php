<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container side-padding">

  <img class="profile-pic offset-top-fourth" src="/<?= config_item('_style_images_path_url') . 'profile_pic.png'; ?>" alt="Профиль пользователя">

  <?php
  if ($this->session->userdata('user')['name']) :
    ?>
    <h3 class="text-center"><?= $this->session->userdata('user')['name'] ?></h3>
    <?php
  endif;
  ?>

  <?php
  if ($this->session->userdata('user')['phone']):
    ?>
    <p class="text-center">Телефон: +7<?= $this->session->userdata('user')['phone'] ?></p>
    <?php
  endif;
  ?>
  <p class="text-center"><?= $this->session->userdata('user')['email'] ?></p>


  <?php
  if ($this->session->userdata('user')['address']):
    ?>
    <p class="text-center"><?= Baza::decode_plain_string_from_db($this->session->userdata('user')['address']) ?></p>
    <?php
  endif;
  ?>


  <p class="text-center">Зарегистрирован: <?= date("Y-m-d H:i:s", $this->session->userdata('user')['reg_time']) ?></p>

  <p class="text-center smaller offset-top-fourth">
    <a href="/catalog">Каталог</a> | 
    <a href="/cart">Резерв</a> | 
    <a href="/logout">Выйти</a>
  </p>


</div>


<?php
//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";
?>