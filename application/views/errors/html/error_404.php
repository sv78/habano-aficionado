<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>404 Page Not Found</title>
    <style type="text/css">

      ::selection { background-color: #807d63; color: #9d9565; }
      ::-moz-selection { background-color: #807d63; color: #9d9565; }

      body {
	background-color: #000;
	margin: 0;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #807d63;
	text-align: center;
      }
      a {
	color: inherit;
      }
      a:hover {
	color: #9d9565;
      }
    </style>
  </head>
  <body>
    <a href="/">
      <img  src="/assets/images/style/menu_logo.png" style="display:block; margin: 40px auto;">
    </a>
    <!--<h1><?php //echo $heading;    ?></h1>-->
    <?php // echo $message; ?>
    <h1>Ошибка 404</h1>
    <p>Запрашиваемая страница не найдена</p>
    <p><a href="/">Перейти на главную страницу сайта</a></p>
  </body>
</html>