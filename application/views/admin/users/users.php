<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h1>Пользователи</h1>

<table class="table table-responsive table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th title="Status">St</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Name</th>
      <th>Address</th>
      <th>Since</th>
      <?php
      if (isset($_SESSION['user']['is_super_admin'])) {
	?>
        <th>Del</th>
	<?php
      }
      ?>
    </tr>
  </thead>  
  <tbody>
    <?php
    foreach ($users as $user) :
      $tr = [];
      $admin_user_tr_style = $user['is_admin'] == 1 ? 'class="success"' : '';
      $reg_user_tr_style = $user['reg_confirmed'] == 0 ? 'class="danger" title="Registration is not confirmed"' : '';
      $tr[] = '<tr ' . $reg_user_tr_style . $admin_user_tr_style . '>';

      $tr[] = '<td>' . $row_counter . '</td>';


      // superadmin feature only
      if (isset($_SESSION['user']['is_super_admin'])) {
	$action_type_attr = 'action-type="status" ';
	$user_id_attr = 'user-id="' . $user['id'] . '" ';
	$class_attr = 'class="cursor-pointer" ';
      } else {
	$action_type_attr = '';
	$user_id_attr = '';
	$class_attr = '';
      }

      if ($user['reg_confirmed']) {
	$status = $user['is_admin'] ? "<span title='Administrator' $class_attr $user_id_attr $action_type_attr>Ad</span>" : "<span title='Customer' $class_attr $user_id_attr $action_type_attr>Cu</span>";
      } else {
	$status = '';
      }
      $tr[] = '<td>' . $status . '</td>';




      $tr[] = '<td><a href="/admin/users/' . $user['id'] . '" target="_self">' . $user['email'] . '</td>';
      $tr[] = '<td>' . $user['phone'] . '</td>';


      $name = $user['name'] != NULL ? '<span class="glyphicon glyphicon-user" aria-hidden="true"></span>' : "";
      $tr[] = '<td title="' . $user['name'] . '">' . $name . '</td>';


      $address = $user['address'] != NULL ? '<span class="glyphicon glyphicon-home" aria-hidden="true"></span>' : "";
      $tr[] = '<td title="' . $user['address'] . '">' . $address . '</td>';

      $tr[] = '<td title="Registered at ' . date('d.m.Y - H:i:s', $user['reg_time']) . '">' . date('d.m.Y', $user['reg_time']) . '</td>';


      //$tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-info-sign" aria-hidden="true" action-type="info" user-id="' . $user['id'] . '"></span></td>';

      if (isset($_SESSION['user']['is_super_admin'])) {
	$tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-remove" aria-hidden="true" action-type="delete" user-id="' . $user['id'] . '"></span></td>';
      }
      echo join('', $tr);
      ?>
      </tr>
      <?php
      $row_counter++;
    endforeach;
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">Всего пользователей : <span id="total_records_counter"><?= $num_of_users ?></span></td>
    </tr>
  </tfoot>
</table>

<?php echo $this->pagination->create_links(); ?>

<p class="text-right small">
  <a href="<?= current_url() ?>">Пользователи</a>
  | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить</a>
</p>

<script>
  document.body.addEventListener('click', clickActionListener, false);
  function clickActionListener(e) {
    if (e.target.hasAttribute('user-id')) {
      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type').toLowerCase() === 'delete') {
	delete_user_by_id(e.target);
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type').toLowerCase() === 'status') {
	toggle_user_status_by_id(e.target)
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type').toLowerCase() === 'info') {
	window.alert('Not Done! Info user id : ' + e.target.getAttribute('user-id'));
	//console.log(e.target.getAttribute('action-type') + " : " + e.target.getAttribute('section-id'));
      }
    }
  }


</script>