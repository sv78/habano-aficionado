<h1>Данные пользователя</h1>
<p class="small">От <?= date('d M Y - H:i:s', time()); ?></p>

<hr>
<?php
if (!$user->reg_confirmed) {
  echo '<p class="bg-danger">Registration is not confirmed</p>';
}

if ($user->id == $this->session->userdata('user')['id']) {
  ?>
  <h4><strong>Это Вы</strong></h4>
  <hr>
  <?php
}
if ($user->name != '') {
  ?>
  <p>Имя : <strong><?= $user->name ?></strong></p>
  <?php
}
if ($user->email != '') {
  ?>
  <p>Email : <strong><?= $user->email ?></strong></p>

  <?php
}
if ($user->phone != '') {
  ?>
  <p>Телефон : <strong>+7<?= $user->phone ?></strong></p>
  <?php
}
if ($user->address != '') {
  ?>
  <pre>Адрес : <?= Baza::decode_plain_string_from_db($user->address) ?></pre>
  <?php
}
?>
<hr>
<p>Статус : <strong><?= $user->is_admin == 1 ? "Administrator" : "Customer" ?></strong></p>
<p class="small">Время регистрации : <strong><?= date('d M Y - H:i:s', $user->reg_time) ?></strong></p>

<?php
if (isset($this->session->userdata('user')['is_super_admin']) && $user->rst_pwd_time != NULL) {
  ?>
  <p class="small">Последняя смена пароля : <strong><?= date('d M Y - H:i:s', $user->rst_pwd_time) ?></strong></p>
  <?php
}
?>
<hr>
<p class="small"><a href="mailto:<?= $user->email ?>?subject=Habano Aficionado Info" title="Отправить e-mail с помощью установленного на Вашем компьютере почтового клиента (например MS Outlook и т.д.)">Отправить e-mail</a></p>
<?php
if (preg_match('/^\+?\d{11}$/', '+7' . $user->phone)) {
  ?>
  <p class="small"><a href="tel:+7<?= $user->phone ?>" title="Позвонить с помощью установленного на Вашем компьютере сервиса для звонков (например Skype и т.д.)">Позвонить</a></p>
  <?php
}
?>

<p class="text-right small"><a href="/admin/users">Пользователи</a></p>