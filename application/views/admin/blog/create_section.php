<h1>Создание нового раздела блога</h1>
<?php
//echo validation_errors();
//echo $upload_error;
//echo $image_lib_error;
//echo $upload_data;
?>

<?php
$form_attributes = array('id' => 'blog_section_create_form');
echo form_open_multipart('', $form_attributes);
?>

<p class="admin-input-title text-primary">Название*</p>
<input id="section_title_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="title" maxlength="255" autocomplete="off" value="<?php echo set_value('title'); ?>">
<?php echo form_error('title') ?>




<p class="admin-input-title text-primary">Ярлык URL*</p>
<input id="section_slug_input" onblur="to_slug(event)" class="form-control admin-input" type="text" name="slug" maxlength="64" autocomplete="off" value="<?php echo set_value('slug'); ?>">
<?php echo form_error('slug') ?>



<span id="section_slug_create_btn" class="btn btn-default btn-sm">Создать ярлык URL из названия</span>



<div class="admin-input-title">
  <div class="checkbox">
    <label for="published_chkbx">
      <input type="checkbox" id="published_chkbx" name="published" <?php echo set_checkbox('published', 'on', TRUE) ?>> Публиковать
    </label>
  </div>
  <?php echo form_error('published') ?>
</div>



<p class="admin-input-title">Вводный текст</p>
<textarea class="form-control admin-textarea" onblur="trim_input_field(event)" name="intro"><?php echo set_value('intro'); ?></textarea>


<?php
if (isset($this->session->userdata('user')['is_super_admin'])) {
  ?>
  <p class="admin-input-title text-muted">Before Content : Extended HTML with JS</p>
  <textarea class="form-control admin-textarea" style="font-size: .75em; height: 250px;" onblur="trim_input_field(event)" name="before_body"><?php echo set_value('before_body'); ?></textarea>
  <?php
}
?>

<p class="admin-input-title">Контент</p>
<textarea id="section_body" name="body"><?php echo set_value('body'); ?></textarea>


<?php
if (isset($this->session->userdata('user')['is_super_admin'])) {
  ?>
  <p class="admin-input-title text-muted">After Content : Extended HTML with JS</p>
  <textarea class="form-control admin-textarea" style="font-size: .75em; height: 250px;" onblur="trim_input_field(event)" name="after_body"><?php echo set_value('after_body'); ?></textarea>
  <?php
}
?>


<p class="admin-input-title"><?= $this->lang->line('habano_input_metatitle') ?></p>
<input id="section_metatitle_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="meta_title" maxlength="255" autocomplete="off" value="<?php echo set_value('meta_title'); ?>">



<span id="section_metatitle_create_btn" class="btn btn-default btn-sm">
  <?= $this->lang->line('habano_input_generate_metatitle_from_title_btn') ?>
</span>



<p class="admin-input-title"><?= $this->lang->line('habano_input_metadescription') ?></p>
<input class="form-control admin-input" onblur="trim_input_field(event)" type="text" name="meta_description" maxlength="255" autocomplete="off" value="<?php echo set_value('meta_description'); ?>">


<p class="admin-input-title"><?= $this->lang->line('habano_input_metakeywords') ?></p>
<input class="form-control admin-input" onblur="trim_input_field(event)" type="text" name="meta_keywords" maxlength="255" autocomplete="off" value="<?php echo set_value('meta_keywords'); ?>">


<p class="admin-input-title"><?= $this->lang->line('habano_input_metarobots') ?></p>
<select class="form-control admin-input admin-max-width-normal" name="meta_robots">
  <?php Baza::create_options('meta_robots', Baza::$meta_robots_options_array); ?>
</select>
<?php echo form_error('meta_robots') ?>


<p class="admin-input-title">Загрузить файл</p>
<input class="admin-input" type="file" name="img">
<div>
  <small style="color: red;"><?php echo $upload_error ?></small>
  <small style="color: red;"><?php echo $image_lib_error ?></small>
  <small style="color: red;"><?php echo $upload_data ?></small>
</div>
<small>Для загрузки допустимы только графические файлы (jpg, jpeg, png, gif)</small>


<div class="admin-input-title">
  <button class="btn btn-success" name="submit" type="submit" value="submit">Сохранить</button>
</div>

</form>

<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);"><?= $this->lang->line('habano_input_reload_page_to_reset_form_after_validation') ?></button>
</div>


<script src="/assets/js/ckeditor_4.6.1_full/ckeditor/ckeditor.js"></script>

<?php
if (isset($this->session->userdata('user')['is_super_admin'])) {
  ?>
  <script>
      CKEDITOR.replace('before_body', {
        customConfig: 'config_superadmin.js'
      });
      CKEDITOR.replace('after_body', {
        customConfig: 'config_superadmin.js'
      });
  </script>
  <?php
}
?>
  
<script>
  CKEDITOR.replace('section_body', {
    customConfig: 'config_admin.js'
  });

  // creating slug from title
  document.getElementById('section_slug_create_btn').onclick = function () {
    create_slug_from_title(document.getElementById('section_title_input'), document.getElementById('section_slug_input'), 64);
  };

  document.getElementById('section_metatitle_create_btn').onclick = function () {
    create_metatitle_from_title(document.getElementById('section_title_input'), document.getElementById('section_metatitle_input'));
  };
</script>