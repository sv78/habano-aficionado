<table class="table table-responsive table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th>Pub</th>
      <th title="Featured">Ftr</th>
      <th>Up</th>
      <th>Img</th>
      <th class="admin-th-title">Article title</th>
      <th class="admin-th-slug">Slug</th>
      <th>Edit</th>
      <th>Del</th>
      <th>By</th>
    </tr>
  </thead>  
  <tbody>
    <?php
    foreach ($articles as $article) :
      $tr = [];
      ?>
      <tr>
	<?php
	$tr[] = '<td>' . $row_counter . '</td>';


	// published by
	if ($article['used_by_user_id'] != NULL) {
	  $published = $article['published'] ? '<span class="text-muted glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-lock" aria-hidden="true"></span>';
	} else {
	  $published = $article['published'] ? '<span class="cursor-pointer glyphicon glyphicon-ok" aria-hidden="true" action-type="published" article-id="' . $article['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-lock" aria-hidden="true" action-type="published" article-id="' . $article['id'] . '"></span>';
	}
	$tr[] = '<td>' . $published . '</td>';


	// featured
	if ($article['used_by_user_id'] != NULL) {
	  $featured = $article['featured'] ? '<span class="text-muted glyphicon glyphicon-star" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-star-empty" aria-hidden="true"></span>';
	} else {
	  $featured = $article['featured'] ? '<span class="cursor-pointer glyphicon glyphicon-star" aria-hidden="true" action-type="featured" article-id="' . $article['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-star-empty" aria-hidden="true" action-type="featured" article-id="' . $article['id'] . '"></span>';
	}
	$tr[] = '<td>' . $featured . '</td>';




	// lift up
	if ($article['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-upload" aria-hidden="true" action-type="liftup" article-id="' . $article['id'] . '"></span></td>';
	}





	// image
	if ($article['img'] == '') {
	  $article_img = "/" . config_item('_blog_not_specified_image_url');
	} else if (!file_exists(config_item('_blog_article_image_path') . $article['img'])) {
	  $article_img = "/" . config_item('_blog_missing_image_url');
	} else {
	  $article_img = "/" . config_item('_blog_article_image_path_url') . $article['img'];
	}

	$tr[] = '<td><img title="' . $article['intro'] . '" src="' . $article_img . '" class="cursor-help blog-img-thumb" alt="thumb"></td>';


	// title
	$tip_arr = [];
	$tip_arr[] = 'Cоздана: ' . $article['created_at'];
	$tip_arr[] = ' | Последняя редакция: ' . $article['modified_at'];
	if ($article['lifted_up_at'] != NULL) {
	  $tip_arr[] = ' | Поднята: ' . date('d M Y', $article['lifted_up_at']) . " в " . date('H:i:s', $article['lifted_up_at']);
	}
	$tip = join('', $tip_arr);
	unset($tip_arr);
	$tr[] = '<td title="' . $tip . '"><a href="/admin/blog-article-preview/' . $article['id'] . '">' . $article['title'] . '</a></td>';

	// slug
	$tr[] = '<td>' . $article['slug'] . '</td>';



	// edit
	if ($article['used_by_user_id'] != NULL) {
	  if (isset($this->session->userdata('user')['is_super_admin']) OR $this->session->userdata('user')['id'] == $article['used_by_user_id']) {
	    $tr[] = '<td><a class="glyphicon glyphicon-floppy-remove" aria-hidden="true" href="/admin/blog-article-edit/' . $article['id'] . '"></a></td>';
	  } else {
	    $tr[] = '<td></td>';
	  }
	} else {
	  $tr[] = '<td><a class="glyphicon glyphicon-pencil" aria-hidden="true" href="/admin/blog-article-edit/' . $article['id'] . '"></a></td>';
	}



	// delete
	if ($article['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-remove" aria-hidden="true" action-type="delete" article-id="' . $article['id'] . '"></span></td>';
	}

	// used by
	if ($article['used_by_user_id'] != NULL) {
	  if ($article['used_by_user_id'] == $this->session->userdata('user')['id']) {
	    $tr[] = '<td><a href="/admin/users/' . $article['used_by_user_id'] . '">You</a></td>';
	  } else {
	    $tr[] = '<td><a class="glyphicon glyphicon-user" aria-hidden="true" href="/admin/users/' . $article['used_by_user_id'] . '"></a></td>';
	  }
	} else {
	  $tr[] = "<td></td>";
	}

	echo join('', $tr);
	?>
      </tr>
      <?php
      $row_counter++;
    endforeach;
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">Articles : <span id="total_records_counter"><?= $num_of_articles ?></span></td>
    </tr>
  </tfoot>
</table>

<script>
  document.body.addEventListener('click', clickActionListener, false);
  function clickActionListener(e) {
    if (e.target.hasAttribute('article-id')) {
      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'delete') {
	delete_article_by_id(e.target);
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'published') {
	toggle_published_item_by_id(e.target)
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'liftup') {
	lift_up(e.target)
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'featured') {
	toggle_featured(e.target)
      }

    }
  }

</script>