<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Разделы блога</h1>

<table class="table table-responsive table-hover">
  <thead>
    <tr>
      <th title="Порядковый номер">#</th>
      <th title="Статус: опубликовано или нет">Pub</th>
      <th title="Поднять вверх">Up</th>
      <th title="Статьи раздела">List</th>
      <th title="Изображение раздела">Img</th>
      <th title="Название раздела" class="admin-th-title">Section title</th>
      <th title="Ярлык URL" class="admin-th-slug">Slug</th>
      <th title="Редактировать">Edit</th>
      <th title="Удалить">Del</th>
      <th title="Статус: открыто другим пользователем или нет">By</th>
    </tr>
  </thead>  
  <tbody>
    <?php
    foreach ($sections as $section) :
      $tr = [];
      ?>
      <tr>
	<?php
	$tr[] = '<td>' . $row_counter . '</td>';


	// published by
	if ($section['used_by_user_id'] != NULL) {
	  $published = $section['published'] ? '<span class="text-muted glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-lock" aria-hidden="true"></span>';
	} else {
	  $published = $section['published'] ? '<span class="cursor-pointer glyphicon glyphicon-ok" aria-hidden="true" action-type="published" section-id="' . $section['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-lock" aria-hidden="true" action-type="published" section-id="' . $section['id'] . '"></span>';
	}
	$tr[] = '<td>' . $published . '</td>';
	
	
	
	// lift up
	if ($section['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-upload" aria-hidden="true" action-type="liftup" section-id="' . $section['id'] . '"></span></td>';
	}
	


	
	// list
	//$tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-eye-open" aria-hidden="true" action-type="preview" section-id="' . $section['id'] . '"></span></td>';
	$tr[] = '<td><a class="glyphicon glyphicon-list" aria-hidden="true" href="' . current_url() . '/' . $section['id'] . '"></a></td>';
	
	

	// image
	if ($section['img'] == '') {
	  $section_img = "/" . config_item('_blog_not_specified_image_url');
	} else if (!file_exists(config_item('_blog_section_image_path') . $section['img'])) {
	  $section_img = "/" . config_item('_blog_missing_image_url');
	} else {
	  $section_img = "/" . config_item('_blog_section_image_path_url') . $section['img'];
	}
	$tr[] = '<td><img title="' . $section['intro'] . '" src="' . $section_img . '" class="cursor-help blog-img-thumb" alt="thumb"></td>';





	// title
	$title_style = $section['slug'] === config_item('_reserved_section_slug') ? 'style="color:red;" ' : '';
	$tr[] = '<td>';
	//$tr[] = '<a href="' . current_url() . "/" . $section['id'] . '" ' . $title_style . '>' . $section['title'] . '</a>';
	$tr[] = '<a href="/admin/blog-section-preview/' . $section['id'] . '" ' . $title_style . '>' . $section['title'] . '</a>';
	$tr[] = '</td>';

	// slug
	$tr[] = '<td>' . $section['slug'] . '</td>';



	// edit
	if ($section['used_by_user_id'] != NULL) {
	  if (isset($this->session->userdata('user')['is_super_admin']) OR $this->session->userdata('user')['id'] == $section['used_by_user_id']) {
	    //$tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-floppy-remove" aria-hidden="true" action-type="edit" section-id="' . $section['id'] . '"></span></td>';
	    $tr[] = '<td><a class="glyphicon glyphicon-floppy-remove" aria-hidden="true" href="/admin/blog-section-edit/' . $section['id'] . '"></a></td>';
	  } else {
	    $tr[] = '<td></td>';
	  }
	} else {
	  //$tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-pencil" aria-hidden="true" action-type="edit" section-id="' . $section['id'] . '"></span></td>';
	  $tr[] = '<td><a class="glyphicon glyphicon-pencil" aria-hidden="true" href="/admin/blog-section-edit/' . $section['id'] . '"></a></td>';
	}




	// delete
	if ($section['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-remove" aria-hidden="true" action-type="delete" section-id="' . $section['id'] . '"></span></td>';
	}

	// used by
	if ($section['used_by_user_id'] != NULL) {
	  if ($section['used_by_user_id'] == $this->session->userdata('user')['id']) {
	    $tr[] = '<td><a href="/admin/users/' . $section['used_by_user_id'] . '">You</a></td>';
	  } else {
	    $tr[] = '<td><a class="glyphicon glyphicon-user" aria-hidden="true" href="/admin/users/' . $section['used_by_user_id'] . '"></a></td>';
	  }
	} else {
	  $tr[] = "<td></td>";
	}

	echo join('', $tr);
	?>
      </tr>
      <?php
      $row_counter++;
    endforeach;
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">Sections : <span id="total_records_counter"><?= $num_of_sections ?></span></td>
    </tr>
  </tfoot>
</table>

<?php echo $this->pagination->create_links(); ?>

<p class="text-right small">
  <a href="/admin/create-new-blog-section">Создать раздел</a> | 
  <a href="/admin/create-new-blog-article">Создать статью</a> | 
  <a href="<?= current_url() ?>">Разделы</a> | 
  <a href="/admin/blog-articles">Все статьи</a> | 
  <a href="<?= "/" . filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить</a>
</p>

<div id="dbg"></div>

<script>
  document.body.addEventListener('click', clickActionListener, false);
  function clickActionListener(e) {
    if (e.target.hasAttribute('section-id')) {
      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'delete') {
	delete_section_by_id(e.target);
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'published') {
	toggle_published_item_by_id(e.target);
      }
      
      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'liftup') {
	lift_up(e.target)
      }
    }
  }


</script>