<?php
//echo "Type of update : " . $this->input->post('submit') . "<br>Modified fields: " . $this->input->post('modified_fields');
//echo "UPDATING…<br>";
//echo $this->input->post('submit<br>');
//echo $this->input->post('modified_fields');
?>

<!-- page nav -->
<p class="text-right small">
  <a href="<?= $this->back_url->get_parent_url() ?>">Вернуться</a>
  | <a href="/admin/blog-section-preview/<?= $item['id'] ?>">Просмотр</a>
  | <a href="/admin/blog-section-preview/<?= $item['id'] ?>" target="_blank">Просмотр в новой вкладке</a>
</p>
<!-- / page nav -->

<h4>Редактирование раздела:</h4>
<p class="small text-muted">Внимание! Будте внимательны создавая, редактируя и сохраняя контент! Неосознанные действия могут привести к потере данных, а также выходу системы из строя.</p>
<h1><?= $title ?></h1>



<!-- new image upload via iframe -->

<?php
if ($item['img'] == NULL OR $item['img'] == '') {
  $img_src = "/" . config_item('_blog_not_specified_image_url');
} else if (!file_exists(config_item('_blog_section_image_path') . $item['img'])) {
  $img_src = "/" . config_item('_blog_missing_image_url');
} else {
  $img_src = "/" . config_item('_blog_section_image_path_url') . $item['img'];
}
?>

<p class="admin-input-title">Вводное изображение раздела</p>
<img id="uploaded_image_tag" class="thumbnail" src="<?= $img_src ?>" alt="<?= $title ?>">
<form action="/admin/upload-image" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="new_file_form_submitted(event)">
  <input type="hidden" name="id" value="<?= $item['id'] ?>">
  <input type="hidden" name="table" value="sections">
  <input id="img_tag" class="hidden" type="file" name="img" onchange="file_input_changed(event)">
  <div>
    <p>
      <label for="img_tag" class="btn btn-sm btn-default">Изменить изображение</label>
      <button type="submit" name="submit" id="file_submit_btn" class="hidden btn btn-sm btn-primary">Загрузить</button>
      <strong id="img_filename_holder" class="small"></strong>
    </p>
  </div>
</form>
<p id="img_filename_loading" class="small text-primary hidden">Loading…</p>
<iframe class="hidden" name="upload_target" src="/admin/empty-iframe"></iframe>
<p class="small text-info">Внимание! Загружаемые файлы должны являться только изображениями. Загрузка других типов файлов недопустима. Размер Файла должен быть не более <?php echo round(config_item('_blog_section_image_upload_config')['max_size'] / 1024) . 'Mb' ?>.</p>

<script>
  function new_file_form_submitted(e) {
    $('#file_submit_btn').addClass('hidden');
    $('#img_filename_holder').html('');
    $('#img_filename_loading').removeClass('hidden');
  }
  function file_input_changed(e) {
    var fakefilepath = e.target.value;
    var ffp_arr = fakefilepath.split('\\');
    var filename = ffp_arr[ffp_arr.length - 1];
    $('#img_filename_holder').html(filename);
    if (e.target.value !== '') {
      $('#file_submit_btn').removeClass('hidden');
    } else {
      $('#file_submit_btn').addClass('hidden');
    }
  }
</script>





<?php
$form_attributes = array('id' => 'blog_section_edit_form');
echo form_open_multipart('', $form_attributes);
?>

<!-- title -->
<p class="admin-input-title text-primary">Заголовок *</p>
<input id="section_title_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="title" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['title'])) ? set_value('title') : $item['title'];
?>" onchange="formChangeEvent(window.event)">
       <?php echo form_error('title') ?>



<!-- slug -->
<p class="admin-input-title text-primary">Ярлык URL *</p>
<input id="section_slug_input" onblur="to_slug(event)" class="form-control admin-input" type="text" name="slug" maxlength="64" autocomplete="off" value="<?php
echo (!isset($item['slug'])) ? set_value('slug') : $item['slug'];
?>" onchange="formChangeEvent(window.event)">
       <?php echo form_error('slug') ?>



<!-- published checkbox -->
<?php
$pub_val = !isset($item['published']) ? set_value('published') : $item['published'];
$pbl_chk = ($pub_val == 1 OR $pub_val == 'on') ? ' checked' : '';
//echo "<h1>" . $pub_val . "</h1>";
?>
<div class="admin-input-title">
  <div class="checkbox">
    <label for="published_chkbx">
      <input type="checkbox" id="published_chkbx" name="published" <?= $pbl_chk ?> onchange="formChangeEvent(window.event)"> Публиковать
    </label>
  </div>
  <?php echo form_error('published') ?>
</div>


<!-- intro -->
<p class="admin-input-title">Вводный текст</p>
<textarea class="form-control admin-textarea" onblur="trim_input_field(event)" name="intro" onchange="formChangeEvent(window.event)"><?php
  echo (!isset($item['intro'])) ? set_value('intro') : $item['intro'];
  ?></textarea>


<?php
if (isset($this->session->userdata('user')['is_super_admin'])) {
  ?>
  <!-- before_body -->
  <p class="admin-input-title text-muted">Before Content : Extended HTML with JS</p>
  <textarea name="before_body"><?php
    echo (!isset($item['before_body'])) ? set_value('before_body') : $item['before_body'];
    ?></textarea><?php
}
?>

<!-- section_body -->
<p class="admin-input-title">Контент</p>
<textarea name="body"><?php
  echo (!isset($item['body'])) ? set_value('body') : $item['body'];
  ?></textarea>


<?php
if (isset($this->session->userdata('user')['is_super_admin'])) {
  ?>
  <!-- after_body -->
  <p class="admin-input-title text-muted">After Content : Extended HTML with JS</p>
  <textarea name="after_body"><?php
    echo (!isset($item['after_body'])) ? set_value('after_body') : $item['after_body'];
    ?></textarea>
  <?php
}
?>

<!-- meta title -->
<p class="admin-input-title">Title tag</p>
<input id="section_metatitle_input" onblur="trim_input_field(event)" onchange="formChangeEvent(window.event)" class="form-control admin-input" type="text" name="meta_title" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['meta_title'])) ? set_value('meta_title') : $item['meta_title'];
?>">


<!-- meta description -->
<p class="admin-input-title">Description meta tag</p>
<input class="form-control admin-input" onblur="trim_input_field(event)" onchange="formChangeEvent(window.event)" type="text" name="meta_description" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['meta_description'])) ? set_value('meta_description') : $item['meta_description'];
?>">


<!-- meta keywords -->
<p class="admin-input-title">Keywords meta tag</p>
<input class="form-control admin-input" onblur="trim_input_field(event)" onchange="formChangeEvent(window.event)" type="text" name="meta_keywords" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['meta_keywords'])) ? set_value('meta_keywords') : $item['meta_keywords'];
?>">


<!-- meta robots select -->
<p class="admin-input-title">Robots meta tag</p>
<select class="form-control admin-input admin-max-width-normal" name="meta_robots" onchange="formChangeEvent(window.event)">
  <?php
  $rbts = (!isset($item['meta_robots'])) ? set_value('meta_robots') : $item['meta_robots'];
  Baza::create_options('meta_robots', Baza::$meta_robots_options_array, $rbts);
  ?>
</select>
<?php echo form_error('meta_robots') ?>






<!-- submit -->
<div class="admin-input-title">
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save') ?>">Сохранить</button>
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save_and_close') ?>">Сохранить и закрыть</button>
</div>

<input id="modified_fields_input" type="hidden" name="modified_fields" value="<?php echo isset($modified_fields) ? $modified_fields : ''; ?>">

</form>

<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);">Перезагрузить</button>
</div>
























<!-- page nav -->
<p class="text-right small">
  <a href="<?= $this->back_url->get_parent_url() ?>">Вернуться</a>
  | <a href="/admin/blog-section-preview/<?= $item['id']; ?>">Просмотр</a>
  | <a href="/admin/blog-section-preview/<?= $item['id'] ?>" target="_blank">Просмотр в новой вкладке</a>
</p>
<!-- / page nav -->



<script src="/assets/js/ckeditor_4.6.1_full/ckeditor/ckeditor.js"></script>

<script>
    // array of input field names that was changed by user (also values that was changed got from server side in hidden field)
    var formChangedInputs = [] = document.getElementById('modified_fields_input').value.split(',');
    formChangedInputs = array_remove_empty_values(formChangedInputs);
    //console.log(formChangedInputs);
</script>

<?php
if (isset($this->session->userdata('user')['is_super_admin'])) {
  ?>
  <script>
    var ck_before_body = CKEDITOR.replace('before_body', {
      customConfig: 'config_superadmin.js'
    });
    var ck_after_body = CKEDITOR.replace('after_body', {
      customConfig: 'config_superadmin.js'
    });

    ck_before_body.on('change', function (evt) {
      formChangedInputs.push(evt.editor.name);
    });

    ck_after_body.on('change', function (evt) {
      formChangedInputs.push(evt.editor.name);
    });

  </script>
  <?php
}
?>

<script>


  var ck_body = CKEDITOR.replace('body', {
    customConfig: 'config_admin.js'
  });

  // checking CKEDITOR for changes
  ck_body.on('change', function (evt) {
    // getData() returns CKEditor's HTML content.
    formChangedInputs.push(evt.editor.name);
  });

  // fires if form inputs changes (CKEditor event is checking too but separately)
  function formChangeEvent(e) {
    formChangedInputs.push(e.target.name);
  }

  // on form submit
  document.getElementById('blog_section_edit_form').onsubmit = function (e) {
    var modifiedInputs = array_remove_duplicates(formChangedInputs);
    formChangedInputs = [];
    if (modifiedInputs !== false) {
      document.getElementById('modified_fields_input').value = modifiedInputs.join(',');
    }
  };


</script>