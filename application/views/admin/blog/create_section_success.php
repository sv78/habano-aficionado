<h1 class="text-success">Готово!</h1>
<h4>Вы создали раздел блога.</h4>
<div class="admin-input-title">
  <p><button class="btn btn-sm" onclick="location.assign(location.href);">Создать новый</button></p>
  <p class="text-left"><a href="/admin/blog-sections">К разделам</a></p>
</div>