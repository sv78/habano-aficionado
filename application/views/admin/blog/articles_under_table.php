<?php echo $this->pagination->create_links(); ?>

<p class="text-right small">
  <a href="/admin/create-new-blog-article">Создать статью</a> | 
  <a href="/admin/blog-sections">Разделы</a> | 
  <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить</a>
</p>
