<h3>Blog section preview mode</h3>
<hr>
<div class="small">
  <p>Meta title : <?= $meta_title ?></p>
  <p>Meta description : <?= $meta_description ?></p>
  <p>Meta keywords : <?= $meta_keywords ?></p>
  <p>Meta robots : <?= $meta_robots ?></p>
  <p>Meta author : <?= $meta_author ?></p>
</div>
<hr>
<h4>Section list preview</h4>
<p>Section title : <?= $title ?></p>
<p>Intro text : <?= $intro ?></p>
<p>Intro image : <?= $img ?></p>
<hr>
<h4>Section content preview</h4>

<div class="blog-article">
  <?php
  $view = array();
  $view[] = $before_body;
  $view[] = $body;
  $view[] = $after_body;
  echo join('', $view);
  ?>
</div>