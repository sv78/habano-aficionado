<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Баннеры</h1>

<p class="text-right small">
  <a href="/admin/create-banner">Создать баннер</a>
   | <a href="/admin/banners">Баннеры</a>
   | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить страницу</a>
</p>

<table class="table table-responsive table-hover" id="items_table">
  <thead>
    <tr>
      <th title="Номер">#</th>
      <th title="Статус : опубликовано или нет">Pub</th>
      <th title="Поднять вверх">Up</th>
      <th title="Изображение баннера">Img</th>
      <th title="Название баннера" class="admin-th-slug">Name Banner</th>
      <th title="Alt Text Attribute" class="admin-th-title">Alt Text</th>
      <th title="Ссылка назначения">Lnk</th>
      <th title="Редактировать баннер">Edit</th>
      <th title="Удалить баннер">Del</th>
      <th title="Баннер в процессе редактирования пользователем или не закрыт им">By</th>
    </tr>
  </thead>  
  <tbody>
    <?php
    foreach ($banners as $banner) :
      $tr = [];
      ?>
      <tr>
	<?php
	$tr[] = '<td>' . $row_counter . '</td>';



	// published by
	if ($banner['used_by_user_id'] != NULL) {
	  $published = $banner['published'] ? '<span class="text-muted glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-lock" aria-hidden="true"></span>';
	} else {
	  $published = $banner['published'] ? '<span class="cursor-pointer glyphicon glyphicon-ok" aria-hidden="true" action-type="published" banner-id="' . $banner['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-lock" aria-hidden="true" action-type="published" banner-id="' . $banner['id'] . '"></span>';
	}
	$tr[] = '<td>' . $published . '</td>';





	// lift up
	if ($banner['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-upload" aria-hidden="true" action-type="liftup" banner-id="' . $banner['id'] . '"></span></td>';
	}



	// image
	if ($banner['img_src'] == '') {
	  $banner_img = "/" . config_item('_banner_not_specified_image_url');
	} else if (!file_exists(config_item('_assets_image_path') . $banner['img_src'])) {
	  $banner_img = "/" . config_item('_banner_missing_image_url');
	} else {
	  $banner_img =  "/" . config_item('_assets_image_path_url') . $banner['img_src'];
	}
	$tr[] = '<td><img src="' . $banner_img . '" class="cursor-help blog-img-thumb" alt="thumb"></td>';

	
	

	// name
	$tr[] = '<td>';
	$tr[] = '<a href="/admin/banner-preview/' . $banner['id'] . '">' . $banner['name'] . '</a>';
	$tr[] = '</td>';


	// alt text
	$tr[] = '<td>' . $banner['alt_text'] . '</td>';



	// url
	$tr[] = '<td><a class="glyphicon glyphicon-link" aria-hidden="true" href="' . $banner['url'] . '" target="_blank"></a></td>';
	
	

	// edit
	if ($banner['used_by_user_id'] != NULL) {
	  if (isset($this->session->userdata('user')['is_super_admin']) OR $this->session->userdata('user')['id'] == $banner['used_by_user_id']) {
	    $tr[] = '<td><a class="glyphicon glyphicon-floppy-remove" aria-hidden="true" href="/admin/banner-edit/' . $banner['id'] . '"></a></td>';
	  } else {
	    $tr[] = '<td></td>';
	  }
	} else {
	  $tr[] = '<td><a class="glyphicon glyphicon-pencil" aria-hidden="true" href="/admin/banner-edit/' . $banner['id'] . '"></a></td>';
	}



	// delete
	if ($banner['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-remove" aria-hidden="true" action-type="delete" banner-id="' . $banner['id'] . '"></span></td>';
	}

	// used by
	if ($banner['used_by_user_id'] != NULL) {
	  if ($banner['used_by_user_id'] == $this->session->userdata('user')['id']) {
	    $tr[] = '<td><a href="/admin/users/' . $banner['used_by_user_id'] . '">You</a></td>';
	  } else {
	    $tr[] = '<td><a class="glyphicon glyphicon-user" aria-hidden="true" href="/admin/users/' . $banner['used_by_user_id'] . '"></a></td>';
	  }
	} else {
	  $tr[] = "<td></td>";
	}

	echo join('', $tr);
	?>
      </tr>
      <?php
      $row_counter++;
    endforeach;
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">Баннеров : <span id="total_records_counter"><?= $num_of_banners ?></span></td>
    </tr>
  </tfoot>
</table>

<?php echo $this->pagination->create_links(); ?>



<p class="text-right small">
  <a href="/admin/create-banner">Создать баннер</a>
   | <a href="/admin/banners">Баннеры</a>
   | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить страницу</a>
</p>





<script>
  document.body.addEventListener('click', clickActionListener, false);
  function clickActionListener(e) {
    e = (!e) ? window.event : e;
    if (e.target.hasAttribute('banner-id')) {

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'delete') {
	delete_banner_by_id(e.target);
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'published') {
	toggle_published_item_by_id(e.target)
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'liftup') {
	lift_up(e.target)
      }

    }
  }

  window.onload = function () {
    if (document.getElementById('items_table')) {
      document.getElementById('items_table').addEventListener('mouseover', tableMouseOverListener, false);
    }
  }

</script>