<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//echo "Type of update : " . $this->input->post('submit') . "<br>Modified fields: " . $this->input->post('modified_fields');
?>

<!-- page nav -->
<p class="text-right small">
  <a href="<?= $this->back_url->get_parent_url() ?>">Вернуться</a>
  | <a href="<?= '/admin/banner-preview/' . $item['id'] ?>">Просмотр</a>
  | <a href="<?= '/admin/banner-preview/' . $item['id'] ?>" target="_blank">Просмотр в новой вкладке</a>
</p>
<!-- / page nav -->

<h4>Редактирование баннера <?= $name ?></h4>
<p class="small text-muted">Внимание! Будте внимательны создавая, редактируя и сохраняя контент! Неосознанные действия могут привести к потере данных, а также выходу системы из строя.</p>
<h1><?= $title ?></h1>





<?php
$form_attributes = array('id' => 'banner_edit_form');
echo form_open_multipart('', $form_attributes);
?>




<?php
if ($img_src == NULL OR $img_src == '') {
  $banner_image_src = "/" . config_item('_banner_not_specified_image_url');
} else if (!file_exists(config_item('_assets_image_path') . $img_src)) {
  $banner_image_src = "/" . config_item('_banner_missing_image_url');
} else {
  $banner_image_src = "/" . config_item('_assets_image_path_url') . $img_src;
}
?>

<div>
  <img class="img-responsive" src="<?= $banner_image_src ?>" alt="<?= $alt_text ?>">
</div>


<!-- destination url link -->
<p class="admin-input-title text-primary">Cсылка баннера * <span class="small text-muted">(т.е. куда ведет ссылка баннера. Абсолютная, если переход на внешний ресурс и относительная, если на данный)</span></p>
<input id="banner_url_input" onblur="prepare_custom_url(event)" class="form-control admin-input" type="text" name="url" maxlength="1024" autocomplete="off" value="<?php
echo (!isset($item['url'])) ? set_value('url') : $item['url'];
?>" onchange="formChangeEvent(window.event)">
       <?php echo form_error('url') ?>



<!-- new tab checkbox -->
<?php
$nt_value = !isset($item['newtab']) ? set_value('newtab') : $item['newtab'];
$nt_checked = ($nt_value == 1 OR $nt_value == 'on') ? ' checked' : '';
?>
<div class="admin-input-title">
  <div class="checkbox">
    <label for="newtab_chkbx">
      <input type="checkbox" id="newtab_chkbx" name="newtab" <?= $nt_checked ?> onchange="formChangeEvent(window.event)"> Открывать в новой вкладке
    </label>
  </div>
  <?php echo form_error('newtab') ?>
</div>


<!-- image src (relative url without 1-st slash) -->
<p class="admin-input-title text-primary">Cсылка на изображение * <span class="small text-muted">(относительный путь от корня сайта и папки /assets/ без первого слеша. Пример: uploads/images/folder1/folder2/file.jpg)</span></p>
<input id="banner_img_src_input" onblur="prepare_relative_uri(event)" class="form-control admin-input" type="text" name="img_src" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['img_src'])) ? set_value('img_src') : $item['img_src'];
?>" onchange="formChangeEvent(window.event)">
       <?php echo form_error('img_src') ?>



<!-- name -->
<p class="admin-input-title text-primary">Имя (название) баннера *</p>
<input id="banner_name_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="name" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['name'])) ? set_value('name') : $item['name'];
?>" onchange="formChangeEvent(window.event)">
       <?php echo form_error('name') ?>






<!-- alt text -->
<p class="admin-input-title text-primary">Альтернативный текст *</p>
<input id="banner_alt_text_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="alt_text" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['alt_text'])) ? set_value('alt_text') : $item['alt_text'];
?>" onchange="formChangeEvent(window.event)">
       <?php echo form_error('alt_text') ?>




<!-- published checkbox -->
<?php
$pbl_value = !isset($item['published']) ? set_value('published') : $item['published'];
$pbl_checked = ($pbl_value == 1 OR $pbl_value == 'on') ? ' checked' : '';
?>
<div class="admin-input-title">
  <div class="checkbox">
    <label for="published_chkbx">
      <input type="checkbox" id="published_chkbx" name="published" <?= $pbl_checked ?> onchange="formChangeEvent(window.event)"> Публиковать
    </label>
  </div>
  <?php echo form_error('published') ?>
</div>







<!-- submit -->
<div class="admin-input-title">
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save') ?>">Сохранить</button>
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save_and_close') ?>">Сохранить и закрыть</button>
</div>

<input id="modified_fields_input" type="hidden" name="modified_fields" value="<?php echo isset($modified_fields) ? $modified_fields : ''; ?>">

</form>

<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);">Перезагрузить</button>
</div>









<script>
  // array of input field names that was changed by user (also values that was changed got from server side in hidden field)
  var formChangedInputs = [] = document.getElementById('modified_fields_input').value.split(',');
  formChangedInputs = array_remove_empty_values(formChangedInputs);
  //console.log(formChangedInputs);


  // fires if form inputs changes (CKEditor event is checking too but separately)
  function formChangeEvent(e) {
    formChangedInputs.push(e.target.name);
  }

  // on form submit
  document.getElementById('banner_edit_form').onsubmit = function (e) {
    var modifiedInputs = array_remove_duplicates(formChangedInputs);
    formChangedInputs = [];
    if (modifiedInputs !== false) {
      document.getElementById('modified_fields_input').value = modifiedInputs.join(',');
    }
  };



</script>