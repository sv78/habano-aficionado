<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Создать баннер</h1>


<?php
$form_attributes = array('id' => 'banner_create_form');
echo form_open_multipart('', $form_attributes);
?>



<p class="admin-input-title text-primary">Имя (название) баннера *</p>
<input id="banner_name_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="name" maxlength="255" autocomplete="off" value="<?php echo set_value('name'); ?>">
<?php echo form_error('name') ?>




<div class="admin-input-title">
  <button class="btn btn-success" type="submit" value="submit">Сохранить</button>
</div>

</form>

<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);">Перезагрузить</button>
</div>
