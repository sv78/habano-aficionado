<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h1>
  <?php echo (isset($this->session->userdata('user')['is_super_admin'])) ? 'Super Administrator' : 'Administrator'; ?>
</h1>
<p><a href="/">Домашняя страница</a></p>
<hr>
<div class="row">

  <div class="col-lg-4">
    <p><a href="/admin/blog-sections">Разделы блога</a></p>
    <p><a href="/admin/blog-articles">Статьи блога</a></p>
    <p><a href="/admin/brands">Бренды</a></p>
    <p><a href="/admin/categories">Категории товаров</a></p>
    <p><a href="/admin/products">Товары</a></p>
    <p><a href="/admin/packs">Упаковки</a></p>
    <p><a href="/admin/banners">Баннеры</a></p>
  </div>

  <div class="col-lg-4">
    <p><a href="/admin/create-new-blog-section">+ Создать раздел блога</a></p>
    <p><a href="/admin/create-new-blog-article">+ Создать статью блога</a></p>
    <p><a href="/admin/create-brand">+ Создать бренд</a></p>
    <p><a href="/admin/create-category">+ Создать категорию товаров</a></p>
    <p><a href="/admin/create-product">+ Создать товар</a></p>
    <p><a href="/admin/create-branner">+ Создать баннер</a></p>
  </div>

  <div class="col-lg-4">
    <p><a href="/admin/orders">Заявки</a></p>
    <p><a href="/admin/users">Пользователи</a></p>
    <p><a href="/admin/file-manager">Менеджер файлов</a></p>
    <p><a href="<?= '/admin/users/' . $this->session->userdata('user')['id']; ?>">Вы</a></p>
  </div>



</div>
