<h3>Бренд : предварительный просмотр</h3>
<div class="small">
  <p>Название : <?= $name ?></p>
  <p>Ярлык URL : <?= $slug ?></p>
  <p>Страна происхождения : <?= $country ?></p>
  <p>Сигарный бренд : <?= $is_cigar_brand == 1 ? 'Yes' : 'No' ?></p>
  <p>Meta title : <?= $meta_title ?></p>
  <p>Meta description : <?= $meta_description ?></p>
  <p>Meta keywords : <?= $meta_keywords ?></p>
  <p>Meta robots : <?= $meta_robots ?></p>
</div>
<hr>

<h4>Бренды в списке : предварительный просмотр</h4>
<p>Название : <?= $name ?></p>
<p>Вводный чистый текст : <?= $intro ?></p>
<p>Вводное изображение : <?= $img ?></p>
<hr>
<h4>Содержимое : предварительный просмотр</h4>

<div class="blog-article">
  <?= $html; ?>
</div>