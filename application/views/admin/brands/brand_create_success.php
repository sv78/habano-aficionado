<h1 class="text-success">Получилось!</h1>
<h4>Вы создали бренд!</h4>
<div class="admin-input-title">
  <p><button class="btn btn-sm" onclick="location.assign(location.href);">Создать еще бренд</button></p>
  <p class="text-left"><a href="/admin/brands">Перейти к списку брендов</a></p>
</div>