<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Бренды</h1>


<table class="table table-responsive table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th>Pub</th>
      <th>Cgr</th>
      <th>Up</th>
      <th>Img</th>
      <th class="admin-th-title">Brand name</th>
      <th class="admin-th-slug">Slug</th>
      <th>Edit</th>
      <th>Del</th>
      <th>By</th>
    </tr>
  </thead>  
  <tbody>
    <?php
    foreach ($brands as $brand) :
      $tr = [];
      ?>
      <tr>
	<?php
	$tr[] = '<td>' . $row_counter . '</td>';


	// published by
	if ($brand['used_by_user_id'] != NULL) {
	  $published = $brand['published'] ? '<span class="text-muted glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-lock" aria-hidden="true"></span>';
	} else {
	  $published = $brand['published'] ? '<span class="cursor-pointer glyphicon glyphicon-ok" aria-hidden="true" action-type="published" brand-id="' . $brand['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-lock" aria-hidden="true" action-type="published" brand-id="' . $brand['id'] . '"></span>';
	}
	$tr[] = '<td>' . $published . '</td>';


	// is cigar brand
	$icb = $brand['is_cigar_brand'] ? '<span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span>' : '';
	$tr[] = '<td>' . $icb . '</td>';



	// lift up
	if ($brand['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-upload" aria-hidden="true" action-type="liftup" brand-id="' . $brand['id'] . '"></span></td>';
	}



	// image
	if ($brand['img'] == '') {
	  $brand_img = "/" . config_item('_brand_not_specified_image_url');
	} else if (!file_exists(config_item('_brands_image_path') . $brand['img'])) {
	  $brand_img = "/" . config_item('_brand_missing_image_url');
	} else {
	  $brand_img = "/" . config_item('_brands_image_path_url') . $brand['img'];
	}

	$tr[] = '<td><img title="' . $brand['intro'] . '" src="' . $brand_img . '" class="cursor-help blog-img-thumb" alt="thumb"></td>';


	// name
	$tip_arr = [];
	//$tip_arr[] = 'Cоздана: ' . $brand['created_at'];
	//$tip_arr[] = ' | Последняя редакция: ' . $brand['modified_at'];
	if ($brand['lifted_up_at'] != NULL) {
	  $tip_arr[] = 'Поднята: ' . date('d M Y', $brand['lifted_up_at']) . " в " . date('H:i:s', $brand['lifted_up_at']);
	}
	$tip = join('', $tip_arr);
	unset($tip_arr);
	$tr[] = '<td title="' . $tip . '"><a href="/admin/brand-preview/' . $brand['id'] . '">' . Baza::decode_plain_string_from_db($brand['name']) . '</a></td>';

	// slug
	$tr[] = '<td>' . $brand['slug'] . '</td>';



	// edit
	if ($brand['used_by_user_id'] != NULL) {
	  if (isset($this->session->userdata('user')['is_super_admin']) OR $this->session->userdata('user')['id'] == $brand['used_by_user_id']) {
	    $tr[] = '<td><a class="glyphicon glyphicon-floppy-remove" aria-hidden="true" href="/admin/brand-edit/' . $brand['id'] . '"></a></td>';
	  } else {
	    $tr[] = '<td></td>';
	  }
	} else {
	  $tr[] = '<td><a class="glyphicon glyphicon-pencil" aria-hidden="true" href="/admin/brand-edit/' . $brand['id'] . '"></a></td>';
	}



	// delete
	if ($brand['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-remove" aria-hidden="true" action-type="delete" brand-id="' . $brand['id'] . '"></span></td>';
	}

	// used by
	if ($brand['used_by_user_id'] != NULL) {
	  if ($brand['used_by_user_id'] == $this->session->userdata('user')['id']) {
	    $tr[] = '<td><a href="/admin/users/' . $brand['used_by_user_id'] . '">You</a></td>';
	  } else {
	    $tr[] = '<td><a class="glyphicon glyphicon-user" aria-hidden="true" href="/admin/users/' . $brand['used_by_user_id'] . '"></a></td>';
	  }
	} else {
	  $tr[] = "<td></td>";
	}

	echo join('', $tr);
	?>
      </tr>
      <?php
      $row_counter++;
    endforeach;
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">Брендов : <span id="total_records_counter"><?= $num_of_brands ?></span></td>
    </tr>
  </tfoot>
</table>

<?php echo $this->pagination->create_links(); ?>

<p class="text-right small">
  <a href="/admin/create-brand">Создать новый бренд</a>
  | <a href="<?= current_url() ?>">Бренды</a>
  | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI') ?>">Обновить страницу</a>
</p>

<script>
  document.body.addEventListener('click', clickActionListener, false);
  function clickActionListener(e) {
    if (e.target.hasAttribute('brand-id')) {
      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'delete') {
	delete_brand_by_id(e.target);
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'published') {
	toggle_published_item_by_id(e.target)
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'liftup') {
	lift_up(e.target)
      }

    }
  }

</script>