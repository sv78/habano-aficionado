<h1>Менеджер файлов</h1>
<p>Используйте данный инструмент для загрузки/выгрузки файлов для данного сайта.</p>
<p class="small">БУДТЕ ВНИМАТЕЛЬНЫ! Во избежание проблем следите чтобы вредоносноное ПО (вирус, трояны и пр.) не попали на сервер!</p>

<!-- Require JS (REQUIRED) -->
<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
<script data-main="/filemanager/main.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>
<!-- Element where elFinder will be created (REQUIRED) -->
<div id="elfinder" style="min-width: 100%; max-width: 100%;"></div>