<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//echo "Type of update : " . $this->input->post('submit') . "<br>Modified fields: " . $this->input->post('modified_fields');
?>

<!-- page nav -->
<p class="text-right small">
  <a href="<?= $this->back_url->get_parent_url() ?>">Вернуться</a>
  | <a href="/admin/category-preview/<?= $item['id'] ?>">Просмотр</a>
  | <a href="/admin/category-preview/<?= $item['id'] ?>" target="_blank">Просмотр в новой вкладке</a>
</p>
<!-- / page nav -->

<h4>Редактирование категории товаров:</h4>
<p class="small text-muted">Внимание! Будте внимательны создавая, редактируя и сохраняя контент! Неосознанные действия могут привести к потере данных, а также выходу системы из строя.</p>
<h1><?= $title ?></h1>



<!-- new image upload via iframe -->

<?php
if ($item['img'] == NULL OR $item['img'] == '') {
  $img_src = "/" . config_item('_category_not_specified_image_url');
} else if (!file_exists(config_item('_categories_image_path') . $item['img'])) {
  $img_src = "/" . config_item('_category_missing_image_url');
} else {
  $img_src = "/" . config_item('_categories_image_path_url') . $item['img'];
}
?>

<p class="admin-input-title">Изображение категории товаров:</p>
<img id="uploaded_image_tag" class="thumbnail" src="<?= $img_src ?>" alt="<?= $title ?>">
<form action="/admin/upload-image" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="new_file_form_submitted(event)">
  <input type="hidden" name="id" value="<?= $item['id'] ?>">
  <input type="hidden" name="table" value="categories">
  <input id="img_tag" class="hidden" type="file" name="img" onchange="file_input_changed(event)">
  <div>
    <p>
      <label for="img_tag" class="btn btn-sm btn-default">Изменить изображение</label>
      <button type="submit" name="submit" id="file_submit_btn" class="hidden btn btn-sm btn-primary">Загрузить</button>
      <strong id="img_filename_holder" class="small"></strong>
    </p>
  </div>
</form>
<p id="img_filename_loading" class="small text-primary hidden">Loading…</p>
<iframe class="hidden" name="upload_target" src="/admin/empty-iframe"></iframe>
<p class="small text-info">Внимание! Загружаемые файлы должны являться только изображениями. Загрузка других типов файлов недопустима. Размер Файла должен быть не более <?php echo round(config_item('_category_image_upload_config')['max_size'] / 1024) . 'Mb' ?>.</p>

<script>
  function new_file_form_submitted(e) {
    $('#file_submit_btn').addClass('hidden');
    $('#img_filename_holder').html('');
    $('#img_filename_loading').removeClass('hidden');
  }
  function file_input_changed(e) {
    var fakefilepath = e.target.value;
    var ffp_arr = fakefilepath.split('\\');
    var filename = ffp_arr[ffp_arr.length - 1];
    $('#img_filename_holder').html(filename);
    if (e.target.value !== '') {
      $('#file_submit_btn').removeClass('hidden');
    } else {
      $('#file_submit_btn').addClass('hidden');
    }
  }
</script>





<?php
$form_attributes = array('id' => 'category_edit_form');
echo form_open_multipart('', $form_attributes);
?>





<!-- name -->
<p class="admin-input-title text-primary">Имя (название) категории товаров *</p>
<input id="category_name_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="name" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['name'])) ? set_value('name') : $item['name'];
?>" onchange="formChangeEvent(window.event)">
       <?php echo form_error('name') ?>








<!-- slug -->
<p class="admin-input-title text-primary">Ярлык URL *</p>
<input id="category_slug_input" onblur="to_slug(event)" class="form-control admin-input" type="text" name="slug" maxlength="64" autocomplete="off" value="<?php
echo (!isset($item['slug'])) ? set_value('slug') : $item['slug'];
?>" onchange="formChangeEvent(window.event)">
       <?php echo form_error('slug') ?>






<!-- published checkbox -->
<?php
$pbl_value = !isset($item['published']) ? set_value('published') : $item['published'];
$pbl_checked = ($pbl_value == 1 OR $pbl_value == 'on') ? ' checked' : '';
?>
<div class="admin-input-title">
  <div class="checkbox">
    <label for="published_chkbx">
      <input type="checkbox" id="published_chkbx" name="published" <?= $pbl_checked ?> onchange="formChangeEvent(window.event)"> Публиковать
    </label>
  </div>
  <?php echo form_error('published') ?>
</div>



<!-- is cigar category checkbox -->
<?php
$icb_value = !isset($item['is_cigar_category']) ? set_value('is_cigar_category') : $item['is_cigar_category'];
$icb_checked = ($icb_value == 1 OR $icb_value == 'on') ? ' checked' : '';
?>
<div class="admin-input-title">
  <div class="checkbox">
    <label for="is_cigar_category_chkbx">
      <input type="checkbox" id="is_cigar_category_chkbx" name="is_cigar_category" <?= $icb_checked ?> onchange="formChangeEvent(window.event)"> Сигарная категория
    </label>
  </div>
  <?php echo form_error('is_cigar_category') ?>
</div>





<!-- featured checkbox -->
<?php
$ftr_value = !isset($item['featured']) ? set_value('featured') : $item['featured'];
$ftr_checked = ($ftr_value == 1 OR $ftr_value == 'on') ? ' checked' : '';
?>
<div class="admin-input-title">
  <div class="checkbox">
    <label for="featured_chkbx">
      <input type="checkbox" id="featured_chkbx" name="featured" <?= $ftr_checked ?> onchange="formChangeEvent(window.event)"> Featured
    </label>
  </div>
  <?php echo form_error('featured') ?>
</div>





<!-- intro -->
<p class="admin-input-title">Вводный текст</p>
<textarea class="form-control admin-textarea" onblur="trim_input_field(event)" name="intro" onchange="formChangeEvent(window.event)"><?php
  echo (!isset($item['intro'])) ? set_value('intro') : $item['intro'];
  ?></textarea>


<!-- html -->
<p class="admin-input-title">Контент</p>
<textarea name="html"><?php
  echo (!isset($item['html'])) ? set_value('html') : $item['html'];
  ?></textarea>



<!-- meta title -->
<p class="admin-input-title">Title tag</p>
<input id="category_metatitle_input" onblur="trim_input_field(event)" onchange="formChangeEvent(window.event)" class="form-control admin-input" type="text" name="meta_title" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['meta_title'])) ? set_value('meta_title') : $item['meta_title'];
?>">


<!-- meta description -->
<p class="admin-input-title">Description meta tag</p>
<input class="form-control admin-input" onblur="trim_input_field(event)" onchange="formChangeEvent(window.event)" type="text" name="meta_description" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['meta_description'])) ? set_value('meta_description') : $item['meta_description'];
?>">


<!-- meta keywords -->
<p class="admin-input-title">Keywords meta tag</p>
<input class="form-control admin-input" onblur="trim_input_field(event)" onchange="formChangeEvent(window.event)" type="text" name="meta_keywords" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['meta_keywords'])) ? set_value('meta_keywords') : $item['meta_keywords'];
?>">





<!-- meta robots select -->
<p class="admin-input-title">Robots meta tag</p>
<select class="form-control admin-input admin-max-width-normal" name="meta_robots" onchange="formChangeEvent(window.event)">
  <?php
  $rbts = (!isset($item['meta_robots'])) ? set_value('meta_robots') : $item['meta_robots'];
  Baza::create_options('meta_robots', Baza::$meta_robots_options_array, $rbts);
  ?>
</select>
<?php echo form_error('meta_robots') ?>





<!-- submit -->
<div class="admin-input-title">
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save') ?>">Сохранить</button>
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save_and_close') ?>">Сохранить и закрыть</button>
</div>

<input id="modified_fields_input" type="hidden" name="modified_fields" value="<?php echo isset($modified_fields) ? $modified_fields : ''; ?>">

</form>

<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);">Перезагрузить</button>
</div>
























<!-- page nav -->
<p class="text-right small">
  <a href="<?= $this->back_url->get_parent_url() ?>">Вернуться</a>
  | <a href="/admin/category-preview/<?= $item['id'] ?>">Просмотр</a>
  | <a href="/admin/category-preview/<?= $item['id'] ?>" target="_blank">Просмотр в новой вкладке</a>
</p>
<!-- / page nav -->



<script src="/assets/js/ckeditor_4.6.1_full/ckeditor/ckeditor.js"></script>

<script>
    // array of input field names that was changed by user (also values that was changed got from server side in hidden field)
    var formChangedInputs = [] = document.getElementById('modified_fields_input').value.split(',');
    formChangedInputs = array_remove_empty_values(formChangedInputs);
    //console.log(formChangedInputs);


  var ck_html = CKEDITOR.replace('html', {
    customConfig: 'config_admin.js'
  });

  // checking CKEDITOR for changes
  ck_html.on('change', function (evt) {
    // getData() returns CKEditor's HTML content.
    formChangedInputs.push(evt.editor.name);
  });

  // fires if form inputs changes (CKEditor event is checking too but separately)
  function formChangeEvent(e) {
    formChangedInputs.push(e.target.name);
  }

  // on form submit
  document.getElementById('category_edit_form').onsubmit = function (e) {
    var modifiedInputs = array_remove_duplicates(formChangedInputs);
    formChangedInputs = [];
    if (modifiedInputs !== false) {
      document.getElementById('modified_fields_input').value = modifiedInputs.join(',');
    }
  };


</script>