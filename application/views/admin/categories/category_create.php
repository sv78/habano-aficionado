<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Создать категорию товаров</h1>

<?php
$form_attributes = array('id' => 'category_create_form');
echo form_open_multipart('', $form_attributes);
?>


<p class="admin-input-title text-primary">Имя (название) категории товаров *</p>
<input id="category_name_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="name" maxlength="255" autocomplete="off" value="<?php echo set_value('name'); ?>">
<?php echo form_error('name') ?>


<p class="admin-input-title text-primary">Ярлык URL *</p>
<input id="slug_input" onblur="to_slug(event)" class="form-control admin-input" type="text" name="slug" maxlength="128" autocomplete="off" value="<?php echo set_value('slug'); ?>">
<?php echo form_error('slug') ?>



<span id="slug_create_btn" class="btn btn-default btn-sm">
  Создать ярлык URL из имени категории
</span>



<div class="admin-input-title">
  <div class="checkbox">
    <label for="is_cigar_category_chkbx">
      <input type="checkbox" id="is_cigar_category_chkbx" name="is_cigar_category" <?php echo set_checkbox('is_cigar_category', 'on', FALSE) ?>> Сигарная категория
    </label>
  </div>
  <?php echo form_error('is_cigar_category') ?>
</div>



<p class="admin-input-title">Title tag</p>
<input id="metatitle_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="meta_title" maxlength="255" autocomplete="off" value="<?php echo set_value('meta_title'); ?>">


<span id="metatitle_create_btn" class="btn btn-default btn-sm">
  Создать title tag из названия категории
</span>


<div class="admin-input-title">
  <button class="btn btn-success" name="submit" type="submit" value="submit">Сохранить</button>
</div>

</form>

<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);">Перезагрузка и сброс данных</button>
</div>


<script>
  // creating slug from title (name)
  document.getElementById('slug_create_btn').onclick = function () {
    create_slug_from_title(document.getElementById('category_name_input'), document.getElementById('slug_input'), 128);
  };

  document.getElementById('metatitle_create_btn').onclick = function () {
    create_metatitle_from_title(document.getElementById('category_name_input'), document.getElementById('metatitle_input'));
  };
</script>