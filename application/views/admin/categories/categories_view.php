<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Категории товаров</h1>

<p class="text-right small">
  <a href="/admin/create-category">Создать новую категорию</a>
  | <a href="<?= current_url() ?>">Категории</a>
  | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить страницу</a>
</p>


<table class="table table-responsive table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th>Pub</th>
      <th>Cgr</th>
      <th>Ftr</th>
      <th>Up</th>
      <th>List</th>
      <th>Img</th>
      <th class="admin-th-title">Category Name</th>
      <th class="admin-th-slug">Slug</th>
      <th>Edit</th>
      <th>Del</th>
      <th>By</th>
    </tr>
  </thead>  
  <tbody>
    <?php
    foreach ($categories as $category) :
      $tr = [];
      ?>
      <tr>
	<?php
	$tr[] = '<td>' . $row_counter . '</td>';


	// published by
	if ($category['used_by_user_id'] != NULL) {
	  $published = $category['published'] ? '<span class="text-muted glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-lock" aria-hidden="true"></span>';
	} else {
	  $published = $category['published'] ? '<span class="cursor-pointer glyphicon glyphicon-ok" aria-hidden="true" action-type="published" category-id="' . $category['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-lock" aria-hidden="true" action-type="published" category-id="' . $category['id'] . '"></span>';
	}
	$tr[] = '<td>' . $published . '</td>';


	// is cigar category
	$icb = $category['is_cigar_category'] ? '<span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span>' : '';
	$tr[] = '<td>' . $icb . '</td>';


	
	
	// featured
	if ($category['used_by_user_id'] != NULL) {
	  $featured = $category['featured'] ? '<span class="text-muted glyphicon glyphicon-star" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-star-empty" aria-hidden="true"></span>';
	} else {
	  $featured = $category['featured'] ? '<span class="cursor-pointer glyphicon glyphicon-star" aria-hidden="true" action-type="featured" category-id="' . $category['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-star-empty" aria-hidden="true" action-type="featured" category-id="' . $category['id'] . '"></span>';
	}
	$tr[] = '<td>' . $featured . '</td>';
	
	

	// lift up
	if ($category['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-upload" aria-hidden="true" action-type="liftup" category-id="' . $category['id'] . '"></span></td>';
	}


	
	
	// list
	$tr[] = '<td><a class="glyphicon glyphicon-list" aria-hidden="true" href="' . current_url() . '/' . $category['id'] . '"></a></td>';
	
	

	// image
	if ($category['img'] == '') {
	  $category_img = "/" . config_item('_category_not_specified_image_url');
	} else if (!file_exists(config_item('_categories_image_path') . $category['img'])) {
	  $category_img = "/" . config_item('_category_missing_image_url');
	} else {
	  $category_img = "/" . config_item('_categories_image_path_url') . $category['img'];
	}

	$tr[] = '<td><img title="' . $category['intro'] . '" src="' . $category_img . '" class="cursor-help blog-img-thumb" alt="thumb"></td>';


	// name
	$tip_arr = [];
	if ($category['lifted_up_at'] != NULL) {
	  $tip_arr[] = 'Поднята: ' . date('d M Y', $category['lifted_up_at']) . " в " . date('H:i:s', $category['lifted_up_at']);
	}
	$tip = join('', $tip_arr);
	unset($tip_arr);
	$tr[] = '<td title="' . $tip . '"><a href="/admin/category-preview/' . $category['id'] . '">' . $category['name'] . '</a></td>';

	// slug
	$tr[] = '<td>' . $category['slug'] . '</td>';



	// edit
	if ($category['used_by_user_id'] != NULL) {
	  if (isset($this->session->userdata('user')['is_super_admin']) OR $this->session->userdata('user')['id'] == $category['used_by_user_id']) {
	    $tr[] = '<td><a class="glyphicon glyphicon-floppy-remove" aria-hidden="true" href="/admin/category-edit/' . $category['id'] . '"></a></td>';
	  } else {
	    $tr[] = '<td></td>';
	  }
	} else {
	  $tr[] = '<td><a class="glyphicon glyphicon-pencil" aria-hidden="true" href="/admin/category-edit/' . $category['id'] . '"></a></td>';
	}



	// delete
	if ($category['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-remove" aria-hidden="true" action-type="delete" category-id="' . $category['id'] . '"></span></td>';
	}

	// used by
	if ($category['used_by_user_id'] != NULL) {
	  if ($category['used_by_user_id'] == $this->session->userdata('user')['id']) {
	    $tr[] = '<td><a href="/admin/users/' . $category['used_by_user_id'] . '">You</a></td>';
	  } else {
	    $tr[] = '<td><a class="glyphicon glyphicon-user" aria-hidden="true" href="/admin/users/' . $category['used_by_user_id'] . '"></a></td>';
	  }
	} else {
	  $tr[] = "<td></td>";
	}

	echo join('', $tr);
	?>
      </tr>
      <?php
      $row_counter++;
    endforeach;
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">Категорий товаров : <span id="total_records_counter"><?= $num_of_categories ?></span></td>
    </tr>
  </tfoot>
</table>

<?php echo $this->pagination->create_links(); ?>

<p class="text-right small">
  <a href="/admin/create-category">Создать новую категорию</a>
  | <a href="<?= current_url() ?>">Категории</a>
  | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить страницу</a>
</p>

<script>
  document.body.addEventListener('click', clickActionListener, false);
  function clickActionListener(e) {
    if (e.target.hasAttribute('category-id')) {
      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'delete') {
	delete_category_by_id(e.target);
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'published') {
	toggle_published_item_by_id(e.target)
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'liftup') {
	lift_up(e.target)
      }
      
      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'featured') {
	toggle_featured(e.target)
      }

    }
  }

</script>