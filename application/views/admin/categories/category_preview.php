<h3>Категория товаров : предварительный просмотр</h3>
<div class="small">
  <p>Название категории : <?= $name ?></p>
  <p>Ярлык URL : <?= $slug ?></p>
  <p>Является сигарной категорией : <?= $is_cigar_category == 1 ? 'Yes' : 'No' ?></p>
  <p>Специально выделенная : <?= $featured == 1 ? 'Yes' : 'No' ?></p>
  <p>Meta title : <?= $meta_title ?></p>
  <p>Meta description : <?= $meta_description ?></p>
  <p>Meta keywords : <?= $meta_keywords ?></p>
  <p>Meta robots : <?= $meta_robots ?></p>
</div>
<hr>

<h4>Категория товаров в списке : предварительный просмотр</h4>
<p>Название : <?= $name ?></p>
<p>Вводный чистый текст : <?= $intro ?></p>
<p>Вводное изображение : <?= $img ?></p>
<hr>
<h4>Содержимое : предварительный просмотр</h4>

<div class="blog-article">
  <?= $html; ?>
</div>