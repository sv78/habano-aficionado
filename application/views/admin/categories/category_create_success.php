<h1 class="text-success">Получилось!</h1>
<h4>Вы создали категорию товаров!</h4>
<div class="admin-input-title">
  <p><button class="btn btn-sm" onclick="location.assign(location.href);">Создать еще категорию</button></p>
  <p class="text-left"><a href="/admin/categories">Перейти к списку категорий товаров</a></p>
</div>