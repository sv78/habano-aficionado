<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//echo "Type of update : " . $this->input->post('submit') . "<br>Modified fields: " . $this->input->post('modified_fields');
?>

<!-- page nav -->
<p class="text-right small">
  <a href="<?= $this->back_url->get_parent_url() ?>">Вернуться</a>
  | <a href="/admin/product-preview/<?= $item['id']; ?>">Просмотр</a>
  | <a href="/admin/product-preview/<?= $item['id']; ?>" target="_blank">Просмотр в новой вкладке</a>
</p>
<!-- / page nav -->

<h4>Редактирование товара:</h4>
<p class="small text-muted">Внимание! Будте внимательны создавая, редактируя и сохраняя контент! Неосознанные действия могут привести к потере данных, а также выходу системы из строя.</p>
<h1><?= $title ?></h1>

<hr>

<!-- new image upload via iframe -->

<!-- image is different in cigar product and normal product -->

<?php
if ($item['img'] == NULL OR $item['img'] == '') {
  $img_src = "/" . config_item('_product_not_specified_image_url');
} else if (!file_exists(config_item('_products_image_path') . $item['img'])) {
  $img_src = "/" . config_item('_product_missing_image_url');
} else {
  $img_src = "/" . config_item('_products_image_path_url') . $item['img'];
}
?>

<h4>Изображение <?php echo $item['is_cigar_product'] == 1 ? 'товара сигарного типа' : 'товара' ?></h4>
<img id="uploaded_image_tag" class="thumbnail" src="<?= $img_src ?>" alt="<?= $title ?>">
<form action="/admin/upload-image" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="new_file_form_submitted(event)">
  <input type="hidden" name="id" value="<?= $item['id'] ?>">
  <input type="hidden" name="table" value="<?php echo $item['is_cigar_product'] == 1 ? 'cigars' : 'products' ?>">
  <input id="img_tag" class="hidden" type="file" name="img" onchange="file_input_changed(event)">
  <div>
    <p>
      <label for="img_tag" class="btn btn-sm btn-default">Изменить изображение</label>
      <button type="submit" name="submit" id="file_submit_btn" class="hidden btn btn-sm btn-primary">Загрузить</button>
      <strong id="img_filename_holder" class="small"></strong>
    </p>
  </div>
</form>
<p id="img_filename_loading" class="small text-primary hidden">Uploading…</p>
<iframe class="hidden" name="upload_target" src="/admin/empty-iframe"></iframe>
<p class="small text-info">Внимание! Загружаемые файлы должны являться только изображениями. Загрузка других типов файлов недопустима. Размер Файла должен быть не более 
  <?php
  echo $item['is_cigar_product'] == 1 ? round(config_item('_cigar_image_upload_config')['max_size'] / 1024) : round(config_item('_product_image_upload_config')['max_size'] / 1024);
  echo ' Mb';
  ?>
</p>

<script>
  function new_file_form_submitted(e) {
    $('#file_submit_btn').addClass('hidden');
    $('#img_filename_holder').html('');
    $('#img_filename_loading').removeClass('hidden');
  }
  function file_input_changed(e) {
    var fakefilepath = e.target.value;
    var ffp_arr = fakefilepath.split('\\');
    var filename = ffp_arr[ffp_arr.length - 1];
    $('#img_filename_holder').html(filename);
    if (e.target.value !== '') {
      $('#file_submit_btn').removeClass('hidden');
    } else {
      $('#file_submit_btn').addClass('hidden');
    }
  }
</script>

<hr>

<!-- LINE OF IMAGES -->


<style>
  #apai .ui-selecting { background: #FECA40; }
  #apai .ui-selected { border: 2px solid red;}
</style>

<h4>Дополнительные изображения товара</h4>

<div id="apai" class="admin-product-additional-images-holder">
  <?php
  $this->baza->show_additional_product_images($item['images']);
  ?>
</div>



<form action="/admin/upload-additional-image" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="additional_image_form_submitted(event)">
  <input type="hidden" name="id" value="<?= $item['id'] ?>">
  <input type="hidden" name="table" value="products">
  <input id="add_img_tag" class="hidden" type="file" name="img" onchange="additional_file_input_changed(event)">
  <div>
    <p>
      <label for="add_img_tag" class="btn btn-sm btn-default">Добавить изображение</label>
      <button type="submit" name="submit" id="add_file_submit_btn" class="hidden btn btn-sm btn-primary">Загрузить</button>
      <strong id="add_img_filename_holder" class="small"></strong>
    </p>
  </div>
</form>
<p id="add_img_filename_loading" class="small text-primary hidden">Uploading…</p>




<p class="text-left">
  <button id="image_order_saver" class="btn btn-sm" onclick="saveImageOrder(event)">Созранить состояние</button>
  <button id="image_deleter" class="btn btn-sm btn-danger hidden" onclick="deleteImageFromOrder(event)">Удалить выделенное</button>
</p>

<script>
  var apai = document.getElementById('apai');
  $(function () {
    $(apai).sortable({axis: 'x'});
    $(apai).disableSelection();
    //$("#apai").selectable();
  });

  apai.addEventListener('click', apaiClickListener, false);



  function additional_image_form_submitted(e) {
    $('#add_file_submit_btn').addClass('hidden');
    $('#add_img_filename_holder').html('');
    $('#add_img_filename_loading').removeClass('hidden');
  }
  function additional_file_input_changed(e) {
    var fakefilepath = e.target.value;
    var ffp_arr = fakefilepath.split('\\');
    var filename = ffp_arr[ffp_arr.length - 1];
    $('#add_img_filename_holder').html(filename);
    if (e.target.value !== '') {
      $('#add_file_submit_btn').removeClass('hidden');
    } else {
      $('#add_file_submit_btn').addClass('hidden');
    }
  }





  function apaiClickListener(e) {
    if (e.target.tagName === 'IMG') {
      removeImageWastedClass(e.target.parentNode);
      $(e.target).addClass('admin-selected-image-to-delete');
      $('#image_deleter').removeClass('hidden');
      //console.log(e.target.src);
    } else {
      removeImageWastedClass(e.target);
      $('#image_deleter').addClass('hidden');
    }

    function removeImageWastedClass(elm) {
      for (var i = 0, len = elm.children.length; i < len; i++) {
        $(elm.children[i]).removeClass('admin-selected-image-to-delete');
      }
    }
  }

  function deleteImageFromOrder(e) {
    var elms = apai.getElementsByClassName('admin-selected-image-to-delete');
    if (elms.length != 1) {
      return;
    }
    var img = elms[0];
    apai.removeChild(img);
    $('#image_deleter').addClass('hidden');
  }

  function saveImageOrder() {
    var new_files_order_arr = [];
    for (var i = 0, len = apai.children.length; i < len; i++) {
      var spl = apai.children[i].src.split('/');
      var fname = spl[spl.length - 1];
      new_files_order_arr.push(fname);
    }
    var files_str = new_files_order_arr.join(',');


    /*if (!confirm("Do you want to save new order of images?"))
     return;*/
    var request = $.ajax({
      url: "/ajax/update-images-by-id",
      method: "POST",
      data: {table: 'products', id: <?= $item['id'] ?>, images: files_str}
    });
    request.done(function (msg) {
      //console.log(msg);
      //document.getElementById('dbg').innerHTML = msg;
      if (msg != '') {
        //console.log("Can not update images. Something went wrong with it…");
        window.alert("Ошибка! Что-то пошло не так…");
        return;
      } else {
        //console.log("Images updated successfully!");
        window.alert('Image order was saved!');
      }
    });
    request.fail(function (jqXHR, textStatus) {
      alert("Error!!!");
    });


  }

</script>


<hr>





<!-- form -->
<?php
$form_attributes = array('id' => 'product_edit_form');
echo form_open_multipart('', $form_attributes);
?>

<h3>Основные характеристики</h3>

<!-- name -->
<p class="admin-input-title text-primary">Имя (название) товара *</p>
<input id="product_name_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="name" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['name'])) ? set_value('name') : $item['name'];
?>" onchange="formChangeEvent(event)">
       <?php echo form_error('name') ?>




<!-- slug -->
<p class="admin-input-title text-primary">Ярлык URL *</p>
<input id="product_slug_input" onblur="to_slug(event)" class="form-control admin-input" type="text" name="slug" maxlength="64" autocomplete="off" value="<?php
echo (!isset($item['slug'])) ? set_value('slug') : $item['slug'];
?>" onchange="formChangeEvent(event)">
       <?php echo form_error('slug') ?>






<div class="row">

  <div class="col-lg-6">

    <!-- category of this product -->
    <p class="admin-input-title text-primary">Категория товара *</p>
    <select id="categories" name="category_id" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
      <?php
      $cat_id = (!isset($item['category_id'])) ? set_value('category_id') : $item['category_id'];
      $this->baza->create_categories_options($cat_id);
      ?>
    </select>
    <?php echo form_error('category_id') ?>



    <!-- brand of this product -->
    <p class="admin-input-title text-primary">Бренд товара *</p>
    <select id="brands" name="brand_id" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
      <?php
      $br_id = (!isset($item['brand_id'])) ? set_value('brand_id') : $item['brand_id'];
      $this->baza->create_brands_options($br_id);
      ?>
    </select>
    <?php echo form_error('brand_id') ?>

  </div>


  <div class="col-lg-6">

    <!-- country made select -->
    <p class="admin-input-title text-primary">Страна производства товара *</p>
    <select id="countries" name="country_made" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
      <?php
      $cntm = (!isset($item['country_made'])) ? set_value('country_made') : $item['country_made'];
      Baza::create_country_options(config_item('_flt_countries'), $cntm);
      ?>
    </select>
    <span class="small text-muted">(может отличаться от страны бренда)</span>
    <?php echo form_error('country_made') ?>

  </div>

  <div class="col-lg-3">

    <!-- published checkbox -->
    <?php
    $pbl_value = !isset($item['published']) ? set_value('published') : $item['published'];
    $pbl_checked = ($pbl_value == 1 OR $pbl_value == 'on') ? ' checked' : '';
    ?>
    <div class="admin-input-title">
      <div class="checkbox">
        <label for="published_chkbx">
          <input type="checkbox" id="published_chkbx" name="published" <?= $pbl_checked ?> onchange="formChangeEvent(event)"> Публиковать
        </label>
      </div>
      <?php echo form_error('published') ?>
    </div>

  </div>

  <div class="col-lg-3">

    <!-- featured checkbox -->
    <?php
    $ftr_value = !isset($item['featured']) ? set_value('featured') : $item['featured'];
    $ftr_checked = ($ftr_value == 1 OR $ftr_value == 'on') ? ' checked' : '';
    ?>
    <div class="admin-input-title">
      <div class="checkbox">
        <label for="featured_chkbx">
          <input type="checkbox" id="featured_chkbx" name="featured" <?= $ftr_checked ?> onchange="formChangeEvent(event)"> Featured
        </label>
      </div>
      <?php echo form_error('featured') ?>
    </div>

  </div>

</div>



<div class="row">

  <div class="col-lg-6">

    <!-- price -->
    <p class="admin-input-title">Цена:</p>
    <input class="form-control admin-input-number" type="number" name="price" min="0" max="16777215" onblur="trim_input_field(event)" onchange="formChangeEvent(event)" autocomplete="off" value="<?php echo (!isset($item['price'])) ? set_value('price') : $item['price']; ?>">

  </div>

  <div class="col-lg-6">

    <!-- quantity -->
    <p class="admin-input-title">Кол-во:</p>
    <input class="form-control admin-input-number" type="number" name="quantity" min="0" max="9999" onblur="trim_input_field(event)" onchange="formChangeEvent(event)" autocomplete="off" value="<?php echo (!isset($item['quantity'])) ? set_value('quantity') : $item['quantity']; ?>">

  </div>

</div>

<?php
if ($product['num_of_packs'] > 0) {
  ?>
  <p style="margin-top: 30px;">Упаковок для этого товара существует : <strong><?= $product['num_of_packs'] ?></strong>
    <br>
    <a class="small" href="/admin/packs-of-product/<?= $item['id']; ?>" >Упаковки этого товара</a>
  </p>
  <?php
}
?>

<p style="margin-top: 30px;"><a class="btn btn-sm btn-primary" href="/admin/create-pack-of-product/<?= $item['id']; ?>" >Cоздать упаковку для этого товара</a></p>







<!-- ============== -->
<!-- CIGAR SETTINGS -->
<!-- ============== -->
<?php
if ($item['is_cigar_product'] == 1) {
  ?>


  <h3 style="margin-top:60px;">Сигарные характеристики</h3>


  <!-- vitolas select -->
  <p class="admin-input-title text-primary">Vitola *</p>
  <select id="vitola_input" name="vitola" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
    <?php
    $vtl_value = (!isset($item['vitola'])) ? set_value('vitola') : $item['vitola'];
    $this->baza->create_vitolas_options($vtl_value);
    ?>
  </select>
  <?php echo form_error('vitola') ?>


  <!-- vitolas preview img -->
  <div>
    <img id="vitola_img_preview" src="#" alt="vitola preview">
  </div>



  <div class="row">

    <div class="col-lg-6">


      <!-- line (seria) select -->
      <p class="admin-input-title text-primary">Series *</p>
      <select id="line_input" name="line" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
        <?php
        $ln_value = (!isset($item['line'])) ? set_value('line') : $item['line'];
        $this->baza->create_lines_options($ln_value);
        ?>
      </select>
      <?php echo form_error('line') ?>




      <!-- length select -->
      <p class="admin-input-title text-primary">Length *</p>
      <select id="length_input" name="length" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
        <?php
        $len_value = (!isset($item['length'])) ? set_value('length') : $item['length'];
        $this->baza->create_lengths_options($len_value);
        ?>
      </select>
      <?php echo form_error('length') ?>


      <!-- cepo select -->
      <p class="admin-input-title text-primary">Cepo (Ring Gauge) *</p>
      <select id="cepo_input" name="cepo" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
        <?php
        $cp_value = (!isset($item['cepo'])) ? set_value('cepo') : $item['cepo'];
        $this->baza->create_cepos_options($cp_value);
        ?>
      </select>
      <?php echo form_error('cepo') ?>


    </div>

    <div class="col-lg-6">

      <!-- intensity select -->
      <p class="admin-input-title text-primary">Intensity *</p>
      <select id="intensity_input" name="intensity" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
        <?php
        $it_value = (!isset($item['intensity'])) ? set_value('intensity') : $item['intensity'];
        $this->baza->create_intensities_options($it_value);
        ?>
      </select>
      <?php echo form_error('intensity') ?>


      <!-- flavours select -->
      <p class="admin-input-title text-primary">Flavour *</p>
      <select id="flavour_input" name="flavour" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
        <?php
        $fl_value = (!isset($item['flavour'])) ? set_value('flavour') : $item['flavour'];
        $this->baza->create_flavours_options($fl_value);
        ?>
      </select>
      <?php echo form_error('flavour') ?>


      <!-- wrapper select -->
      <p class="admin-input-title text-primary">Wrapper *</p>
      <select id="wrapper_input" name="wrapper" class="form-control admin-input admin-max-width-normal" onchange="formChangeEvent(event)">
        <?php
        $wr_value = (!isset($item['wrapper'])) ? set_value('wrapper') : $item['wrapper'];
        $this->baza->create_wrappers_options($wr_value);
        ?>
      </select>
      <?php echo form_error('wrapper') ?>

    </div>

  </div>

  <?php
}
?>
<!-- ================= -->
<!-- // CIGAR SETTINGS -->
<!-- ================= -->









<h3 style="margin-top:50px;">Описание, информация</h3>



<!-- intro -->
<p class="admin-input-title">Вводный текст</p>
<textarea class="form-control admin-textarea" onblur="trim_input_field(event)" name="intro" onchange="formChangeEvent(event)"><?php
  echo (!isset($item['intro'])) ? set_value('intro') : $item['intro'];
  ?></textarea>


<!-- html -->
<p class="admin-input-title">Контент</p>
<textarea name="html"><?php
  echo (!isset($item['html'])) ? set_value('html') : $item['html'];
  ?></textarea>



<!-- meta title -->
<p class="admin-input-title">Title tag</p>
<input id="product_metatitle_input" onblur="trim_input_field(event)" onchange="formChangeEvent(event)" class="form-control admin-input" type="text" name="meta_title" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['meta_title'])) ? set_value('meta_title') : $item['meta_title'];
?>">


<!-- meta description -->
<p class="admin-input-title">Description meta tag</p>
<input class="form-control admin-input" onblur="trim_input_field(event)" onchange="formChangeEvent(event)" type="text" name="meta_description" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['meta_description'])) ? set_value('meta_description') : $item['meta_description'];
?>">


<!-- meta keywords -->
<p class="admin-input-title">Keywords meta tag</p>
<input class="form-control admin-input" onblur="trim_input_field(event)" onchange="formChangeEvent(event)" type="text" name="meta_keywords" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['meta_keywords'])) ? set_value('meta_keywords') : $item['meta_keywords'];
?>">





<!-- meta robots select -->
<p class="admin-input-title">Robots meta tag</p>
<select class="form-control admin-input admin-max-width-normal" name="meta_robots" onchange="formChangeEvent(event)">
  <?php
  $rbts = (!isset($item['meta_robots'])) ? set_value('meta_robots') : $item['meta_robots'];
  Baza::create_options('meta_robots', Baza::$meta_robots_options_array, $rbts);
  ?>
</select>
<?php echo form_error('meta_robots') ?>





<!-- submit -->
<div class="admin-input-title">
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save') ?>">Сохранить</button>
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save_and_close') ?>">Сохранить и закрыть</button>
</div>

<input id="modified_fields_input" type="hidden" name="modified_fields" value="<?php echo isset($modified_fields) ? $modified_fields : ''; ?>">

</form>

<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);">Перезагрузить</button>
</div>




<!-- page nav -->
<p class="text-right small">
  <a href="<?= $this->back_url->get_parent_url() ?>">Вернуться</a>
  | <a href="/admin/product-preview/<?= $item['id']; ?>">Просмотр</a>
  | <a href="/admin/product-preview/<?= $item['id']; ?>" target="_blank">Просмотр в новой вкладке</a>
</p>
<!-- / page nav -->


<!--
<?php
$this->db->where('id', $item['brand_id']);
$query = $this->db->get('brands');
$bbrr = $query->row_array()['name'];
?>

<hr>

<p><small style="color: red;">This is automatically generated strings for title, description and keywords that uses Sergey Neuman pattern. You can copy and paste value into corresponding fields.</small></p>

<pre>Сигары <?= $item['name']; ?></pre>

<pre>Купить аутентичные (оригинальные) сигары <?= $item['name']; ?> в Москве. Марка - <?= $bbrr; ?>, vitola - <?= $this->config->item('_flt_vitolas')[$item['vitola']]['name']; ?>, серия - <?= $this->config->item('_flt_lines')[$item['line']]['name']; ?></pre>

<pre><?= $item['name']; ?>, сигары, Habanos S.A., <?= $bbrr; ?>, <?= $this->config->item('_flt_vitolas')[$item['vitola']]['name']; ?>, <?= $this->config->item('_flt_lines')[$item['line']]['name']; ?>, специалист Habanos</pre>
-->


<script src="/assets/js/ckeditor_4.6.1_full/ckeditor/ckeditor.js"></script>

<script>
    // array of input field names that was changed by user (also values that was changed got from server side in hidden field)
    var formChangedInputs = [] = document.getElementById('modified_fields_input').value.split(',');
    formChangedInputs = array_remove_empty_values(formChangedInputs);
    //console.log(formChangedInputs);


    var ck_html = CKEDITOR.replace('html', {
      customConfig: 'config_admin.js'
    });

    // checking CKEDITOR for changes
    ck_html.on('change', function (evt) {
      // getData() returns CKEditor's HTML content.
      formChangedInputs.push(evt.editor.name);
    });

    // fires if form inputs changes (CKEditor event is checking too but separately)
    function formChangeEvent(e) {
      formChangedInputs.push(e.target.name);
      if (e.target.name == 'vitola') {
        show_vitola_image(e.target);
      }
    }

    function show_vitola_image(sel) {
      var img_preview = document.getElementById('vitola_img_preview');
      for (var i = 0, len = sel.children.length; i < len; i++) {
        if (sel.children[i].value == sel.value) {
          img_preview.src = "/<?= config_item('_vitolas_image_path_url'); ?>" + sel.children[i].getAttribute('img_src');
          break;
        }
      }
    }

    // on form submit
    document.getElementById('product_edit_form').onsubmit = function (e) {
      var modifiedInputs = array_remove_duplicates(formChangedInputs);
      formChangedInputs = [];
      if (modifiedInputs !== false) {
        document.getElementById('modified_fields_input').value = modifiedInputs.join(',');
      }
    };

    window.onload = function () {
      if (document.getElementById('vitola_input')) {
        show_vitola_image(document.getElementById('vitola_input'));
      }
    }


</script>