<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h3>Товар : предварительный просмотр</h3>
<div class="small">
  <h4>Название : <?= $name ?></h4>
  <img src="/<?= config_item('_products_image_path_url') . $img; ?>" alt="<?= $name ?>">
  <div>
    <?php
    foreach ($images as $image) {
      echo '<img src="/' . config_item('_products_additional_image_path_url') . $image . '" alt="' . $image . '">';
    }
    ?>
  </div>
  <p>Цена : <?= $price ?> руб.</p>
  <p>Наличие : <?= $quantity ?> шт.</p>
  <p>Варианты с упаковкой: <?= $num_of_packs ?></p>
  <p>Ярлык URL : <?= $slug ?></p>
  <p>Категория товара : <?= $category_name ?></p>
  <p>Бренд: <?= $brand_name ?></p>
  <img src="/<?= config_item('_brands_image_path_url') . $brand_img; ?>" alt="<?= $brand_name ?>">
  <p>Страна бренда: <?php echo config_item('_flt_countries')[$country]['name']; ?></p>
  <p>Страна производства продукта: <?php echo config_item('_flt_countries')[$country_made]['name']; ?></p>
  <p>Сигарный товар : <?= $is_cigar_product == 1 ? 'Yes' : 'No' ?></p>
  <p>Meta title : <?= $meta_title ?></p>
  <p>Meta description : <?= $meta_description ?></p>
  <p>Meta keywords : <?= $meta_keywords ?></p>
  <p>Meta robots : <?= $meta_robots ?></p>

  <?php
  if ($is_cigar_product == 1):
    ?>
    <hr>
    <p>Vitola : <?= config_item('_flt_vitolas')[$vitola]['name'] ?></p>
    <p>Line : <?= config_item('_flt_lines')[$line]['name'] ?></p>
    <p>Length : <?= config_item('_flt_lengths')[$length]['mm'] . ' mm / ' . config_item('_flt_lengths')[$length]['inches'] ?></p>
    <p>Cepo : <?= $cepo ?></p>
    <p>Intensity : <?= config_item('_flt_intensities')[$intensity]['name'] ?></p>
    <p>Flavour : <?= config_item('_flt_flavours')[$flavour]['name'] ?></p>
    <p>Wrapper : <?= config_item('_flt_wrappers')[$wrapper]['name'] ?></p>
    <?php
  endif;
  ?>
</div>
<hr>

<p>Вводный текст : <?= $intro ?></p>
<hr>
<h4>Содержимое : предварительный просмотр</h4>

<div class="blog-article">
  <?= $html; ?>
</div>