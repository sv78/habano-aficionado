<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1><?= $title ?> </h1>
<h4>Товары в категории <?= $title ?></h4>

<p class="text-right small">
  <a href="/admin/create-product">Создать товар</a>
  | <a href="<?= current_url() ?>"><?= $title ?></a>
  | <a href="/admin/categories">Категории</a>
  | <a href="/admin/products">Товары</a>
  | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить</a>
</p>