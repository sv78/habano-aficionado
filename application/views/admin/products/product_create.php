<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Создать новый товар</h1>

<?php
$form_attributes = array('id' => 'product_create_form');
echo form_open_multipart('', $form_attributes);
?>


<!-- category of this product -->
<p class="admin-input-title text-primary">Категория товара *</p>
<select id="categories" name="category_id" class="form-control admin-input admin-max-width-normal">
  <?php
  $this->baza->create_categories_options(set_value('category_id'));
  ?>
</select>
<?php echo form_error('category_id') ?>



<!-- brand of this product -->
<p class="admin-input-title text-primary">Бренд товара *</p>
<select id="brands" name="brand_id" class="form-control admin-input admin-max-width-normal">
  <?php
  $this->baza->create_brands_options(set_value('brand_id'));
  ?>
</select>
<?php echo form_error('brand_id') ?>



<!-- name -->
<p class="admin-input-title text-primary">Название товара *</p>
<input id="product_name_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="name" maxlength="255" autocomplete="off" value="<?php echo set_value('name'); ?>">
<?php echo form_error('name') ?>


<!-- slug -->
<p class="admin-input-title text-primary">Ярлык URL *</p>
<input id="slug_input" onblur="to_slug(event)" class="form-control admin-input" type="text" name="slug" maxlength="128" autocomplete="off" value="<?php echo set_value('slug'); ?>">
<?php echo form_error('slug') ?>


<!-- slug generator -->
<span id="slug_create_btn" class="btn btn-default btn-sm">
  Создать ярлык URL из названия товара
</span>


<!-- is cigar product -->
<div class="admin-input-title">
  <div class="checkbox">
    <label for="is_cigar_product_chkbx">
      <input type="checkbox" id="is_cigar_product_chkbx" name="is_cigar_product" <?php echo set_checkbox('is_cigar_product', 'on', FALSE) ?>>
      <span class="text-primary">Сигарный товар *</span>
    </label>
    <span> (Внимание! Данная характеристика не может быть изменена в дальнейшем.)</span>
  </div>
  <?php echo form_error('is_cigar_product') ?>
</div>


<!-- title tag -->
<p class="admin-input-title">Title tag</p>
<input id="metatitle_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="meta_title" maxlength="255" autocomplete="off" value="<?php echo set_value('meta_title'); ?>">

<!-- title tag generator -->
<span id="metatitle_create_btn" class="btn btn-default btn-sm">
  Создать title tag из названия товара
</span>


<!-- submit -->
<div class="admin-input-title">
  <button class="btn btn-success" name="submit" type="submit" value="submit">Сохранить</button>
</div>

</form>

<!-- reload btn -->
<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);"><?= $this->lang->line('habano_input_reload_page_to_reset_form_after_validation') ?></button>
</div>


<script>
  // creating slug from title (name)
  document.getElementById('slug_create_btn').onclick = function () {
    create_slug_from_title(document.getElementById('product_name_input'), document.getElementById('slug_input'), 128);
  };

  document.getElementById('metatitle_create_btn').onclick = function () {
    create_metatitle_from_title(document.getElementById('product_name_input'), document.getElementById('metatitle_input'));
  };
</script>