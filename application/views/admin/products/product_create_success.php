<h1 class="text-success">Получилось!</h1>
<h4>Вы создали товар!</h4>
<div class="admin-input-title">
  <p><button class="btn btn-sm" onclick="location.assign(location.href);">Создать еще товар</button></p>
  <p class="text-left"><a href="/admin/products">Перейти к списку всех товаров</a></p>
</div>