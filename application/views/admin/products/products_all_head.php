<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Все товары</h1>

<p class="text-right small">
  <a href="/admin/create-product">Создать новый товар</a>
  | <a href="<?= current_url() ?>">Товары</a>
  | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить страницу</a>
</p>