<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<table class="table table-responsive table-hover" id="items_table">
  <thead>
    <tr>
      <th title="Порядковый номер">#</th>
      <th title="Статус : опубликовано или нет">Pub</th>
      <th title="Является ли сигарным товаром">Cgr</th>
      <th title="Статус : специальный или нет">Ftr</th>
      <!--<th>Up</th>-->
      <th title="Изображение товара">Img</th>
      <th title="Название товара, его категория и бренд" class="admin-th-title">Product Name / Category / Brand</th>
      <th title="Ярлык URL" class="admin-th-slug">Slug</th>
      <th title="Цена товара за единицу">Price</th>
      <th title="Количество единиц товара">Qty</th>
      <th title="Количество упаковок данного товара">Pks</th>
      <th title="Создать упаковку для данного товара">Mpk</th>
      <th title="Редактировать товар">Edit</th>
      <th title="Удалить товар">Del</th>
      <th title="Товар в процессе редактирования пользователем или не закрыт им">By</th>
    </tr>
  </thead>  
  <tbody>
    <?php
    foreach ($products as $product) :
      $tr = [];
      ?>
      <tr>
	<?php
	$tr[] = '<td>' . $row_counter . '</td>';


	// published by
	if ($product['used_by_user_id'] != NULL) {
	  $published = $product['published'] ? '<span class="text-muted glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-lock" aria-hidden="true"></span>';
	} else {
	  $published = $product['published'] ? '<span class="cursor-pointer glyphicon glyphicon-ok" aria-hidden="true" action-type="published" product-id="' . $product['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-lock" aria-hidden="true" action-type="published" product-id="' . $product['id'] . '"></span>';
	}
	$tr[] = '<td>' . $published . '</td>';


	// is cigar product
	$icb = $product['is_cigar_product'] ? '<span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span>' : '';
	$tr[] = '<td>' . $icb . '</td>';


	// featured
	if ($product['used_by_user_id'] != NULL) {
	  $featured = $product['featured'] ? '<span class="text-muted glyphicon glyphicon-star" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-star-empty" aria-hidden="true"></span>';
	} else {
	  $featured = $product['featured'] ? '<span class="cursor-pointer glyphicon glyphicon-star" aria-hidden="true" action-type="featured" product-id="' . $product['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-star-empty" aria-hidden="true" action-type="featured" product-id="' . $product['id'] . '"></span>';
	}
	$tr[] = '<td>' . $featured . '</td>';



	// lift up
	/*
	  if ($product['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	  } else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-upload" aria-hidden="true" action-type="liftup" product-id="' . $product['id'] . '"></span></td>';
	  }
	 */


	// image
	if ($product['img'] == '') {
	  $product_img = "/" . config_item('_product_not_specified_image_url');
	} else if (!file_exists(config_item('_products_image_path') . $product['img'])) {
	  $product_img = "/" . config_item('_product_missing_image_url');
	} else {
	  $product_img = "/" . config_item('_products_image_path_url') . $product['img'];
	}

	$tr[] = '<td><img title="' . $product['intro'] . '" src="' . $product_img . '" class="cursor-help blog-img-thumb" alt="thumb"></td>';


	// name
	$tip_arr = [];
	//$tip_arr[] = 'Cоздана: ' . $product['created_at'];
	//$tip_arr[] = ' | Последняя редакция: ' . $product['modified_at'];
	if ($product['lifted_up_at'] != NULL) {
	  $tip_arr[] = 'Поднята: ' . date('d M Y', $product['lifted_up_at']) . " в " . date('H:i:s', $product['lifted_up_at']);
	}
	$tip = join('', $tip_arr);
	unset($tip_arr);
	$tr[] = '<td title="' . $tip . '">';
	//$tr[] = '<span class="small">' . $product['category_name'] . '</span><br>';
	$tr[] = '<a href="/admin/product-preview/' . $product['id'] . '">' . Baza::decode_plain_string_from_db($product['name']) . '</a>';
	$tr[] = '<br><span class="small">' . $product['category_name'] . ' | ' . Baza::decode_plain_string_from_db($product['brand_name']) . '</span>';
	$tr[] = '</td>';




	// slug
	$tr[] = '<td>' . $product['slug'] . '</td>';




	// price
	if ($product['used_by_user_id'] != NULL) {
	  $tr[] = '<td class="small">' . $product['price'] . '</td>';
	} else {
	  $tr[] = '<td class="small" action-type="price" product-id="' . $product['id'] . '">' . $product['price'] . '</span></td>';
	}


	// quantity (number of products available)
	if ($product['used_by_user_id'] != NULL) {
	  $tr[] = '<td class="small">' . $product['quantity'] . '</td>';
	} else {
	  $tr[] = '<td class="small" action-type="quantity" product-id="' . $product['id'] . '">' . $product['quantity'] . '</span></td>';
	}




	// packs (number of packs available)
	if ($product['num_of_packs'] != 0) {
	  $tr[] = '<td class="small"><a href="/admin/packs-of-product/' . $product['id'] . '" title="Перейти к упаковкам этого товара">' . $product['num_of_packs'] . '</a></td>';
	} else {
	  $tr[] = '<td class="small">' . $product['num_of_packs'] . '</td>';
	}



	// make pack for this product
	if ($product['used_by_user_id'] != NULL) {
	  if (isset($this->session->userdata('user')['is_super_admin']) OR $this->session->userdata('user')['id'] == $product['used_by_user_id']) {
	    $pack_link = '';
	  } else {
	    $pack_link = '';
	  }
	} else {
	  $pack_link = '<a href="/admin/create-pack-of-product/' . $product['id'] . '" class="small glyphicon glyphicon-th-large" aria-hidden="true" title="Создать упаковку для этого товара"></a>';
	}
	$tr[] = '<td>' . $pack_link . '</td>';






	// edit
	if ($product['used_by_user_id'] != NULL) {
	  if (isset($this->session->userdata('user')['is_super_admin']) OR $this->session->userdata('user')['id'] == $product['used_by_user_id']) {
	    $tr[] = '<td><a class="glyphicon glyphicon-floppy-remove" aria-hidden="true" href="/admin/product-edit/' . $product['id'] . '"></a></td>';
	  } else {
	    $tr[] = '<td></td>';
	  }
	} else {
	  $tr[] = '<td><a class="glyphicon glyphicon-pencil" aria-hidden="true" href="/admin/product-edit/' . $product['id'] . '"></a></td>';
	}



	// delete
	if ($product['used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-remove" aria-hidden="true" action-type="delete" product-id="' . $product['id'] . '"></span></td>';
	}

	// used by
	if ($product['used_by_user_id'] != NULL) {
	  if ($product['used_by_user_id'] == $this->session->userdata('user')['id']) {
	    $tr[] = '<td><a href="/admin/users/' . $product['used_by_user_id'] . '">You</a></td>';
	  } else {
	    $tr[] = '<td><a class="glyphicon glyphicon-user" aria-hidden="true" href="/admin/users/' . $product['used_by_user_id'] . '"></a></td>';
	  }
	} else {
	  $tr[] = "<td></td>";
	}

	echo join('', $tr);
	?>
      </tr>
      <?php
      $row_counter++;
    endforeach;
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">Товаров : <span id="total_records_counter"><?= $num_of_products ?></span></td>
    </tr>
  </tfoot>
</table>

<?php echo $this->pagination->create_links(); ?>


<script>
  document.body.addEventListener('click', clickActionListener, false);
  function clickActionListener(e) {
    e = (!e) ? window.event : e;
    if (e.target.hasAttribute('product-id')) {

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'delete') {
	delete_product_by_id(e.target);
      }
      /*
       if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'quantity') {
       quantity_change_by_id(e.target);
       }
       */
      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'published') {
	toggle_published_item_by_id(e.target)
      }
      /*
       if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'liftup') {
       //lift_up(e.target)
       return;
       }
       */
      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'featured') {
	toggle_featured(e.target)
      }

    }
  }

  window.onload = function () {
    if (document.getElementById('items_table')) {
      document.getElementById('items_table').addEventListener('mouseover', tableMouseOverListener, false);
    }
  }

</script>