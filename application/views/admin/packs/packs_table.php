<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<table class="table table-responsive table-hover" id="items_table">
  <thead>
    <tr>
      <th title="Номер">#</th>
      <th title="Статус : опубликовано или нет">Pub</th>
      <th title="Изображение упаковки">Img</th>
      <th title="Название упаковки продукта" class="admin-th-title">Name Of Product Pack</th>
      <!--<th class="admin-th-slug">Pack Name</th>-->
      <th title="Цена упаковки">Price</th>
      <th title="Единиц товара в упаковке">In Pack</th>
      <th title="Количество упаковок товара">Qty</th>
      <th title="Редактировать упаковку">Edit</th>
      <th title="Удалить упаковку">Del</th>
      <th title="Товар этой упаковки в процессе редактирования пользователем или не закрыт им">By</th>
    </tr>
  </thead>  
  <tbody>
    <?php
    foreach ($packs as $pack) :
      $tr = [];
      ?>
      <tr>
	<?php
	$tr[] = '<td>' . $row_counter . '</td>';



	// published by
	if ($pack['product_used_by_user_id'] != NULL) {
	  $published = $pack['published'] ? '<span class="text-muted glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="text-muted glyphicon glyphicon-lock" aria-hidden="true"></span>';
	} else {
	  $published = $pack['published'] ? '<span class="cursor-pointer glyphicon glyphicon-ok" aria-hidden="true" action-type="published" pack-id="' . $pack['id'] . '"></span>' : '<span class="cursor-pointer glyphicon glyphicon-lock" aria-hidden="true" action-type="published" pack-id="' . $pack['id'] . '"></span>';
	}
	$tr[] = '<td>' . $published . '</td>';


	// image
	if ($pack['img'] == '') {
	  $pack_img = "/" . config_item('_pack_not_specified_image_url');
	} else if (!file_exists(config_item('_packs_image_path') . $pack['img'])) {
	  $pack_img = "/" . config_item('_pack_missing_image_url');
	} else {
	  $pack_img = "/" . config_item('_packs_image_path_url') . $pack['img'];
	}

	$tr[] = '<td><img src="' . $pack_img . '" class="cursor-help blog-img-thumb" alt="thumb"></td>';


	// name
	$tr[] = '<td>';
	$tr[] = '<a href="/admin/pack-preview/' . $pack['id'] . '">' . $pack['name'] . '</a>';
	$tr[] = '<br>';
	if (!isset($product_id)) {
	  $tr[] = '<a title="Смотреть упаковки данного товара" class="small" href="/admin/packs-of-product/' . $pack['product_id'] . '">Packs of</a>&nbsp;&nbsp;';
	}
	$tr[] = '<span class="small">' . $pack['product_name'] . '</span>';
	$tr[] = '</td>';


	// price
	$tr[] = '<td class="small">' . $pack['price'] . '</td>';


	// number of items in one pack
	$tr[] = '<td class="small">' . $pack['num_of_items'] . '</td>';


	// quantity of packs available
	$tr[] = '<td class="small">' . $pack['quantity'] . '</td>';
	
	
	


	// edit
	if ($pack['product_used_by_user_id'] != NULL) {
	  if (isset($this->session->userdata('user')['is_super_admin']) OR $this->session->userdata('user')['id'] == $pack['product_used_by_user_id']) {
	    $tr[] = '<td></td>';
	  } else {
	    $tr[] = '<td></td>';
	  }
	} else {
	  $tr[] = '<td><a class="glyphicon glyphicon-pencil" aria-hidden="true" href="/admin/pack-edit/' . $pack['id'] . '"></a></td>';
	}



	// delete
	if ($pack['product_used_by_user_id'] != NULL) {
	  $tr[] = '<td></td>';
	} else {
	  $tr[] = '<td><span class="cursor-pointer glyphicon glyphicon-remove" aria-hidden="true" action-type="delete" pack-id="' . $pack['id'] . '"></span></td>';
	}

	// used by
	if ($pack['product_used_by_user_id'] != NULL) {
	  if ($pack['product_used_by_user_id'] == $this->session->userdata('user')['id']) {
	    $tr[] = '<td><a href="/admin/users/' . $pack['product_used_by_user_id'] . '">You</a></td>';
	  } else {
	    $tr[] = '<td><a class="glyphicon glyphicon-user" aria-hidden="true" href="/admin/users/' . $pack['product_used_by_user_id'] . '"></a></td>';
	  }
	} else {
	  $tr[] = "<td></td>";
	}

	echo join('', $tr);
	?>
      </tr>
      <?php
      $row_counter++;
    endforeach;
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">Упаковок : <span id="total_records_counter"><?= $num_of_packs ?></span></td>
    </tr>
  </tfoot>
</table>

<?php echo $this->pagination->create_links(); ?>


<script>
  document.body.addEventListener('click', clickActionListener, false);
  function clickActionListener(e) {
    e = (!e) ? window.event : e;
    if (e.target.hasAttribute('pack-id')) {

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'delete') {
	delete_pack_by_id(e.target);
      }

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'published') {
	toggle_published_item_by_id(e.target)
      }

    }
  }

  window.onload = function () {
    if (document.getElementById('items_table')) {
      document.getElementById('items_table').addEventListener('mouseover', tableMouseOverListener, false);
    }
  }

</script>