<h3>Упаковка товара: <?= $product_name ?>: предварительный просмотр</h3>
<div class="small">
  <p>Название : <?= $name ?></p>
  <p>Штук товара в упаковке : <?= $num_of_items ?> шт.</p>
  <p>Цена упаковки: <?= $price ?> руб.</p>
  <p>Кол-во : <?= $quantity ?> шт.</p>
</div>
<hr>
<img src="/<?= config_item('_packs_image_path_url') . $img; ?>" alt="<?= $name; ?>">
<hr>
<h4>Содержимое : предварительный просмотр</h4>

<div class="blog-article">
  <?= $html; ?>
</div>