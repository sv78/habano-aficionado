<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div class="row">
  <div class="col-lg-6">
    <h1><?= $title ?> </h1>
    <h4>Упаковки этого товара</h4>
  </div>

  <div class="col-lg-6">
    <?php if (isset($product_img)): ?>
      <img class="img-responsive" src="/<?= config_item('_products_image_path_url') . $product_img; ?>">
    <?php endif; ?>
  </div>
</div>




<p class="text-right small">
  <a href="<?= current_url() ?>">Упаковки этого товара</a>
  <?php if ($product_used_by_user_id === NULL): ?>
    | <a href="/admin/create-pack-of-product/<?= $product_id; ?>">Создать еще упаковку для этого товара</a>
  <?php endif; ?>

  | <a href="/admin/packs">Все упаковки</a>
  | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить</a>
</p>