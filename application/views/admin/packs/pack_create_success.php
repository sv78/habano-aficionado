<h1 class="text-success">Получилось!</h1>
<h4>Вы создали упаковку!</h4>
<div class="admin-input-title">
  <p><button class="btn btn-sm" onclick="location.assign(location.href);">Создать еще упаковку для этого товара</button></p>
  <p class="text-left"><a href="/admin/packs">Перейти к списку упаковок</a></p>
</div>