<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1>Все упаковки</h1>

<p class="text-right small">
  <a href="/admin/products">Товары</a>
  | <a href="/admin/packs">Упаковки</a>
  | <a href="<?= filter_input(INPUT_SERVER, 'REQUEST_URI'); ?>">Обновить страницу</a>
</p>