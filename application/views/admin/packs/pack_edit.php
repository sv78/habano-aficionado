<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//echo "Type of update : " . $this->input->post('submit') . "<br>Modified fields: " . $this->input->post('modified_fields');
?>

<!-- page nav -->
<p class="text-right small">
  <a href="<?= $this->back_url->get_parent_url() ?>">Вернуться</a>
  | <a href="/admin/pack-preview/<?= $item['id'] ?>">Просмотр</a>
  | <a href="/admin/pack-preview/<?= $item['id'] ?>" target="_blank">Просмотр в новой вкладке</a>
</p>
<!-- / page nav -->

<h4>Редактирование упаковки товара <?= $item['product_name'] ?></h4>
<p class="small text-muted">Внимание! Будте внимательны создавая, редактируя и сохраняя контент! Неосознанные действия могут привести к потере данных, а также выходу системы из строя.</p>
<h1><?= $title ?></h1>



<!-- new image upload via iframe -->

<?php
if ($item['img'] == NULL OR $item['img'] == '') {
  $img_src = "/" . config_item('_pack_not_specified_image_url');
} else if (!file_exists(config_item('_packs_image_path') . $item['img'])) {
  $img_src = "/" . config_item('_pack_missing_image_url');
} else {
  $img_src = "/" . config_item('_packs_image_path_url') . $item['img'];
}
?>

<p class="admin-input-title">Изображение упаковки</p>
<img id="uploaded_image_tag" class="thumbnail" src="<?= $img_src ?>" alt="<?= $title ?>">
<form action="/admin/upload-image" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="new_file_form_submitted(event)">
  <input type="hidden" name="id" value="<?= $item['id'] ?>">
  <input type="hidden" name="table" value="packs">
  <input id="img_tag" class="hidden" type="file" name="img" onchange="file_input_changed(event)">
  <div>
    <p>
      <label for="img_tag" class="btn btn-sm btn-default">Изменить изображение</label>
      <button type="submit" name="submit" id="file_submit_btn" class="hidden btn btn-sm btn-primary">Загрузить</button>
      <strong id="img_filename_holder" class="small"></strong>
    </p>
  </div>
</form>
<p id="img_filename_loading" class="small text-primary hidden">Loading…</p>
<iframe class="hidden" name="upload_target" src="/admin/empty-iframe"></iframe>
<p class="small text-info">Внимание! Загружаемые файлы должны являться только изображениями. Загрузка других типов файлов недопустима. Размер Файла должен быть не более <?php echo round(config_item('_pack_image_upload_config')['max_size'] / 1024) . 'Mb' ?>.</p>

<script>
  function new_file_form_submitted(e) {
    $('#file_submit_btn').addClass('hidden');
    $('#img_filename_holder').html('');
    $('#img_filename_loading').removeClass('hidden');
  }
  function file_input_changed(e) {
    var fakefilepath = e.target.value;
    var ffp_arr = fakefilepath.split('\\');
    var filename = ffp_arr[ffp_arr.length - 1];
    $('#img_filename_holder').html(filename);
    if (e.target.value !== '') {
      $('#file_submit_btn').removeClass('hidden');
    } else {
      $('#file_submit_btn').addClass('hidden');
    }
  }
</script>





<?php
$form_attributes = array('id' => 'pack_edit_form');
echo form_open_multipart('', $form_attributes);
?>




<!-- name -->
<p class="admin-input-title text-primary">Имя (название) упаковки *</p>
<input id="pack_name_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="name" maxlength="255" autocomplete="off" value="<?php
echo (!isset($item['name'])) ? set_value('name') : $item['name'];
?>" onchange="formChangeEvent(window.event)">
       <?php echo form_error('name') ?>





<!-- published checkbox -->
<?php
$pbl_value = !isset($item['published']) ? set_value('published') : $item['published'];
$pbl_checked = ($pbl_value == 1 OR $pbl_value == 'on') ? ' checked' : '';
?>
<div class="admin-input-title">
  <div class="checkbox">
    <label for="published_chkbx">
      <input type="checkbox" id="published_chkbx" name="published" <?= $pbl_checked ?> onchange="formChangeEvent(window.event)"> Публиковать
    </label>
  </div>
  <?php echo form_error('published') ?>
</div>




<!-- quantity -->
<p class="admin-input-title">Продукта штук в упаковке:</p>
<input class="form-control admin-input-number" type="number" name="num_of_items" min="0" max="9999" onblur="trim_input_field(event)" onchange="formChangeEvent(event)" autocomplete="off" value="<?php echo (!isset($item['num_of_items'])) ? set_value('num_of_items') : $item['num_of_items']; ?>">


<!-- price -->
<p class="admin-input-title">Цена за упаковку:</p>
<input class="form-control admin-input-number" type="number" name="price" min="0" max="16777215" onblur="trim_input_field(event)" onchange="formChangeEvent(event)" autocomplete="off" value="<?php echo (!isset($item['price'])) ? set_value('price') : $item['price']; ?>">


<!-- quantity -->
<p class="admin-input-title">Кол-во:</p>
<input class="form-control admin-input-number" type="number" name="quantity" min="0" max="9999" onblur="trim_input_field(event)" onchange="formChangeEvent(event)" autocomplete="off" value="<?php echo (!isset($item['quantity'])) ? set_value('quantity') : $item['quantity']; ?>">



<!-- html -->
<p class="admin-input-title">Контент</p>
<textarea name="html"><?php
  echo (!isset($item['html'])) ? set_value('html') : $item['html'];
  ?></textarea>





<!-- submit -->
<div class="admin-input-title">
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save') ?>">Сохранить</button>
  <button class="btn btn-success" name="submit" value="<?= config_item('_submit_save_and_close') ?>">Сохранить и закрыть</button>
</div>

<input id="modified_fields_input" type="hidden" name="modified_fields" value="<?php echo isset($modified_fields) ? $modified_fields : ''; ?>">

</form>

<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);">Перезагрузить</button>
</div>
























<!-- page nav -->
<p class="text-right small">
  <a href="<?= $this->back_url->get_parent_url() ?>">Вернуться</a>
  | <a href="/admin/pack-preview/<?= $item['id'] ?>">Просмотр</a>
  | <a href="/admin/pack-preview/<?= $item['id'] ?>" target="_blank">Просмотр в новой вкладке</a>
</p>
<!-- / page nav -->



<script src="/assets/js/ckeditor_4.6.1_full/ckeditor/ckeditor.js"></script>

<script>
    // array of input field names that was changed by user (also values that was changed got from server side in hidden field)
    var formChangedInputs = [] = document.getElementById('modified_fields_input').value.split(',');
    formChangedInputs = array_remove_empty_values(formChangedInputs);
    //console.log(formChangedInputs);


    var ck_html = CKEDITOR.replace('html', {
      customConfig: 'config_admin.js'
    });

    // checking CKEDITOR for changes
    ck_html.on('change', function (evt) {
      // getData() returns CKEditor's HTML content.
      formChangedInputs.push(evt.editor.name);
    });

    // fires if form inputs changes (CKEditor event is checking too but separately)
    function formChangeEvent(e) {
      formChangedInputs.push(e.target.name);
    }

    // on form submit
    document.getElementById('pack_edit_form').onsubmit = function (e) {
      var modifiedInputs = array_remove_duplicates(formChangedInputs);
      formChangedInputs = [];
      if (modifiedInputs !== false) {
	document.getElementById('modified_fields_input').value = modifiedInputs.join(',');
      }
    };


</script>