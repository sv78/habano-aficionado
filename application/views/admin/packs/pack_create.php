<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
if ($product_used_by_user_id !== NULL) {
  echo 'Unable to create pack for this product!<br>It needs to be saved and closed properly before.';
  return;
}
?>


<h1>Создать упаковку</h1>
<h5>Для <strong><?= $product_name ?></strong></h5>

<img src="/<?= config_item('_products_image_path_url') . $product_img; ?>" alt="image of product here">

<?php
$form_attributes = array('id' => 'pack_create_form');
echo form_open_multipart('', $form_attributes);
?>




<p class="admin-input-title text-primary">Имя (название) упаковки *</p>
<input id="pack_name_input" onblur="trim_input_field(event)" class="form-control admin-input" type="text" name="name" maxlength="255" autocomplete="off" value="<?php echo set_value('name'); ?>">
<?php echo form_error('name') ?>







<div class="admin-input-title">
  <button class="btn btn-success" type="submit" value="submit">Сохранить</button>
</div>

</form>

<div class="admin-input-title">
  <button class="btn btn-sm btn-danger" id="page_reload_btn" onclick="location.assign(location.href);">Перезагрузить</button>
</div>
