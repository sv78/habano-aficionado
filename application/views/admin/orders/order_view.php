<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<p class="text-left small">
  <a href="<?= $this->back_url->get_parent_url() ?>">&lt;&lt; Назад к заявкам</a>
</p>

<div class="text-center">
  <h2>Заявка <span class="small">от <?= date('d M Y - H:i:s', $order['time']); ?></span></h2>
  <hr>
  <h4><strong class="text-primary"><?= config_item('_requests_phone_country_code') . $order['phone'] ?></strong></h4>
  <?php
  if ($order['name'] != NULL && $order['name'] != ''):
    ?>
    <p>Имя<br><strong><?= $order['name'] ?></strong></p>
    <?php
  endif;
  ?>

  <?php
  if ($order['address'] != NULL && $order['address'] != ''):
    ?>
    <p>Адрес<br><strong><?= $order['address'] ?></strong></p>
    <?php
  endif;
  ?>

  <p>Email<br><strong><?= $customer->email ?></strong></p>
  <p class="small"><a href="/admin/users/<?= $order['user_id']; ?>">Инфо о заказчике</a></p>
  <hr>

  <?php
  $total_sum = 0;
  if (count($products_and_amounts) > 0) {
    foreach ($products_and_amounts as $pra):
      $product = $pra[0];
      $amount = $pra[1];
      ?>
      <h3><?= Baza::decode_plain_string_from_db($product['name']); ?></h3>
      <p class="small"><a href="/catalog/<?= $product['category_slug'] . '/' . $product['slug']; ?>">Ссылка в каталоге</a></p>
      <?php
      $img_class_name = $product['is_cigar_product'] == 1 ? 'admin-order-cigar-view-img' : 'admin-order-product-view-img';
      ?>
      <img class="<?= $img_class_name ?>" src="/<?= config_item('_products_image_path_url') . Baza::decode_plain_string_from_db($product['img']) ?>" alt="<?= Baza::decode_plain_string_from_db($product['name']) ?>">

      <p>Цена: <?= $product['price'] ?> руб.</p>
      <p>Заказано штук: <?= $amount ?></p>

      <?php
      $item_sum = $amount * $product['price'];
      $total_sum += $item_sum;
      ?>
      <p>Итого за позицию: <strong class="text-primary"><?= $item_sum ?> руб.</strong></p>

      <hr>
      <?php
    endforeach;
  }



  if (count($packs_and_amounts) > 0) {
    foreach ($packs_and_amounts as $pca):
      $pack = $pca[0];
      $amount = $pca[1];
      ?>
      <h3><?= Baza::decode_plain_string_from_db($pack['name']); ?></h3>
      <p class="small">Производная от <strong><?= $pack['product_name'] ?></strong></p>
      <p class="small"><a href="/catalog/<?= $pack['category_slug'] . '/' . $pack['product_slug']; ?>">Ссылка в каталоге</a></p>
      <?php
      $img_class_name = 'admin-order-product-view-img';
      ?>
      <img class="<?= $img_class_name ?>" src="/<?= config_item('_packs_image_path_url') . Baza::decode_plain_string_from_db($pack['img']); ?>" alt="<?= Baza::decode_plain_string_from_db($pack['name']) ?>">

      <p>Цена: <?= $pack['price'] ?> руб.</p>
      <p>Заказано штук: <?= $amount ?></p>

      <?php
      $item_sum = $amount * $pack['price'];
      $total_sum += $item_sum;
      ?>
      <p>Итого за позицию: <strong class="text-primary"><?= $item_sum ?> руб.</strong></p>

      <hr>
      <?php
    endforeach;
  }
  ?>
  <h3>Итого: <strong class="text-success"><?= $total_sum ?> руб.</strong> </h3>

</div>