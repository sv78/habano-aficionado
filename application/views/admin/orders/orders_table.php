<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<table class="table table-responsive table-hover" id="items_table">
  <thead>
    <tr>
      <th title="Номер">#</th>
      <th title="Время поступления заявки">Time</th>
      <th title="Телефон, оставленный заказчиком">Phone</th>
      <th title="Имя, оставленное заказачиком">Name</th>
      <th title="Адрес, оставленный заказчиком">Address</th>
      <?php
      if (!empty($this->session->userdata('user')['is_super_admin']) && $this->session->userdata('user')['is_super_admin'] == 1) {
	echo '<th title="Удалить заявку">Del</th>';
      }
      ?>
    </tr>
  </thead>  
  <tbody>
    <?php
    foreach ($orders as $order) :
      $tr = [];
      $tr_class_attr = $order['is_new'] == 1 ? 'class="success"' : '';
      ?>
      <tr <?= $tr_class_attr ?>>
	<?php
	$tr[] = '<td>' . $row_counter . '</td>';


	// time of order comes
	$tr[] = '<td>';
	$tr[] = '<a href="/admin/orders/' . $order['id'] . '">';
	$tr[] = date('Y.m.d H:i', $order['time']);
	$tr[] = '</a>';
	$tr[] = '</td>';


	// phone of customer
	$tr[] = '<td>' . config_item('_requests_phone_country_code') . $order['phone'] . '</td>';



	// mark name of customer
	$tr[] = '<td>';
	if ($order['name'] != NULL && $order['name'] != '') {
	  $tr[] = '<span class="small glyphicon glyphicon-option-horizontal" aria-hidden="true"></span>';
	}
	$tr[] = '</td>';


	// mark address of customer
	$tr[] = '<td>';
	if ($order['address'] != NULL && $order['address'] != '') {
	  $tr[] = '<span class="small glyphicon glyphicon-option-horizontal" aria-hidden="true"></span>';
	}
	$tr[] = '</td>';


	if (!empty($this->session->userdata('user')['is_super_admin']) && $this->session->userdata('user')['is_super_admin'] == 1) {
	  $tr[] = '<td>';
	  $tr[] = '<span class="cursor-pointer glyphicon glyphicon-remove" aria-hidden="true" action-type="delete" order-id="' . $order['id'] . '"></span>';
	  $tr[] = '</td>';
	}



	echo join('', $tr);
	?>
      </tr>
      <?php
      $row_counter++;
    endforeach;
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="20">Заявок : <span id="total_records_counter"><?= $num_of_orders ?></span></td>
    </tr>
  </tfoot>
</table>

<?php echo $this->pagination->create_links(); ?>


<script>
  document.body.addEventListener('click', clickActionListener, false);
  function clickActionListener(e) {
    e = (!e) ? window.event : e;
    if (e.target.hasAttribute('order-id')) {

      if (e.target.hasAttribute('action-type') && e.target.getAttribute('action-type') === 'delete') {
	delete_order_by_id(e.target);
      }

    }
  }

  window.onload = function () {
    if (document.getElementById('items_table')) {
      document.getElementById('items_table').addEventListener('mouseover', tableMouseOverListener, false);
    }
  }

</script>