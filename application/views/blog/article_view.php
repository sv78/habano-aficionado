<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?= $before_body ?>
<div class="container side-padding">
  <?= $body ?>
</div>
<?= $after_body ?>
