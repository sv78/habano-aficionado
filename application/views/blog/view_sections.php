<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
  <h1>Блог о&nbsp;сигарах, дегустациях, табаководстве<br>и&nbsp;актуальных событиях</h1>

  <!-- надпись параметры поиска перечеркнутый линией 1 -->
  <div class="lined-text-small-container">
    <div class="line-text-line"></div>
    <div class="line-text-text">Разделы:</div>
  </div>

  <?php
  if (count($sections) < 1):
    ?>
    <h4 class="text-center">Разделы отсутствуют на данной странице</h4>
    <p class="text-center"><a href="<?= current_url() ?>">Блог</a></p>
    <?php
  endif;
  ?>



  <div class="row blog-anounce-cards">

    <?php
    foreach ($sections as $section):

      if ($section['img'] == '') {
	$img_src = "/" . config_item('_blog_not_specified_image_url');
      } else if (!file_exists(config_item('_blog_section_image_path') . $section['img'])) {
	$img_src = "/" . config_item('_blog_missing_image_url');
      } else {
	$img_src = "/" . config_item('_blog_section_image_path_url') . $section['img'];
      }
      ?>
      <div class="blog-square-cell column col-lg-3 col-sm-6 col-xs-12">
        <a href="<?= current_url() . "/" . $section['slug'] ?>">
  	<img src="<?= $img_src ?>" alt="<?= $section['title'] ?>" class="img-responsive">
        </a>
        <h3><a href="<?= current_url() . "/" . $section['slug'] ?>"><?= Baza::decode_plain_string_from_db($section['title']) ?></a></h3>
	<?php
	if ($section['intro'] != '') {
	  $intro_str = mb_substr(Baza::decode_plain_string_from_db($section['intro']), 0, config_item('_blog_section_intro_length')) . config_item('_blog_text_further_item');
	} else {
	  $intro_str = '';
	}
	?>
        <p class="small"><a href="<?= current_url() . "/" . $section['slug'] ?>"><?= $intro_str ?></a></p>
      </div>

      <?php
    endforeach;
    ?>

  </div>


  <div class="pagination">
    <?= $this->pagination->create_links() ?>
  </div>

</div>