<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?= $before_body ?>

<div class="container side-padding">
  <?= $body ?>
</div>

<?= $after_body ?>

<div class="container">

  <?php
  foreach ($articles as $article):
    if ($article['img'] == '') {
      $img_src = "/" . config_item('_blog_not_specified_image_url');
    } else if (!file_exists(config_item('_blog_article_image_path') . $article['img'])) {
      $img_src = "/" . config_item('_blog_missing_image_url');
    } else {
      $img_src = "/" . config_item('_blog_article_image_path_url') . $article['img'];
    }
    ?>

    <div class="row blog-announce-row-horizontal">

      <div class="column col-lg-2 col-md-3 col-xs-12">
        <a href="<?= current_url() . "/" . $article['slug'] ?>">
  	<img src="<?= $img_src ?>" alt="<?= $article['title'] ?>">
        </a>
      </div>


      <div class="column col-lg-10 col-md-9 col-xs-12"> 
        <h3>
  	<a href="<?= current_url() . "/" . $article['slug'] ?>">
	    <?= Baza::decode_plain_string_from_db($article['title']) ?>
  	</a>
        </h3>
        <p><?= mb_substr(Baza::decode_plain_string_from_db($article['intro']), 0, config_item('_blog_article_intro_length')) . config_item('_blog_text_further_item') ?></p>
        <p class="article-date">От <?= $article['created_at'] ?></p>
        <a class="blog-read-more" href="<?= current_url() . "/" . $article['slug'] ?>">Читать далее</a>
      </div>


    </div>
    <?php
  endforeach;
  ?>


  <?php
  if (count($articles) < 1 && !empty($this->input->get('page'))): // page это счетчик страниц, но здесь он в виде hardcode !!!
    ?>
    <h4 class="text-center">Статьи отсутствуют на данной странице раздела</h4>
    <p class="text-center"><a href="<?= current_url() ?>">В начало раздела</a></p>
    <?php
  endif;
  ?>


  <div class="pagination">
    <?= $this->pagination->create_links() ?>
  </div>

</div>