<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!$brand) {
  return;
}
?>
<div class="container side-padding">
  <p class="text-center">
    <a href="/brands/<?= Baza::decode_plain_string_from_db($brand['slug']); ?>">Смотреть все сигары марки <?= Baza::decode_plain_string_from_db($brand['name']) ?></a>
  </p>

</div>