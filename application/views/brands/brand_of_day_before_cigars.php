<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!$brand) {
  return;
}

if ($brand['img'] == '') {
  $img_src = "/" . config_item('_brand_not_specified_image_url');
} else if (!file_exists(config_item('_brands_image_path') . $brand['img'])) {
  $img_src = "/" . config_item('_brand_missing_image_url');
} else {
  $img_src = "/" . config_item('_brands_image_path_url') . $brand['img'];
}
?>



<div class="container side-padding">

  <h3 class="text-center"><?= Baza::decode_plain_string_from_db($brand['name']) ?></h3>
  <img class="center-block" src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($brand['name']) ?>">
  
  <div class="lined-text-header-container">
    <div class="line-text-line"></div>
    <div class="line-text-text">Марка дня</div>
  </div>

</div>