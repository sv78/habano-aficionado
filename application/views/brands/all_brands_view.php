<?php
defined('BASEPATH') OR exit('No direct script access allowed');


if (!$brands OR count($brands) == 0) {
  return;
}
?>

<div class="container side-padding">

  <h1><span class="color-ha-default">Марки</span> <span class="color-ha-light">Сигар</span></h1>

  <h3 class="text-center uppercase smaller">У нас представлены только лучшие и&nbsp;оригинальные кубинские бренды</h3>

  <div class="row brands-collection-container">

    <?php
    foreach ($brands as $brand):
      ?>
      <div class="brand-container column col-lg-3 col-sm-6 col-xs-12">
        <div class="brand-img-wrapper">
  	<a href="/brands/<?= Baza::decode_plain_string_from_db($brand['slug']); ?>">
	    <?php
	    if ($brand['img'] == '') {
	      $img_src = "/" . config_item('_brand_not_specified_image_url');
	    } else if (!file_exists(config_item('_brands_image_path') . $brand['img'])) {
	      $img_src = "/" . config_item('_brand_missing_image_url');
	    } else {
	      $img_src = "/" . config_item('_brands_image_path_url') . $brand['img'];
	    }
	    ?>
  	  <img src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($brand['name']) ?>">
  	</a>
        </div>
        <p>
  	<a href="/brands/<?= Baza::decode_plain_string_from_db($brand['slug']); ?>"><?= Baza::decode_plain_string_from_db($brand['name']) ?></a>
        </p>
      </div>
      <?php
    endforeach;
    ?>

  </div>

  <script>
    document.body.style.backgroundColor = "#181818";
  </script>

</div>