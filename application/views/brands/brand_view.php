<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ($brand['img'] == '') {
  $img_src = "/" . config_item('_brand_not_specified_image_url');
} else if (!file_exists(config_item('_brands_image_path') . $brand['img'])) {
  $img_src = "/" . config_item('_brand_missing_image_url');
} else {
  $img_src = "/" . config_item('_brands_image_path_url') . $brand['img'];
}
?>

<div class="container side-padding">
  
  <h1><?= Baza::decode_plain_string_from_db($brand['name'])?></h1>

  <div class="brand-container">
    <div class="brand-img-wrapper">
      <img class="center-block" src="<?= $img_src ?>" alt="<?= Baza::decode_plain_string_from_db($brand['name']) ?>">
    </div>
  </div>

</div>

<div class="container side-padding">
  <?= Baza::decode_html_string_from_db($brand['html'])?>
</div>
