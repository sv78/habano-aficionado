<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {

  public function get_product_by_category_slug_and_product_slug($category_slug = NULL, $product_slug = NULL) {
    $sql = 'SELECT DISTINCT products.*, '
	    . 'brands.id AS brand_id, '
	    . 'brands.slug AS brand_slug, '
	    . 'brands.name AS brand_name, '
	    . 'brands.country AS brand_country, '
	    . 'categories.id AS category_id, '
	    . 'categories.slug AS category_slug, '
	    . 'categories.name AS category_name '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'WHERE products.slug = "' . $product_slug . '" '
	    . 'AND categories.slug = "' . $category_slug . '" '
	    . 'AND products.published = 1 '
	    . 'AND categories.published = 1 '
	    . 'AND brands.published = 1';
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function get_packs_by_product_id($product_id = NULL) {
    $this->db->where('product_id', $product_id);
    $this->db->where('published', 1);
    $query = $this->db->get('packs');
    return $query->result_array();
  }

  public function count_search_results_by_params($params = NULL) {
    $sql = 'SELECT DISTINCT COUNT(products.id) AS num_of_products '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'WHERE products.is_cigar_product = 1 '
	    . 'AND products.published = 1 '
	    . 'AND categories.published = 1 '
	    . 'AND brands.published = 1 ';

    if ($params['flt_country'] != NULL && $params['flt_country'] != -1) {
      $sql .= 'AND brands.country = ' . $params['flt_country'] . ' ';
    }
    if ($params['flt_brand_id'] != NULL && $params['flt_brand_id'] != -1) {
      $sql .= 'AND products.brand_id = ' . $params['flt_brand_id'] . ' ';
    }
    if ($params['flt_vitola'] != NULL && $params['flt_vitola'] != -1) {
      $sql .= 'AND products.vitola = ' . $params['flt_vitola'] . ' ';
    }
    if ($params['flt_line'] != NULL && $params['flt_line'] != -1) {
      $sql .= 'AND products.line = ' . $params['flt_line'] . ' ';
    }
    if ($params['flt_length'] != NULL && $params['flt_length'] != -1) {
      $sql .= 'AND products.length = ' . $params['flt_length'] . ' ';
    }
    if ($params['flt_cepo'] != NULL && $params['flt_cepo'] != -1) {
      $sql .= 'AND products.cepo = ' . $params['flt_cepo'] . ' ';
    }
    if ($params['flt_intensity'] != NULL && $params['flt_intensity'] != -1) {
      $sql .= 'AND products.intensity = ' . $params['flt_intensity'] . ' ';
    }
    if ($params['flt_wrapper'] != NULL && $params['flt_wrapper'] != -1) {
      $sql .= 'AND products.wrapper = ' . $params['flt_wrapper'] . ' ';
    }
    if ($params['flt_flavour'] != NULL && $params['flt_flavour'] != -1) {
      $sql .= 'AND products.flavour = ' . $params['flt_flavour'] . ' ';
    }
    if ($params['flt_price_max'] != NULL && $params['flt_price_max'] != -1) {
      $sql .= 'AND products.price <= ' . $params['flt_price_max'] . ' ';
    }

    $query = $this->db->query($sql);
    return $query->row()->num_of_products;
  }

  public function get_cigars_by_filter_params($params = NULL) {

    $sql = 'SELECT products.*, '
	    . 'categories.name AS category_name, '
	    . 'categories.slug AS category_slug, '
	    . 'brands.name AS brand_name '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'WHERE is_cigar_product = 1 '
	    . 'AND products.published = 1 '
	    . 'AND categories.published = 1 '
	    . 'AND brands.published = 1 ';

    // country where
    if ($params['flt_country'] != '' && $params['flt_country'] != -1) {
      $sql .= 'AND brands.country = ' . $params['flt_country'] . ' ';
    }
    // brand id where
    if ($params['flt_brand_id'] != '' && $params['flt_brand_id'] != -1) {
      $sql .= 'AND products.brand_id = ' . $params['flt_brand_id'] . ' ';
    }
    // vitola where
    if ($params['flt_vitola'] != '' && $params['flt_vitola'] != -1) {
      $sql .= 'AND products.vitola = ' . $params['flt_vitola'] . ' ';
    }
    // line where
    if ($params['flt_line'] != '' && $params['flt_line'] != -1) {
      $sql .= 'AND products.line = ' . $params['flt_line'] . ' ';
    }
    // length where
    if ($params['flt_length'] != '' && $params['flt_length'] != -1) {
      $sql .= 'AND products.length = ' . $params['flt_length'] . ' ';
    }
    // cepo where
    if ($params['flt_cepo'] != '' && $params['flt_cepo'] != -1) {
      $sql .= 'AND products.cepo = ' . $params['flt_cepo'] . ' ';
    }
    // intensity where
    if ($params['flt_intensity'] != '' && $params['flt_intensity'] != -1) {
      $sql .= 'AND products.intensity = ' . $params['flt_intensity'] . ' ';
    }
    // wrapper where
    if ($params['flt_wrapper'] != '' && $params['flt_wrapper'] != -1) {
      $sql .= 'AND products.wrapper = ' . $params['flt_wrapper'] . ' ';
    }
    // flavour where
    if ($params['flt_flavour'] != '' && $params['flt_flavour'] != -1) {
      $sql .= 'AND products.flavour = ' . $params['flt_flavour'] . ' ';
    }
    // price max where
    if ($params['flt_price_max'] != '' && $params['flt_price_max'] != -1) {
      $sql .= 'AND products.price <= ' . $params['flt_price_max'] . ' ';
    }

    $sql .= 'ORDER BY '
	    . 'products.featured DESC, '
	    . 'products.name ASC ';

    $limit = $params['cigars_at_once'];

    $offset = $params['current_request_number'] == 1 ? 0 : $limit * ($params['current_request_number'] - 1);

    $sql .= "LIMIT $limit OFFSET $offset ";

    //return $sql;
    
    $query = $this->db->query($sql);
    return $query->result_array();
  }

}
