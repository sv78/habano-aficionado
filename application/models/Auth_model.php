<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// class
class Auth_model extends CI_Model {

  public function get_user_by_email($email) {
    $this->db->where('email', $email);
    $query = $this->db->get('users');
    if (count($query->result()) == 0) {
      return FALSE;
    }
    return $query->row_array();
  }
  
  public function get_user_by_id($uid) {
    $this->db->where('id', $uid);
    $query = $this->db->get('users');
    if (count($query->result()) == 0) {
      return FALSE;
    }
    return $query->row_array();
  }

  public function block_ip($ip) {
    $this->db->where('ip', $ip);
    $this->db->update('ips', array('block_time' => time()));
  }

  public function get_ip_data($ip) {
    $this->db->where('ip', $ip);
    $query = $this->db->get('ips');
    if (count($query->result()) == 0) {
      return FALSE;
    }
    //$this->ip_data = $query->row_array();
    return $query->row_array();
  }

  public function set_or_update_ip_to_db($ip) {
    $ip_data = $this->get_ip_data($ip);

    //если новая запись в базу
    if (!$ip_data) {
      $this->db->insert('ips', array(
	  'ip' => $ip,
	  'access_time' => time()
	      //'access_attempt_counter' => 1 // db default settings
      ));
      return;
    }

    // если ip уже существует в базе

    if ($ip_data['block_time'] != NULL) {
      return;
    }

    if ($ip_data['access_attempt_counter'] >= config_item('_ip_login_access_attempt_max_number')) {
      $this->block_ip($ip);
      return;
    }

    $this->db->where('ip', $ip);
    $this->db->update('ips', array(
	'access_time' => time(),
	'access_attempt_counter' => $ip_data['access_attempt_counter'] + 1
    ));
    //$this->db->trans_start();
    //$this->db->trans_complete();
  }

  public function update_old_temp_pwds_to_null() {
    $expired = time() - config_item('_rst_pwd_conf_time_limit');
    $this->db->where('temp_password !=', NULL);
    $this->db->where('rst_pwd_time < ', $expired);
    $this->db->update('users', array('temp_password' => NULL, 'reg_confirm_code' => NULL));
  }

  public function set_temp_password($db_data = NULL) {
    if ($db_data === NULL) {
      echo "<br>DB error!";
      return false;
    }
    $data = array(
	'temp_password' => $db_data['temp_password'],
	'reg_confirm_code' => $db_data['reg_confirm_code'],
	'rst_pwd_time' => time()
    );

    //$this->db->trans_start(TRUE); // test mode if TRUE as argument
    $this->db->trans_start();
    $this->db->where('id', $db_data['user_id']);
    $res = $this->db->update('users', $data);
    $this->db->trans_complete();
    //echo $this->db->trans_status() ? "ok<br>" : "bad<br>";
    return (bool) $res;
  }

  public function set_user_registration($dbdata = NULL) {
    if ($dbdata === NULL) {
      echo "<br>DB error!";
      return false;
    }

    $data = array(
	'email' => $dbdata['email'],
	'password' => $dbdata['password'],
	'reg_confirm_code' => $dbdata['reg_confirm_code'],
	'reg_time' => time()
    );

    //$this->db->trans_start(TRUE); // test mode if TRUE as argument
    $this->db->trans_start();
    $res = $this->db->insert('users', $data);
    $this->db->trans_complete();
    //echo $this->db->trans_status() ? "ok<br>" : "bad<br>";
    return (bool) $res;
  }

  public function get_all_unconfirmed_users() {
    $this->db->where('reg_confirmed', 0);
    $query = $this->db->get('users');
    return $query->result_array();
  }

  public function get_all_pwd_resets() {
    $this->db->select('id, reg_confirm_code');
    $this->db->where('temp_password !=', NULL);
    $this->db->from('users');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function set_reg_confirmation($uid = NULL) {
    $data = array(
	'reg_confirmed' => 1,
	'reg_confirm_code' => NULL
    );
    $this->db->trans_start();
    $this->db->where('id', $uid);
    $res = $this->db->update('users', $data);
    $this->db->trans_complete();
    return (bool) $res;
  }
  
  public function set_new_password($uid = NULL) {
    $user_data = $this->get_user_by_id($uid);
    $data = array(
	'password' => $user_data['temp_password'],
	'temp_password' => NULL,
	'reg_confirm_code' => NULL,
    );
    $this->db->trans_start();
    $this->db->where('id', $uid);
    $res = $this->db->update('users', $data);
    $this->db->trans_complete();
    return (bool) $res;
  }

}

// end of class 