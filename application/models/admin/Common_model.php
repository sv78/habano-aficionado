<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model {

  public function set_item_used_by_user($table = null, $item_id = null, $user_id = null) {
    $this->db->where('id', $item_id);
    $res = $this->db->update($table, array('used_by_user_id' => $user_id));
    return $res;
  }

  public function unset_item_used_by_user($table = null, $item_id = null, $user_id = null) {
    $this->db->where('id', $item_id);
    $res = $this->db->update($table, array('used_by_user_id' => NULL));
    return $res;
  }

  public function update_image($table, $id, $img_file_name) {
    $data = array(
	'img' => $img_file_name
    );
    $this->db->where('id', $id);
    $res = $this->db->update($table, $data);
    return $res;
  }

  public function get_published_by_id($table = NULL, $id = NULL) {
    $query = $this->db->get_where($table, array('id' => $id));
    if (!isset($query->row()->published)) {
      return FALSE;
    }
    return $query->row()->published;
  }

  public function invert_published($table = NULL, $id = NULL) {
    $published_current = $this->get_published_by_id($this->input->post('table'), $this->input->post('id'));
    $published_new = intval($published_current) === 0 ? 1 : 0;
    $data = array('published' => $published_new);
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update($table, $data);
    $this->db->trans_complete();
    return $published_new;
  }

  public function lift_up_item_by_id($table = NULL, $id = NULL) {
    if (!$this->db->table_exists($table)) {
      echo 'Error : table does not exist or NULL';
      return false;
    }
    if (preg_match('/[^0-9]/', $id)) {
      echo 'Error : wrong ID or NULL';
      return false;
    }
    $data = array('lifted_up_at' => time());
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update($table, $data);
    $this->db->trans_complete();
    return $res;
  }

  public function toggle_item_featured_by_id($table = NULL, $id = NULL) {
    $item = $this->get_item_by_id($table, $id);
    if (!$this->db->field_exists('featured', $table)) {
      return FALSE;
    }
    $current_featured = $item->featured;
    $new_featured = $current_featured == 0 ? 1 : 0;
    $data = array('featured' => $new_featured);
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update($table, $data);
    $this->db->trans_complete();
    return $res;
  }

  public function get_item_by_id($table = NULL, $id = NULL, $return_type = FALSE) { // return array if true, and object if false
    if (!$this->db->table_exists($table)) {
      //echo 'Error : table does not exist or NULL';
      return FALSE;
    }
    if (preg_match('/[^0-9]/', $id)) {
      //echo 'Error : wrong ID or NULL';
      return FALSE;
    }
    $this->db->where('id', $id);
    $query = $this->db->get($table);
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    if ($return_type === TRUE) {
      return $query->row_array();
    }
    return $query->row();
  }

  public function update_item_images($table = NULL, $id = NULL, $images_str = '') {
    if (!$this->db->table_exists($table)) {
      //echo 'Error : table does not exist or NULL';
      return FALSE;
    }
    if (preg_match('/[^0-9]/', $id)) {
      //echo 'Error : wrong ID or NULL';
      return FALSE;
    }
    $this->db->where('id', $id);
    $res = $this->db->update($table, array('images' => $images_str));
    return $res;
  }

  public function update_item_images_set_last($table = NULL, $id = NULL, $file_name = FALSE) {
    if (!$this->db->table_exists($table)) {
      //echo 'Error : table does not exist or NULL';
      return FALSE;
    }
    if (preg_match('/[^0-9]/', $id)) {
      //echo 'Error : wrong ID or NULL';
      return FALSE;
    }
    if ($file_name === false OR $file_name == '') {
      return FALSE;
    }
    $item = $this->get_item_by_id($table, $id, true);
    $images = unserialize(Baza::decode_plain_string_from_db($item['images']));
    $images[] = $file_name;
    $images_str = serialize($images);
    $this->db->where('id', $id);
    $res = $this->db->update($table, array('images' => $images_str));
    return $res;
  }

}
