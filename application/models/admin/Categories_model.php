<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Brands Model
 */
class Categories_model extends CI_Model {

  public function set_category($dbdata = NULL) {
    $data = array(
	'slug' => $dbdata['slug'],
	'name' => $dbdata['name'],
	'is_cigar_category' => $dbdata['is_cigar_category'],
	'meta_title' => $dbdata['meta_title'],
	'lifted_up_at' => time()
    );
    $res = $this->db->insert('categories', $data);
    return $res;
  }

  public function get_categories($limit = 0, $offset = 0) {
    $this->db->order_by('lifted_up_at', 'DESC');
    $query = $this->db->get('categories', $limit, $offset);
    return $query->result_array();
  }

  public function delete_category_by_id($id) {
    $this->db->trans_start();
    $this->db->where('id', $id);
    $this->db->delete('categories');
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  public function update_category($id = NULL, $dbdata = NULL, $fields_to_update = NULL) {
    $data = array();
    foreach ($fields_to_update as $fld) {
      $data[$fld] = $dbdata[$fld];
    }
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update('categories', $data);
    $this->db->trans_complete();
    return $res;
  }

}
