<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Brands Model
 */
class Brands_model extends CI_Model {

  public function set_brand($dbdata = NULL) {
    $data = array(
	'country' => $dbdata['country'],
	'slug' => $dbdata['slug'],
	'name' => $dbdata['name'],
	'is_cigar_brand' => $dbdata['is_cigar_brand'],
	'meta_title' => $dbdata['meta_title'],
	'lifted_up_at' => time()
    );
    $res = $this->db->insert('brands', $data);
    return $res;
  }

  public function get_brands($limit = 0, $offset = 0) {
    $this->db->order_by('lifted_up_at', 'DESC');
    $query = $this->db->get('brands', $limit, $offset);
    return $query->result_array();
  }

  public function delete_brand_by_id($id) {
    $this->db->trans_start();
    $this->db->where('id', $id);
    $this->db->delete('brands');
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  public function update_brand($id = NULL, $dbdata = NULL, $fields_to_update = NULL) {
    $data = array();
    foreach ($fields_to_update as $fld) {
      $data[$fld] = $dbdata[$fld];
    }
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update('brands', $data);
    $this->db->trans_complete();
    return $res;
  }
  
  public function check_and_fix_brand_of_day ($id) {
    $this->db->where('id',$id);
    $brand = $this->db->get('brands')->row();
    if (!$brand) {
      return;
    }
    if ($brand->is_cigar_brand == 0 && $brand->brand_of_day == 1) {
      $this->db->where('id',$id);
      $this->db->update('brands',array('brand_of_day' => NULL));
    }
  }

}
