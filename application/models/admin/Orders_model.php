<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_model extends CI_Model {

  public function get_orders($limit = 0, $offset = 0) {
    $this->db->order_by('time', 'DESC');
    $query = $this->db->get('orders', $limit, $offset);
    return $query->result_array();
  }

  public function set_order_visited($id = NULL) {
    $this->db->where('id', $id);
    $this->db->update('orders', array('is_new' => 0));
  }

  public function delete_order_by_id($id) {
    if ($this->db->delete('orders', array('id' => $id))) {
      return TRUE;
    }
    return FALSE;
  }

}
