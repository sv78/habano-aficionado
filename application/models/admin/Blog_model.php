<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Blog Model
 */
class Blog_model extends CI_Model {

  public function get_sections($limit = 0, $offset = 0) {
    $this->db->order_by('lifted_up_at', 'DESC');
    $this->db->order_by('title', 'ASC');
    $query = $this->db->get('sections', $limit, $offset);
    return $query->result_array();
  }
  
  public function update_section($id = NULL, $dbdata = NULL, $fields_to_update = NULL) {
    $data = array();
    foreach ($fields_to_update as $fld) {
      $data[$fld] = $dbdata[$fld];
    }
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update('sections', $data);
    $this->db->trans_complete();
    return $res;
  }
  
  public function update_article($id = NULL, $dbdata = NULL, $fields_to_update = NULL) {
    $data = array();
    foreach ($fields_to_update as $fld) {
      $data[$fld] = $dbdata[$fld];
    }
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update('articles', $data);
    $this->db->trans_complete();
    return $res;
  }

  public function set_section($dbdata = NULL) {
    if ($dbdata === NULL) {
      echo "<br>DB error!";
      return;
    }
    //$slug = url_title($this->input->post('title'), 'dash', TRUE);
    //echo "<h1>" . $this->input->post('slug') . "</h1>";
    //return TRUE;
    $data = array(
	'title' => $dbdata['title'],
	'slug' => $dbdata['slug'],
	'published' => $dbdata['published'],
	'intro' => $dbdata['intro'],
	'img' => $dbdata['upload_file_name'],
	'body' => $dbdata['body'],
	'meta_title' => $dbdata['meta_title'],
	'meta_description' => $dbdata['meta_description'],
	'meta_keywords' => $dbdata['meta_keywords'],
	'meta_robots' => $dbdata['meta_robots'],
	'lifted_up_at' => time()
    );
    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $data['before_body'] = $dbdata['before_body'];
      $data['after_body'] = $dbdata['after_body'];
    }

    $this->db->trans_start();
    $res = $this->db->insert('sections', $data);
    $this->db->trans_complete();
    return $res;
  }

  public function set_article($dbdata = NULL) {
    if ($dbdata === NULL) {
      echo "<br>DB error!";
      return;
    }

    $data = array(
	'section_id' => $dbdata['section_id'],
	'title' => $dbdata['title'],
	'slug' => $dbdata['slug'],
	'published' => $dbdata['published'],
	'featured' => $dbdata['featured'],
	'intro' => $dbdata['intro'],
	'img' => $dbdata['upload_file_name'],
	'body' => $dbdata['body'],
	'meta_title' => $dbdata['meta_title'],
	'meta_description' => $dbdata['meta_description'],
	'meta_keywords' => $dbdata['meta_keywords'],
	'meta_author' => $dbdata['meta_author'],
	'meta_robots' => $dbdata['meta_robots'],
	'created_at' => date("Y-m-d H:i:s"),
	'lifted_up_at' => time()
    );
    if (isset($this->session->userdata('user')['is_super_admin'])) {
      $data['before_body'] = $dbdata['before_body'];
      $data['after_body'] = $dbdata['after_body'];
    }

    //$this->db->trans_start(TRUE); // test mode if TRUE as argument
    $this->db->trans_start();
    $res = $this->db->insert('articles', $data);
    $this->db->trans_complete();
    //echo $this->db->trans_status() ? "ok<br>" : "bad<br>";
    return $res;
  }

  public function get_sections_ids_and_titles() {
    $this->db->select('id,title');
    $query = $this->db->get('sections');
    return $query->result_array();
  }

  public function section_id_by_slug($slug = NULL) {
    if ($slug === NULL OR $slug === '' OR $slug === FALSE) {
      return FALSE;
    }
    $this->db->select('id');
    $query = $this->db->get_where('sections', array('slug' => $slug, 'published' => 1));
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    return $query->row()->id;
  }

  public function get_section_by_id($id = NULL) {
    if ($id === NULL OR $id === '' OR $id === FALSE) {
      return FALSE;
    }
    $query = $this->db->get_where('sections', array('id' => $id));
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    return $query->row_array();
  }

  public function get_article_by_id($id = NULL) {
    if ($id === NULL OR $id === '' OR $id === FALSE) {
      return FALSE;
    }
    $query = $this->db->get_where('articles', array('id' => $id));
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    return $query->row_array();
  }

  /*
    public function get_published_by_id($table = NULL, $id = NULL) {
    $query = $this->db->get_where($table, array('id' => $id));
    if (!isset($query->row()->published)) {
    return FALSE;
    }
    return $query->row()->published;
    }
   */
  /*
    public function invert_published($table = NULL, $id = NULL) {
    $published_current = $this->get_published_by_id($this->input->post('table'), $this->input->post('id'));
    $published_new = intval($published_current) === 0 ? 1 : 0;
    $data = array('published' => $published_new);
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update($table, $data);
    $this->db->trans_complete();
    return $published_new;
    }
   */

  public function delete_article_by_id($id = NULL) {
    $this->db->trans_start();
    $this->db->where('id', $id);
    $this->db->delete('articles');
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  public function delete_section_by_id($id = NULL) {
    $this->db->where('section_id', $id);
    $this->db->from('articles');
    $count = $this->db->count_all_results();
    if ($count > 0) {
      return FALSE;
    }
    $this->db->trans_start();
    $this->db->where('id', $id);
    $this->db->delete('sections');
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  public function article_id_by_slug($slug = NULL) {
    if ($slug === NULL OR $slug === '' OR $slug === FALSE) {
      return FALSE;
    }
    $this->db->select('id');
    $query = $this->db->get_where('articles', array('slug' => $slug, 'published' => 1));
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    return $query->row()->id;
  }

  public function section_by_slug($slug = NULL) {
    if ($slug === NULL OR $slug === '' OR $slug === FALSE) {
      return FALSE;
    }
    $query = $this->db->get_where('sections', array('slug' => $slug, 'published' => 1));
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    return $query->row();
  }

  public function get_articles($section_id = NULL, $limit = 0, $offset = 0) {
    if ($section_id === '' || $section_id === FALSE) {
      return FALSE;
    }
    //$query = $this->db->query("SELECT * FROM articles WHERE section_id = $section_id LIMIT $limit OFFSET $offset");
    $this->db->order_by('featured', 'DESC');
    $this->db->order_by('lifted_up_at', 'DESC');
    $this->db->order_by('created_at', 'DESC');
    //$this->db->order_by('title', 'ASC');
    if ($section_id === NULL) {
      $query = $this->db->get('articles', $limit, $offset);
    } else {
      $query = $this->db->get_where('articles', array('section_id' => $section_id), $limit, $offset);
    }
    return $query->result_array();
  }

  public function get_article_by_id_and_section_id($article_id = NULL, $section_id = NULL) {
    if ($article_id === NULL OR $article_id === FALSE OR $section_id === NULL OR $section_id === FALSE) {
      return FALSE;
    }
    $query = $this->db->get_where('articles', array('id' => $article_id, 'section_id' => $section_id, 'published' => 1));
    return $query->row();
  }

}
