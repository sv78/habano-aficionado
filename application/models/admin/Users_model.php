<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

  public function get_users($limit = 0, $offset = 0) {
    $this->db->order_by('is_admin', 'DESC');
    $this->db->order_by('reg_confirmed', 'DESC');
    $this->db->order_by('reg_time', 'DESC');
    $query = $this->db->get('users', $limit, $offset);
    return $query->result_array();
  }

  public function invert_status($id = NULL) {
    $is_admin = $this->get_is_admin_by_id($id);
    $new_value = $is_admin === 1 ? 0 : 1;
    $data = array('is_admin' => $new_value);
    $this->db->trans_start();
    $this->db->where('id', $id);
    $this->db->update('users', $data);
    $this->db->trans_complete();
    return $new_value;
  }

  public function get_user_by_id($id = NULL) {
    $this->db->where('id', $id);
    $query = $this->db->get('users');
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    return $query->row();
  }

  private function get_is_admin_by_id($id = NULL) {
    $this->db->select('is_admin');
    $this->db->where('id', $id);
    $query = $this->db->get('users');
    if (!isset($query->row()->is_admin)) {
      return FALSE;
    }
    return (int) $query->row()->is_admin;
  }

  public function delete_user_by_id($id) {
    if ($this->db->delete('users', array('id' => $id))) {
      return TRUE;
    }
    return FALSE;
  }

}
