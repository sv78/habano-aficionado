<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Banners Model
 */
class Banners_model extends CI_Model {

  public function set_banner($dbdata = NULL) {
    $data = array(
	'name' => $dbdata['name']
    );
    $res = $this->db->insert('banners', $data);
    return $res;
  }

  public function get_banners($limit = 0, $offset = 0) {
    $this->db->order_by('lifted_up_at', 'DESC');
    $query = $this->db->get('banners', $limit, $offset);
    return $query->result_array();
  }
  
  public function get_published_banners() {
    $this->db->where('published', 1);
    $this->db->order_by('lifted_up_at', 'DESC');
    $query = $this->db->get('banners');
    return $query->result_array();
  }

  public function get_banner_by_id($id = NULL, $type_of_return = FALSE) {
    $this->db->where('id', $id);
    $query = $this->db->get('banners');
    if ($type_of_return) {
      return $query->result_array();
    }
    return $query->row_array();
  }

  public function delete_banner_by_id($id) {
    $this->db->trans_start();
    $this->db->where('id', $id);
    $this->db->delete('banners');
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE) {
      return FALSE;
    }
    return TRUE;
  }
  
  public function update_banner($id = NULL, $dbdata = NULL, $fields_to_update = NULL) {
    $data = array();
    foreach ($fields_to_update as $fld) {
      $data[$fld] = $dbdata[$fld];
    }
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update('banners', $data);
    $this->db->trans_complete();
    return $res;
  }

}
