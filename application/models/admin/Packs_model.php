<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Brands Model
 */
class Packs_model extends CI_Model {

  public function set_pack($dbdata = NULL) {
    $data = array(
	'product_id' => $dbdata['product_id'],
	'name' => $dbdata['name']
    );
    $res = $this->db->insert('packs', $data);
    return $res;
  }

  public function get_packs_by_product_id($product_id = NULL, $limit = 0, $offset = 0, $type_of_result = false) {
    $sql = 'SELECT packs.*, '
	    . 'products.id AS product_id, '
	    . 'products.name AS product_name, '
	    . 'products.used_by_user_id AS product_used_by_user_id '
	    . 'FROM packs '
	    . 'LEFT JOIN products '
	    . 'ON packs.product_id = products.id ';
    if ($product_id != NULL) {
      $sql .= 'WHERE packs.product_id = ' . $product_id . ' ';
    }
    $sql .= 'ORDER BY '
	    . 'product_name ASC, '
	    . 'name ASC '
	    . "LIMIT $limit OFFSET $offset ";
    $query = $this->db->query($sql);
    if ($type_of_result) {
      return $query->row_array();
    }
    return $query->result_array();
  }

  public function delete_pack_by_id($id) {
    $this->db->trans_start();
    $this->db->where('id', $id);
    $this->db->delete('packs');
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  public function get_pack_by_id($id = NULL, $type_of_return = FALSE) {
    $sql = 'SELECT packs.*, '
	    . 'products.name AS product_name, '
	    . 'products.used_by_user_id AS product_used_by_user_id '
	    . 'FROM packs '
	    . 'LEFT JOIN products '
	    . 'ON packs.product_id = products.id '
	    . 'WHERE packs.id = ' . $id;
    $query = $this->db->query($sql);
    if ($type_of_return) {
      return $query->result_array();
    }
    return $query->row_array();
  }
  
  public function get_pack_with_product_by_pack_id($id = NULL, $type_of_return = FALSE) {
    $sql = 'SELECT packs.*, '
	    . 'products.slug AS product_slug, '
	    . 'products.name AS product_name, '
	    . 'categories.slug AS category_slug '
	    . 'FROM packs '
	    . 'LEFT JOIN products '
	    . 'ON packs.product_id = products.id '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'WHERE packs.id = ' . $id;
    $query = $this->db->query($sql);
    if ($type_of_return) {
      return $query->result_array();
    }
    return $query->row_array();
  }
  
  
  public function update_pack($id = NULL, $dbdata = NULL, $fields_to_update = NULL) {
    $data = array();
    foreach ($fields_to_update as $fld) {
      $data[$fld] = $dbdata[$fld];
    }
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update('packs', $data);
    $this->db->trans_complete();
    return $res;
  }

  // ================
  // delete under all
  // ================

  private function get_brand_country_by_brand_id($brand_id) {
    $this->db->select('country');
    $this->db->where('id', $brand_id);
    $query = $this->db->get('brands');
    return $query->row()->country;
  }

  public function get_products($limit = 0, $offset = 0) {
    $sql = 'SELECT products.*, '
	    . 'categories.name AS category_name, '
	    . 'brands.name AS brand_name, '
	    . 'COUNT(packs.id) AS num_of_packs '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'LEFT JOIN packs '
	    . 'ON products.id = packs.product_id '
	    . 'GROUP BY products.id '
	    . 'ORDER BY '
	    . 'products.is_cigar_product DESC, '
	    . 'category_name ASC, '
	    . 'products.featured DESC, '
	    . 'products.name ASC '
	    . "LIMIT $limit OFFSET $offset ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function get_products_by_category_id($cat_id = NULL, $limit = 0, $offset = 0) {
    $sql = 'SELECT products.*, '
	    . 'categories.name AS category_name, '
	    . 'brands.name AS brand_name, '
	    . 'COUNT(packs.id) AS num_of_packs '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'LEFT JOIN packs '
	    . 'ON products.id = packs.product_id '
	    . 'WHERE products.category_id = ' . $cat_id . ' '
	    . 'GROUP BY products.id '
	    . 'ORDER BY '
	    . 'products.is_cigar_product DESC, '
	    . 'category_name ASC, '
	    . 'products.featured DESC, '
	    . 'products.name ASC '
	    . "LIMIT $limit OFFSET $offset ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function get_product_by_id($id = NULL, $type_of_return = FALSE) {
    $sql = 'SELECT products.*, '
	    . 'categories.name AS category_name, '
	    . 'brands.name AS brand_name, '
	    . 'brands.country AS country, '
	    . 'brands.img AS brand_img, '
	    . 'COUNT(packs.id) AS num_of_packs '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'LEFT JOIN packs '
	    . 'ON products.id = packs.product_id '
	    . 'WHERE products.id = ' . $id . ' '
	    . 'GROUP BY products.id';
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  

}
