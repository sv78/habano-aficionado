<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Brands Model
 */
class Products_model extends CI_Model {

  public function set_product($dbdata = NULL) {
    $data = array(
	'category_id' => $dbdata['category_id'],
	'brand_id' => $dbdata['brand_id'],
	'slug' => $dbdata['slug'],
	'name' => $dbdata['name'],
	'is_cigar_product' => $dbdata['is_cigar_product'],
	'meta_title' => $dbdata['meta_title'],
	'lifted_up_at' => time()
    );

    $brand_country_id = $this->get_brand_country_by_brand_id($data['brand_id']);
    if (!$brand_country_id) {
      return FALSE;
    }

    $data['country_made'] = $brand_country_id;

    $res = $this->db->insert('products', $data);
    return $res;
  }

  private function get_brand_country_by_brand_id($brand_id) {
    $this->db->select('country');
    $this->db->where('id', $brand_id);
    $query = $this->db->get('brands');
    return $query->row()->country;
  }

  public function get_products($limit = 0, $offset = 0) {
    $sql = 'SELECT products.*, '
	    . 'categories.name AS category_name, '
	    . 'brands.name AS brand_name, '
	    . 'COUNT(packs.id) AS num_of_packs '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'LEFT JOIN packs '
	    . 'ON products.id = packs.product_id '
	    . 'GROUP BY products.id '
	    . 'ORDER BY '
	    . 'products.is_cigar_product DESC, '
	    . 'category_name ASC, '
	    . 'products.featured DESC, '
	    . 'products.name ASC '
	    . "LIMIT $limit OFFSET $offset ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
  
  public function get_products_by_category_id ($cat_id = NULL, $limit = 0, $offset = 0) {
    $sql = 'SELECT products.*, '
	    . 'categories.name AS category_name, '
	    . 'brands.name AS brand_name, '
	    . 'COUNT(packs.id) AS num_of_packs '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'LEFT JOIN packs '
	    . 'ON products.id = packs.product_id '
	    . 'WHERE products.category_id = ' . $cat_id . ' '
	    . 'GROUP BY products.id '
	    . 'ORDER BY '
	    . 'products.is_cigar_product DESC, '
	    . 'category_name ASC, '
	    . 'products.featured DESC, '
	    . 'products.name ASC '
	    . "LIMIT $limit OFFSET $offset ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function delete_product_by_id($id) {
    $this->db->trans_start();
    $this->db->where('id', $id);
    $this->db->delete('products');
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  public function get_product_by_id($id = NULL, $type_of_return = FALSE) {
    /*
    $sql = 'SELECT products.*, '
	    . 'categories.name AS category_name, '
	    . 'brands.name AS brand_name, '
	    . 'brands.country AS country, '
	    . 'brands.img AS brand_img, '
	    . 'COUNT(packs.id) AS num_of_packs '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'LEFT JOIN packs '
	    . 'ON products.id = packs.product_id '
	    . 'WHERE products.id = ' . $id . ' '
	    . 'GROUP BY products.id';
     * 
     */
    
    $sql = 'SELECT products.*, '
	    . 'categories.name AS category_name, '
	    . 'categories.slug AS category_slug, '
	    . 'brands.name AS brand_name, '
	    . 'brands.country AS country, '
	    . 'brands.img AS brand_img, '
	    . 'COUNT(packs.id) AS num_of_packs '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'LEFT JOIN packs '
	    . 'ON products.id = packs.product_id '
	    . 'WHERE products.id = ' . $id . ' '
	    . 'GROUP BY products.id';
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function update_product($id = NULL, $dbdata = NULL, $fields_to_update = NULL) {
    $data = array();
    foreach ($fields_to_update as $fld) {
      $data[$fld] = $dbdata[$fld];
    }
    $this->db->trans_start();
    $this->db->where('id', $id);
    $res = $this->db->update('products', $data);
    $this->db->trans_complete();
    return $res;
  }

}
