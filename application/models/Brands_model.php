<?php

class Brands_model extends CI_Model {

  public function get_cigar_brands() {
    $this->db->order_by('lifted_up_at', 'DESC');
    $this->db->order_by('name', 'ASC');
    $this->db->where('is_cigar_brand', 1);
    $this->db->where('published', 1);
    $query = $this->db->get('brands');
    return $query->result_array();
  }
  
  public function get_brand_of_day_id () {
    $this->db->where('published', 1);
    $this->db->where('is_cigar_brand', 1);
    $this->db->where('brand_of_day', 1);
    $query = $this->db->get('brands');
    $brand_of_day_id = ($query->row()) ? $query->row()->id : FALSE;
    if ($brand_of_day_id == NULL OR !$brand_of_day_id) {
      return FALSE;
    }
    return $brand_of_day_id;
  }

  public function get_cigars_by_brand_id($brand_id = NULL, $limit = 0, $offset = 0) {
    $sql = 'SELECT products.*, '
	    . 'brands.name AS brand_name, '
	    . 'categories.slug AS category_slug '
	    . 'FROM products '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'WHERE products.brand_id = ' . $brand_id . ' '
	    . 'AND products.is_cigar_product = 1 '
	    . 'AND products.published = 1 '
	    . 'AND brands.published = 1 '
	    . 'AND categories.published = 1 '
	    //. 'AND products.price > 0 '
	    //. 'AND products.quantity > 0 '
	    . 'ORDER BY products.featured DESC, '
	    . 'products.name ASC '
	    . "LIMIT $limit OFFSET $offset";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function count_cigars_by_brand_id($brand_id = NULL) {
    $sql = 'SELECT COUNT(products.id) AS num_of_products '
	    . 'FROM products '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'WHERE products.brand_id = ' . $brand_id . ' '
	    . 'AND products.is_cigar_product = 1 '
	    . 'AND products.published = 1 '
	    . 'AND brands.published = 1 '
	    //. 'AND products.price > 0 '
	    //. 'AND products.quantity > 0 '
	    . 'GROUP BY products.id ';
    $query = $this->db->query($sql);
    return count($query->result_array());
  }

  public function get_brand_by_slug($slug = NULL) {
    $this->db->where('published', 1);
    $this->db->where('slug', $slug);
    $query = $this->db->get('brands');
    return $query->row_array();
  }
  
  public function get_brand_by_id($id = NULL) {
    $this->db->where('id', $id);
    $this->db->where('published', 1);
    $query = $this->db->get('brands');
    return $query->row_array();
  }

}
