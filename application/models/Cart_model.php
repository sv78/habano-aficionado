<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// class
class Cart_model extends CI_Model {

  public function update_user_phone_address($db_data = NULL) {
    $data = array();
    $data = $db_data;
    $this->db->where('id', $this->session->userdata('user')['id']);
    $this->db->update('users', $data);
  }

  public function create_order($db_data = NULL) {
    if ($this->db->insert('orders', $db_data)) {
      return TRUE;
    }
    return FALSE;
  }

}

// end of class 