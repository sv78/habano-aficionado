<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model {

  public function get_category_by_slug($slug = NULL) {
    $this->db->where('published', 1);
    $this->db->where('slug', $slug);
    $query = $this->db->get('categories');
    return $query->row_array();
  }

  public function get_cigars_by_category_id($category_id = NULL, $limit = 0, $offset = 0) {
    $sql = 'SELECT products.*, '
	    . 'brands.name AS brand_name, '
	    . 'categories.slug AS category_slug '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'WHERE products.category_id = ' . $category_id . ' '
	    . 'AND products.is_cigar_product = 1 '
	    . 'AND products.published = 1 '
	    . 'AND categories.published = 1 '
	    . 'AND brands.published = 1 '
	    //. 'AND products.price > 0 '
	    //. 'AND products.quantity > 0 '
	    . 'ORDER BY products.featured DESC, '
	    . 'brand_name ASC, '
	    . 'products.name ASC '
	    . "LIMIT $limit OFFSET $offset";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
  public function get_products_by_category_id($category_id = NULL, $limit = 0, $offset = 0) {
    $sql = 'SELECT products.*, '
	    . 'brands.name AS brand_name, '
	    . 'categories.slug AS category_slug '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'WHERE products.category_id = ' . $category_id . ' '
	    . 'AND products.is_cigar_product != 1 '
	    . 'AND products.published = 1 '
	    . 'AND categories.published = 1 '
	    . 'AND brands.published = 1 '
	    //. 'AND products.price > 0 '
	    //. 'AND products.quantity > 0 '
	    . 'ORDER BY products.featured DESC, '
	    . 'brand_name ASC, '
	    . 'products.name ASC '
	    . "LIMIT $limit OFFSET $offset";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function count_cigars_by_category_id($category_id = NULL) {
    $sql = 'SELECT COUNT(products.id) AS num_of_products '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'WHERE products.category_id = ' . $category_id . ' '
	    . 'AND products.is_cigar_product = 1 '
	    . 'AND products.published = 1 '
	    . 'AND categories.published = 1 '
	    . 'AND brands.published = 1 '
	    //. 'AND products.price > 0 '
	    //. 'AND products.quantity > 0 '
	    . 'GROUP BY products.id ';
    $query = $this->db->query($sql);
    return count($query->result_array());
  }
  
  public function count_products_by_category_id($category_id = NULL) {
    $sql = 'SELECT COUNT(products.id) AS num_of_products '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'WHERE products.category_id = ' . $category_id . ' '
	    . 'AND products.is_cigar_product != 1 '
	    . 'AND products.published = 1 '
	    . 'AND categories.published = 1 '
	    . 'AND brands.published = 1 '
	    //. 'AND products.price > 0 '
	    //. 'AND products.quantity > 0 '
	    . 'GROUP BY products.id ';
    $query = $this->db->query($sql);
    return count($query->result_array());
  }

  public function get_categories_for_slider() {
    $this->db->where('published', 1);
    $this->db->order_by('featured', 'DESC');
    $this->db->order_by('lifted_up_at', 'DESC');
    $this->db->order_by('name', 'ASC');
    $query = $this->db->get('categories');
    return $query->result_array();
  }
  
}
