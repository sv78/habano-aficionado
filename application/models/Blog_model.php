<?php

class Blog_model extends CI_Model {

  public function get_sections($limit = 0, $offset = 0) {
    $this->db->order_by('lifted_up_at', 'DESC');
    $this->db->order_by('title', 'ASC');
    $query = $this->db->get_where('sections', array('published' => 1, 'slug !=' => config_item('_reserved_section_slug')), $limit, $offset);
    return $query->result_array();
  }

  public function get4items() {
    $sql = 'SELECT articles.*, '
	    . 'sections.slug AS section_slug '
	    . 'FROM articles '
	    . 'LEFT JOIN sections '
	    . 'ON articles.section_id = sections.id '
	    . 'WHERE articles.featured = 1 '
	    . 'AND articles.published = 1 '
	    . 'AND sections.published = 1 '
	    . 'AND sections.slug != "uncategorized" '
	    . 'ORDER BY articles.lifted_up_at DESC '
	    . 'LIMIT 4 OFFSET 0';
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function get_sections_ids_and_titles() {
    $this->db->select('id,title');
    $query = $this->db->get('sections');
    return $query->result_array();
  }

  public function section_id_by_slug($slug = NULL) {
    if ($slug === NULL OR $slug === '' OR $slug === FALSE) {
      return FALSE;
    }
    $this->db->select('id');
    $query = $this->db->get_where('sections', array('slug' => $slug, 'published' => 1));
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    return $query->row()->id;
  }

  public function article_id_by_slug($slug = NULL) {
    if ($slug === NULL OR $slug === '' OR $slug === FALSE) {
      return FALSE;
    }
    $this->db->select('id');
    $query = $this->db->get_where('articles', array('slug' => $slug, 'published' => 1));
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    return $query->row()->id;
  }

  public function section_by_slug($slug = NULL) {
    if ($slug === NULL OR $slug === '' OR $slug === FALSE) {
      return FALSE;
    }
    $query = $this->db->get_where('sections', array('slug' => $slug, 'published' => 1));
    if (!isset($query->row()->id)) {
      return FALSE;
    }
    return $query->row();
  }

  public function get_articles($section_id = NULL, $limit = 0, $offset = 0) {
    if ($section_id === NULL || $section_id === '' || $section_id === FALSE) {
      return FALSE;
    }
    //$query = $this->db->query("SELECT * FROM articles WHERE section_id = $section_id LIMIT $limit OFFSET $offset");
    $this->db->order_by('featured', 'DESC');
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get_where('articles', array('section_id' => $section_id, 'published' => 1), $limit, $offset);
    return $query->result_array();
  }

  public function get_article_by_id_and_section_id($article_id = NULL, $section_id = NULL) {
    if ($article_id === NULL OR $article_id === FALSE OR $section_id === NULL OR $section_id === FALSE) {
      return FALSE;
    }
    $query = $this->db->get_where('articles', array('id' => $article_id, 'section_id' => $section_id, 'published' => 1));
    return $query->row();
  }

}
