<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shopcart {

  private $CI;
  private $cart_products;
  private $cart_packs;

  public function __construct() {
    $this->CI = & get_instance();

    //$this->load->helper('cookie');
    $this->cart_products = Baza::decode_plain_string_from_db(get_cookie('cart_products'));
    $this->cart_packs = Baza::decode_plain_string_from_db(get_cookie('cart_packs'));
  }

  public function get_top_panel_icon_value() {
    if ($this->cart_products == '' && $this->cart_packs == '') {
      return "Пусто";
    } else {
      return "Ваш резерв";
    }
  }

  public function parse_cart_items($items) { // items is array like [[item_id, amount_number],[item_id, amount_number], ...]
    $items = trim($items, '[');
    $items = trim($items, ']');
    $items = preg_split("/(\],\[)+/", $items); // ],[
    $items = $this->reorder_cart_items($items);
    return $items;
  }

  public function reorder_cart_items($arr) {
    $new_arr = array();
    foreach ($arr as $item) :
      $new_arr[] = explode(',', $item);
    endforeach;
    return $new_arr;
  }

}
