<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Back_url {

  private $CI;
  private $curr_url;

  public function __construct() {
    $this->CI = & get_instance();
    $this->CI->load->helper('url');
    
    // current url
    $this->curr_url = filter_input(INPUT_SERVER, 'REQUEST_URI');

    // if first load
    if (!isset($_SESSION['back_url'])) {
      $_SESSION['back_url'] = array($this->curr_url, $this->curr_url);
    }

    if ($this->curr_url == $_SESSION['back_url'][0]) {
      return;
    }
    if (preg_match('/ajax/i', $this->curr_url)) {
      return;
    }

    if (preg_match('/iframe/i', $this->curr_url)) {
      return;
    }

    if (preg_match('/upload/i', $this->curr_url)) {
      return;
    }

    // if second request or more (assumed that session exists)
    $bu = $this->CI->session->userdata('back_url');

    array_unshift($_SESSION['back_url'], $this->curr_url);
    $_SESSION['back_url'] = array_slice($_SESSION['back_url'], 0, 2, true);
  }

  public function get() {
    return $_SESSION['back_url'][1];
  }

  public function set_parent_url() {
    $_SESSION['parent_url'] = $this->curr_url;
  }

  public function get_parent_url() {
    if (!isset($_SESSION['parent_url'])) {
      // return site_url();
      return "/";
    }
    return $_SESSION['parent_url'];
  }

}
