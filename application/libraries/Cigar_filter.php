<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cigar_filter {
  

  private $CI;

  public function __construct() {
    return;
    $this->CI = & get_instance();

    $this->CI->load->library('form_validation');

    $this->CI->form_validation->set_rules('fb_country', 'Country', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_brand', 'Brand', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_vitola', 'Vitola', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_line', 'Line', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_length', 'Length', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_cepo', 'Cepo', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_intensity', 'Intensity', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_wrapper', 'Wrapper', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_flavour', 'Flavour', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_price_max', 'Price Max Range', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_found_items_number', 'Found Results Number', 'required|trim|integer');
    $this->CI->form_validation->set_rules('fb_request_id', 'Request ID', 'required|trim|integer');

    if ($this->CI->form_validation->run() === FALSE) {
      show_404();
      //$this->CI->session->unset_userdata('filter_params');
      //$this->CI->load->view('templates/public/filter_block');
    } else {
      $this->CI->dbdata['flt_country'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_country'));
      $this->CI->dbdata['flt_brand_id'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_brand'));
      $this->CI->dbdata['flt_vitola'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_vitola'));
      $this->CI->dbdata['flt_line'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_line'));
      $this->CI->dbdata['flt_length'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_length'));
      $this->CI->dbdata['flt_cepo'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_cepo'));
      $this->CI->dbdata['flt_intensity'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_intensity'));
      $this->CI->dbdata['flt_wrapper'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_wrapper'));
      $this->CI->dbdata['flt_flavour'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_flavour'));
      $this->CI->dbdata['flt_price_max'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_price_max'));

      $this->CI->dbdata['flt_found_items_number'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_found_items_number'));
      $this->CI->dbdata['flt_request_id'] = Baza::encode_plain_string_to_db($this->CI->input->post('fb_request_id'));

      $num_of_products = $this->CI->dbdata['flt_found_items_number'];

      $this->CI->session->set_userdata('filter_params', $this->CI->dbdata);
      
    }

    //$this->CI->load->model('products_model');
    //$num_of_products = $this->CI->products_model->count_search_results_by_params($this->CI->dbdata); // it's the same method as ajax request makes for pre count cigars by params

    if (empty($this->CI->session->userdata('filter_params')) OR uri_string() != 'catalog') {
      $this->CI->load->view('templates/public/filter_block');
      return;
    }

    $num_of_products = $this->CI->session->userdata('filter_params')['flt_found_items_number'];


    $this->CI->load->library('pagination');

    $pgn_config = config_item('_pagination_config');
    $pgn_config['base_url'] = current_url();
    $pgn_config['total_rows'] = $num_of_products; // usually number of rows in a table out of database
    $pgn_config['per_page'] = config_item("_cigars_items_per_page");
    //$pgn_config['per_page'] = 2;
    $this->CI->pagination->initialize($pgn_config);

    $limit = $pgn_config['per_page'];
    $page_index = intval($this->CI->input->get($pgn_config['query_string_segment']));
    if ($page_index == FALSE) {
      $page_index = 1;
    }
    $offset = $page_index * $pgn_config['per_page'] - $pgn_config['per_page'];



    $sql = 'SELECT products.*, '
	    . 'categories.name AS category_name, '
	    . 'categories.slug AS category_slug, '
	    . 'brands.name AS brand_name '
	    . 'FROM products '
	    . 'LEFT JOIN categories '
	    . 'ON products.category_id = categories.id '
	    . 'LEFT JOIN brands '
	    . 'ON products.brand_id = brands.id '
	    . 'WHERE is_cigar_product = 1 '
	    . 'AND products.published = 1 '
	    . 'AND categories.published = 1 '
	    . 'AND brands.published = 1 ';

    // country where
    if ($this->CI->session->userdata('filter_params')['flt_country'] != '' && $this->CI->session->userdata('filter_params')['flt_country'] != -1) {
      $sql .= 'AND brands.country = ' . $this->CI->session->userdata('filter_params')['flt_country'] . ' ';
    }
    // brand id where
    if ($this->CI->session->userdata('filter_params')['flt_brand_id'] != '' && $this->CI->session->userdata('filter_params')['flt_brand_id'] != -1) {
      $sql .= 'AND products.brand_id = ' . $this->CI->session->userdata('filter_params')['flt_brand_id'] . ' ';
    }
    // vitola where
    if ($this->CI->session->userdata('filter_params')['flt_vitola'] != '' && $this->CI->session->userdata('filter_params')['flt_vitola'] != -1) {
      $sql .= 'AND products.vitola = ' . $this->CI->session->userdata('filter_params')['flt_vitola'] . ' ';
    }
    // line where
    if ($this->CI->session->userdata('filter_params')['flt_line'] != '' && $this->CI->session->userdata('filter_params')['flt_line'] != -1) {
      $sql .= 'AND products.line = ' . $this->CI->session->userdata('filter_params')['flt_line'] . ' ';
    }
    // length where
    if ($this->CI->session->userdata('filter_params')['flt_length'] != '' && $this->CI->session->userdata('filter_params')['flt_length'] != -1) {
      $sql .= 'AND products.length = ' . $this->CI->session->userdata('filter_params')['flt_length'] . ' ';
    }
    // cepo where
    if ($this->CI->session->userdata('filter_params')['flt_cepo'] != '' && $this->CI->session->userdata('filter_params')['flt_cepo'] != -1) {
      $sql .= 'AND products.cepo = ' . $this->CI->session->userdata('filter_params')['flt_cepo'] . ' ';
    }
    // intensity where
    if ($this->CI->session->userdata('filter_params')['flt_intensity'] != '' && $this->CI->session->userdata('filter_params')['flt_intensity'] != -1) {
      $sql .= 'AND products.intensity = ' . $this->CI->session->userdata('filter_params')['flt_intensity'] . ' ';
    }
    // wrapper where
    if ($this->CI->session->userdata('filter_params')['flt_wrapper'] != '' && $this->CI->session->userdata('filter_params')['flt_wrapper'] != -1) {
      $sql .= 'AND products.wrapper = ' . $this->CI->session->userdata('filter_params')['flt_wrapper'] . ' ';
    }
    // flavour where
    if ($this->CI->session->userdata('filter_params')['flt_flavour'] != '' && $this->CI->session->userdata('filter_params')['flt_flavour'] != -1) {
      $sql .= 'AND products.flavour = ' . $this->CI->session->userdata('filter_params')['flt_flavour'] . ' ';
    }
    // price max where
    if ($this->CI->session->userdata('filter_params')['flt_price_max'] != '' && $this->CI->session->userdata('filter_params')['flt_price_max'] != -1) {
      $sql .= 'AND products.price <= ' . $this->CI->session->userdata('filter_params')['flt_price_max'] . ' ';
    }

    $sql .= 'ORDER BY '
	    . 'products.featured DESC, '
	    . 'products.name ASC '
	    . "LIMIT $limit OFFSET $offset ";
    
    //echo $sql;
    //print_r($this->CI->session->userdata('filter_params'));
    
    $query = $this->CI->db->query($sql);
    $data['products'] = $query->result_array();




    if (count($data['products']) < 1 && !empty($this->CI->input->get($pgn_config['query_string_segment']))): // page
      redirect(current_url());
    endif;


    $this->CI->load->view('templates/public/filter_block');
    $this->CI->load->view('templates/public/search_result_head_string');
    $this->CI->load->view('templates/public/cigars_array', $data);
    $this->CI->load->view('templates/public/pagination', $data);

    //echo 'still works…';
  }

}
