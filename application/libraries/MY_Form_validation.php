<?php

// class
class MY_Form_validation extends CI_Form_validation {

  public function __construct($config = array()) {
    parent::__construct($config);
  }

  public function alpha_numeric_dash($str) {
    return (bool) !preg_match("/[^0-9A-Za-z-]/i", $str);
  }

  public function robots_metatag($str) {
    return (bool) !preg_match("/[^noidexflw, ]/i", $str);
  }

  public function checkbox($str) {
    return (bool) (strtolower($str) === 'on') ? TRUE : FALSE;
  }

  public function reserved_slugs($str) {
    $str = strtolower($str);
    foreach (config_item('_reserved_slugs') as $reserved_slug) :
      if ($str == $reserved_slug) {
	return FALSE;
      }
    endforeach;
    return TRUE;
  }
  
  public function cart_phone ($str) {
    // here mask is +7 (999) 999 99 99
    if (strlen($str) != 18) {
      return FALSE;
    }
    $patt = '/^\+\d \(\d{3}\) \d{3} \d{2} \d{2}$/';
    return (bool) preg_match($patt, $str);
  }

  public function pwd_regexp($str) {
    return (bool) !preg_match("/[^0-9A-Za-z\-\_\!\@\#\$\%\^\&\*\(\)\~\+\?\.\[\]\{\}]/i", $str);
  }

}

// end of class