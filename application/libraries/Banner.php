<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Banner {

  private $CI;

  public function __construct() {
    $this->CI = & get_instance();
  }

  public function get() {
    $this->CI->load->model('admin/banners_model');
    $banners = $this->CI->banners_model->get_published_banners();
    if (count($banners) == 0 && !$banners) {
      return FALSE;
    }
    $banners_div = array();
    $banners_div[] = '<div id="ha_banner_container" class="container main-banner-container">';
    $banners_div[] = '<div class="section-devider"></div>';
    $banners_div[] = '<div id="ha_banner" class="owl-carousel">';
    foreach ($banners as $banner):
      $banners_div[] = '<div>';
      $newtab = $banner['newtab'] == 1 ? '_blank' : '_self';
      $banners_div[] = '<a href="' . $banner['url'] . '" target="' . $newtab . '">';
      $img_src = "/" . config_item('_assets_image_path_url') . $banner['img_src'];
      $banners_div[] = '<img src="' . $img_src . '" alt="' . $banner['alt_text'] . '">';
      $banners_div[] = '</a>';
      $banners_div[] = '</div>';
    endforeach;
    $banners_div[] = '</div>';
    $banners_div[] = '<div class="section-devider"></div>';
    $banners_div[] = '</div>';
    return implode('', $banners_div);
  }

}
