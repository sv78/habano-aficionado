<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recently_seen_products {

  private $CI;

  public function __construct() {
    $this->CI = & get_instance();
    //$this->CI->load->helper('url');
    // current url
    //$this->curr_url = "/" . filter_input(INPUT_SERVER, 'REQUEST_URI');
  }

  public function set($product = NULL) {
    $rs_product = array(); // recently seen product
    $rs_product['id'] = $product['id'];
    $rs_product['name'] = Baza::decode_plain_string_from_db($product['name']);
    $rs_product['price'] = $product['price'];
    $rs_product['url'] = '/catalog/' . $product['category_slug'] . '/' . $product['slug'];
    $rs_product['is_cigar_product'] = $product['is_cigar_product'];

    if ($product['img'] == '') {
      $img_src = "/" . config_item('_product_not_specified_image_url');
    } else if (!file_exists(config_item('_products_image_path') . $product['img'])) {
      $img_src = "/" . config_item('_product_missing_image_url');
    } else {
      $img_src = "/" . config_item('_products_image_path_url') . $product['img'];
    }
    
    $rs_product['img_src'] = $img_src;


    $product_is_in_session = FALSE;
    if (!isset($_SESSION['recently_seen_products'])) {
      $_SESSION['recently_seen_products'] = array();
    }
    foreach ($_SESSION['recently_seen_products'] as $sess_product) {
      if ($sess_product['id'] == $rs_product['id']) {
	$product_is_in_session = TRUE;
      }
    }
    if (!$product_is_in_session) {
      array_unshift($_SESSION['recently_seen_products'], $rs_product);
    }

    if (count($_SESSION['recently_seen_products']) > config_item('_recently_products_amount_to_remember')) {
      array_pop($_SESSION['recently_seen_products']);
    }

    //echo "<hr>";
    //print_r($_SESSION['recently_seen_products']);
  }

}
