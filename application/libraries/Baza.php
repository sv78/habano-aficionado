<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Baza {
  // ==== STATIC VARS ====

  /**
   * Проверяет ясляется ли имя годным для аплоадинга.
   * @param type $form_file_input_name имя поля ввода файла (input type='file')
   * @return boolean
   */
  static function is_uploading_file_name_is_valid($file_name) {
    if ($file_name === NULL || $file_name === '' || $file_name === FALSE) {
      return FALSE;
    }
    // если найдет что-либо кроме символов указанных в регулярном выражении, то даст 1 - значит имя файла не годится
    // т.е. если 0 или FALSE то все норм, а если 1 или TRUE, то файл не пропустим
    if (preg_match('/[^0-9a-zA-Z\._-]/', $file_name)) {
      return FALSE;
    }
    return TRUE;
  }

  static $meta_robots_options_array = [
      // ['option value', 'option text'],
      ['index, follow', 'index, follow'],
      ['noindex, follow', 'noindex, follow'],
      ['index, nofollow', 'index, nofollow'],
      ['noindex, nofollow', 'noindex, nofollow']
  ];

  // ==== STATIC METHODS ====

  static function create_options($select_name = NULL, $options_array = NULL, $selected_value = '') {
    if ($select_name === NULL OR $options_array === NULL OR $options_array === 0) {
      return FALSE;
    }
    $len = count($options_array);
    for ($i = 0; $i < $len; $i++) {
      $selected_attr = ($options_array[$i][0] == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $options_array[$i][0] . '" ' . set_select($select_name, $options_array[$i][0]) . $selected_attr . '>' . $options_array[$i][1] . '</option>';
    }
  }

  static function create_country_options($options_array = NULL, $selected_value = '') {
    if ($options_array === NULL OR $options_array === 0) {
      return FALSE;
    }
    $len = count($options_array);
    for ($i = 0; $i < $len; $i++) {
      $selected_attr = ($i == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $i . '" ' . $selected_attr . '>' . $options_array[$i]['name'] . '</option>';
    }
  }

  public function create_sections_options($selected_value = '') {
    $this->CI->load->model('blog_model');
    $sections = $this->CI->blog_model->get_sections();
    foreach ($sections as $section) {
      $selected_attr = ($section['id'] == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $section['id'] . '" ' . $selected_attr . '>' . $section['title'] . '</option>';
    }
  }

  public function create_categories_options($selected_value = '') {
    $this->CI->load->model('admin/categories_model');
    $items = $this->CI->categories_model->get_categories();
    foreach ($items as $item) {
      $selected_attr = ($item['id'] == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $item['id'] . '" ' . $selected_attr . '>' . $item['name'] . '</option>';
    }
  }

  public function create_vitolas_options($selected_value = '') {
    $options_array = config_item('_flt_vitolas');
    $len = count($options_array);
    for ($i = 0; $i < $len; $i++) {
      $selected_attr = ($i == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $i . '" img_src="' . $options_array[$i]['img'] . '" ' . $selected_attr . '>' . $options_array[$i]['name'] . '</option>';
    }
  }

  public function create_lines_options($selected_value = '') {
    $options_array = config_item('_flt_lines');
    $len = count($options_array);
    for ($i = 0; $i < $len; $i++) {
      $selected_attr = ($i == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $i . '" ' . $selected_attr . '>' . $options_array[$i]['name'] . '</option>';
    }
  }

  public function create_lengths_options($selected_value = '') {
    $options_array = config_item('_flt_lengths');
    $len = count($options_array);
    for ($i = 0; $i < $len; $i++) {
      $selected_attr = ($i == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $i . '" ' . $selected_attr . '>' . $options_array[$i]['mm'] . 'mm | ' . $options_array[$i]['inches'] . '</option>';
    }
  }

  public function create_cepos_options($selected_value = '') {
    $options_array = config_item('_flt_cepos');
    $len = count($options_array);
    for ($i = 0; $i < $len; $i++) {
      $selected_attr = ($options_array[$i] == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $options_array[$i] . '" ' . $selected_attr . '>' . $options_array[$i] . '</option>';
    }
  }

  public function create_intensities_options($selected_value = '') {
    $options_array = config_item('_flt_intensities');
    $len = count($options_array);
    for ($i = 0; $i < $len; $i++) {
      $selected_attr = ($i == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $i . '" ' . $selected_attr . '>' . $options_array[$i]['name'] . '</option>';
    }
  }

  public function create_flavours_options($selected_value = '') {
    $options_array = config_item('_flt_flavours');
    $len = count($options_array);
    for ($i = 0; $i < $len; $i++) {
      $selected_attr = ($i == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $i . '" ' . $selected_attr . '>' . $options_array[$i]['name'] . '</option>';
    }
  }

  public function create_wrappers_options($selected_value = '') {
    $options_array = config_item('_flt_wrappers');
    $len = count($options_array);
    for ($i = 0; $i < $len; $i++) {
      $selected_attr = ($i == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $i . '" ' . $selected_attr . '>' . $options_array[$i]['name'] . '</option>';
    }
  }

  public function create_brands_options($selected_value = '') {
    $this->CI->load->model('admin/brands_model');
    $items = $this->CI->brands_model->get_brands();
    foreach ($items as $item) {
      $selected_attr = ($item['id'] == $selected_value) ? ' selected ' : ''; // html5 specification
      echo '<option value="' . $item['id'] . '" ' . $selected_attr . '>' . $item['name'] . '</option>';
    }
  }

  public function show_additional_product_images($unserialized_str) {
    if ($unserialized_str == '') {
      return;
    }
    $arr = unserialize($unserialized_str);
    foreach ($arr as $img_src) {
      echo '<img src="/' . config_item('_products_additional_image_path_url') . $img_src . '" alt="' . $img_src . '">';
    }
  }

  static function encode_plain_string_to_db($str) {
    return (string) htmlentities(addslashes(trim($str)));
  }

  static function encode_html_string_to_db($str) {
    return (string) addslashes(trim($str));
  }

  static function decode_plain_string_from_db($str) {
    return (string) stripslashes($str);
  }

  static function decode_html_string_from_db($str) {
    return (string) stripslashes(html_entity_decode($str));
  }

  // ==== PROPERTIES ====

  protected $CI; // переменная для глобального объяекта CI по ссылке

  // ==== CONSTRUCTOR ====

  public function __construct() {
    // передача глобального объекта CI в переменную нашего класса по ссылке (не создавая копии то есть)
    $this->CI = & get_instance();
  }

  // ==== METHODS ====

  public function is_super_admin($email = NULL, $password = NULL) {
    foreach (config_item('_super_admins') as $superadmin):
      if ($email === $superadmin[0] && password_verify($password, $superadmin[1])) {
	return TRUE;
      }
    endforeach;
    return FALSE;
  }

  public static function is_email_of_super_admin($email = NULL) {
    foreach (config_item('_super_admins') as $superadmin):
      if ($email === $superadmin[0]) {
	return TRUE;
      }
    endforeach;
    return FALSE;
  }

  public function check_captcha_exists($str) {
    $expired = time() - config_item('_captcha_config')['expiration'];
    $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip = ? AND time > ?';
    //$binds = array($this->CI->input->post('captcha'), $this->CI->input->ip_address(), $expired);
    $binds = array($str, $this->CI->input->ip_address(), $expired);
    $query = $this->CI->db->query($sql, $binds);
    $row = $query->row();
    if ($row->count == 0) {
      return FALSE;
    }
    return TRUE;
  }

  public function resize_by_width($imgfile = NULL, $width = 50, $quality = 100) {
    if ($imgfile === NULL OR strlen($imgfile) < 3 OR $imgfile === FALSE) {
      return FALSE;
    }
    if (!is_integer($width) OR ! is_integer($quality)) {
      return FALSE;
    }
    if (file_exists($imgfile)):
      $config['image_library'] = 'gd2';
      //$config['image_library'] = 'ImageMagick';
      $config['source_image'] = $imgfile;
      //$config['create_thumb'] = TRUE;
      $config['master_dim'] = 'width';
      $config['maintain_ratio'] = TRUE;
      $config['quality'] = $quality;
      $config['width'] = $width;
      $config['height'] = 'auto';
      $this->CI->load->library('image_lib', $config);
      $this->CI->image_lib->resize();
    endif;
    return TRUE;
  }

  public function resize_and_crop_blog_section_image($uploaded_file_data = NULL) {
    if ($uploaded_file_data === NULL) {
      return FALSE;
    }

    $img_width_dest = config_item('_blog_section_image_width');
    $img_height_dest = config_item('_blog_section_image_height');

    $img_width_curr = $uploaded_file_data['image_width'];
    $img_height_curr = $uploaded_file_data['image_height'];

    $this->CI->load->library('image_lib');


    // кропим в квадрат только если стороны не равны, иначе не кропим ваще
    if ($img_width_curr !== $img_height_curr) {
      $crop_config = config_item('_blog_section_image_crop_config');
      // кропим по меньшей стороне (найдем ее для этого)
      // меняем вычисленные настройки
      if ($img_width_curr > $img_height_curr) {
	$crop_config['height'] = $img_height_curr;
	$crop_config['width'] = $img_height_curr;
	$crop_config['x_axis'] = floor(($img_width_curr - $img_height_curr) / 2);
      } else {
	$crop_config['width'] = $img_width_curr;
	$crop_config['height'] = $img_width_curr;
	$crop_config['y_axis'] = floor(($img_height_curr - $img_width_curr) / 2);
      }
      $crop_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($crop_config);
      if (!$this->CI->image_lib->crop()) {
	return FALSE;
      }
    }

    // resize
    if ($img_width_curr !== $img_width_dest OR $img_height_curr !== $img_height_dest) {
      $resize_config = config_item('_blog_section_image_resize_config');
      $resize_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($resize_config);
      if (!$this->CI->image_lib->resize()) {
	return FALSE;
      }
    }
    return TRUE;
  }

  public function resize_and_crop_blog_article_image($uploaded_file_data = NULL) {
    if ($uploaded_file_data === NULL) {
      return FALSE;
    }

    $img_width_dest = config_item('_blog_article_image_width');
    $img_height_dest = config_item('_blog_article_image_height');

    $img_width_curr = $uploaded_file_data['image_width'];
    $img_height_curr = $uploaded_file_data['image_height'];

    $this->CI->load->library('image_lib');


    // кропим в квадрат только если стороны не равны, иначе не кропим ваще
    if ($img_width_curr !== $img_height_curr) {
      $crop_config = config_item('_blog_article_image_crop_config');
      // кропим по меньшей стороне (найдем ее для этого)
      // меняем вычисленные настройки
      if ($img_width_curr > $img_height_curr) {
	$crop_config['height'] = $img_height_curr;
	$crop_config['width'] = $img_height_curr;
	$crop_config['x_axis'] = floor(($img_width_curr - $img_height_curr) / 2);
      } else {
	$crop_config['width'] = $img_width_curr;
	$crop_config['height'] = $img_width_curr;
	$crop_config['y_axis'] = floor(($img_height_curr - $img_width_curr) / 2);
      }
      $crop_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($crop_config);
      if (!$this->CI->image_lib->crop()) {
	return FALSE;
      }
    }

    // resize
    if ($img_width_curr !== $img_width_dest OR $img_height_curr !== $img_height_dest) {
      $resize_config = config_item('_blog_article_image_resize_config');
      $resize_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($resize_config);
      if (!$this->CI->image_lib->resize()) {
	return FALSE;
      }
    }
    return TRUE;
  }

  public function resize_and_crop_brand_image($uploaded_file_data = NULL) {
    if ($uploaded_file_data === NULL) {
      return FALSE;
    }

    $img_width_dest = config_item('_brand_image_width');
    $img_height_dest = config_item('_brand_image_height');

    $img_width_curr = $uploaded_file_data['image_width'];
    $img_height_curr = $uploaded_file_data['image_height'];

    $this->CI->load->library('image_lib');

    $q = 1.41726618705; // здесь кроп будет в соотношении width / height = 1.4172

    $crop_config = config_item('_brand_image_crop_config');

    // меняем вычисленные настройки
    if (round($img_height_curr * $q) <= $img_width_curr) {
      $crop_config['height'] = $img_height_curr;
      $crop_config['width'] = round($img_height_curr * $q);
      $crop_config['y_axis'] = 0;
      $crop_config['x_axis'] = round(($img_width_curr - $img_height_curr * $q) / 2);
    } else {
      $crop_config['width'] = $img_width_curr;
      $crop_config['height'] = round($img_width_curr / $q);
      $crop_config['x_axis'] = 0;
      $crop_config['y_axis'] = round(($img_height_curr - $img_width_curr / $q) / 2);
    }

    $crop_config['source_image'] = $uploaded_file_data['full_path'];
    $this->CI->image_lib->initialize($crop_config);
    if (!$this->CI->image_lib->crop()) {
      return FALSE;
    }

    // resize
    if ($img_width_curr !== $img_width_dest OR $img_height_curr !== $img_height_dest) {
      $resize_config = config_item('_brand_image_resize_config');
      $resize_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($resize_config);
      if (!$this->CI->image_lib->resize()) {
	return FALSE;
      }
    }
    return TRUE;
  }

  public function resize_and_crop_category_image($uploaded_file_data = NULL) {
    if ($uploaded_file_data === NULL) {
      return FALSE;
    }

    $img_width_dest = config_item('_category_image_width');
    $img_height_dest = config_item('_category_image_height');

    $img_width_curr = $uploaded_file_data['image_width'];
    $img_height_curr = $uploaded_file_data['image_height'];

    $this->CI->load->library('image_lib');


    // кропим в квадрат только если стороны не равны, иначе не кропим ваще
    if ($img_width_curr !== $img_height_curr) {
      $crop_config = config_item('_category_image_crop_config');
      // кропим по меньшей стороне (найдем ее для этого)
      // меняем вычисленные настройки
      if ($img_width_curr > $img_height_curr) {
	$crop_config['height'] = $img_height_curr;
	$crop_config['width'] = $img_height_curr;
	$crop_config['x_axis'] = floor(($img_width_curr - $img_height_curr) / 2);
      } else {
	$crop_config['width'] = $img_width_curr;
	$crop_config['height'] = $img_width_curr;
	$crop_config['y_axis'] = floor(($img_height_curr - $img_width_curr) / 2);
      }
      $crop_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($crop_config);
      if (!$this->CI->image_lib->crop()) {
	return FALSE;
      }
    }

    // resize
    if ($img_width_curr !== $img_width_dest OR $img_height_curr !== $img_height_dest) {
      $resize_config = config_item('_category_image_resize_config');
      $resize_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($resize_config);
      if (!$this->CI->image_lib->resize()) {
	return FALSE;
      }
    }
    return TRUE;
  }

  public function resize_and_crop_product_image($uploaded_file_data = NULL) {
    if ($uploaded_file_data === NULL) {
      return FALSE;
    }

    $img_width_dest = config_item('_product_image_width');
    $img_height_dest = config_item('_product_image_height');

    $img_width_curr = $uploaded_file_data['image_width'];
    $img_height_curr = $uploaded_file_data['image_height'];

    $this->CI->load->library('image_lib');


    // кропим в квадрат только если стороны не равны, иначе не кропим ваще
    if ($img_width_curr !== $img_height_curr) {
      $crop_config = config_item('_product_image_crop_config');
      // кропим по меньшей стороне (найдем ее для этого)
      // меняем вычисленные настройки
      if ($img_width_curr > $img_height_curr) {
	$crop_config['height'] = $img_height_curr;
	$crop_config['width'] = $img_height_curr;
	$crop_config['x_axis'] = floor(($img_width_curr - $img_height_curr) / 2);
      } else {
	$crop_config['width'] = $img_width_curr;
	$crop_config['height'] = $img_width_curr;
	$crop_config['y_axis'] = floor(($img_height_curr - $img_width_curr) / 2);
      }
      $crop_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($crop_config);
      if (!$this->CI->image_lib->crop()) {
	return FALSE;
      }
    }

    // resize
    if ($img_width_curr !== $img_width_dest OR $img_height_curr !== $img_height_dest) {
      $resize_config = config_item('_product_image_resize_config');
      $resize_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($resize_config);
      if (!$this->CI->image_lib->resize()) {
	return FALSE;
      }
    }
    return TRUE;
  }

  public function resize_and_crop_cigar_image($uploaded_file_data = NULL) {
    if ($uploaded_file_data === NULL) {
      return FALSE;
    }

    $img_width_dest = config_item('_cigar_image_width');
    $img_height_dest = config_item('_cigar_image_height');

    $img_width_curr = $uploaded_file_data['image_width'];
    $img_height_curr = $uploaded_file_data['image_height'];

    $this->CI->load->library('image_lib');

    $q = 6.666666; // здесь кроп будет в соотношении width / height = 1.4172

    $crop_config = config_item('_cigar_image_crop_config');

    // меняем вычисленные настройки
    if (round($img_height_curr * $q) <= $img_width_curr) {
      $crop_config['height'] = $img_height_curr;
      $crop_config['width'] = round($img_height_curr * $q);
      $crop_config['y_axis'] = 0;
      $crop_config['x_axis'] = round(($img_width_curr - $img_height_curr * $q) / 2);
    } else {
      $crop_config['width'] = $img_width_curr;
      $crop_config['height'] = round($img_width_curr / $q);
      $crop_config['x_axis'] = 0;
      $crop_config['y_axis'] = round(($img_height_curr - $img_width_curr / $q) / 2);
    }

    $crop_config['source_image'] = $uploaded_file_data['full_path'];
    $this->CI->image_lib->initialize($crop_config);
    if (!$this->CI->image_lib->crop()) {
      return FALSE;
    }

    // resize
    if ($img_width_curr !== $img_width_dest OR $img_height_curr !== $img_height_dest) {
      $resize_config = config_item('_cigar_image_resize_config');
      $resize_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($resize_config);
      if (!$this->CI->image_lib->resize()) {
	return FALSE;
      }
    }
    return TRUE;
  }

  public function resize_and_crop_pack_image($uploaded_file_data = NULL) {
    if ($uploaded_file_data === NULL) {
      return FALSE;
    }

    $img_width_dest = config_item('_pack_image_width');
    $img_height_dest = config_item('_pack_image_height');

    $img_width_curr = $uploaded_file_data['image_width'];
    $img_height_curr = $uploaded_file_data['image_height'];

    $this->CI->load->library('image_lib');


    // кропим в квадрат только если стороны не равны, иначе не кропим ваще
    if ($img_width_curr !== $img_height_curr) {
      $crop_config = config_item('_pack_image_crop_config');
      // кропим по меньшей стороне (найдем ее для этого)
      // меняем вычисленные настройки
      if ($img_width_curr > $img_height_curr) {
	$crop_config['height'] = $img_height_curr;
	$crop_config['width'] = $img_height_curr;
	$crop_config['x_axis'] = floor(($img_width_curr - $img_height_curr) / 2);
      } else {
	$crop_config['width'] = $img_width_curr;
	$crop_config['height'] = $img_width_curr;
	$crop_config['y_axis'] = floor(($img_height_curr - $img_width_curr) / 2);
      }
      $crop_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($crop_config);
      if (!$this->CI->image_lib->crop()) {
	return FALSE;
      }
    }

    // resize
    if ($img_width_curr !== $img_width_dest OR $img_height_curr !== $img_height_dest) {
      $resize_config = config_item('_pack_image_resize_config');
      $resize_config['source_image'] = $uploaded_file_data['full_path'];
      $this->CI->image_lib->initialize($resize_config);
      if (!$this->CI->image_lib->resize()) {
	return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Ищет значение поля $item_to_find
   * по определенному значению $related_item_value поля $related_item
   * в таблице $table_name.
   * Возвращает значение или FALSE в случае неудачи или отсутствия.
   * Удобно например найти id по slug или наоборот.
   * Можно использовать в таблицах с 2-мя ключами например
   * 
   * 
   * @function get_item_by_related_item($item_to_find, $related_item, $related_item_value, $table_name);
   * @param type $item_to_find
   * @param type $related_item
   * @param type $related_item_value
   * @param type $table_name
   * @return mixed
   */
  public function get_item_by_related_item($item_to_find = NULL, $related_item = NULL, $related_item_value = NULL, $table_name = NULL) {
    // check if arguments wrong
    if ($item_to_find === NULL OR $item_to_find === '' OR $item_to_find === FALSE OR $related_item === NULL OR $related_item === '' OR $related_item === FALSE OR $table_name === NULL OR $table_name === '' OR $table_name === FALSE) {
      return FALSE;
    }
    if (!isset($related_item_value)) {
      return FALSE;
    }

    $this->CI->load->database();
    $this->CI->db->select($item_to_find);
    $query = $this->CI->db->get_where($table_name, array($related_item => $related_item_value));
    if (!isset($query->row()->$item_to_find)) {
      return FALSE;
    }
    return $query->row()->$item_to_find;
  }

}
