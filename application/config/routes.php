<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */





// AUTH

$route['register'] = 'auth/register';
$route['registration-confirmation/(:any)'] = 'auth/confirm_registration';
$route['password-reset-confirmation/(:any)'] = 'auth/confirm_reset_password';
$route['logout'] = 'auth/logout';
$route['login'] = 'auth/login';
$route['reset-password'] = 'auth/reset_password';
$route['profile'] = 'auth/user_profile';



// ADMIN

$route['admin'] = 'admin/common';

$route['admin/file-manager'] = 'admin/common/file_manager';



$route['admin/blog-sections'] = 'admin/blog/blog_sections';
$route['admin/blog-sections/(:num)'] = 'admin/blog/show_section_articles';
$route['admin/blog-section-preview/(:num)'] = 'admin/blog/section_preview';
$route['admin/blog-section-edit/(:num)'] = 'admin/blog/section_edit';

$route['admin/blog-articles'] = 'admin/blog/all_articles';
$route['admin/blog-article-preview/(:num)'] = 'admin/blog/article_preview';
$route['admin/blog-article-edit/(:num)'] = 'admin/blog/article_edit';

$route['admin/create-new-blog-section'] = 'admin/blog/create_new_blog_section';
$route['admin/create-new-blog-article'] = 'admin/blog/create_new_blog_article';


$route['admin/brands'] = 'admin/brands/all_brands';
$route['admin/create-brand'] = 'admin/brands/create_brand';
$route['admin/brand-preview/(:num)'] = 'admin/brands/brand_preview';
$route['admin/brand-edit/(:num)'] = 'admin/brands/brand_edit';

$route['admin/create-category'] = 'admin/categories/create_category';
$route['admin/categories'] = 'admin/categories/all_categories';
$route['admin/category-preview/(:num)'] = 'admin/categories/category_preview';
$route['admin/category-edit/(:num)'] = 'admin/categories/category_edit';

$route['admin/categories/(:num)'] = 'admin/products/products_by_category';

$route['admin/products'] = 'admin/products/all_products';
$route['admin/create-product'] = 'admin/products/create_product';
$route['admin/product-edit/(:num)'] = 'admin/products/product_edit';
$route['admin/product-preview/(:num)'] = 'admin/products/product_preview';

$route['admin/packs'] = 'admin/packs/all_packs';
$route['admin/packs-of-product/(:num)'] = 'admin/packs/packs_of_product';
$route['admin/create-pack-of-product/(:num)'] = 'admin/packs/create_pack';
$route['admin/pack-edit/(:num)'] = 'admin/packs/pack_edit';
$route['admin/pack-preview/(:num)'] = 'admin/packs/pack_preview';


$route['admin/banners'] = 'admin/banners/all_banners';
$route['admin/create-banner'] = 'admin/banners/create_banner';
$route['admin/banner-edit/(:num)'] = 'admin/banners/banner_edit';
$route['admin/banner-preview/(:num)'] = 'admin/banners/banner_preview';


$route['admin/users'] = 'admin/users/index';
$route['admin/users/(:num)'] = 'admin/users/user';

$route['admin/orders'] = 'admin/orders/index'; // actually such record is not necessary because index is default method
$route['admin/orders/(:num)'] = 'admin/orders/order/$1';




// IFRAME

$route['admin/empty-iframe'] = 'admin/common/empty_iframe';
$route['admin/upload-image'] = 'admin/common/iframe_upload_image';
$route['admin/upload-additional-image'] = 'admin/common/iframe_upload_additional_image';



// AJAX

$route['ajax/delete-section-by-id'] = 'admin/ajax/delete_section_by_id';
$route['ajax/delete-article-by-id'] = 'admin/ajax/delete_article_by_id';
$route['ajax/delete-brand-by-id'] = 'admin/ajax/delete_brand_by_id';
$route['ajax/delete-category-by-id'] = 'admin/ajax/delete_category_by_id';
$route['ajax/delete-product-by-id'] = 'admin/ajax/delete_product_by_id';
$route['ajax/delete-pack-by-id'] = 'admin/ajax/delete_pack_by_id';
$route['ajax/delete-order-by-id'] = 'admin/ajax/delete_order_by_id';
$route['ajax/delete-banner-by-id'] = 'admin/ajax/delete_banner_by_id';

$route['ajax/toggle-published-item-by-id'] = 'admin/ajax/toggle_published_by_id';
$route['ajax/lift-up-item-by-id'] = 'admin/ajax/lift_up_item_by_id';
$route['ajax/toggle-item-featured-by-id'] = 'admin/ajax/toggle_item_featured_by_id';
$route['ajax/update-images-by-id'] = 'admin/ajax/update_images_by_id';

$route['ajax/toggle-user-status-by-id'] = 'admin/ajax/toggle_user_status_by_id';
$route['ajax/delete-user-by-id'] = 'admin/ajax/delete_user_by_id';

// AJAX public for filter

$route['ajax/get_picked_filter_array'] = 'ajax_public/get_picked_filter_array';
$route['ajax/get_picked_filter_window'] = 'ajax_public/get_picked_filter_window';
$route['ajax/count_search_results'] = 'ajax_public/count_search_results';
$route['ajax/get_cigars_by_params'] = 'ajax_public/get_cigars_by_params';



// CART
$route['cart'] = 'cart';

// BLOG


$route['blog'] = 'blog';
$route['blog/(:any)'] = 'blog/section/$1';
$route['blog/(:any)/(:any)'] = 'blog/article/$1/$2';



// BRANDS

$route['brands'] = 'brands';
$route['brands/(:any)'] = 'brands/brand/$1';



// catalog

$route['catalog'] = 'catalog';
$route['catalog/(:any)'] = 'catalog/product_category/$1';
$route['catalog/(:any)/(:any)'] = 'catalog/product/$1/$2';



// DEFAULTS

//$route['default_controller'] = 'home_page_controller';
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



// SINGLE PAGES IN 1-ST SEGMENT MADE FROM ARTICLES
// must be last in this routes list
// 1-st segment based on uncategorized section articles pages
$route['(:any)'] = 'blog/uncategorized_article';