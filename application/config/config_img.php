<?php

defined('BASEPATH') OR exit('No direct script access allowed');

///====================
// === IMAGES PATHS ===
///====================

$config['_style_images_path_url'] = 'assets/images/style/';

$config['_assets_image_path'] = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/';

$config['_blog_section_image_path'] = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/images/blog/sections/';
$config['_blog_article_image_path'] = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/images/blog/articles/';
$config['_brands_image_path'] = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/images/brands/';
$config['_vitolas_image_path'] = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/images/vitolas/';
$config['_categories_image_path'] = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/images/categories/';
$config['_products_image_path'] = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/images/products/';
$config['_products_additional_image_path'] = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/images/products_additional_images/';
$config['_packs_image_path'] = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/images/packs/';

$config['_assets_image_path_url'] = 'assets/';

$config['_blog_section_image_path_url'] = 'assets/images/blog/sections/';
$config['_blog_article_image_path_url'] = 'assets/images/blog/articles/';
$config['_brands_image_path_url'] = 'assets/images/brands/';
$config['_vitolas_image_path_url'] = 'assets/images/vitolas/';
$config['_categories_image_path_url'] = 'assets/images/categories/';
$config['_products_image_path_url'] = 'assets/images/products/';
$config['_products_additional_image_path_url'] = 'assets/images/products_additional_images/';
$config['_packs_image_path_url'] = 'assets/images/packs/';

$config['_blog_missing_image_url'] = 'assets/images/image_not_found.gif';
$config['_blog_not_specified_image_url'] = 'assets/images/image_not_specified.gif';
$config['_brand_missing_image_url'] = 'assets/images/brand_image_not_found.gif';
$config['_brand_not_specified_image_url'] = 'assets/images/brand_image_not_specified.gif';
$config['_vitola_missing_image_url'] = 'assets/images/image_not_found.gif';
$config['_vitola_not_specified_image_url'] = 'assets/images/image_not_specified.gif';
$config['_category_missing_image_url'] = 'assets/images/image_not_found.gif';
$config['_category_not_specified_image_url'] = 'assets/images/image_not_specified.gif';
$config['_product_missing_image_url'] = 'assets/images/product_image_not_found.gif';
$config['_product_not_specified_image_url'] = 'assets/images/product_image_not_specified.gif';
$config['_cigar_missing_image_url'] = 'assets/images/cigar_image_not_found.gif';
$config['_cigar_not_specified_image_url'] = 'assets/images/cigar_image_not_specified.gif';
$config['_pack_missing_image_url'] = 'assets/images/image_not_found.gif';
$config['_pack_not_specified_image_url'] = 'assets/images/image_not_specified.gif';
$config['_banner_missing_image_url'] = 'assets/images/image_not_found.gif';
$config['_banner_not_specified_image_url'] = 'assets/images/image_not_specified.gif';







//====================
// === IMAGE SIZES ===
//====================
// 
// 
// blog section destination image sizes
$config['_blog_section_image_width'] = 300;
$config['_blog_section_image_height'] = 300;
// 
// blog article destination image sizes
$config['_blog_article_image_width'] = 300;
$config['_blog_article_image_height'] = 300;
//
// brand destination image sizes
$config['_brand_image_width'] = 197;
$config['_brand_image_height'] = 139;
//
// vitola destination image sizes
//$config['_vitola_image_width'] = 300;
//$config['_vitola_image_height'] = 300;
//
// category destination image sizes
$config['_category_image_width'] = 300;
$config['_category_image_height'] = 300;
//
// product destination image sizes
$config['_product_image_width'] = 300;
$config['_product_image_height'] = 300;
//
// cigar destination image sizes
$config['_cigar_image_width'] = 300;
$config['_cigar_image_height'] = 100;
//
// pack destination image sizes
$config['_pack_image_width'] = 300;
$config['_pack_image_height'] = 300;






//=======================
// === UPLOAD CONFIGS ===
//=======================
//
//
// blog section intro image upload
$config['_blog_section_image_upload_config'] = [
    'upload_path' => './assets/images/blog/sections/',
    'allowed_types' => 'gif|jpg|png',
    'file_ext_tolower' => TRUE,
    'overwrite' => FALSE,
    'max_size' => '1024', // in Kb (1Mb now)
    'max_filename' => '0', // default
    'max_filename_increment' => '999', // when overwrite is TRUE
    'remove_spaces' => TRUE, // default
	//'file_name' => '',
	//'max_width' => '1024',
	//'max_height' => '768',
];
//
// blog article intro image upload
$config['_blog_article_image_upload_config'] = [
    'upload_path' => './assets/images/blog/articles/',
    'allowed_types' => 'gif|jpg|png',
    'file_ext_tolower' => TRUE,
    'overwrite' => FALSE,
    'max_size' => '1024', // in Kb (1Mb now)
    'max_filename' => '0', // default
    'max_filename_increment' => '999', // when overwrite is TRUE
    'remove_spaces' => TRUE, // default
	//'file_name' => '',
	//'max_width' => '1024',
	//'max_height' => '768',
];
//
// brand image upload
$config['_brand_image_upload_config'] = [
    'upload_path' => './assets/images/brands/',
    'allowed_types' => 'gif|jpg|png',
    'file_ext_tolower' => TRUE,
    'overwrite' => FALSE,
    'max_size' => '1024', // in Kb (1Mb now)
    'max_filename' => '0', // default
    'max_filename_increment' => '999', // when overwrite is TRUE
    'remove_spaces' => TRUE, // default
	//'file_name' => '',
	//'max_width' => '1024',
	//'max_height' => '768',
];
//
// category image upload
$config['_category_image_upload_config'] = [
    'upload_path' => './assets/images/categories/',
    'allowed_types' => 'gif|jpg|png',
    'file_ext_tolower' => TRUE,
    'overwrite' => FALSE,
    'max_size' => '1024', // in Kb (1Mb now)
    'max_filename' => '0', // default
    'max_filename_increment' => '999', // when overwrite is TRUE
    'remove_spaces' => TRUE, // default
	//'file_name' => '',
	//'max_width' => '1024',
	//'max_height' => '768',
];
//
// product image upload
$config['_product_image_upload_config'] = [
    'upload_path' => './assets/images/products/',
    'allowed_types' => 'gif|jpg|png',
    'file_ext_tolower' => TRUE,
    'overwrite' => FALSE,
    'max_size' => '1024', // in Kb (1Mb now)
    'max_filename' => '0', // default
    'max_filename_increment' => '999', // when overwrite is TRUE
    'remove_spaces' => TRUE, // default
	//'file_name' => '',
	//'max_width' => '1024',
	//'max_height' => '768',
];
//
// cigar image upload
$config['_cigar_image_upload_config'] = [
    'upload_path' => './assets/images/products/',
    'allowed_types' => 'gif|jpg|png',
    'file_ext_tolower' => TRUE,
    'overwrite' => FALSE,
    'max_size' => '1024', // in Kb (1Mb now)
    'max_filename' => '0', // default
    'max_filename_increment' => '999', // when overwrite is TRUE
    'remove_spaces' => TRUE, // default
	//'file_name' => '',
	//'max_width' => '1024',
	//'max_height' => '768',
];
//
// pack image upload
$config['_pack_image_upload_config'] = [
    'upload_path' => './assets/images/packs/',
    'allowed_types' => 'gif|jpg|png',
    'file_ext_tolower' => TRUE,
    'overwrite' => FALSE,
    'max_size' => '1024', // in Kb (1Mb now)
    'max_filename' => '0', // default
    'max_filename_increment' => '999', // when overwrite is TRUE
    'remove_spaces' => TRUE, // default
	//'file_name' => '',
	//'max_width' => '1024',
	//'max_height' => '768',
];







//=====================
// === CROP CONFIGS ===
//=====================
//
//
// blog section intro image crop
$config['_blog_section_image_crop_config'] = [
    'image_library' => 'gd2',
    'source_image' => '', // set this
    'maintain_ratio' => FALSE,
    'quality' => 100,
    'width' => 0, // set it programmatically
    'height' => 0, // set it programmatically
    'x_axis' => 0, // set it programmatically
    'y_axis' => 0, // set it programmatically
];
//
// blog article intro image crop
$config['_blog_article_image_crop_config'] = [
    'image_library' => 'gd2',
    'source_image' => '', // set this
    'maintain_ratio' => FALSE,
    'quality' => 100,
    'width' => 0, // set it programmatically
    'height' => 0, // set it programmatically
    'x_axis' => 0, // set it programmatically
    'y_axis' => 0, // set it programmatically
];
//
// brand image crop
$config['_brand_image_crop_config'] = [
    'image_library' => 'gd2',
    'source_image' => '', // set this
    'maintain_ratio' => FALSE,
    'quality' => 100,
    'width' => 0, // set it programmatically
    'height' => 0, // set it programmatically
    'x_axis' => 0, // set it programmatically
    'y_axis' => 0, // set it programmatically
];
//
// category intro image crop
$config['_category_image_crop_config'] = [
    'image_library' => 'gd2',
    'source_image' => '', // set this
    'maintain_ratio' => FALSE,
    'quality' => 100,
    'width' => 0, // set it programmatically
    'height' => 0, // set it programmatically
    'x_axis' => 0, // set it programmatically
    'y_axis' => 0, // set it programmatically
];
//
// product image crop
$config['_product_image_crop_config'] = [
    'image_library' => 'gd2',
    'source_image' => '', // set this
    'maintain_ratio' => FALSE,
    'quality' => 100,
    'width' => 0, // set it programmatically
    'height' => 0, // set it programmatically
    'x_axis' => 0, // set it programmatically
    'y_axis' => 0, // set it programmatically
];
//
// cigar image crop
$config['_cigar_image_crop_config'] = [
    'image_library' => 'gd2',
    'source_image' => '', // set this
    'maintain_ratio' => FALSE,
    'quality' => 100,
    'width' => 0, // set it programmatically
    'height' => 0, // set it programmatically
    'x_axis' => 0, // set it programmatically
    'y_axis' => 0, // set it programmatically
];
//
// pack image crop
$config['_pack_image_crop_config'] = [
    'image_library' => 'gd2',
    'source_image' => '', // set this
    'maintain_ratio' => FALSE,
    'quality' => 100,
    'width' => 0, // set it programmatically
    'height' => 0, // set it programmatically
    'x_axis' => 0, // set it programmatically
    'y_axis' => 0, // set it programmatically
];





//=======================
// === RESIZE CONFIGS ===
//=======================
//
//
// blog section intro image resize
$config['_blog_section_image_resize_config'] = [
    'image_library' => 'gd2',
    //'image_library' => 'ImageMagick',
    'source_image' => '', // set this
    //'create_thumb' => TRUE,
    'master_dim' => 'width',
    'maintain_ratio' => TRUE,
    'quality' => 100,
    'width' => 300, // set it to corrent value after development
    'height' => 1
];
//
// blog article intro image resize
$config['_blog_article_image_resize_config'] = [
    'image_library' => 'gd2',
    //'image_library' => 'ImageMagick',
    'source_image' => '', // set this
    //'create_thumb' => TRUE,
    'master_dim' => 'width',
    'maintain_ratio' => TRUE,
    'quality' => 100,
    'width' => 300, // set it to corrent value after development
    'height' => 1
];
//
// brand image resize
$config['_brand_image_resize_config'] = [
    'image_library' => 'gd2',
    //'image_library' => 'ImageMagick',
    'source_image' => '', // set this
    //'create_thumb' => TRUE,
    'master_dim' => 'width',
    'maintain_ratio' => TRUE,
    'quality' => 100,
    'width' => 197,
    'height' => 1
];
//
// category intro image resize
$config['_category_image_resize_config'] = [
    'image_library' => 'gd2',
    //'image_library' => 'ImageMagick',
    'source_image' => '', // set this
    //'create_thumb' => TRUE,
    'master_dim' => 'width',
    'maintain_ratio' => TRUE,
    'quality' => 100,
    'width' => 270, // set it to corrent value after development
    'height' => 1
];
//
// product image resize
$config['_product_image_resize_config'] = [
    'image_library' => 'gd2',
    //'image_library' => 'ImageMagick',
    'source_image' => '', // set this
    //'create_thumb' => TRUE,
    'master_dim' => 'width',
    'maintain_ratio' => TRUE,
    'quality' => 100,
    'width' => 450, // set it to corrent value after development
    'height' => 1
];
//
// cigar image resize
$config['_cigar_image_resize_config'] = [
    'image_library' => 'gd2',
    //'image_library' => 'ImageMagick',
    'source_image' => '', // set this
    //'create_thumb' => TRUE,
    'master_dim' => 'width',
    'maintain_ratio' => TRUE,
    'quality' => 100,
    'width' => 660, // check it on habanos.com
    'height' => 1
];
//
// pack image resize
$config['_pack_image_resize_config'] = [
    'image_library' => 'gd2',
    //'image_library' => 'ImageMagick',
    'source_image' => '', // set this
    //'create_thumb' => TRUE,
    'master_dim' => 'width',
    'maintain_ratio' => TRUE,
    'quality' => 100,
    'width' => 450, // set it to corrent value after development
    'height' => 1
];