<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['csrf_protection'] = TRUE;



$config['_requests_phone_country_code'] = '+7'; // russia

$config['_recently_products_amount_to_remember'] = 4;


// CONTACTS INFO
$config['_contacts']['topnavbar_phone_str'] = '+7 499 978 22 87';
$config['_contacts']['topnavbar_mobile_str'] = '+7 926 000 11 22';
$config['_contacts']['phone'] = '+74999782287';
$config['_contacts']['mobile'] = '+79260001122';

$config['_footer_address_string'] = 'Официальный магазин сигар Habanos<br>
Москва, ул.&nbsp;Краснопролетарская&nbsp;8, стр.&nbsp;1<br>
Тел. +7&nbsp;499&nbsp;978&nbsp;22&nbsp;87<br>
info@habano-aficionado.ru';


$config['_footer_info_text_string'] = 'Минздравсоцразвития России предупреждает: курение и&nbsp;чрезмерное употребление алкоголя вредит вашему здоровью!<br>
Мы не&nbsp;продаем табачные изделия лицам моложе 18&nbsp;лет.<br>
Мы не&nbsp;осуществляем дистанционную торговлю. Торговая деятельность осуществляется в&nbsp;магазине по&nbsp;указанному адресу.<br>
Копирование, цитирование и&nbsp;распространение материалов данного сайта без согласования с&nbsp;администрацией сайта является нарушением авторских прав ст.&nbsp;146&nbsp;УК&nbsp;РФ.<br>
Информация сайта не&nbsp;является публичной офертой и&nbsp;носит информационный характер.';

$config['_social_links']['facebook'] = 'https://www.facebook.com/Habano-Aficionado-1036460219868924';
$config['_social_links']['twitter'] = '#twitter';
$config['_social_links']['vk'] = '#vk';
$config['_social_links']['instagram'] = '#instagram';


// SUBMIT TYPES
$config['_submit_save'] = 'save';
$config['_submit_save_and_close'] = 'save_and_close';


// CART ITEMS
$config['_cart_item_types'] = array(
    'product' => 'product',
    'pack' => 'pack'
);


// BLOG
$config['_reserved_section_slug'] = 'uncategorized'; // for articles that creates pages


$config['_reserved_slugs'] = array(
    'catalog',
    'ajax',
    'admin',
    'auth',
    'profile',
    'register',
    'login',
    'logout',
    'reset-password',
    'password-reset-confirmation',
    'registration-confirmation',
    'index.php',
    'assets',
    'system',
    'filemanager',
    'application'
);


// AUTHENTIFICATION AND REGISTRATION
//
//
$config['_reg_conf_time_limit'] = 7200; // in seconds
$config['_rst_pwd_conf_time_limit'] = 300; // in seconds

$config['_ip_login_blocking_enabled'] = FALSE; // !!! set to TRUE in production enviroment
$config['_ip_login_block_time_limit'] = 600; // in seconds
$config['_ip_login_access_attempts_period'] = 30; // in seconds
$config['_ip_login_access_attempt_max_number'] = 10;

$config['_super_admins'] = array(
    ['fatbody@yandex.ru', '$2y$10$kFXNmL.s/qPY8VseIEW54ulLeyxCpo2jGTxI9BLtRWaJCJ/chYOgi'],
        //['superadmin@site.com', '$2y$10$vQ/jMXg3LH60bsii8mhChO2dPMGJU8w77rw1nho0z7AyqUHiIqAuu'],
);


// EMAILS
//
//
$config['_email_source_site_email'] = 'info@habano-aficionado.ru';
$config['_email_from'] = 'Habano Aficionado';
$config['_email_order_request_recipients'] = 'fatbody@yandex.ru, sneuman@ya.ru';



// length of blog article intro text
$config['_blog_section_intro_length'] = 250; // number of symbols
$config['_blog_article_intro_length'] = 360;
$config['_blog_text_further_item'] = ' …';



// PAGINATION
//
//
$config['_pagination_config'] = [
    //'base_url' => current_url(), // !!! will be assigned and overridden programmatically
    'page_query_string' => TRUE,
    //'num_links'	=>  3,
    'use_page_numbers' => TRUE,
    'query_string_segment' => 'page',
    'first_link' => 'В начало',
    'last_link' => 'Последняя',
    'prev_link' => 'Назад',
    'next_link' => 'Далее',
    //'display_pages' => false,
    //'total_rows' => 1, // !!! will be assigned and overridden programmatically
    'per_page' => 4 // items on page
];

$config['_cigars_items_per_page'] = 12;
$config['_products_items_per_page'] = 12;
$config['_blog_sections_per_page'] = 16;
$config['_blog_articles_per_page'] = 16;

$config['_admin_blog_sections_per_page'] = 20;
$config['_admin_blog_articles_per_page'] = 20;
$config['_admin_users_per_page'] = 20;
$config['_admin_orders_per_page'] = 20;
$config['_admin_brands_per_page'] = 20;
$config['_admin_categories_per_page'] = 20;
$config['_admin_products_per_page'] = 20;
$config['_admin_packs_per_page'] = 20;
$config['_admin_banners_per_page'] = 20;




// CAPCHA
// 
//
$config['_captcha_config'] = [
    //'word' => 'Word',
    'img_path' => filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/captcha/',
    //'img_url' => 'http://' . filter_input(INPUT_SERVER, 'HTTP_HOST') . '/assets/captcha/',
    'font_path' => filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/assets/captcha_fonts/Agent_Orange.ttf',
    'img_width' => 240,
    'img_height' => 100,
    'expiration' => 7200, // in seconds
    'word_length' => 8,
    'font_size' => 24,
    'img_id' => 'captcha_img',
    //'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
    'pool' => '0123456789', // we use only digits
    'colors' => array(
        'background' => array(0, 0, 0),
        'border' => array(59, 59, 48),
        'text' => array(157, 149, 101),
        'grid' => array(70, 64, 45)
    )
];

$config['_product_price_zero_substitution'] = "нет на складе";
