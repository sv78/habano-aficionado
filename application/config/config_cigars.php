<?php

defined('BASEPATH') OR exit('No direct script access allowed');


// FLAVOURS - вкусы

$config['_flt_flavours'] = array(
    array(
        'name' => 'Traditional',
    ),
    array(
        'name' => 'Coffee & Chocolate',
    ),
    array(
        'name' => 'Fruits & Plants',
    ),
    array(
        'name' => 'Herbs & Spices',
    ),
);


// INTENSITY - крепость

$config['_flt_intensities'] = array(
    array(
        'name' => 'Full',
    ),
    array(
        'name' => 'Medium To Full',
    ),
    array(
        'name' => 'Medium',
    ),
    array(
        'name' => 'Light To Medium',
    ),
    array(
        'name' => 'Light',
    )
);


// WRAPPER - покровный лист

$config['_flt_wrappers'] = array(
    array(
        'name' => 'Claro',
        'img' => '',
    ),
    array(
        'name' => 'Colorado Claro',
        'img' => '',
    ),
    array(
        'name' => 'Colorado',
        'img' => '',
    ),
    array(
        'name' => 'Colorado Maduro',
        'img' => '',
    ),
    array(
        'name' => 'Maduro',
        'img' => '',
    ),
);




// CEPO (RING GAUGE)

$config['_flt_cepos'] = array(26, 28, 30, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 52, 54, 55, 56, 57);





// LENGTH (mm)

$config['_flt_lengths'] = array(
    array(
        'mm' => 90,
        'inches' => '3 1/2”',
    ),
    array(
        'mm' => 100,
        'inches' => '4”',
    ),
    array(
        'mm' => 101,
        'inches' => '4”',
    ),
    array(
        'mm' => 102,
        'inches' => '4”',
    ),
    array(
        'mm' => 105,
        'inches' => '4 1/8”',
    ),
    array(
        'mm' => 110,
        'inches' => '4 3/8”',
    ),
    array(
        'mm' => 115,
        'inches' => '4 1/2”',
    ),
    array(
        'mm' => 117,
        'inches' => '4 5/8”',
    ),
    array(
        'mm' => 119,
        'inches' => '4 3/4”',
    ),
    array(
        'mm' => 120,
        'inches' => '4 3/4”',
    ),
    array(
        'mm' => 123,
        'inches' => '4 7/8”',
    ),
    array(
        'mm' => 124,
        'inches' => '4 7/8”',
    ),
    array(
        'mm' => 125,
        'inches' => '4 7/8”',
    ),
    array(
        'mm' => 126,
        'inches' => '4”',
    ),
    array(
        'mm' => 127,
        'inches' => '5”',
    ),
    array(
        'mm' => 129,
        'inches' => '5 1/8”',
    ),
    array(
        'mm' => 130,
        'inches' => '5 1/8”',
    ),
    array(
        'mm' => 132,
        'inches' => '5 1/4”',
    ),
    array(
        'mm' => 133,
        'inches' => '5 1/4”',
    ),
    array(
        'mm' => 135,
        'inches' => '5 3/8”',
    ),
    array(
        'mm' => 136,
        'inches' => '5 3/8”',
    ),
    array(
        'mm' => 140,
        'inches' => '5 1/2”',
    ),
    array(
        'mm' => 141,
        'inches' => '5 1/2”',
    ),
    array(
        'mm' => 142,
        'inches' => '5 5/8”',
    ),
    array(
        'mm' => 143,
        'inches' => '5 5/8”',
    ),
    array(
        'mm' => 144,
        'inches' => '5 5/8”',
    ),
    array(
        'mm' => 145,
        'inches' => '5 3/4”',
    ),
    array(
        'mm' => 150,
        'inches' => '5 7/8”',
    ),
    array(
        'mm' => 152,
        'inches' => '6”',
    ),
    array(
        'mm' => 155,
        'inches' => '6 1/8”',
    ),
    array(
        'mm' => 156,
        'inches' => '6 1/8”',
    ),
    array(
        'mm' => 158,
        'inches' => '6 1/4”',
    ),
    array(
        'mm' => 159,
        'inches' => '6 1/4”',
    ),
    array(
        'mm' => 160,
        'inches' => '6 1/4”',
    ),
    array(
        'mm' => 162,
        'inches' => '6 3/8”',
    ),
    array(
        'mm' => 165,
        'inches' => '6 1/2”',
    ),
    array(
        'mm' => 166,
        'inches' => '6 1/2”',
    ),
    array(
        'mm' => 170,
        'inches' => '6 3/4”',
    ),
    array(
        'mm' => 178,
        'inches' => '7”',
    ),
    array(
        'mm' => 184,
        'inches' => '7 1/4”',
    ),
    array(
        'mm' => 192,
        'inches' => '7 1/2”',
    ),
    array(
        'mm' => 194,
        'inches' => '7 5/8”',
    ),
    array(
        'mm' => 235,
        'inches' => '9 1/4”',
    ),
    array(
        'mm' => 146,
        'inches' => '5 3/4”',
    ),
    array(
        'mm' => 164,
        'inches' => '6 1/2”',
    ),
);



// LINES (became SERIES now but left as lines in DB) - be carefull!!!

$config['_flt_lines'] = array(
    array(
        'name' => 'Regular Catalogue',
    ),
    array(
        'name' => 'Gran Reserva',
    ),
    array(
        'name' => 'Reserva',
    ),
    array(
        'name' => 'Limited Editions',
    ),
    array(
        'name' => 'La Casa Del Habano',
    ),
    array(
        'name' => 'Regional Editions',
    ),
    array(
        'name' => 'Anejados',
    )
);













// VITOLAS

$config['_flt_vitolas'] = array(
    array(
        'name' => 'Salomon',
        'img' => 'salomon.png',
        'rg_mm' => 22.62,
        'length_mm' => 184
    ),
    array(
        'name' => 'Laguito No. 6',
        'img' => 'laguito_no6.png',
        'rg_mm' => 22.23,
        'length_mm' => 166
    ),
    array(
        'name' => '80 Aniversario',
        'img' => 'cigardummy.png',
        'rg_mm' => 21.83,
        'length_mm' => 165
    ),
    array(
        'name' => 'Montesco',
        'img' => 'montesco.png',
        'rg_mm' => 21.83,
        'length_mm' => 130
    ),
    array(
        'name' => 'Sublimes',
        'img' => 'sublimes.png',
        'rg_mm' => 21.43,
        'length_mm' => 164
    ),
    array(
        'name' => 'Piramide Extra',
        'img' => 'piramide_extra.png',
        'rg_mm' => 21.43,
        'length_mm' => 160
    ),
    array(
        'name' => 'Geniales',
        'img' => 'geniales.png',
        'rg_mm' => 21.43,
        'length_mm' => 150
    ),
    array(
        'name' => 'Laguito No. 5',
        'img' => 'laguito_no5.png',
        'rg_mm' => 21.43,
        'length_mm' => 144
    ),
    array(
        'name' => 'Duke',
        'img' => 'duke.png',
        'rg_mm' => 21.43,
        'length_mm' => 140
    ),
    array(
        'name' => 'Torres',
        'img' => 'cigardummy.png',
        'rg_mm' => 21.43,
        'length_mm' => 110
    ),
    array(
        'name' => 'Romeo',
        'img' => 'romeo.png',
        'rg_mm' => 20.64,
        'length_mm' => 162
    ),
    array(
        'name' => 'Piramides',
        'img' => 'piramides.png',
        'rg_mm' => 20.64,
        'length_mm' => 156
    ),
    array(
        'name' => 'Canonazo',
        'img' => 'canonazo.png',
        'rg_mm' => 20.64,
        'length_mm' => 150
    ),
    array(
        'name' => 'Campanas',
        'img' => 'campanas.png',
        'rg_mm' => 20.64,
        'length_mm' => 140
    ),
    array(
        'name' => 'Genios 5',
        'img' => 'genios.png',
        'rg_mm' => 20.64,
        'length_mm' => 140
    ),
    array(
        'name' => 'Edmundo',
        'img' => 'edmundo.png',
        'rg_mm' => 20.64,
        'length_mm' => 135
    ),
    array(
        'name' => 'Petit Belicosos',
        'img' => 'petit_belicosos.png',
        'rg_mm' => 20.64,
        'length_mm' => 125
    ),
    array(
        'name' => 'Petit No. 2',
        'img' => 'cigardummy.png',
        'rg_mm' => 20.64,
        'length_mm' => 120
    ),
    array(
        'name' => 'Laguito No. 4',
        'img' => 'laguito_no4.png',
        'rg_mm' => 20.64,
        'length_mm' => 119
    ),
    array(
        'name' => 'Magicos 5',
        'img' => 'magicos.png',
        'rg_mm' => 20.64,
        'length_mm' => 115
    ),
    array(
        'name' => 'Petit Edmundo',
        'img' => 'petit_edmundo.png',
        'rg_mm' => 20.64,
        'length_mm' => 110
    ),
    array(
        'name' => 'Magnum 50',
        'img' => 'magnum_50.png',
        'rg_mm' => 19.84,
        'length_mm' => 160
    ),
    array(
        'name' => 'Dobles',
        'img' => 'dobles.png',
        'rg_mm' => 19.84,
        'length_mm' => 155
    ),
    array(
        'name' => 'Gordito',
        'img' => 'gordito.png',
        'rg_mm' => 19.84,
        'length_mm' => 141
    ),
    array(
        'name' => 'Petit Piramides',
        'img' => 'petit_piramides.png',
        'rg_mm' => 19.84,
        'length_mm' => 127
    ),
    array(
        'name' => 'Robustos',
        'img' => 'robustos.png',
        'rg_mm' => 19.84,
        'length_mm' => 124
    ),
    array(
        'name' => 'D No. 5',
        'img' => 'd_no5.png',
        'rg_mm' => 19.84,
        'length_mm' => 110
    ),
    array(
        'name' => 'Petit Robustos',
        'img' => 'petit_robustos.png',
        'rg_mm' => 19.84,
        'length_mm' => 102
    ),
    array(
        'name' => 'D No. 6',
        'img' => 'cigardummy.png',
        'rg_mm' => 19.84,
        'length_mm' => 90
    ),
    array(
        'name' => 'Prominentes',
        'img' => 'prominentes.png',
        'rg_mm' => 19.45,
        'length_mm' => 194
    ),
    array(
        'name' => 'Hermosos No. 3',
        'img' => 'cigardummy.png',
        'rg_mm' => 19.05,
        'length_mm' => 140
    ),
    array(
        'name' => 'Hermosos No. 4',
        'img' => 'hermosos_no4.png',
        'rg_mm' => 19.05,
        'length_mm' => 127
    ),
    array(
        'name' => 'Gran Corona',
        'img' => 'gran_corona.png',
        'rg_mm' => 18.65,
        'length_mm' => 235
    ),
    array(
        'name' => 'Julieta No. 2',
        'img' => 'julieta_no2.png',
        'rg_mm' => 18.65,
        'length_mm' => 178
    ),
    array(
        'name' => 'Tacos',
        'img' => 'tacos.png',
        'rg_mm' => 18.65,
        'length_mm' => 158
    ),
    array(
        'name' => 'Club Allones',
        'img' => 'cigardummy.png',
        'rg_mm' => 18.65,
        'length_mm' => 135
    ),
    array(
        'name' => 'Exquisitos',
        'img' => 'exquisitos.png',
        'rg_mm' => 18.26,
        'length_mm' => 145
    ),
    array(
        'name' => 'Coronas Gordas',
        'img' => 'coronas_gordas.png',
        'rg_mm' => 18.26,
        'length_mm' => 143
    ),
    array(
        'name' => 'Forum',
        'img' => 'forum.png',
        'rg_mm' => 18.26,
        'length_mm' => 135
    ),
    array(
        'name' => 'Mareva Gruesa',
        'img' => 'mareva_gruesa.png',
        'rg_mm' => 18.26,
        'length_mm' => 120
    ),
    array(
        'name' => 'Mananitas',
        'img' => 'mananitas.png',
        'rg_mm' => 18.26,
        'length_mm' => 100
    ),
    array(
        'name' => 'Conservas JLP',
        'img' => 'conservas.png',
        'rg_mm' => 17.46,
        'length_mm' => 140
    ),
    array(
        'name' => 'Coloniales',
        'img' => 'coloniales.png',
        'rg_mm' => 17.46,
        'length_mm' => 132
    ),
    array(
        'name' => 'Half Corona',
        'img' => 'half_corona.png',
        'rg_mm' => 17.46,
        'length_mm' => 90
    ),
    array(
        'name' => 'Dalias',
        'img' => 'dalias.png',
        'rg_mm' => 17.07,
        'length_mm' => 170
    ),
    array(
        'name' => 'Cazadores',
        'img' => 'cazadores.png',
        'rg_mm' => 17.07,
        'length_mm' => 162
    ),
    array(
        'name' => 'Cazadores JLP',
        'img' => 'cazadores_jlp.png',
        'rg_mm' => 17.07,
        'length_mm' => 152
    ),
    array(
        'name' => 'Conservas',
        'img' => 'conservas.png',
        'rg_mm' => 17.07,
        'length_mm' => 145
    ),
    array(
        'name' => 'Petit Cazadores',
        'img' => 'petit_cazadores.png',
        'rg_mm' => 17.07,
        'length_mm' => 105
    ),
    array(
        'name' => 'Petit Bouquet',
        'img' => 'petit_bouquet.png',
        'rg_mm' => 17.07,
        'length_mm' => 101
    ),
    array(
        'name' => 'Cervantes',
        'img' => 'cervantes.png',
        'rg_mm' => 16.67,
        'length_mm' => 165
    ),
    array(
        'name' => 'Coronas Grandes',
        'img' => 'coronas_grandes.png',
        'rg_mm' => 16.67,
        'length_mm' => 155
    ),
    array(
        'name' => 'Coronas',
        'img' => 'coronas.png',
        'rg_mm' => 16.67,
        'length_mm' => 142
    ),
    array(
        'name' => 'Cosacos',
        'img' => 'cosacos.png',
        'rg_mm' => 16.67,
        'length_mm' => 135
    ),
    array(
        'name' => 'Brevas JLP',
        'img' => 'brevas_jlp.png',
        'rg_mm' => 16.67,
        'length_mm' => 133
    ),
    array(
        'name' => 'Eminentes',
        'img' => 'eminentes.png',
        'rg_mm' => 16.67,
        'length_mm' => 132
    ),
    array(
        'name' => 'Marevas',
        'img' => 'marevas.png',
        'rg_mm' => 16.67,
        'length_mm' => 129
    ),
    array(
        'name' => 'Petit Coronas',
        'img' => 'petit_coronas.png',
        'rg_mm' => 16.67,
        'length_mm' => 129
    ),
    array(
        'name' => 'Favoritos',
        'img' => 'favoritos.png',
        'rg_mm' => 16.67,
        'length_mm' => 120
    ),
    array(
        'name' => 'Minutos',
        'img' => 'minutos.png',
        'rg_mm' => 16.67,
        'length_mm' => 110
    ),
    array(
        'name' => 'Cristales',
        'img' => 'cristales.png',
        'rg_mm' => 16.27,
        'length_mm' => 150
    ),
    array(
        'name' => 'Laguito Especial',
        'img' => 'laguito_especial.png',
        'rg_mm' => 15.88,
        'length_mm' => 192
    ),
    array(
        'name' => 'Cremas',
        'img' => 'cremas.png',
        'rg_mm' => 15.88,
        'length_mm' => 140
    ),
    array(
        'name' => 'Nacionales',
        'img' => 'nacionales.png',
        'rg_mm' => 15.88,
        'length_mm' => 140
    ),
    array(
        'name' => 'Cremas JLP',
        'img' => 'cremas_jlp.png',
        'rg_mm' => 15.88,
        'length_mm' => 136
    ),
    array(
        'name' => 'Almuerzos',
        'img' => 'almuerzos.png',
        'rg_mm' => 15.88,
        'length_mm' => 130
    ),
    array(
        'name' => 'Petit Cetros',
        'img' => 'petit_cetros.png',
        'rg_mm' => 15.88,
        'length_mm' => 129
    ),
    array(
        'name' => 'Standard',
        'img' => 'standard.png',
        'rg_mm' => 15.88,
        'length_mm' => 123
    ),
    array(
        'name' => 'Standard Mano',
        'img' => 'standard_mano.png',
        'rg_mm' => 15.88,
        'length_mm' => 123
    ),
    array(
        'name' => 'Coronitas',
        'img' => 'coronitas.png',
        'rg_mm' => 15.88,
        'length_mm' => 117
    ),
    array(
        'name' => 'Reyes',
        'img' => 'reyes.png',
        'rg_mm' => 15.88,
        'length_mm' => 110
    ),
    array(
        'name' => 'Secretos 5',
        'img' => 'secretos.png',
        'rg_mm' => 15.88,
        'length_mm' => 110
    ),
    array(
        'name' => 'Perlas',
        'img' => 'perlas.png',
        'rg_mm' => 15.88,
        'length_mm' => 102
    ),
    array(
        'name' => 'Culebras',
        'img' => 'culebras.png',
        'rg_mm' => 15.48,
        'length_mm' => 146
    ),
    array(
        'name' => 'Belvederes',
        'img' => 'belvederes.png',
        'rg_mm' => 15.48,
        'length_mm' => 125
    ),
    array(
        'name' => 'Laguito No. 1',
        'img' => 'laguito_no1.png',
        'rg_mm' => 15.08,
        'length_mm' => 192
    ),
    array(
        'name' => 'Laguito No. 2',
        'img' => 'laguito_no2.png',
        'rg_mm' => 15.08,
        'length_mm' => 152
    ),
    array(
        'name' => 'Petit Cetros JLP',
        'img' => 'petit_cetros_jlp.png',
        'rg_mm' => 15.08,
        'length_mm' => 127
    ),
    array(
        'name' => 'Trabuco',
        'img' => 'trabucos.png',
        'rg_mm' => 15.08,
        'length_mm' => 110
    ),
    array(
        'name' => 'Vegueritos',
        'img' => 'vegueritos.png',
        'rg_mm' => 14.68,
        'length_mm' => 127
    ),
    array(
        'name' => 'Vegueritos Mano',
        'img' => 'vegueritos_mano.png',
        'rg_mm' => 14.68,
        'length_mm' => 127
    ),
    array(
        'name' => 'Cadetes',
        'img' => 'kdt_cadetes.png',
        'rg_mm' => 14.29,
        'length_mm' => 115
    ),
    array(
        'name' => 'Sports',
        'img' => 'sports.png',
        'rg_mm' => 13.89,
        'length_mm' => 117
    ),
    array(
        'name' => 'Epicures',
        'img' => 'epicures.png',
        'rg_mm' => 13.89,
        'length_mm' => 110
    ),
    array(
        'name' => 'Placeras',
        'img' => 'placeras.png',
        'rg_mm' => 13.49,
        'length_mm' => 125
    ),
    array(
        'name' => 'Deliciosos',
        'img' => 'deliciosos.png',
        'rg_mm' => 13.10,
        'length_mm' => 159
    ),
    array(
        'name' => 'Seoane',
        'img' => 'seoane.png',
        'rg_mm' => 13.10,
        'length_mm' => 126
    ),
    array(
        'name' => 'Julieta No. 6',
        'img' => 'julieta_no6.png',
        'rg_mm' => 13.10,
        'length_mm' => 120
    ),
    array(
        'name' => 'Palmitas',
        'img' => 'palmitas.png',
        'rg_mm' => 12.70,
        'length_mm' => 152
    ),
    array(
        'name' => 'Entreactos',
        'img' => 'entreactos.png',
        'rg_mm' => 11.91,
        'length_mm' => 100
    ),
    array(
        'name' => 'Laguito No. 3',
        'img' => 'laguito_no3.png',
        'rg_mm' => 10.32,
        'length_mm' => 115
    ),
);
