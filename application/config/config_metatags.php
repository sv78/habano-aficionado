<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// META TAGS VALUES FOR STATIC AND SERVICE PAGES
// 
// DEFAULTS
// static at all public pages
$config['_metatags']['default']['title'] = 'Habano-Aficionado.ru';
$config['_metatags']['default']['description'] = 'Habano-Aficionado.ru';
$config['_metatags']['default']['keywords'] = 'Habano-Aficionado.ru';
$config['_metatags']['default']['author'] = 'Habano-Aficionado.ru';
$config['_metatags']['default']['robots'] = 'noindex, nofollow';
$config['_metatags']['default']['owner'] = 'Habano-Aficionado.ru';
$config['_metatags']['default']['copyright'] = 'Habano-Aficionado.ru';
$config['_metatags']['default']['subject'] = 'Habano-Aficionado.ru';
//
//
//
//
// MAIN PAGE
$config['_metatags']['index']['title'] = 'Habano-Aficionado - сайт для ценителей настоящих кубинских сигар';
$config['_metatags']['index']['description'] = 'Актуальная информация о настоящих кубинских сигарах, кубинском табаководстве, событиях в мире Habanos и культуре потребления. Наша цель - обеспечить комфортный выбор аутентичных сигар от Habanos s.a. с получением подтверждения их наличия у продавца, имеющего статус Habanos Specialist.';
$config['_metatags']['index']['keywords'] = 'Сигары, хьюмидоры, перельницы, гильотины, аксессуары, подарки, принадлежности для курения в Москве заказать и купить в клубном магазине «Собрание Раритетов», Habanos Specialist, Краснопролетарская 8, стр.1';
$config['_metatags']['index']['robots'] = 'index, follow';
 
// /blog
$config['_metatags']['blog']['title'] = 'О сигарах, дегустациях, табаководстве, актуальных событиях';
$config['_metatags']['blog']['description'] = 'Статьи и материалы посвященные сигарам, дегустациям, табаководству и событиям, связанным с миром Habanos';
$config['_metatags']['blog']['keywords'] = 'Habanos, дегустации, события, кубинские сигары, аутентичные, оригинальные, подтверждение подлинности';
$config['_metatags']['blog']['robots'] = 'index, follow';

// /cart
$config['_metatags']['cart']['title'] = 'Резерв заказа';
$config['_metatags']['cart']['description'] = 'Резерв заказа';
$config['_metatags']['cart']['keywords'] = 'Резерв заказа';
$config['_metatags']['cart']['robots'] = 'noindex, nofollow';
 

// /auth
$config['_metatags']['login']['title'] = 'Вход';
$config['_metatags']['login']['description'] = 'Вход';
$config['_metatags']['login']['keywords'] = 'Вход';
$config['_metatags']['login']['robots'] = 'index, nofollow';

$config['_metatags']['register']['title'] = 'Регистрация';
$config['_metatags']['register']['description'] = 'Регистрация';
$config['_metatags']['register']['keywords'] = 'Регистрация';
$config['_metatags']['register']['robots'] = 'index, nofollow';

$config['_metatags']['regconf']['title'] = 'Подтверждение регистрации';
$config['_metatags']['regconf']['description'] = 'Подтверждение регистрации';
$config['_metatags']['regconf']['keywords'] = 'Подтверждение регистрации';
$config['_metatags']['regconf']['robots'] = 'noindex, nofollow';

$config['_metatags']['rstpwd']['title'] = 'Восстановление доступа';
$config['_metatags']['rstpwd']['description'] = 'Восстановление доступа';
$config['_metatags']['rstpwd']['keywords'] = 'Восстановление доступа';
$config['_metatags']['rstpwd']['robots'] = 'noindex, nofollow';

$config['_metatags']['rstpwdconf']['title'] = 'Подтверждение восстановления доступа';
$config['_metatags']['rstpwdconf']['description'] = 'Подтверждение восстановления доступа';
$config['_metatags']['rstpwdconf']['keywords'] = 'Подтверждение восстановления доступа';
$config['_metatags']['rstpwdconf']['robots'] = 'noindex, nofollow';

$config['_metatags']['userprofile']['title'] = 'Ваш профиль';
$config['_metatags']['userprofile']['description'] = 'Ваш профиль';
$config['_metatags']['userprofile']['keywords'] = 'Ваш профиль';
$config['_metatags']['userprofile']['robots'] = 'noindex, nofollow';
 

// BRANDS PAGE
$config['_metatags']['brands']['title'] = 'Купить кубинские сигары Cohiba, Кохиба, Коиба, Montecristo, Монтекристо, Partagas, Партагас, Romeo y Julieta, Ромео и Джульета, Hoyo de Monterrey, Хойо де Монтеррей, Upmann, Упманн в Москве заказать и купить в клубном магазине «Собрание Раритетов»';
$config['_metatags']['brands']['description'] = 'Оригинальные сигары Cohiba, Кохиба, Коиба, Montecristo, Монтекристо, Partagas, Партагас, Romeo y Julieta, Ромео и Джульета, Hoyo de Monterrey, Хойо де Монтеррей, Upmann, Упманн продаются в сертифицированных магазинах Habanos Specialist (Специалист Habanos), La Casa del Habano, Habanos Point, заказать и купить в клубном магазине «Собрание Раритетов»';
$config['_metatags']['brands']['keywords'] = 'сигары, Cohiba, Кохиба, Коиба, Montecristo, Монтекристо, Partagas, Партагас, Romeo y Julieta, Ромео и Джульета, Hoyo de Monterrey, Хойо де Монтеррей, Upmann, Упманн, купить, кубинские, аутентичные, оригинальные,  в Москве, заказать и купить в клубном магазине «Собрание Раритетов»';
$config['_metatags']['brands']['robots'] = 'index, follow';


// CATALOG PAGE DEFAULT
$config['_metatags']['catalog']['title'] = 'Купить кубинские сигары, хьюмидоры, перельницы, гильотины, аксессуары, подарки, принадлежности для курения в Москве';
$config['_metatags']['catalog']['description'] = 'Habanos S.A. рекомендует воспользоваться услугами специализированных магазинов, имеющих статус Habanos Specialist, Специалист Habanos. Сигары, хьюмидоры, перельницы, гильотины, аксессуары, подарки, принадлежности для курения в Москве заказать и купить в клубном магазине «Собрание Раритетов», Habanos Specialist';
$config['_metatags']['catalog']['keywords'] = 'купить кубинские аутентичные, оригинальные сигары, хьюмидоры, перельницы, гильотины, аксессуары, подарки, принадлежности для курения в Москве';
$config['_metatags']['catalog']['robots'] = 'index, follow';